<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |{{trans('dashboard.my_account') }}
    */
	
	"answer_quiz" => "Responde el quiz",
	"quiz_results" => "Resultados del quiz",
	"fill_out_brief" => "Llena el brief",
	"approves_the_concept" => "Aprueba el concepto",
	"download_files" => "Descarga los archivos",
	"fill_out_agreement" => "Completa el acuerdo",
	"sign_the_agreement" => "Firma el acuerdo",
	"select_domain" => "Selecciona el dominio",
	"visual_styles_title" => "Estilos visuales para redes sociales",
	"visual_styles_description" => "Los estilos visuales darán a tu marca una presencia sólida y uniforme en las redes sociales",
	"see_result_and_download_files" => "Ver resultados y descargar archivos",
	"approve_concept_one" => "Para aprobar el concepto #1 haz click aquí: ",
	"approve_concept_two" => "Para aprobar el concepto #2 haz click aquí: ",
	"approve_concept_three" => "Para aprobar el concepto #3 haz click aquí: ",
	"send_brief" => "Enviar Brief",
	"attach_concept" => "Adjuntar concepto",
	"approved" => "Aprobado",
	"user" => "Usuario",
	"select_developer" => "Selecciona al desarrollador",
	"approve_job" => "Aprobar trabajo",
	"select_designer" => "Selecciona al diseñador",
	"see_resume" => "Ver hoja de vida",
	"click_here" => "Click aquí",
	"chat_with_us" => "Chatea con nosotros",
	"how_does_it_work" => "¿Cómo funciona?",
	"complete_pc_test" => "Por favor completa el powering content quiz",
	"web_info" => "Información del sitio Web",
	"send_content" => "Envia el contenido",
	"approve_copy" => "Aprobar Copy",
	"enter_the_url_of_the_website" => "Ingrese la dirección del sitio Web",
	"go_to_website" => "Ver el sitio web desde el navegador",
	"download_visual_styles" => "Descarga los estilos visuales",
	"send_the_brand_files" => "Envia los archivos de marca",
	"send_files" => "Enviar archivos",
	"delivery_dates" => "Calendario de Entregas",
	"here" => "Aquí",
	"hi" => "Hola",
	"footer_part_one" => "Si tienes algun comentario o duda, envianos un correo a ",
	"footer_part_two" => ". Uno de nuestros asesores respondera tan rapido como sea posible, también recuerda que siempre puedes ver todos los detalles y fechas de entrega para este producto logeandote en ",
	"footer_part_three" => "con tu usuario y contraseña.",
	"best_regards" => "Atentamente, ",
	"login_into" => "Ingresa a tu cuenta de WebsiteBar",
	"email_mistake" => "Si estas recibiendo este correo por error, dejanolos saber enviando un correo a ",
	"click_here" => "HAZ CLICK AQUí"
];

	