<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |{{trans('dashboard.my_account') }}
    */
	
	"sites" => "Sitios",
	"addons" => "Adicionales",
	"servers" => "Servidores",
	"tickets" => "Tickets",
	"open_tickets" => "Tickets Abiertos",
	"themes" => "Temas",
	"options" => "Opciones",
	"select_customer" => "Selecciona el cliente"
	
];

	