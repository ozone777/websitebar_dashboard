<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |{{trans('dashboard.my_account') }}
    */
	
	"sites" => "Sites",
	"addons" => "Addons",
	"servers" => "Servers",
	"tickets" => "Tickets",
	"open_tickets" => "Open tickets",
	"themes" => "Themes",
	"options" => "Options",
	"select_customer" => "Select customer"
	
];

	