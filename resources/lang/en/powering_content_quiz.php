<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |{{trans('dashboard.my_account') }}
    */
	
	"powering_content_quiz" => "Visual Style Quiz",
	"powering_content_quiz_description" => "With the visual style quiz you can know the personality of your brand. As see in the Powering Content book by Laura Busche",
	"powering_content_quiz_help" => "Answer the Quiz from 1 to 5 how close your brand is to the concepts of the columns. For example 1 if your brand is very bohemian or 5 if your brand is very corporate",
	"bohemian" => "Bohemian",
	"outdoorsy" => "Outdoorsy",	
	"informal" 	=> "Informal",
	"laid_black" => "Laid Black",	
	"subtle" => "Subtle",	
	"delicate" => "Delicate",	
	"understated" => "Understated",
	"monochrome" => "Monochrome",
	"flowy" => "Flowy",	
	"rugged" => "Rugged",		
	"handmade" => "Handmade",	
	"rustic" => "Rustic",
	"corporate" => "Corporate",
	"glamorous" => "Glamorous",
	"elegant" => "Elegant",
	"driven" => "Driven",
	"exuberant" => "Exuberant",
	"bold" => "Bold",
	"dramatic" => "Dramatic",
	"multicolor" => "Multicolor",
	"sharp" => "Sharp",	
	"clean" => "Clean",
	"digital" => "Digital",
	"pixel_perfect" => "Pixel perfect",
    "send_answers" => "Enviar Quiz",
    "send_answers" => "Enviar Quiz",
	"visual_character" => "Visual Character",
	"visual_contrast" => "Visual Contrast",
	"visual_geometry" => "Visual Geometry",
	"visual_character_explanation_1" => "Your brand's visual character leans towards casual. Aim for informal, unstructured design elements that project freshness and spontaneity",
	"visual_character_explanation_2" => "Your brand's visual character leans towards formal. Aim for graphics that signal efficiency, status, or productivity",
	"visual_contrast_explanation_1" => "Your brand's visual contrast is low. Images are easy on the eyes and there is not much tension with color or sizes. Use ample white spaces.",
	"visual_contrast_explanation_2" => "Your brand's visual is high. Images are striking and eye-catching. Visual tension is created with satured colors and bold typography",
	"visual_geometry_explanation_1" => "Your brand's visual geometry favors organic shapes. Your aesthetic is best represented using textures, aged looks, and uneven shapes",
	"visual_geometry_explanation_2" => "Your brand's visual geometry favors sharp shapes. Your aesthetic is best represented using filled in, well-defined figures and precise strokes",
	"visual_contrast_explanation_2" => "Your brand's visual geometry favors sharp shapes. Your aesthetic is best represented using filled in, well-defined figures and precise strokes",
	"test_result" => "Resultado de test",
	"test_result" => "Test result",
	"test_result_explanation_1" => "Add your numbers for lines A through D. This is your visual character score",
	"test_result_explanation_2" => "Add your numbers for lines E through H. This is your visual contrast score",
	"test_result_explanation_3" => "Add your numbers for lines I through L. This is your visual geometry score",
	"casual" => "Casual",
	"formal" => "Formal",
	"low" => "Low",
	"high" => "High",
	"organic" => "Organic",
	"sharp" => "Sharp",
	"earthy_naive" => "Earthy Naive",
	"urban_hipster" => "Urban Hipster",
	"genuinely_passionate" => "Genuinely Passionate",
	"practical_joy" => "Practical Joy",
	"bespoke_chic" => "Bespoke Chic",
	"clean_performance" => "Clean Performance",
	"classic_credibility" => "Classic Credibility",
	"consistent_satisfaction" => "Consistent Satisfaction",
	"visual_character" => "Visual Character",
	"visual_contrast" => "Visual Contrast",
	"visual_geometry" => "Visual Geometry",
	"visual_style_name" => "Visual style name",
	"find_your_visual_style" => "Find your visual style",
	"find_your_visual_style_description" => "Depending on the combination of results you just got, you will find your brand falling under one of the following eight visual styles:",
	"visual_style_name" => "Visual style name",
	"design_tips" => "Design tips",
	"real_life_example" => "Real-life example",
	"earthy_name_tip" => "graphics with muted, warm colors. Search for hand drawn, handwritten, nature, or hand lettering fonts.",
	"urban_hipster_tip" => "Thin, modern fonts that reflect youth with neutral color palettes. Search for Art Deco, hipster, geometric display, geometric grotesque, monoline, or modern fonts.",
	"genuinely_passionate_tip" => "Vibrant, eye-catching hand drawn fonts combined with high-contrast backgrounds. Search for brush, graffiti, or script fonts.",
	"practical_joy_tip" => "Overlay with lively, saturated color palettes. Search for Humanist sans-serif fonts.",
	"bespoke_chic_tip" => "Pair with pastel or neutral tones and plenty of white space. Search for vintage serifs, handmade serifs, or thin script fonts.",
	"clean_performance_tip" => "style, neutral, corporate fonts. Go for shades of grey and plenty of white space.",
	"classic_credibility_tip" => "Blacks or primary colors. Embellished or energetic backgrounds. Search for signature, street art, and bold script fonts.",
	"consistent_satisfaction_tip" => "Thick, condensed sans-serifs with sharp edges. Search for thick, narrow, collegiate, or poster fonts. Combine with rich, highly saturated colors.",
	"send_answers" => "Send answers",
	"quiz_result" => "Quiz result"
	 
];

	