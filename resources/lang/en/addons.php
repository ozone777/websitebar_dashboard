<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |{{trans('dashboard.my_account') }}
    */
	
	"answer_quiz" => "Answer quiz",
	"quiz_results" => "Quiz result",
	"fill_out_brief" => "Fill out brief",
	"approves_the_concept" => "Approves the concept",
	"download_files" => "Download files",
	"fill_out_agreement" => "Complete the agreement",
	"sign_the_agreement" => "Sign the agreement",
	"select_domain" => "Select domain",
	"visual_styles_title" => "Visual styles for social media",
	"visual_styles_description" => "Visual styles will give your brand a solid and uniform presence in social networks",
	"see_result_and_download_files" => "See result and download files",
	"approve_concept_one" => "To approve the concept #1 click here: ",
	"approve_concept_two" => "To approve the concept #2 click here: ",
	"approve_concept_three" => "To approve the concept #3 click here: ",
	"send_brief" => "Send brief",
	"attach_concept" => "Adjuntar concepto",
	"approved" => "Approved",
	"user" => "User",
	"select_developer" => "Select developer",
	"approve_job" => "Approve job",
	"select_designer" => "Select Designer",
	"see_resume" => "See resume",
	"click_here" => "Click here",
	"chat_with_us" => "Chat with us",
	"how_does_it_work" => "How does it work?",
	"complete_pc_test" => "Please complete the powering content quiz",
	"web_info" => "Website information",
	"send_content" => "Send content",
	"approve_copy" => "Approve Copy",
	"enter_the_url_of_the_website" => "Enter the url of the website",
	"go_to_website" => "See the website in the browser",
	"download_visual_styles" => "Download visual styles",
	"send_the_brand_files" => "Send the brand files",
	"send_files" => "Send files",
	"delivery_dates" => "Delivery Dates",
	"here" => "Here",
	"hi" => "Hi",
	"footer_part_one" => "If you have any comments or questions, send us an email at ",
	"footer_part_two" => ". One of our consultants will reply as soon as possible. Also remember that you can review key details and dates related to this mission by logging in at ",
	"footer_part_three" => "with your username and password.",
	"best_regards" => "Best regards, ",
	"login_into" => "Log into your WebsiteBar account",
	"email_mistake" => "If you are receiving this email by mistake, let us know at ",
	"click_here" => "CLICK HERE"
	
	
];

	