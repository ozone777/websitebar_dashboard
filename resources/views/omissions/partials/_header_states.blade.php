<?php
	
function state_translator($obj,$lang){
	if($lang == "en"){
		return $obj->en;
	}else if($lang == "es"){
		return $obj->es;
	}
}
	
?>

<div class="row">
	<div class="col-md-12">

		@if(isset($selected_state))

		@if(($role == "administrator") || ($role == "contributor"))

		<form id="states_form">

			<div id="states_panel" class="osocial_panel">


		
				<p>Administrar estados</p>
		
				@foreach($states as $state)
	
				<input type="radio" name="state" label="{{$state->label}}" value="{{$state->id}}" class="ostates" @if($state->label == $selected_state->ostate()->label) checked @endif>&nbsp;{{state_translator($state,$global_lang)}}. Desde ->  {{$state->updated_at}}
				<br />
				@endforeach
	

				<br />

			</div>

			<section>
				<nav>
					<ul class="steps">
			
			
						@foreach($states as $state)
			
						<li id="state_{{$state->id}}" state_id="{{$state->id}}" @if($state->label == $selected_state->ostate()->label) class="current" @endif><a href="#" title=""><em>{{state_translator($state,$global_lang)}}</em><span></span></a></li>
			
			
						@endforeach
					</ul>
				</nav>
			</section>

		</form>

		@else

		<section>
			<nav>
				<ul class="steps">
			
			
					@foreach($states as $state)
			
					<li id="state_{{$state->id}}" state_id="{{$state->id}}" @if($state->label == $selected_state->ostate()->label) class="current" @endif><a href="#" title=""><em>{{state_translator($state,$global_lang)}}</em><span></span></a></li>
			
			
					@endforeach
				</ul>
			</nav>
		</section>
		@endif

		@endif

	</div>
</div>


<script type="text/javascript">
	
//States logic
//$("#states_form input[type='radio']:checked").val();
@if(($role == "administrator") || ($role == "contributor"))

  	 $(".ostates").click(function() {
	
		template_aux = 0;
  	 	time_interval = $("#time_interval_facebook").val();
  	 	ostate = $(this).val();
		label = $(this).attr('label');
		
		$("#loading_area").html('<div class="cssload-loader"></div>');
	
  	     jQuery.ajax({
  	       url: "/mission_submitted",
  	       type: 'post',
  	       dataType: 'json',
  	        data: {label : $(this).attr('label'), ostate_mission_id: "{{$selected_state->id}}", time_interval: time_interval, current_url: "{{Request::fullUrl()}}", _token: "{{csrf_token()}}"},
  	  		success: function(data){
  			  $(".cssload-loader" ).remove();
			  location.reload();
  	 	}

  	     });


  	 });
	 
	 
  	 $("#reload_ostates").click(function() {
	
	template_aux = 0;
  	 	time_interval = $("#time_interval_facebook").val();
  	 	ostate = $(this).val();
	stepy = $(".stepy-active").attr('id');
	stepy = stepy.replace("titleClick-demo-head-", "");
	
	//Debug
  	 	console.log("time interval: "+time_interval);
  	 	console.log("ostate_id: "+$(this).val());
  	 	console.log("selected_state_id: {{$selected_state->id}}");
	console.log("template: "+stepy);
	state_id = $("#states_form input[type='radio']:checked").val();
	
  	     jQuery.ajax({
  	       url: "/mission_submitted",
  	       type: 'post',
  	       dataType: 'json',
  	        data: {state_id : state_id, stepy: stepy, ostate_mission_id: "{{$selected_state->id}}", time_interval: time_interval, current_url: "{{Request::fullUrl()}}", _token: "{{csrf_token()}}"},
  	  		success: function(data){
		location.reload();
  	 	}

  	     });


  	 });

 @endif
	
</script>
