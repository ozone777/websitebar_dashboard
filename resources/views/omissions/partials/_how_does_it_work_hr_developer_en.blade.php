
<div class="row">
	<div class="col-md-12">
		
		<h3>HOW DOES THE PROCESS WORK?</h3>
		<p>The entire process may take between 1 and 2 weeks, depending on how long it takes you to complete the rounds and review approvals:</p>
		
		<ol>
			<li>Choose a developer.</li>
			<li>Once you have chosen one of the developers, he/she will contact your company's project manager via email or phone to schedule a call.</li> 
			<li>In this meeting you must define the deliverables corresponding to the package of hours you have purchased. These must be added to the project’s formal “List of Deliverables”. Then you must digitally sign the agreement.</li>
			<li>All deliverables will be ready within 7 days.</li>
			<li>Once you verify satisfaction with the deliverables, this commissioned work will be formally finalized.</li>
			
		</ol> 
		
	</div>
</div>



