<div class="row">
	<div class="col-md-8">
  	  <h2>{{trans('powering_content_quiz.powering_content_quiz')}}</h2>
  	  <p>{{trans('powering_content_quiz.powering_content_quiz_description')}}</p>     
	  
	</div>
	
	<div class="col-md-4">
  	 	 <img src="/powering_content_book.jpg" alt="Powering content" width="290px" height="100%"> 
	</div>
	
</div>  

<div class="row">
	<div class="col-md-12">     
		
		<i>{{trans('powering_content_quiz.powering_content_quiz_help')}}</i>   

	</div>
</div>  

<div class="row">
	<div class="col-md-12">
		
		 <form action="/answer_test" method="post"> 
		
			 <input type="hidden" name="omission_id" value="{{$wp_omission->id}}">
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
			 
			 <input type="hidden" name="next_state" value="{{$next_state}}">
			 <input type="hidden" name="selected_state_id" value="{{$selected_state->id}}">
			 
 
  <table class="table">
    <thead>
      <tr>
        <th></th>
        <th></th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>4</th>
        <th>5</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
		
      <tr id="pc_quiz_row_0">
        <th>A</th>
        <th>{{trans('powering_content_quiz.bohemian')}}</th>
		<th><input type="radio" name="pc_bohemian_value" value="1" @if($wp_omission->pc_bohemian_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_bohemian_value" value="2" @if($wp_omission->pc_bohemian_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_bohemian_value" value="3" @if($wp_omission->pc_bohemian_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_bohemian_value" value="4" @if($wp_omission->pc_bohemian_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_bohemian_value" value="5" @if($wp_omission->pc_bohemian_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.corporate')}}</th>
      </tr>
	  
      <tr id="pc_quiz_row_1">
        <th>B</th>
        <th>{{trans('powering_content_quiz.outdoorsy')}}</th>
		<th><input type="radio" name="pc_outdoorsy_value"value="1" @if($wp_omission->pc_outdoorsy_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_outdoorsy_value"value="2" @if($wp_omission->pc_outdoorsy_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_outdoorsy_value"value="3" @if($wp_omission->pc_outdoorsy_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_outdoorsy_value"value="4" @if($wp_omission->pc_outdoorsy_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_outdoorsy_value"value="5" @if($wp_omission->pc_outdoorsy_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.glamorous')}}</th>
      </tr>
	  
      <tr id="pc_quiz_row_2">
        <th>C</th>
        <th>{{trans('powering_content_quiz.informal')}}</th>
		<th><input type="radio" name="pc_informal_value"value="1" @if($wp_omission->pc_informal_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_informal_value"value="2" @if($wp_omission->pc_informal_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_informal_value"value="3" @if($wp_omission->pc_informal_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_informal_value"value="4" @if($wp_omission->pc_informal_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_informal_value"value="5" @if($wp_omission->pc_informal_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.elegant')}}</th>
      </tr>
	  
      <tr id="pc_quiz_row_3">
        <th>D</th>
        <th>{{trans('powering_content_quiz.laid_black')}}</th>
		<th><input type="radio" name="pc_laid_back_value"value="1" @if($wp_omission->pc_laid_back_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_laid_back_value"value="2" @if($wp_omission->pc_laid_back_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_laid_back_value"value="3" @if($wp_omission->pc_laid_back_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_laid_back_value"value="4" @if($wp_omission->pc_laid_back_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_laid_back_value"value="5" @if($wp_omission->pc_laid_back_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.driven')}}</th>
      </tr>
	  
      <tr id="pc_quiz_row_4">
        <th>E</th>
        <th>{{trans('powering_content_quiz.subtle')}}</th>
		<th><input type="radio" name="pc_subtle_value"value="1" @if($wp_omission->pc_subtle_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_subtle_value"value="2" @if($wp_omission->pc_subtle_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_subtle_value"value="3" @if($wp_omission->pc_subtle_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_subtle_value"value="4" @if($wp_omission->pc_subtle_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_subtle_value"value="5" @if($wp_omission->pc_subtle_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.exuberant')}}</th>
      </tr>
	  
      <tr id="pc_quiz_row_5">
        <th>F</th>
        <th>{{trans('powering_content_quiz.delicate')}}</th>
		<th><input type="radio" name="pc_delicate_value"value="1" @if($wp_omission->pc_delicate_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_delicate_value"value="2" @if($wp_omission->pc_delicate_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_delicate_value"value="3" @if($wp_omission->pc_delicate_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_delicate_value"value="4" @if($wp_omission->pc_delicate_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_delicate_value"value="5" @if($wp_omission->pc_delicate_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.bold')}}</th>
      </tr>
	  
	  
      <tr id="pc_quiz_row_6">
        <th>G</th>
        <th>{{trans('powering_content_quiz.understated')}}</th>
		<th><input type="radio" name="pc_understated_value"value="1" @if($wp_omission->pc_understated_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_understated_value"value="2" @if($wp_omission->pc_understated_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_understated_value"value="3" @if($wp_omission->pc_understated_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_understated_value"value="4" @if($wp_omission->pc_understated_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_understated_value"value="5" @if($wp_omission->pc_understated_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.dramatic')}}</th>
      </tr>
	  
      <tr id="pc_quiz_row_7">
        <th>H</th>
        <th>{{trans('powering_content_quiz.monochrome')}}</th>
		<th><input type="radio" name="pc_monochrome_value"value="1" @if($wp_omission->pc_monochrome_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_monochrome_value"value="2" @if($wp_omission->pc_monochrome_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_monochrome_value"value="3" @if($wp_omission->pc_monochrome_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_monochrome_value"value="4" @if($wp_omission->pc_monochrome_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_monochrome_value"value="5" @if($wp_omission->pc_monochrome_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.multicolor')}}</th>
      </tr>
	  
      <tr id="pc_quiz_row_8">
        <th>I</th>
        <th>{{trans('powering_content_quiz.flowy')}}</th>
		<th><input type="radio" name="pc_flowy_value"value="1" @if($wp_omission->pc_flowy_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_flowy_value"value="2" @if($wp_omission->pc_flowy_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_flowy_value"value="3" @if($wp_omission->pc_flowy_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_flowy_value"value="4" @if($wp_omission->pc_flowy_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_flowy_value"value="5" @if($wp_omission->pc_flowy_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.sharp')}}</th>
      </tr>
	  
	  
      <tr id="pc_quiz_row_9">
        <th>J</th>
        <th>{{trans('powering_content_quiz.rugged')}}</th>
		<th><input type="radio" name="pc_rugged_value"value="1" @if($wp_omission->pc_rugged_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_rugged_value"value="2" @if($wp_omission->pc_rugged_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_rugged_value"value="3" @if($wp_omission->pc_rugged_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_rugged_value"value="4" @if($wp_omission->pc_rugged_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_rugged_value"value="5" @if($wp_omission->pc_rugged_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.clean')}}</th>
      </tr>
	  
      <tr id="pc_quiz_row_10">
        <th>K</th>
        <th>{{trans('powering_content_quiz.handmade')}}</th>
		<th><input type="radio" name="pc_handmade_value"value="1" @if($wp_omission->pc_handmade_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_handmade_value"value="2" @if($wp_omission->pc_handmade_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_handmade_value"value="3" @if($wp_omission->pc_handmade_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_handmade_value"value="4" @if($wp_omission->pc_handmade_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_handmade_value"value="5" @if($wp_omission->pc_handmade_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.digital')}}</th>
      </tr>
	  
      <tr id="pc_quiz_row_11">
        <th>L</th>
        <th>{{trans('powering_content_quiz.rustic')}}</th>
		<th><input type="radio" name="pc_rustic_value"value="1" @if($wp_omission->pc_rustic_value ==1) checked @endif></th>
		<th><input type="radio" name="pc_rustic_value"value="2" @if($wp_omission->pc_rustic_value ==2) checked @endif></th>
		<th><input type="radio" name="pc_rustic_value"value="3" @if($wp_omission->pc_rustic_value ==3) checked @endif></th>
		<th><input type="radio" name="pc_rustic_value"value="4" @if($wp_omission->pc_rustic_value ==4) checked @endif></th>
		<th><input type="radio" name="pc_rustic_value"value="5" @if($wp_omission->pc_rustic_value ==5) checked @endif></th>
		<th>{{trans('powering_content_quiz.pixel_perfect')}}</th>
      </tr>
      
    </tbody>
  </table>
  
  	<button type="submit" class="form-control button-next btn btn-info">{{trans('powering_content_quiz.send_answers')}}</button>
  
	</form>
  
</div>
</div>

<script>
	
	@if($wp_omission->pc_errors != null)
	
	$(document).ready(function(){

    	
    	$.notify({
        	icon: 'pe-7s-info',
        	message: "{{$wp_omission->pc_errors}}"

        },{
            type: 'info',
            timer: 4000
        });
		
		var pc_errors = "{{$wp_omission->pc_rows_error}}";
		var pc_errors_array = pc_errors.split(",");
		
		for (i = 0; i < pc_errors_array.length; i++) {
			$("#pc_quiz_row_"+pc_errors_array[i]).attr("bgcolor","#dc8a84");
		} 
		
		//$("#pc_quiz_row_1").attr("bgcolor","#FF0000")
		

	});
	
	@endif
	
</script>



