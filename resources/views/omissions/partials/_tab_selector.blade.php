<script>
	
	 $(document).on('ready', function() {
	
	 @foreach($states as $state)
		 
		 @if(isset($selected_state))
		 	@if($selected_state->ostate()->label  == $state->label)
		 		$("#titleClick-demo-head-{{$state->selected_index}}" ).trigger( "click" );
		 	@endif
		 @endif
		 
	 @endforeach
	
	 });				
	
</script>
	 