<?php

if(isset($webtender)){
	$first_name = strtoupper(get_user_meta( $webtender->ID, "first_name", true )); 
	$last_name = strtoupper(get_user_meta( $webtender->ID, "last_name", true )); 
}

?>

<p>
	Entre los suscritos a saber OZONE LABS S.A.S, debidamente representada por PEDRO CONSUEGRA TAMARA, identificado con la cédula de ciudadanía número 72.344.448 de BARRANQUILLA, mayor de edad, domiciliado en BARRANQUILLA, quien en adelante se denominará EL CONTRATANTE, de una parte y, de la otra {{$first_name}} {{$last_name}}, mayor de edad, de quien en adelante se denominará EL CONTRATISTA, han convenido en celebrar el presente contrato de obra por encargo, sujeto al siguiente:<br /> <br />CLAUSULADO PRIMERA.- OBJETO En virtud del presente contrato, EL CONTRATANTE, encarga a EL CONTRATISTA la realización de la obra denominada:  {{strtoupper(get_the_title( icl_object_id( $product->id, 'product', false, $global_lang ) ))}} 
	
	<br /> <br />SEGUNDA.- OBLIGACIONES DE LAS PARTES EL CONTRATANTE se compromete a:  Proporcionar a EL CONTRATISTA toda la información básica para la ejecución del trabajo, que será disponible, fiable, correcta, actualizada y completa.  Pagar cumplidamente el valor del contrato como contraprestación a la elaboración de la siguiente <strong>Lista de entregables</strong> que componen la obra encargada:

</p>


 	@include('omissions/partials/_fields', array('ids' => $ids))

	@include('omissions/partials/_fields', array('ids' => $ids))
 		
<p>
	Entregar en tiempo la información necesaria para la realización de la obra. EL CONTRATISTA se compromete a:  Realizar su mejor esfuerzo en la creación de la obra contratada de modo diligente y competente, dentro de los plazos acordados.  Corregir la obra cuando ésta incumpla el encargo, siempre y cuando EL CONTRATANTE, solicite a EL CONTRATISTA.  En el caso de subcontratación puntual de algún producto o servicio por parte de EL CONTRATISTA a un tercero, EL CONTRATISTA garantizará y será responsable del resultado final del trabajo de dichos terceros y de la observancia de los derechos de autor involucrados. TERCERA.- CESIÓN DE LA(S) OBRA(S) OBJETO DEL CONTRATO: EL CONTRATISTA cede en exclusiva EL CONTRATANTE, la obra denominada: APP REALITY NOSTALGIOSITY que mediante este contrato se le encargaron y por ello, EL CONTRATISTA no podrá ceder a terceros ninguno de los derechos ni obligaciones establecidas en el presente contrato, salvo autorización expresa y por escrito de EL CONTRATANTE. La cesión de estos derechos se extiende a nivel mundial y durará hasta que la citada obra pase al dominio público. CUARTA.- DURACIÓN Y ENTREGA DE LA(S) OBRA(S): La entrega de la(s) obra(s) creadas en desarrollo del presente contrato, se llevará a cabo dentro de un plazo de ( ) ____ días contados a partir de la firma del mismo. Este plazo es prorrogable de mutuo acuerdo entre las partes. Una vez finalizado el desarrollo del objeto del contrato y aceptada por EL CONTRATANTE toda la obra creadas, EL CONTRATISTA deberá hacer entrega formal de las mismas, junto con cualquier otra documentación que se haya generado con motivo de este contrato, de tal forma que sólo existirá una copia de la totalidad del material relativo al objeto del contrato en poder exclusivo de EL CONTRATANTE. La entrega de la obra se efectuará en las instalaciones de EL CONTRATISTA. QUINTA.- PRECIO Y FORMA DE PAGO: Como remuneración económica por concepto de la ejecución de la obra contratada, presente y en contraprestación por la cesión de los derechos patrimoniales de autor la obra EL CONTRATISTA, percibirá la suma de $1.000.000 (Un millón de pesos) de la siguiente manera: 30% al inicio de la Obra y 30% cuando la app esté funcionando en las tiendas Itunes y Android y 40 % restante cuando se entregue el manual de usuario y archivos fuentes al final con el recibo a satisfacción. Que quede instalado en por lo menos un computador de Ozone.
	SEXTA. FORMA DE ENTREGA DE LA OBRA: 

	Versión beta. Versión no dinámica de la app martes 12 Junio.
	Versión final dinamica 27 de Junio. 
	Entrega de archivos fuentes e instalación 29 de Junio.



	SÉPTIMA. CALIDAD DE LA OBRA: El contratante se compromete a realizar una obra de calidad.OCTAVA. PROTOTIPO APROBADO: Se entenderá como prototipo aprobado al diseño de producto elaborado por encargo de manera inicial, que es aprobado y conocido por ambas partes antes de la firma de este contrato.
	. NOVENA.- PROPIEDAD INTELECTUAL: Como consecuencia de la cesión de los derechos patrimoniales de EL CONTRATISTA a EL CONTRATANTE, la titularidad de dichos derechos patrimoniales de la obra contratada le corresponde en exclusividad al CONTRATANTE. NOVENA.- CONFIDENCIALIDAD: Las partes se comprometen, con carácter mutuo y recíproco, a tratar como “confidencial” toda la información técnica, comercial o de cualquier otra naturaleza comprendida y/o que se derive directa o indirectamente de las indicaciones que la contraparte le haya facilitado para el desarrollo del objeto del presente contrato (en adelante “la información confidencial”). En consecuencia ninguna parte podrá revelar total o parcialmente, de palabra, por escrito o de cualquier otra forma, a ninguna persona física o jurídica, ya sea de carácter público o privado, la Información Confidencial, sin el consentimiento expreso y por escrito de la contraparte. La misma confidencialidad que se imponen a las partes o a terceros que intervengan en la ejecución, deberá ser impuesta por cada una de las partes a sus trabajadores (por cuenta propia o ajena, con relación laboral o mercantil) que de modo directo o indirecto están relacionados con el objeto del contrato. Será obligación de las mismas partes hacer firmar a sus trabajadores un documento vinculante por el que adquieren tal obligación. Este compromiso de confidencialidad, tanto entre las partes como de éstas con sus trabajadores y contratistas, permanecerá durante la vigencia del presente contrato así como un año después de la finalización del mismo. DÉCIMA.- JURISDICCIÓN: Cualquier disputa o 
	diferencia que surja con ocasión del objeto y obligaciones de este contrato serán arregladas en lo posible mediante la negociación y la conciliación entre las partes, para lo cual se contará con un período de treinta (30) días calendario. Sin embargo, en cualquier momento las partes podrán llevar cualquier disputa o controversia o reclamo relacionado con este contrato, incluyendo la existencia, validez o terminación de éste, ante un Tribunal de Arbitramento. La sede de dicho Tribunal de Arbitramento será la ciudad de Barranquilla y estará conformado por un (1) árbitro que será un abogado especializado en propiedad 
	industrial o con experiencia laboral no menor a cinco años en dicho campo, que fallará en derecho dando aplicación a la ley colombiana. Dicho árbitro será nombrado por el Centro de Conciliación y Arbitraje de la Cámara de Comercio. Los costos de procedimiento de arbitraje serán aquellos estipulados por las partes. Sin embargo, cada parte en la disputa correrá con sus propios gastos, incluyendo costos de abogados, traducciones, peritajes y costos de traslado del perito. En constancia de lo anterior se firma en la ciudad.


	de __________________, a los ___ días del mes ____ de ____, en dos copias iguales. EL CONTRATANTE _______________________ CC.____________________ 

	________________________________
	Firma


	NIT:___________________ EL CONTRATISTA ______________________ 
	CC.____________________ 



	________________________________
	Firma
	
	
</p>