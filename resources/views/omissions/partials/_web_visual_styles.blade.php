@if(($wp_omission->pc_visual_character != null) & ( $wp_omission->pc_visual_character != null) & ($wp_omission->pc_visual_geometry != null))

<div class="row">
	<div class="col-md-12">
			
			<h2>{{trans('powering_content_quiz.quiz_result')}}</h2>
			
			<i>{{trans('powering_content_quiz.test_result_explanation_1')}}: {{$wp_omission->pc_visual_character}}</i>
			<br />
			<i>{{trans('powering_content_quiz.test_result_explanation_2')}}: {{$wp_omission->pc_visual_contrast}}</i>
			<br />
			<i>{{trans('powering_content_quiz.test_result_explanation_3')}}: {{$wp_omission->pc_visual_geometry}}</i>
			<br />
			       
			  <table class="table">
			    <thead>
			      <tr>
					<th></th>
			        <th><p class="pc_test_header">{{trans('powering_content_quiz.visual_character')}}</p></th>
			        <th><p class="pc_test_header">{{trans('powering_content_quiz.visual_contrast')}}</p></th>
			        <th><p class="pc_test_header">{{trans('powering_content_quiz.visual_geometry')}}</p></th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr>
					<td>4-11</td>
			        <td><div id="visual_character_1" class="pc_test_result">{{trans('powering_content_quiz.visual_character_explanation_1')}}</div></td>
			        <td><div id="visual_contrast_1" class="pc_test_result">{{trans('powering_content_quiz.visual_contrast_explanation_1')}}</div></td>
			        <td><div id="visual_geometry_1" class="pc_test_result">{{trans('powering_content_quiz.visual_geometry_explanation_1')}}</div></td>
			      </tr>
			      <tr>
					<td>12-22</td>
			        <td><div id="visual_character_2" class="pc_test_result">{{trans('powering_content_quiz.visual_character_explanation_2')}}</div></td>
			        <td><div id="visual_contrast_2" class="pc_test_result">{{trans('powering_content_quiz.visual_contrast_explanation_2')}}</div></td>
			        <td><div id="visual_geometry_2" class="pc_test_result">{{trans('powering_content_quiz.visual_geometry_explanation_2')}}</div></td>
			      </tr>
			     
			    </tbody>
			  </table>
			  
		  
		  <!-- 
		  
		
		  
		  -->
			  
			  <table id="table_result1" class="table">
			    <thead>
			      <tr>
			        <th><p class="pc_test_header">{{trans('powering_content_quiz.visual_character')}}</p></th>
			        <th><p class="pc_test_header">{{trans('powering_content_quiz.visual_contrast')}}</p></th>
			        <th><p class="pc_test_header">{{trans('powering_content_quiz.visual_geometry')}}</p></th>
					<th><p class="pc_test_header">{{trans('powering_content_quiz.visual_style_name')}}</p></th>
			      </tr>
			    </thead>
			    <tbody>
			      <tr id="casual_low_organic">
					<td>{{trans('powering_content_quiz.casual')}}</td>
			        <td>{{trans('powering_content_quiz.low')}}</td>
			        <td>{{trans('powering_content_quiz.organic')}}</td>
			        <td>{{trans('powering_content_quiz.earthy_naive')}}</td>
			      </tr>
			      <tr id="casual_low_sharp">
					<td>{{trans('powering_content_quiz.casual')}}</td>
			        <td>{{trans('powering_content_quiz.low')}}</td>
			        <td>{{trans('powering_content_quiz.sharp')}}</td>
			        <td>{{trans('powering_content_quiz.urban_hipster')}}</td>
			      </tr>
			      <tr id="casual_high_organic">
					<td>{{trans('powering_content_quiz.casual')}}</td>
			        <td>{{trans('powering_content_quiz.high')}}</td>
			        <td>{{trans('powering_content_quiz.organic')}}</td>
			        <td>{{trans('powering_content_quiz.genuinely_passionate')}}</td>
			      </tr>
			      <tr id ="casual_high_sharp">
					<td>{{trans('powering_content_quiz.casual')}}</td>
			        <td>{{trans('powering_content_quiz.high')}}</td>
			        <td>{{trans('powering_content_quiz.sharp')}}</td>
			        <td>{{trans('powering_content_quiz.practical_joy')}}</td>
			      </tr>
			      <tr id="formal_low_organic">
					<td>{{trans('powering_content_quiz.formal')}}</td>
			        <td>{{trans('powering_content_quiz.low')}}</td>
			        <td>{{trans('powering_content_quiz.organic')}}</td>
			        <td>{{trans('powering_content_quiz.bespoke_chic')}}</td>
			      </tr>
			      <tr id="formal_low_sharp">
					<td>{{trans('powering_content_quiz.formal')}}</td>
			        <td>{{trans('powering_content_quiz.low')}}</td>
			        <td>{{trans('powering_content_quiz.sharp')}}</td>
			        <td>{{trans('powering_content_quiz.clean_performance')}}</td>
			      </tr>
			      <tr id="formal_high_organic">
					<td>{{trans('powering_content_quiz.formal')}}</td>
			        <td>{{trans('powering_content_quiz.high')}}</td>
			        <td>{{trans('powering_content_quiz.organic')}}</td>
			        <td>{{trans('powering_content_quiz.classic_credibility')}}</td>
			      </tr>
			      <tr id="formal_high_sharp">
					<td>{{trans('powering_content_quiz.formal')}}</td>
			        <td>{{trans('powering_content_quiz.high')}}</td>
			        <td>{{trans('powering_content_quiz.sharp')}}</td>
			        <td>{{trans('powering_content_quiz.consistent_satisfaction')}}</td>
			      </tr>
			    </tbody>
			  </table>
			  
			  
			  
			  <table id="table_result2" class="table">
			    <thead>
			      <tr>
			        <th><p class="pc_test_header">{{trans('powering_content_quiz.visual_style_name')}}</p></th>
			        <th><p class="pc_test_header">{{trans('powering_content_quiz.design_tips')}}</p></th>
			        <th><p class="pc_test_header">{{trans('powering_content_quiz.real_life_example')}}</p></th>
			      </tr>
			    </thead>
			    <tbody>
					
  			      <tr id="earthy_naive">
  					<td>{{trans('powering_content_quiz.earthy_naive')}}</td>
  			        <td>{{trans('powering_content_quiz.earthy_name_tip')}}</td>
  			        <td>Panera Bread</td>
  			      </tr>
				  
  			      <tr id="urban_hipster">
  					<td>{{trans('powering_content_quiz.urban_hipster')}}</td>
  			        <td>{{trans('powering_content_quiz.urban_hipster_tip')}}</td>
  			        <td>Chipotle</td>
  			      </tr>
				  
  			      <tr id="genuinely_passionate">
  					<td>{{trans('powering_content_quiz.genuinely_passionate')}}</td>
  			        <td>{{trans('powering_content_quiz.genuinely_passionate_tip')}}</td>
  			        <td>Peet’s Coffee & Tea</td>
  			      </tr>
				  
  			      <tr id="practical_joy">
  					<td>{{trans('powering_content_quiz.practical_joy')}}</td>
  			        <td>{{trans('powering_content_quiz.practical_joy_tip')}}</td>
  			        <td>Dunkin Donuts</td>
  			      </tr>
				  
  			      <tr id="bespoke_chic">
  					<td>{{trans('powering_content_quiz.bespoke_chic')}}</td>
  			        <td>{{trans('powering_content_quiz.bespoke_chic_tip')}}</td>
  			        <td>Anthropologie</td>
  			      </tr>
				  
  			      <tr id="clean_performance">
  					<td>{{trans('powering_content_quiz.clean_performance')}}</td>
  			        <td>{{trans('powering_content_quiz.clean_performance_tip')}}</td>
  			        <td>Calvin Klein</td>
  			      </tr>
				  
  			      <tr id="classic_credibility">
  					<td>{{trans('powering_content_quiz.classic_credibility')}}</td>
  			        <td>{{trans('powering_content_quiz.classic_credibility_tip')}}</td>
  			        <td>Ray-Ban</td>
  			      </tr>
				  
  			      <tr id="consistent_satisfaction">
  					<td>{{trans('powering_content_quiz.consistent_satisfaction')}}</td>
  			        <td>{{trans('powering_content_quiz.consistent_satisfaction_tip')}}</td>
  			        <td>DKNY</td>
  			      </tr>
					
  			    </tbody>
  			  </table>
			  
			  
			
	</div>
</div>

@endif

<script type="text/javascript">
	$(document).ready(function(){
		
		visual_character ="";
		visual_contrast ="";
		visual_geometry ="";
		visual_site_name = "";
		
		@if(($wp_omission->pc_visual_character >= 4 ) & ( $wp_omission->pc_visual_character <= 11))
			visual_character = "casual";
			$("#visual_character_1").css( "background-color", "#74ae9a" );
			$("#visual_character_1").css( "color", "#fff" );
		@elseif(($wp_omission->pc_visual_character >= 12 ) & ( $wp_omission->pc_visual_character <= 20))
			$("#visual_character_2").css( "background-color", "#74ae9a" );
			$("#visual_character_2").css( "color", "#fff" );
			visual_character = "formal";
		@endif
		
		@if(($wp_omission->pc_visual_contrast >= 4 ) & ( $wp_omission->pc_visual_contrast <= 11))
			visual_contrast = "low";
			$("#visual_contrast_1").css( "background-color", "#74ae9a" );
			$("#visual_contrast_1").css( "color", "#fff" );
		@elseif(($wp_omission->pc_visual_contrast >= 12 ) & ( $wp_omission->pc_visual_contrast <= 20))
			visual_contrast = "high";
			$("#visual_contrast_2").css( "background-color", "#74ae9a" );
			$("#visual_contrast_2").css( "color", "#fff" );
		@endif
		
		@if(($wp_omission->pc_visual_geometry >= 4 ) & ( $wp_omission->pc_visual_geometry <= 11))
			visual_geometry = "organic";
			$("#visual_geometry_1").css( "background-color", "#74ae9a" );
			$("#visual_geometry_1").css( "color", "#fff" );
		@elseif(($wp_omission->pc_visual_geometry >= 12 ) & ( $wp_omission->pc_visual_geometry <= 20))
			visual_geometry = "sharp";
			$("#visual_geometry_2").css( "background-color", "#74ae9a" );
			$("#visual_geometry_2").css( "color", "#fff" );
		@endif
		
		
		console.log("visual_character : "+visual_character);
		console.log("visual_contrast : "+visual_contrast);
		console.log("visual_geometry : "+visual_geometry);	
		
		@if(($wp_omission->pc_visual_character != null) & ( $wp_omission->pc_visual_character != null) & ($wp_omission->pc_visual_geometry != null))
			
			
		$("#"+visual_character+"_"+visual_contrast+"_"+visual_geometry).css("background", "#74ae9a" );
		$("#"+visual_character+"_"+visual_contrast+"_"+visual_geometry).css("color", "#fff" );
		
		if((visual_character == "casual") & (visual_contrast == "low") & (visual_geometry == "organic")){
			visual_site_name = "earthy_naive";	
		}else if((visual_character == "casual") & (visual_contrast == "low") & (visual_geometry == "sharp")){
			visual_site_name = "urban_hipster";	
		}else if((visual_character == "casual") & (visual_contrast == "high") & (visual_geometry == "organic")){
		   	visual_site_name = "genuinely_passionate";	
		}else if((visual_character == "casual") & (visual_contrast == "high") & (visual_geometry == "sharp")){
		   	visual_site_name = "practical_joy";	
		}else if((visual_character == "formal") & (visual_contrast == "low") & (visual_geometry == "organic")){
		   	visual_site_name = "bespoke_chic";	
		}else if((visual_character == "formal") & (visual_contrast == "low") & (visual_geometry == "sharp")){
		   	visual_site_name = "clean_performance";	
		}else if((visual_character == "formal") & (visual_contrast == "high") & (visual_geometry == "organic")){
		   	visual_site_name = "classic_credibility";	
		}else if((visual_character == "formal") & (visual_contrast == "high") & (visual_geometry == "sharp")){
		   	visual_site_name = "consistent_satisfaction";	
		}
		
		$("#"+visual_site_name).css("background", "#74ae9a" );
		$("#"+visual_site_name).css("color", "#fff" );
		
		@endif
		
	});
	
</script>
	
		  