
<div class="row">
	<div class="col-md-12">
		
		<h3>¿CÓMO FUNCIONA EL PROCESO?</h3>
		<p>El proceso completo puede demorar entre 1 y 2 semanas, dependiendo de cuánto tiempo te tome completar las rondas y aprobaciones de revisión:</p>
		
		<ol>
			<li>Escoge a un diseñador.</li>
			<li>Una vez hayas escogido a alguno de los diseñadores, este te contactara de vuelta vía correo electrónico o telefonica para agendar una videoconferencia con el.</li> 
			<li>En esta reunión se deberá definir la lista de entregables correspondientes al paquete de las horas que hayas comprado, estos deben quedar consignados en el campo de texto: Lista de entregables y posteriormente firmar el acuerdo de manera digital.</li>
			<li>En un tiempo máximo de 7 días deberán estar listos todos los entregables acordados.</li>
			<li>Si el recibo es a satisfacción se dará por terminado el trabajo.</li>
			
		</ol> 
		
	</div>
</div>



