
<div class="row">
	<div class="col-md-12">
		
		<h3>HOW DOES THE PROCESS WORK?</h3>
		<p>The entire process can take up to 72 hours, depending on how long it takes you to complete the brief</p>
		
		<ol>
			<li>After the purchase has been made, we will send you a link where you can answer a series of questions that will help us to configure the SSL Certificate for your domain.</li>
			<li>Your SSL Certificate will be ready and installed within 72 hours.</li>
			
		</ol> 
		
	</div>
</div>



