<script>

$( document ).ready(function() {
	var obj = [];
	
    $(".omissions").observe_field(1, function( ) {
		
		$("#loading_area").html('<div class="cssload-loader"></div>');
		
		object_id = $(this).attr("omission_id");
		meta_key = $(this).attr("meta_key");
		
		obj["input_"+"_"+meta_key+"_"+object_id] = $(this);
		obj["input_"+"_"+meta_key+"_"+object_id].removeClass("text_area_fancy_done").addClass("text_area_fancy");
		
      if ($(this).val() != ""){
 		 
        jQuery.ajax({
          url: "/save_brief",
          type: 'post',
          dataType: 'json',
          data: {meta_value : $(this).val(), omission_id: $(this).attr("omission_id"), meta_key: $(this).attr("meta_key")},
     		success: function(data){
   			$(".cssload-loader").remove();
			obj["input_"+"_"+data["meta_key"]+"_"+data["object_id"]].removeClass("text_area_fancy").addClass("text_area_fancy_done");
			
   		}
        });
      }
    });
	
    $(".omissions_check").click(function() {
    
	  $("#loading_area").html('<div class="cssload-loader"></div>');
	   
	   if($(this).is(":checked")){
		   ovalue = "1"; 
	   }else{
	   	   ovalue = "0"; 
	   }
	  
        jQuery.ajax({
          url: "/save_brief",
          type: 'post',
          dataType: 'json',
           data: {meta_value : ovalue, omission_id: $(this).attr("omission_id"), meta_key: $(this).attr("meta_key"), selected_state_id: "{{$selected_state->id}}", next_state: $(this).attr("next_state"),next_state_logic: $(this).attr("next_state_logic"),next_state_logic_result: $(this).attr("next_state_logic_result"), _token: "{{csrf_token()}}"},
     		success: function(data){
  			$(".cssload-loader").remove();
			if(data["reload"] =="true"){
				location.reload();
			}
   		}
        });
	  
    });
	
    $(".omissions_radio").click(function() {
		
		$("#loading_area").html('<div class="cssload-loader"></div>');
	   
        jQuery.ajax({
          url: "/save_brief",
          type: 'post',
          dataType: 'json',
           data: {meta_value : $(this).val(), omission_id: $(this).attr("omission_id"), meta_key: $(this).attr("meta_key"), selected_state_id: "{{$selected_state->id}}", next_state: $(this).attr("next_state"),next_state_logic: $(this).attr("next_state_logic"),next_state_logic_result: $(this).attr("next_state_logic_result"),  _token: "{{csrf_token()}}"},
     		success: function(data){
  			$(".cssload-loader").remove();
   		}
        });
	  
    });
	
	
    $(".omissions_select").change(function() {
		
		$("#loading_area").html('<div class="cssload-loader"></div>');
		
		object_id = $(this).attr("omission_id");
		meta_key = $(this).attr("meta_key");
		
		obj["input_"+"_"+meta_key+"_"+object_id] = $(this);
		obj["input_"+"_"+meta_key+"_"+object_id].removeClass("text_area_fancy_done").addClass("text_area_fancy");
	   
        jQuery.ajax({
          url: "/save_brief",
          type: 'post',
          dataType: 'json',
           data: {meta_value : $(this).val(), omission_id: $(this).attr("omission_id"), meta_key: $(this).attr("meta_key"), selected_state_id: "{{$selected_state->id}}", next_state: $(this).attr("next_state"),next_state_logic: $(this).attr("next_state_logic"),next_state_logic_result: $(this).attr("next_state_logic_result"),  _token: "{{csrf_token()}}"},
     		success: function(data){
				
	   			$(".cssload-loader").remove();
				obj["input_"+"_"+data["meta_key"]+"_"+data["object_id"]].removeClass("text_area_fancy").addClass("text_area_fancy_done");
				if(data["reload"] == "true"){
					location.reload();
				}
   		}
        });
	  
    });
	
	
    $(".omissions_approval").click(function() {
		
		$("#loading_area").html('<div class="cssload-loader"></div>');
	   
        jQuery.ajax({
          url: "/save_brief",
          type: 'post',
          dataType: 'json',
           data: {meta_value : $(this).val(), omission_id: $(this).attr("omission_id"), meta_key: $(this).attr("meta_key"), selected_state_id: "{{$selected_state->id}}", next_state: $(this).attr("next_state"),next_state_logic: $(this).attr("next_state_logic"),next_state_logic_result: $(this).attr("next_state_logic_result"), _token: "{{csrf_token()}}"},
     		success: function(data){
  			$(".cssload-loader").remove();
			location.reload();
			
   		}
        });
	  
    });
	
	
	
	function moveprogressbar(percentage,div_id) {
	  var elem = document.getElementById(div_id);   
	  var width = 10;
	 // elem.style.backgroundColor = "blue";
      elem.style.width = percentage + '%'; 
      //elem.innerHTML = percentage * 1  + '%';
	  
	  /*
	  var id = setInterval(frame, 10);
	  function frame() {
	    if (width >= 100) {
	      clearInterval(id);
	    } else {
	      width++; 
	      elem.style.width = width + '%'; 
	      elem.innerHTML = width * 1  + '%';
	    }
	  }
	  */
	}
	
	
	function progressFunction(i,evt){  
		
		console.log("addEventListener progress")
		console.log("i: "+ i)
		
		if (evt.lengthComputable) {
		      var percentComplete = evt.loaded / evt.total;
		      //Do something with upload progress
		      console.log(percentComplete);
			  percente_value = percentComplete * 100;
			  moveprogressbar(percente_value,"myBar_"+i);
		}
	 }  
	
	$('.upload_brief_file').each(function () {
		
		//Se cargan a esta funciones para cada uploader del brief
		
		var uploadField =  $(this).find('input')[0];
		console.log("upload_brief_file");
		peter = "peter";
		
		$(uploadField).on('change', function (e) {
			
			var xhr_array = [];
			var xhr_timestamps = [];
			section = $(this).attr("section");
			
		    for (var i = 0; i < $(this).get(0).files.length; ++i) {
		       
			console.log($(this).get(0).files[i].name);
		    file_title = $(this).get(0).files[i].name;
			
			var fileNameOriginal = $(this).get(0).files[i].name;
			var fileinput = $(this).get(0).files[i];
			
			var d = new Date();
			var timestamp = d.getTime();
			
			var iSize = ($(this)[0].files[0].size / 1024);
            iSize = (Math.round((iSize / 1024) * 100) / 100)
			
			if(iSize<=50){
				//var oform = new FormData($(this).parent('form')[0]);

				var oform = new FormData();
				
				oform.append("omission_id", "{{$wp_omission->id}}");
				oform.append("section", section);
				oform.append("_token", "{{csrf_token()}}");
				oform.append("file", fileinput);
				oform.append("timestamp", timestamp);
				oform.append("title", file_title);
				
				console.log("Santa monica");
				console.log(fileinput);
			  
			  upload_progress_html = "";
			   upload_progress_html += '<div class="row">';
			  upload_progress_html += '<div class="col-md-6">';
			  upload_progress_html += '<div id ="progress_container_'+timestamp+'">';
			  upload_progress_html += '<label>'+fileNameOriginal+'</label>';
			  upload_progress_html += '<div class="progress">';
			  upload_progress_html += '<div class="progress-bar" id="myBar_'+timestamp+'" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%">';
			  upload_progress_html += '</div>';
			  upload_progress_html += '</div>';
			  upload_progress_html += '</div>';
			  upload_progress_html += '</div>';
			  upload_progress_html += '</div>';
				/*
				upload_progress_html = "";
				upload_progress_html += '<div id ="progress_container_'+timestamp+'" class="row">';
				upload_progress_html += '<div class="col-md-6">';
				upload_progress_html += '<label>'+fileNameOriginal+'</label>';
				upload_progress_html += '<div class="progress_container">';
				upload_progress_html += '<div class="progress">';
				upload_progress_html += '<div id="myBar_'+timestamp+'" class="bar"></div>';
				upload_progress_html += '</div>';
				upload_progress_html += '</div>';
				upload_progress_html += '</div>';
				upload_progress_html += '</div>';
			    */
	
				$("#upload_brief_area_"+section).prepend(upload_progress_html);
				
				$.ajax({
				   xhr: function()
				    {
				      xhr_array[i] = new window.XMLHttpRequest();
					  
				      //Upload progress
				      xhr_array[i].upload.addEventListener("progress",progressFunction.bind(null, timestamp), false);
				      //Download progress
				      xhr_array[i].addEventListener("progress", function(evt){
				        if (evt.lengthComputable) {
				          var percentComplete = evt.loaded / evt.total;
				          //Do something with download progress
						  console.log("Download")
				          console.log(percentComplete,"myBar_"+xhr_timestamps[i]);
						  
				        }
				      }, false);
					
					 
					return xhr_array[i];
					  
				    },
					url: '/wp_omissions_files',
					data: oform,
					dataType: 'html',
					method: 'post',
					cache: false,
					contentType: false,
					processData: false,
					fail: function (dat) {
						console.log(dat);
						console.log("fallo!");
					},
					success: function (data) {
						
						console.log("File uploaded")
						
						parsedjson = JSON.parse(data);
						parsed = parsedjson["wp_omissions_file"];
						
						htmlsection = '';
						
						if((parsed.extension == "png") || (parsed.extension == "PNG") || (parsed.extension == "jpg") || (parsed.extension == "JPG") || (parsed.extension == "gif") || (parsed.extension == "GIF") || (parsed.extension == "jpeg") || (parsed.extension == "JPEG")){
						
						htmlsection+='<div class="img-wrap">';
						htmlsection+='<span class="close"><a href="/delete_file/'+parsed.id+'?current_url={{URL::full()}}" class="delete_file_from_brief" omissions_file_id="'+parsed.id+'">&times;</a></span>'
						htmlsection+='<span class="close"><a href="/delete_file/'+parsed.id+'?current_url={{URL::full()}}" class="delete_file_from_brief" omissions_file_id="'+parsed.id+'" omission_id="{{$wp_omission->id}}" form_number="{{$fnumber}}" current_url="{{URL::full()}}" section ="'+parsed.section+'">&times;</a></span>'
						htmlsection+='<a class="fancybox" href="/files/{{$wp_omission->id}}/'+parsed.file+'.'+parsed.extension+'" data-fancybox-group="gallery"><img src="/files/{{$wp_omission->id}}/'+parsed.file+'.thumb.'+parsed.extension+'" alt="" /></a>'
						htmlsection+='</div>';
						
						}else{
							htmlsection+='<div class="img-wrap">';
							htmlsection+=' <span class="close"><a href="/delete_file/'+parsed.id+'?current_url={{URL::full()}}" class="delete_file_from_brief" omissions_file_id="'+parsed.id+'" omission_id="{{$wp_omission->id}}" form_number="{{$fnumber}}" current_url="{{URL::full()}}" section ="'+parsed.section+'">&times;</a></span>';
							htmlsection+='<a class="fancybox" href="/files/{{$wp_omission->id}}/'+parsed.file+'.'+parsed.extension+'" data-fancybox-group="gallery"><img src="/file.png" alt=""/></a>';
							htmlsection+='</div>';
						}
						
						$("#displayImages_"+parsed.section).append(htmlsection);
						$("#progress_container_"+parsedjson["timestamp"]).remove();
						
					}
						
				});						
			}else{
				alert("File: "+fileNameOriginal+" is bigger than 50MB")
			}
			
			}
			
		});
		
		
		$('.fancybox').fancybox();
		
	});
	
	
	$(document).on('ready', function() {
	 
	 	$(".radio_approval").each(function(index, element) {
			
			 if($(this).is(':checked')){ 
	 			var date = new Date($(this).attr("approval_time"));
	 			console.log("fecha");
	 			console.log(date);
				html ="";
				html+='<span class="stamp is-draft">';
				html+="{{trans('addons.approved')}}";
				html+="<label>{{trans('addons.user')}}: "+$(this).attr("user_login")+"</label>";
				html+="<label>"+date.toLocaleString()+"</label>";
				html+="</span>";
				html+="<br />";
				$("#rubber_"+$(this).attr("field_id")).append(html);
				input_name = $(this).attr("name");
				$(".approvals_"+input_name).attr('disabled', true);
			 }
			
		});
		
	    /*
		var date = new Date("{{$wp_omission->concept_approved_time}}"+" UTC");
	 
		html ="";
		html+='<span class="stamp is-draft">';
		html+="{{trans('addons.approved')}}";
		html+="<label>{{trans('addons.user')}}: {{$wp_omission->concept_approved_by}}</label>";
		html+="<label>"+date.toString()+"</label>";
		html+="</span>";
		html+="<br />";
	
		$("#approve_concept_{{$wp_omission->concept_selected}}").append(html);
	
		$(".approve_concept").attr('disabled', true);
	    */
		
   
	});	 

});

</script>