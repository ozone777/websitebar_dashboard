<?php
$section = $object->section;
$fnumber = $object->fnumber;

if($global_lang == "es"){
$help_text = $object->help_text_es;
}else if($global_lang == "en"){
$help_text = $object->help_text;
}
?>

<div class="row">
	           <div class="col-md-12" id="displayImages{{$fnumber}}">
				   
				
				   
	  			<label>{{$help_text}}</label>
				<div id="displayImages_{{$section}}">
					@foreach($array as $file)	 
					
					
					
					@if($file->section == $section)
					
					
					
					@if(($file->extension == "png") || ($file->extension == "PNG") || ($file->extension == "jpg") || ($file->extension == "JPG") || ($file->extension == "gif") || ($file->extension == "GIF") || ($file->extension == "jpeg") || ($file->extension == "JPEG"))
		   			
				
		   			
					<div class="img-wrap">
						
					    <span class="close"><a href="/delete_file/{{$file->id}}?current_url={{URL::full()}}" class="delete_file_from_brief" omissions_file_id="{{$file->id}}" omission_id="{{$wp_omission->id}}" form_number="{{$fnumber}}" current_url="{{URL::full()}}" section ="logo">&times;</a></span>
					    <a class="fancybox" href="/files/{{$wp_omission->id}}/{{$file->file}}.{{$file->extension}}" data-fancybox-group="gallery" title="{{$file->title}}"><img src="/files/{{$wp_omission->id}}/{{$file->file}}.thumb.{{$file->extension}}" alt="" /></a>
					</div>
					
					@else
					
					<div class="img-wrap">
					    <span class="close"><a href="/delete_file/{{$file->id}}?current_url={{URL::full()}}" class="delete_file_from_brief" omissions_file_id="{{$file->id}}" omission_id="{{$wp_omission->id}}" form_number="{{$fnumber}}" current_url="{{URL::full()}}" section ="logo">&times;</a></span>
					    <a class="fancybox" href="/files/{{$wp_omission->id}}/{{$file->file}}.{{$file->extension}}" data-fancybox-group="gallery" title="{{$file->title}}"><img src="/file.png" alt="" /></a>
					</div>
					
					@endif
					@endif
					@endforeach
					
				
									
				</div>
				
				<div id="spinner{{$fnumber}}" class="cssload-loader" style="display:none;position:absolute;top:50%;"></div>
	          </div>
	    </div>
		<br />


@if($permissions == "admin")

  @if(($role == "administrator") || ($role == "contributor"))
  

  <div id="upload_brief_area_{{$section}}" class="upload_brief_file">

	  <form action="{{ route('wp_omissions_files.store') }}" method="POST" class="form-horizontal frm-logoUpload" enctype="multipart/form-data">
		  <input type="file" id="Uploadid{{$fnumber}}" name="file" class="Uploadid" section="{{$section}}" multiple>
			
		  <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <input type="hidden" name="omission_id" value="{{ $wp_omission->id }}">
		  <input type="hidden" name="section" value="{{$section}}">
			
	  </form>

	  <br />
  </div>

  @endif

@else

<div id="upload_brief_area_{{$section}}" class="upload_brief_file">
	<form action="{{ route('wp_omissions_files.store') }}" method="POST" class="form-horizontal frm-logoUpload" enctype="multipart/form-data">
	
		<input type="file" id="Uploadid{{$fnumber}}" name="file" class="Uploadid" section="{{$section}}" multiple>
			
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="omission_id" value="{{ $wp_omission->id }}">
		<input type="hidden" name="section" value="{{$section}}">
			
	</form>

	<br />

</div>

@endif
