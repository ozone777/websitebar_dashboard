		
		<?php
	
		$designers = get_users("blog_id=1&orderby=nicename&role=$role");

		?>	
		
	    <table class="table table-striped">
	      <thead>
	        <tr>
	          <th>Select</th>
	          <th>Avatar</th>
	          <th>Bio</th>
	        </tr>
	      </thead>
	      <tbody>
	
		@foreach ( $designers as $user )
		 <?php
		 	$avatar = get_user_meta( $user->ID, "simple_local_avatar", true ); 
			$user_description = get_user_meta( $user->ID, "description", true ); 
			$first_name = get_user_meta( $user->ID, "first_name", true ); 
			$last_name = get_user_meta( $user->ID, "last_name", true ); 
		 ?>
	      <tr>
	        <td width="10%"><input type="radio" user_id="{{$user->ID}}" next_state="{{$next_state}}" name="webtender_selected" class="wp_select_user" value="{{$user->ID}}" @if($wp_omission->wp_selected_user == $user->ID) checked @endif></td>
	        <td width="20%">
				
				@if($avatar[96])
				<img src="{{$avatar[96]}}" style="text-align: center">
				@endif
				
			</td>
			<td width="60%">
				<p style="font-weight:bold;">{{$first_name}} {{$last_name}}</p>
				<p>{{$user_description}}</p>
				<p><a target="_blank" href="{{$user->user_url}}">{{trans('addons.see_resume')}}</a></p>
			</td>
	      </tr>
		
		@endforeach
	</table>
		
		
<script>
	
 $(".wp_select_user").click(function() {
	 
	 	$("#loading_area").html('<div class="cssload-loader"></div>');
		
  	     jQuery.ajax({
  	       url: "/wp_select_user",
  	       type: 'post',
  	       dataType: 'json',
  	        data: {user_id : $(this).val(), current_url: "{{Request::fullUrl()}}", next_state: $(this).attr("next_state"), omission_id: "{{$wp_omission->id}}", selected_state_id: "{{$selected_state->id}}", user_id: $(this).attr("user_id"), _token: "{{csrf_token()}}" },
  	  		success: function(data){
				$(".cssload-loader" ).remove();
				location.reload();
  	 		}

  	     });
  });
  
 $(document).on('ready', function() {
	 
	 @if($wp_omission->wp_selected_user)
	 
	 	$(".wp_select_user").attr('disabled', true);
	 
	 @endif
 
 });	 
  
  
	
</script>