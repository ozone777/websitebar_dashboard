
<div class="row">
	<div class="col-md-12">
		
		<h3>HOW DOES THE PROCESS WORK?</h3>
		<p>The entire process can take between 3 and 4 weeks, depending on how long it takes you to complete the rounds and revision approvals:</p>
		
		<ol>
			<li>After the purchase has been made, we will send you a link where you can answer a series of questions that will help us learn more about your brand.</li>
			<li>Once we get your answers, we will conduct research and create two brand concepts in a period of 10 days.</li>
			<li>We will then email a round of brand concepts for your review. We’ll also schedule a 30-minute feedback call with our Senior Branding Consultant (Laura Busche).</li>
			<li>Within 7 days we will incorporate your feedback and email a final package with all the assets included in your brand kit.</li>
		</ol> 
		
	</div>
</div>