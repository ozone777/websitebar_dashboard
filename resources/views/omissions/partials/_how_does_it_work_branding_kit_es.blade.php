
<div class="row">
	<div class="col-md-12">
		
		<h3>¿CÓMO FUNCIONA EL PROCESO?</h3>
		<p>El proceso completo puede demorar entre 3 y 4 semanas, dependiendo de cuánto tiempo te tome completar las rondas y aprobaciones de revisión:</p>
		
		<ol>
			<li>Después de que la compra se haya realizado, te enviaremos un link donde podrás respondernos una serie de preguntas que nos ayudarán a conocer más sobre tu marca. </li>
			<li>Una vez que obtengamos tus respuestas, realizaremos una investigación y crearemos dos conceptos de marca en un tiempo de 10 días.</li>
			<li>Después de esto enviaremos un link con los conceptos de marca a tu correo electrónico para revisión y agendamos una videoconferencia de retroalimentación contigo y nuestra consultora en branding Laura Busche de 30 minutos.</li>
			<li>En un tiempo de 7 días incorporaremos los cambios de la retroalimentación y te enviaremos un paquete a tu correo con todos los archivos del kit de marca.</li>
		</ol> 
		
	</div>
</div>