@if(isset($todos[0]))

<div class="row">
		<div class="col-md-12">
			
	
			<h3>{{trans('addons.delivery_dates')}}</h3>
         	<div id="calendar" class="fc fc-unthemed fc-ltr"></div>
			<br />
		</div>
</div>


	 <script>

	   $(document).ready(function() {

	     $('#calendar').fullCalendar({
			  height: 500,
	       header: {
	         left: 'prev,next today',
	         center: 'title',
	         right: 'month,basicWeek,basicDay'
	       },
	       defaultDate: '2019-01-12',
	       navLinks: true, // can click day/week names to navigate views
	       editable: true,
	       eventLimit: true, // allow "more" link when too many events
	       events: [
		
		@foreach($todos as $todo)
		
	         {
	          
			   @if($global_lang =="es")
	             title: "{{$todo->es}}",
			   @elseif($global_lang =="en")
				title: "{{$todo->en}}",
			   @endif
			   start: "{{$todo->start_time}}",
	           end: "{{$todo->end_time}}"
	         }
			 
		@if($todo != end($todos))
				,
		@endif
		
		@endforeach
			 
	       ]
	     });

	   });

	 </script>
@endif