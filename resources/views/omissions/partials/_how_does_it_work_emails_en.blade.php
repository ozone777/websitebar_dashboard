
<div class="row">
	<div class="col-md-12">
		
		<h3>¿CÓMO FUNCIONA EL PROCESO?</h3>
		<p>The entire process can take up to 72 hours, depending on how long it takes you to complete the brief.</p>
		
		<ol>
			<li>After the purchase has been made, we will send you a link where you can answer a series of questions that will help us set up your emails.</li>
			<li>Your emails will be ready within 72 hours.</li>
			
			
		</ol> 
		
	</div>
</div>



