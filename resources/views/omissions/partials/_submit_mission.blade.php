  
  <div class="row">
		<div class="col-md-12">
			
   <form action="/mission_submitted" method="POST" id ="send_mission_form" enctype="multipart/form-data">
	  
    	<input type="hidden" name="_token" value="{{ csrf_token() }}">	
		<input type="hidden" name="ostate_mission_id" value="{{$selected_state->id}}">
		<input type="hidden" name="label" value="{{$label}}">
		
		<button type="submit" class="form-control button-next btn btn-info" @if($button_id) id = "{{$button_id}}" @endif>{{$button_title}}</button>

    </form>
	
</div>
</div>
<br />