<?php

if(isset($webtender)){
	$first_name = strtoupper(get_user_meta( $webtender->ID, "first_name", true )); 
	$last_name = strtoupper(get_user_meta( $webtender->ID, "last_name", true )); 
}

if(isset($product_owner)){
	$owner_first_name = strtoupper(get_user_meta( $product_owner->ID, "first_name", true )); 
	$owner_last_name = strtoupper(get_user_meta( $product_owner->ID, "last_name", true )); 
}

?>

<p>
	Entre los suscritos a saber <strong>{{$owner_first_name}} {{$owner_last_name}}</strong>, quien en adelante se denominará EL CONTRATANTE, de una parte y, de la otra <strong>{{$first_name}} {{$last_name}}</strong>, mayor de edad, de quien en adelante se denominará EL CONTRATISTA, han convenido en celebrar el presente contrato de obra por encargo, sujeto a las siguientes clausulas:<br /> <br />
	
	PRIMERA - OBJETO: En virtud del presente contrato, EL CONTRATANTE, encarga a EL CONTRATISTA la realización de la obra denominada: <strong>{{strtoupper(get_the_title( icl_object_id( $product->id, 'product', false, $global_lang ) ))}}.</strong> <br /><br />
	
	SEGUNDA - OBLIGACIONES DE LAS PARTES: EL CONTRATANTE se compromete a:  Proporcionar a EL CONTRATISTA toda la información básica para la ejecución del trabajo, que será disponible, fiable, correcta, actualizada y completa. <br /><br />

	TERCERA - LISTA DE ENTREGBLES: a continuación se definira la lista de entregables, esta debe ser definida por mutuo acuerdo en una videoconferencia de 30 minutos con el creativo previamente seleccionado y el  cliente. <br />

</p>
 	@include('omissions/partials/_fields', array('ids' => $fields))	
<p>

	
	CUARTA - CESIÓN DE LA(S) OBRA(S) OBJETO DEL CONTRATO: EL CONTRATISTA cede en exclusiva EL CONTRATANTE, la obra denominada: <strong>{{strtoupper(get_the_title( icl_object_id( $product->id, 'product', false, $global_lang ) ))}}</strong> que mediante este contrato se le encargaron y por ello, EL CONTRATISTA no podrá ceder a terceros ninguno de los derechos ni obligaciones establecidas en el presente contrato, salvo autorización expresa y por escrito de EL CONTRATANTE. La cesión de estos derechos se extiende a nivel mundial y durará hasta que la citada obra pase al dominio público. <br /> <br />
	
	QUINTA - DURACIÓN Y ENTREGA DE LA(S) OBRA(S): La entrega de la(s) obra(s) creadas en desarrollo del presente contrato, se llevará a cabo dentro de un plazo de (7) días habiles contados a partir de aceptacion de los terminos de este mismo acuerdo. <br /> <br />
	
	SEXTA - CALIDAD DE LA OBRA: El contratante se compromete a realizar una obra de calidad.<br /> <br />
	
	SEPTIMA - PROPIEDAD INTELECTUAL: Como consecuencia de la cesión de los derechos patrimoniales de EL CONTRATISTA a EL CONTRATANTE, la titularidad de dichos derechos patrimoniales de la obra contratada le corresponde en exclusividad al CONTRATANTE.<br /> <br />
	
	OCTAVA - CONFIDENCIALIDAD: Las partes se comprometen, con carácter mutuo y recíproco, a tratar como “confidencial” toda la información técnica, comercial o de cualquier otra naturaleza comprendida y/o que se derive directa o indirectamente de las indicaciones que la contraparte le haya facilitado para el desarrollo del objeto del presente contrato (en adelante “la información confidencial”). En consecuencia ninguna parte podrá revelar total o parcialmente, de palabra, por escrito o de cualquier otra forma, a ninguna persona física o jurídica, ya sea de carácter público o privado, la Información Confidencial, sin el consentimiento expreso y por escrito de la contraparte. La misma confidencialidad que se imponen a las partes o a terceros que intervengan en la ejecución, deberá ser impuesta por cada una de las partes a sus trabajadores (por cuenta propia o ajena, con relación laboral o mercantil) que de modo directo o indirecto están relacionados con el objeto del contrato. Será obligación de las mismas partes hacer firmar a sus trabajadores un documento vinculante por el que adquieren tal obligación. Este compromiso de confidencialidad, tanto entre las partes como de éstas con sus trabajadores y contratistas, permanecerá durante la vigencia del presente contrato así como un año después de la finalización del mismo.
	<br /> <br />
	 NOVENA - JURISDICCIÓN: Cualquier disputa o 
	diferencia que surja con ocasión del objeto y obligaciones de este contrato serán arregladas en lo posible mediante la negociación y la conciliación entre las partes, para lo cual Website Bar nombrara a una tercera persona encargada de hacer una auditoria para establecer el monto del dinero que deba devolverse en caso de que exista un incumplimiento por parte DEL CONTRATANTE se estipulara el valor de devolución del dinero que en ningun caso podrá ser mayor al 100% del valor pagado por el CONTRATANTE.

<br /> <br />

	@include('omissions/partials/_fields', array('ids' => $approvals))
	
</p>