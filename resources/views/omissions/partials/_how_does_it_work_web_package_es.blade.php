
<div class="row">
	<div class="col-md-12">
		
		<h3>¿CÓMO FUNCIONA EL PROCESO?</h3>

	
			<p>El proceso completo puede demorar entre 2 y 4 semanas, dependiendo de cuánto tiempo te tome completar las rondas y aprobaciones de revisión:</p>

			<ol>
				<li>Después de que la compra se haya realizado, te enviaremos un link donde donde podrás respondernos una serie de preguntas que nos ayudarán a conocer más sobre tu marca. </li>
				<li>Una vez que obtengamos tus respuestas, realizaremos una investigación y crearemos un concepto de sitio web en un tiempo máximo de 3 días, , este será enviado a tu correo electrónico con un link para aprobación. (Este concepto solo es de diseño).</li>
				<li>Una vez aprobado el concepto de diseño de sitio Web, te enviaremos un link vía correo electrónico donde podrás darnos toda la información necesaria correspondientes a las secciones de tu sitio web.</li>
				<li>Una vez que obtengamos toda la información necesaria, enviaremos el concepto con el copy marketing propuesto por el experto a tu correo electrónico con un link para aprobación en un tiempo máximo de 7 días.</li>
				<li>En caso de que ya cuentes con un dominio configuraremos todo lo necesario para que tu nuevo sitio web apunte a este.</li>

			</ol> 

	</div>
</div>