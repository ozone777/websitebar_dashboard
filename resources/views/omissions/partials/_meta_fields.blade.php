@if($row[0]->type == "iframe")

	<?php
		$meta = $omissionmeta[$row[0]->meta_key];  
		$field = $row[0];
	?>

	@if(($role == "administrator") || ($role == "Contributor") || ($role == "editor"))

   	 	<div class="row">
  			<div class="col-md-6">
				<label>{{trans('addons.enter_the_url_of_the_website')}}</label>
				<input type="text" name="{{$field->meta_key}}" omission_id="{{$wp_omission->id}}" meta_key="{{$field->meta_key}}" value="{{$meta->meta_value}}" class="form-control omissions" placeholder="">
			<br />
			</div>
		</div>
	
	@endif
	
 	<div class="row">
		<div class="col-md-12">
			
			@if(isset($meta->meta_value))
				<p><a target="_blank" href="{{$meta->meta_value}}">{{trans('addons.go_to_website')}} {{$meta->meta_value}}</a></p> <br />
				<iframe src="{{$meta->meta_value}}" width="100%" height="800"></iframe>
			@endif
	
		</div>
	</div>
	

@elseif($row[0]->type == "chat")

@include('dashboard/_chat',array('object_id' => $index, "object" => "Wp_omissions_field", "user_id" => $current_user->ID, "icon" => $current_user->profile_icon, "chat_text" => $omissionmeta[$index], "div_class" => "$index", "tooltip" => trans('omissions/partials.chat_with_your_web_team'), "dbfield" => $index, "omission_id" =>$wp_omission->id, "row_object" => $row[0],'lang' => $lang))

@elseif($row[0]->type == "file")

	@include('omissions/partials/_upload_form', array ('object' => $row[0], 'array' => $files,'updated_view' => $current_view, 'permissions' => $row[0]->permissions, 'lang' => $global_lang))

@elseif($row[0]->type == "one_file")

    <div class="row">
  		<div class="col-md-12">
			
			<?php
				$meta = $omissionmeta[$row[0]->meta_key];
				if($meta->meta_value)
				  $file_route = "/solo_files/$wp_omission->id/$meta->meta_value";
			?>
			
		@if($file_route)
			
	<iframe style="width:100%; max-width: 100%; margin-bottom:10px; height: 500px; border-radius: 5px; border: 1px solid #d9d9d9;" src="/ViewerJS/#../{{$file_route}}" seamless="" allowfullscreen="allowfullscreen"></iframe>
		
		@endif
	
		</div>
	</div>

	
	@if($row[0]->permissions == "admin")

		@if(($role == "administrator") || ($role == "Contributor") || ($role == "editor"))
		
		<div class="row">
			<div class="col-md-6">

				<form action="/save_omission_file" method="POST" enctype="multipart/form-data">
		
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="omission_id" value="{{$wp_omission->id}}">
					<input type="hidden" name="meta_key" value="{{$row[0]->meta_key}}">
					<input type="file" id="filem" name="filem">
					@if($global_lang =="es")
					<button type="submit" class="form-control button-next btn btn-info">{{$row[0]->help_text_es}}</button>
					@elseif($global_lang =="en")
					<button type="submit" class="form-control button-next btn btn-info">{{$row[0]->help_text}}</button>
					@endif
				</form>
			</div>
		</div>
		
		@endif
	
	@elseif($row[0]->permissions == "all")
	
	    <div class="row">
	  		<div class="col-md-6">

	    		<form action="/save_omission_file" method="POST" enctype="multipart/form-data">
		
	        		<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="omission_id" value="{{$wp_omission->id}}">
					<input type="hidden" name="meta_key" value="{{$row[0]->meta_key}}">
	        		<input type="file" id="filem" name="filem">
					@if($global_lang =="es")
	        			<button type="submit" class="form-control button-next btn btn-info">{{$row[0]->help_text_es}}</button>
	    			@elseif($global_lang =="en")
						<button type="submit" class="form-control button-next btn btn-info">{{$row[0]->help_text}}</button>
					@endif
				</form>
	
			</div>
		</div>
	
	@endif
		
@else
<?php


//Logica de como funciona:
//Se declara un Array con los formatos de elementos a insertar (estos formatos vienen con variables especiales que pueden ser reemplazadas)

$code_array["textarea"] = '<label>help_text_var</label><textarea rows="7" cols="50" name="meta_key_var" omission_id="omission_id_var" meta_key="meta_key_var"  class = "class_var" placeholder= "placeholder_var">meta_value_var</textarea>';

$code_array["text"] = '<label>help_text_var</label><input type="text" name="meta_key_var" omission_id="omission_id_var" meta_key="meta_key_var" value="meta_value_var" class = "class_var" placeholder= "placeholder_var">';

$code_array["radio"] = 'placeholder_var <input type="radio" name="meta_key_var" omission_id="omission_id_var" meta_key="meta_key_var" value="meta_value_var" class ="omissions_radio" next_state="next_state_var" next_state_logic="next_state_logic_var" next_state_logic_result="next_state_logic_result_var" check_var>';

$code_array["check"] = '<input type="checkbox" name="meta_key_var" omission_id="omission_id_var" meta_key="meta_key_var" value="meta_value_var" class="omissions_check" next_state="next_state_var" next_state_logic="next_state_logic_var" next_state_logic_result="next_state_logic_result_var" check_var> placeholder_var';

$code_array["approval"] = 'placeholder_var <input type="radio" user_login="user_login_var" field_id="field_id_var" name="meta_key_var" approval_time="approval_time_var" omission_id="omission_id_var" next_state="next_state_var" next_state_logic="next_state_logic_var" meta_key="meta_key_var" value="meta_value_var" class ="omissions_approval radio_approval approvals_meta_key_var" next_state_logic_result="next_state_logic_result_var" check_var><div id="rubber_field_id_var"></div>';

$code_array["select"] = '<select name="meta_key_var" omission_id="omission_id_var" meta_key="meta_key_var" next_state="next_state_var" next_state_logic="next_state_logic_var" next_state_logic_result="next_state_logic_result_var" class ="form-control omissions_select"><option value="">placeholder_var</option>select_options_var</select>';

$code_array["label"] = '<label>placeholder_var</label>';
	
$topline = '<div class="row">';
$bottonline = '</div>';	   


	$html = $topline;
	$fields = "";
	
	if(isset($row)){
	
	foreach ($row as $field) {
	  
	  $boostrap_column = 12/count($row);
	  $type = $field->type;
	  $code = $code_array[$type];
	  
	  //Replace operations check and radio are special cases 
	  if($global_lang == "es"){
	  	$placeholder = $field->placeholder_es;
		$help_text = $field->help_text_es;
	  }else if($global_lang == "en"){
	  	$placeholder = $field->placeholder;
		$help_text = $field->help_text;
	  }
	  
	  $meta_value = $omissionmeta[$field->meta_key]->meta_value;
	  
	  if($field->type == "radio"){
	  	  $code = str_replace("meta_value_var", $field->set_value, $code);
		  if(isset($omissionmeta[$field->meta_key])){
		    if($field->set_value == $meta_value){
		  	  $code = str_replace("check_var","checked= 'checked'", $code);
		    }else{
		  	  $code = str_replace("check_var","", $code);
		    }
	      }
		  
  		$next_state_var = $field->next_state;
  		$next_state_logic_var = $field->next_state_logic;
		$next_state_logic_result_var = $field->next_state_logic_result;
  		
  		if(isset($next_state_var)){
  			$code = str_replace("next_state_var",$next_state_var, $code);
  		}
  		if(isset($next_state_logic_var)){
  			$code = str_replace("next_state_logic_var",$next_state_logic_var, $code);
  		}
  		if(isset($next_state_logic_result_var)){
  			$code = str_replace("next_state_logic_result_var",$next_state_logic_result_var, $code);
  		}
		  
	  }if($field->type == "approval"){
		  
	  	  $code = str_replace("meta_value_var", $field->set_value, $code);
		  $code = str_replace("field_id_var", $field->id, $code);
		  
		  if(isset($omissionmeta[$field->meta_key])){
			  
		    if($field->set_value == $meta_value){
		  	  $code = str_replace("check_var","checked= 'checked'", $code);
		    }else{
		  	  $code = str_replace("check_var","", $code);
		    }
			
			$meta_created_at_value = $omissionmeta[$field->meta_key]->created_at;
			$meta_user_login= $omissionmeta[$field->meta_key]->user_login;
			$code = str_replace("approval_time_var", "$meta_created_at_value "."UTC", $code);
			$code = str_replace("user_login_var",$meta_user_login, $code);
	      }
		  
		$next_state_var = $field->next_state;
		$next_state_logic_var = $field->next_state_logic;
		$next_state_logic_result_var = $field->next_state_logic_result;
		
		if(isset($next_state_var)){
			$code = str_replace("next_state_var",$next_state_var, $code);
		}
		if(isset($next_state_logic_var)){
			$code = str_replace("next_state_logic_var",$next_state_logic_var, $code);
		}
  		if(isset($next_state_logic_result_var)){
  			$code = str_replace("next_state_logic_result_var",$next_state_logic_result_var, $code);
  		}
		  
	  }else if($field->type == "check"){
		  $code = str_replace("meta_value_var","1", $code);
		  
		  if(isset($omissionmeta[$field->meta_key])){
		    if($meta_value == "1"){
			  $code = str_replace("check_var","checked= 'checked'", $code);
		    }else{
		  	  $code = str_replace("check_var","", $code);
		    }
	      }else{
	      	$code = str_replace("check_var","", $code);
	      }
		  
  		$next_state_var = $field->next_state;
  		$next_state_logic_var = $field->next_state_logic;
  		$next_state_logic_result_var = $field->next_state_logic_result;
		
  		if(isset($next_state_var)){
  			$code = str_replace("next_state_var",$next_state_var, $code);
  		}
  		if(isset($next_state_logic_var)){
  			$code = str_replace("next_state_logic_var",$next_state_logic_var, $code);
  		}
  		if(isset($next_state_logic_result_var)){
  			$code = str_replace("next_state_logic_result_var",$next_state_logic_result_var, $code);
  		}
		
	  }else if($field->type == "select"){
	  
		  if(!isset($select_options_array)){
			  $select_options ="";
			  $select_options_array = explode(",",$field->set_value);
			  foreach($select_options_array as $option){
				  if($option == $meta_value){
				  	$select_options.= "<option value='$option' selected>$option</option>";
				  }else{
				  	$select_options.= "<option value='$option'>$option</option>";
				  }
			  }
		  }else{
			  foreach($select_options_array as $option){
				  if($option->domain_zone == $meta_value){
				  	$select_options.= "<option value='$option->domain_zone' selected>$option->domain_zone</option>";
				  }else{
				  	$select_options.= "<option value='$option->domain_zone'>$option->domain_zone</option>";
				  }
			  }
		  }
			
    		$next_state_var = $field->next_state;
    		$next_state_logic_var = $field->next_state_logic;
    		$next_state_logic_result_var = $field->next_state_logic_result;
			
    		if(isset($next_state_var)){
    			$code = str_replace("next_state_var",$next_state_var, $code);
    		}
    		if(isset($next_state_logic_var)){
    			$code = str_replace("next_state_logic_var",$next_state_logic_var, $code);
    		}
    		if(isset($next_state_logic_result_var)){
    			$code = str_replace("next_state_logic_result_var",$next_state_logic_result_var, $code);
    		}
    		if(isset($select_options)){
    			$code = str_replace("select_options_var",$select_options, $code);
    		}
	  
	  }else{
		  if(isset($omissionmeta[$field->meta_key])){
		  	$code = str_replace("meta_value_var", $meta_value, $code);
		  }else{
		  	$code = str_replace("meta_value_var", "", $code);
		  }
	  }
	  
	  $code = str_replace("help_text_var", $help_text, $code);
	  $code = str_replace("meta_key_var", $field->meta_key, $code);
	  $code = str_replace("omission_id_var", $omission_id, $code);
	  $code = str_replace("placeholder_var", $placeholder, $code);
	  $code = str_replace("class_var", $class, $code);
	  
	  $code = "<div class='col-md-$boostrap_column'>" . $code . '</div>';
	  
	  $fields = $fields . $code;
    }
	
	$html = $topline . $fields .$bottonline ;
    print_r($html);
    print_r("<br />");
	
	}
	
?>
@endif


