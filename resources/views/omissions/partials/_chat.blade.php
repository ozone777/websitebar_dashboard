<?php

if($lang){
	
	if($lang == "es"){
		$help_text = $row_object->help_text_es;
	}else if($lang == "en"){
		$help_text = $row_object->help_text;
	}
	
}

?>

<div class="row">
		<div class="col-md-12">

<label>{{$help_text}}</label>

<div id="chat_frame_{{$object_id}}" class="frame" style="width:100%;overflow:scroll; height:300px;  padding-bottom: 20px;  padding-top: 25px">

	<ul class="{{$div_class}}_{{$object_id}}"></ul>
	
</div>

</div>
</div>


<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">

@if($current_user->profile_icon)


<input class="form-control mytext" id="chat_{{$object_id}}" icon="{{$icon}}" user_id="{{$user_id}}" div_class="{{$div_class}}" object="{{$object}}" object_id="{{$object_id}}" dbfield="{{$dbfield}}" omission_id="{{$omission_id}}" placeholder="{{trans('dashboard.type_message')}}"/>

@else

<input class="form-control mytext" id="chat_{{$object_id}}" icon="/chat-icon.jpg" user_id="{{$user_id}}" div_class="{{$div_class}}" object="{{$object}}" object_id="{{$object_id}}" dbfield="{{$dbfield}}" omission_id="{{$omission_id}}" placeholder="{{trans('dashboard.type_message')}}"/>

@endif

	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12">
		
@if($current_user->profile_icon)

	<button class="form-control button-next btn btn-info chat_button" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;" icon="{{$icon}}" user_id="{{$user_id}}" div_class="{{$div_class}}" object="{{$object}}" object_id="{{$object_id}}" dbfield="{{$dbfield}}" omission_id="{{$omission_id}}" placeholder="{{trans('dashboard.type_message')}}" type="button">{{trans('dashboard.send')}}</button>

@else

	<button class="form-control button-next btn btn-info chat_button" icon="/chat-icon.jpg" user_id="{{$user_id}}" div_class="{{$div_class}}" object="{{$object}}" object_id="{{$object_id}}" dbfield="{{$dbfield}}" omission_id="{{$omission_id}}" style="padding: 0px 0px 0px 0px; margin: 0px 0px 0px 0px;"  type="button">{{trans('dashboard.send')}}</button>

@endif

</div>

</div>

						
						
<script>
	$(document).ready(function(){
		
		
								
		var string = "{!!$chat_text!!}";
		var array = string.split("|");
		/*
		console.log("object_id: {{$object_id}}")
		console.log("icon: {{$icon}}")
		console.log("user_id: {{$user_id}}")
		console.log("object: {{$object}}")
		console.log("chattext: {{$object}}")
		console.log(array);
		*/
							
		for (index = 0; index < array.length; ++index) {
								
			data = array[index].split(">");
			
			if(data[1]){
				
				if("{{$user_id}}" == data[0]){
					if(data[4]){
					if(data[1]!="/chat-icon.jpg"){
						iconaux = "/profile_pics/"+data[0]+"/"+data[1]
					}else{
						iconaux = data[1];
					}
					insertChat("{{$div_class}}_{{$object_id}}",iconaux,data[2],"you", data[4],data[5],0); 
				}
			}else{
				if(data[4]){
					if(data[1]!="/chat-icon.jpg"){
						iconaux = "/profile_pics/"+data[0]+"/"+data[1]
					}else{
						iconaux = data[1];
					}
					insertChat("{{$div_class}}_{{$object_id}}",iconaux,data[2],"me",data[4],data[5],0);
				}
			}
			
			}
								
		}
							
	});
</script>
