
<div class="row">
	<div class="col-md-12">
		
		<h3>HOW DOES THE PROCESS WORK?</h3>

	
			<p>The entire process may take between 2 and 4 weeks, depending on how long it takes you to complete the rounds and review approvals:</p>

			<ol>
				<li>After the purchase has been made, we will send you a link where you can answer a series of questions that will help us learn more about your brand.</li>
				<li>Once we get your answers, we will conduct research and create a website concept in a maximum time of 3 business days. We will then send the design concept to your email with a link for approval. (This concept is a prototype with placeholder text).</li>
				<li>Once you’ve approved the design concept, we’ll send you a link via email where you can give us all the necessary information for us to create the text that’ll fill different sections of your website.</li>
				<li>Once we obtain all the information, we will send a copy proposal to your email with a link for approval in a maximum time of 7 days.</li>
				<li>In case you already have a domain we will configure everything necessary for your new website to point to it.
</li>

			</ol> 

	</div>
</div>