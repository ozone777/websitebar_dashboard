@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')

	<div class="row">
		<div class="col-md-12">
			<h1>{{get_the_title( icl_object_id( $product->id, 'product', false, $global_lang ) )}}</h1>
			
		</div>
	</div>
	
	@include('omissions/partials/_todo_calendar')
	
	@include('omissions/partials/_header_states')
	
	 <div class="demo">
	   <div id="titleClick-demo">
		   

       	<fieldset title="{{trans('addons.send_the_brand_files')}}">
       		<legend></legend>
		
			@include('omissions/partials/_fields', array('ids' => [1]))
       		
			@include('omissions/partials/_submit_mission',array('button_title' => trans('addons.send_files'), "label" => "social_media_kit_waiting_quiz", "omission_state_id" => $selected_state->id))
		
       	</fieldset>
		   
    	<fieldset title="{{trans('addons.answer_quiz')}}">
    		<legend></legend>
		
    		@include('omissions/partials/_powering_content_test',array('product_id' => $product->id, "next_state" => "social_media_kit_creating_styles"))
		
    	</fieldset>
		
		
    	<fieldset title="{{trans('addons.quiz_results')}}">
    		<legend></legend>
		
    		@include('omissions/partials/_social_visual_styles')
		
    	</fieldset>
	
	
   	<fieldset title="{{trans('addons.download_visual_styles')}}">
   		<legend></legend>

   			@include('omissions/partials/_visual_styles_for_designers')
			
			@include('omissions/partials/_fields', array('ids' => [2]))

   	</fieldset>
	   
		 <input type="submit" value="Finish!" />
	 </div>
	 </div>

	<script>
 
	
	 
     $(document).on('ready', function() {

      $('#titleClick-demo').stepy({ titleClick: true });
	  
	  $(".stepy-navigator").remove();
	  
     });
	 
 
	 </script>
	 
	 @include('omissions/partials/_tab_selector')
	 @include('omissions/partials/_brief_js_logic')
			
@endsection




