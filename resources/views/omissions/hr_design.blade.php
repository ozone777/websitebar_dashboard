@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')

	<div class="row">
		<div class="col-md-12">
			<h1>{{get_the_title( icl_object_id( $product->id, 'product', false, $global_lang ) )}}</h1>
			
		</div>
	</div>
	
	@include('omissions/partials/_todo_calendar')
	
	@include('omissions/partials/_header_states')


	 <div class="demo">
	   <div id="titleClick-demo">
	   
 	   
    	<fieldset title="{{trans('addons.select_designer')}}">
    		<legend></legend>
			
			@include('omissions/partials/_fields', array('ids' => [1,2]))
			
    		@include('omissions/partials/_select_wp_user',array("role" => "editor", "next_state" => "hr_design_waiting_agreement"))
		
	    </tbody>
	  </table>
		
    	</fieldset>
	
	
   	<fieldset title="{{trans('addons.fill_out_agreement')}}">
   		<legend></legend>

			@if($global_lang == "es")
   				@include('omissions/partials/_agreement_es', array('fields' => [3,4], 'approvals' => [5]))
			@elseif($global_lang == "en")
				@include('omissions/partials/_agreement_en', array('fields' => [3,4], 'approvals' => [5]))
			@endif

   	</fieldset>
	
	
   	<fieldset title="{{trans('addons.approve_job')}}">
   		<legend></legend>

   			@include('omissions/partials/_fields', array('ids' => [6]))

   	</fieldset>
	
	
	<fieldset title="{{trans('addons.how_does_it_work')}}">
	   <legend></legend>
      
		   @include("omissions/partials/_how_does_it_work_hr_designer_$global_lang")
	  
	 </fieldset>
	
	

	 
		 <input type="submit" value="Finish!" />
	 </div>
	 </div>

	<script>
 
	
	 
     $(document).on('ready', function() {

      $('#titleClick-demo').stepy({ titleClick: true });
	  
	  $(".stepy-navigator").remove();
	  
     });
	 

	 </script>
	 
	 @include('omissions/partials/_tab_selector')
	 @include('omissions/partials/_brief_js_logic')
	 
			
@endsection




