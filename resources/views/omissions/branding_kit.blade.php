@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')

	<div class="row">
		<div class="col-md-12">
			<h1>{{get_the_title( icl_object_id( $product->id, 'product', false, $global_lang ) )}}</h1>
			
		</div>
	</div>
	
	@include('omissions/partials/_todo_calendar')
	
	@include('omissions/partials/_header_states')

	 <div class="omissions_tabs">
		 
	   <div id="titleClick-demo">
	   
	  <fieldset title="{{trans('addons.fill_out_brief')}}">
	       <legend></legend>
      
 		
 		@include('omissions/partials/_fields', array('ids' => [1,2,3,4,5,6,7,8,9,10,11,12,13]))
 		
		@include('omissions/partials/_submit_mission',array('button_title' => trans('addons.send_brief'), "label" => "branding_kit_creating_concepts", "omission_state_id" => $selected_state->id))
	  	 
	  
	     </fieldset>
		 
		 
  	<fieldset title="{{trans('addons.approves_the_concept')}}">
       <legend></legend>
	   
	   @include('omissions/partials/_fields', array('ids' => [14,15,16,17]))
  
     </fieldset>
	 
	 
   	<fieldset title="{{trans('addons.download_files')}}">
        <legend></legend>
    
		@include('omissions/partials/_fields', array('ids' => [18]))
  	 
  
      </fieldset>
	  
  	<fieldset title="{{trans('addons.how_does_it_work')}}">
  	   <legend></legend>
      
		   @include("omissions/partials/_how_does_it_work_branding_kit_$global_lang")
	  
  	 </fieldset>
	
  

	 
		 <input type="submit" value="Finish!" />
	 </div>
	 </div>

	<script>
 
	
	 
     $(document).on('ready', function() {

      $('#titleClick-demo').stepy({ titleClick: true });
	  
	  $(".stepy-navigator").remove();
	  
     });
	 
	
	 </script>
	 
	 @include('omissions/partials/_tab_selector')
	 @include('omissions/partials/_brief_js_logic')
			
@endsection




