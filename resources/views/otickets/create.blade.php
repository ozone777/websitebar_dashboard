@extends('layout')

@section('header')

<style>

	.list th {
	    background-color: #cecece;
	    text-align: left;
	    color: black;
	    padding: 0.4em 0.3em 0.4em 0.4em;
	    font-weight: 700;
	    font-size: 14px;
	}
	
	.list {
	    width: 100%;
	    border-spacing: 5px;
	    border-collapse: collapse;
	    color: #333;
	    /* border: 1px solid #ccc; */
	    font-size: 14px;
	}
	
	input {
	    -webkit-appearance: textfield;
	    background-color: white;
	    -webkit-rtl-ordering: logical;
	    cursor: text;
	    padding: 1px;
	    border-width: 2px;
	    border-style: inset;
	    border-color: initial;
	    border-image: initial;
	}

</style>  
@endsection

@section('content')

 <form action="{{ route('otickets.store') }}" method="POST" enctype="multipart/form-data">
     <input type="hidden" name="_token" value="{{ csrf_token() }}">
	 

<table class="list">
	<tbody><tr>
		<th colspan="2">Open a Ticket</th>
	</tr>

	<tr class="list_entry">
		
		<td class="table_form_header">Summary</td>
		<td><br /> <input name="summary" id="summary" class="form-control" type="text" maxlength="64" size="40"> <br /></td>
	</tr>

	
		<tr class="list_entry">
			<td class="table_form_header">Regarding</td>
			<td>
				<select name="regarding" class="form-control">
					<option value="">Please make a selection</option>
					
					 @foreach($sites as $site)
					
						<option value="{{$site->domain}}">Pete: {{$site->domain}} </option>
						
					@endforeach
					
					<option value="Other">Other (general, billing, etc)</option>
					
					
				</select><br />
			</td>
		</tr>
	

	<tr class="list_entry">
		<td class="table_form_header">Description</td>
		<td>
			<textarea cols="100" class="form-control" rows="18" class="ticket_textarea" name="body" wrap="soft" maxlength="50000"></textarea><br />
		</td>
	</tr>
	<tr class="list_entry">
		<td></td>
		<td colspan="3">
			<input class="et_manage_submit" type="submit" value="Open Ticket">
		</td>
	</tr>
</tbody></table>

</form>

@endsection

