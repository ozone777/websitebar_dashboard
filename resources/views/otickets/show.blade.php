@extends('layout')

@section('header')

<style>

	.list th {
	    background-color: #cecece;
	    text-align: left;
	    color: black;
	    padding: 0.4em 0.3em 0.4em 0.4em;
	    font-weight: 700;
	    font-size: 14px;
	}
	
	.package-table tbody tr:nth-child(odd){background:#FAFAFA;}
	.package-table td{border-top:0px !important; border-right:1px solid #fff !important;}
	.package-table tbody tr:nth-child(even){background:#DFDFDF; }

</style>  
@endsection

@section('content')

<div class="row">
            <div class="col-md-12">
			
			@if(($role== "administrator") || ($role == "contributor"))
			
			<select id ="oticket_sw">
			  
			  <option @if($oticket->status == "OPEN") selected="selected" @endif value="OPEN">OPEN</option>
			  <option @if($oticket->status == "CLOSED") selected="selected" @endif value="CLOSED">CLOSED</option>
			  
			</select>
			
			<br />
			
			@endif
	
<table class="table package-table">
	<tbody><tr>
		<th colspan="5">{{$oticket->id}} {{$oticket->summary}}</th>
	</tr>

	<tr class="list_head">
		<td>Status</td>
		<td nowrap="">Opened</td>
		<td nowrap="">Last Updated</td>
		<td nowrap="">Closed On</td>
		<td nowrap="">Regarding</td>
	</tr>

	<tr>
		<td>{{$oticket->status}}</td>
		<td>
			
			{{$oticket->created_at}}
			by <strong>{{$oticket->opened_by_login}}</strong>
		</td>
		<td>
			
			{{$oticket->updated_at}}
			by <strong>{{$oticket->updated_by_login}}</strong>
		</td>
		<td>
			@if($oticket->closed_at)
			{{$oticket->closed_at}}
			by <strong>{{$oticket->closed_at_login}}</strong>
			@endif
		</td>
		<td>
			
				{{$oticket->regarding}}<br>
			
		</td>
	</tr>
	
</tbody></table>
   
   
   <table class="table package-table">
   	<tbody><tr class="list_head">
   		<td colspan="5">&nbsp;</td>
   	</tr>

	

   		<tr class="list_entry">
   			<td class="support_left" valign="top">
   				
   				<strong>{{$oticket->opened_by_login}}</strong>
   				<br>
   				{{$oticket->created_at}}
   			</td>
   			<td colspan="4" class="ticketDescription" style=" ">{{$oticket->body}}</td>
   		</tr>
		
		 @foreach($oanswers as $oanswer)
	
   		<tr class="list_entry_alt">
   			<td class="col-md-3" valign="top">
   				
   				<strong>{{$oanswer->user_login}}</strong>
			
   				<br>
				
   				{{$oanswer->created_at}}
   			</td>
   			<td colspan="4" class="col-md-9">
   				{{$oanswer->body}}
				
				<br />
				@if($oanswer->file)
					
					<a href="/dashboard_tickets/{{$oticket->id}}/{{$oanswer->file}}">{{$oanswer->file}}</a>
					
				@endif
   			</td>
   		</tr>
		
		@endforeach
	
   		<tr class="list_entry">
   			<td class="col-md-3">
   				Add an Update<br>
   				<span class="hint">Update text required.
   			</span></td>
   			<td class="col-md-3">
				<form action="/store_answer" method="post"  enctype="multipart/form-data">
					
				<input type="hidden" id="_token" name="_token" value="{{csrf_token()}}">
				<input type="hidden" id="oticket_id" name="oticket_id" value="{{$oticket->id}}">
   				<textarea cols="65" rows="12" name="body" maxlength="50000"></textarea>
				
   				<br />
				
	            <div class="form-group">
	              <label>File</label>
                  <input type="file" id="filem" name="filem">
	            </div>
				
				<br />
				
   				<input class="et_manage_submit" type="submit" value="Add Update">
			
				</form>
				
   			</td>
   		</tr>

   </tbody></table>
   
							   
</div>
</div>   

<script>
	
	$( document ).ready(function() {
	  
		$( "#oticket_sw" ).change(function() {
		  
			jQuery.ajax({
				url: "/update_status",
				type: 'post',
				dataType: 'json',
				data: {oticket_id : {{$oticket->id}}, value: $(this).val(), _token: "{{csrf_token()}}"},
				success: function(data){
					
	  			 location.reload();
					
				}
			});
		  
		});
	  
	});
	
</script>


   
@endsection



