@extends('layout')

@section('header')

<style>

	.list th {
	    background-color: #cecece;
	    text-align: left;
	    color: black;
	    padding: 0.4em 0.3em 0.4em 0.4em;
	    font-weight: 700;
	    font-size: 14px;
	}

</style>  
@endsection

@section('content')

	<div class="row">
		<div class="col-md-12">
			<h1>{{trans('dashboard.need_help')}}</h1>
		</div>
	</div>
			
<div class="row">
            <div class="col-md-12">

			
			<table class="table table-responsive list">
				<tbody><tr>
					<th colspan="6">Open Tickets</th>
				</tr>

				<tr class="list_head">
					<td>TicketID</td>
					<td>Summary</td>
					<td>Regarding</td>
					<td>Status</td>
					<td nowrap="" align="right">Opened</td>
					<td nowrap="" align="right">Last Updated</td>
				</tr>

	
				  @foreach($open_tickets as $open_ticket)
		

					<tr class="list_entry" style="">
						<td><a href="/otickets/{{$open_ticket->id}}">{{$open_ticket->id}}</a></td>
						<td><a href="/otickets/{{$open_ticket->id}}">{{$open_ticket->summary}}</a></td>
						<td>
				
								{{$open_ticket->regarding}}<br>
				
						</td>
						<td>{{$open_ticket->status}}</td>
						<td nowrap="" align="right">
							{{$open_ticket->created_at}} <br>

							<span class="hint">
								
								<strong>{{$open_ticket->opened_by_login}}</strong>
							</span>
						</td>
						<td nowrap="" align="right">
							{{$open_ticket->updated_at}}<br>
							<span class="hint">
								
								<strong>{{$open_ticket->updated_by_login}}</strong>
							</span>
						</td>
					</tr>
	
				@endforeach

				<tr class="list_entry" style="border-bottom-width: 0px">
					<td colspan="6" align="right"><a href="/otickets/create">Open a new Support Ticket</a></td>
				</tr>



				<tr colspan="6">
					<td>&nbsp;</td>
				</tr>



				<tr>
					<th colspan="6">Recently Closed Tickets</th>
				</tr>

				<tr class="list_head">
					<td>TicketID</td>
					<td>Summary</td>
					<td>Regarding</td>
					<td>Status</td>
					<td nowrap="" align="right">Opened</td>
					<td nowrap="" align="right">Last Updated</td>
				</tr>

	
		

					  @foreach($closed_tickets as $closed_ticket)
		

						<tr class="list_entry" style="">
							<td><a href="/otickets/{{$closed_ticket->id}}">{{$closed_ticket->id}}</a></td>
							<td><a href="/otickets/{{$closed_ticket->id}}">{{$closed_ticket->summary}}</a></td>
							<td>
				
									{{$closed_ticket->regarding}}<br>
				
							</td>
							<td>{{$closed_ticket->status}}</td>
							<td nowrap="" align="right">
								{{$closed_ticket->created_at}} <br>

								<span class="hint">
									 <strong>{{$closed_ticket->opened_by_login}}</strong>
								</span>
							</td>
							<td nowrap="" align="right">
								{{$closed_ticket->updated_at}}<br>
								
								<span class="hint">
								
									<strong>{{$closed_ticket->updated_by_login}}</strong>
								</span>
								
							</td>
						</tr>
	
					@endforeach
	

				<tr class="list_entry">
					<td colspan="6" align="right"><a href="/all_tickets">Show all tickets</a></td>
				</tr>

			</tbody></table>
			
				
		   </div>
</div>
   
@endsection

