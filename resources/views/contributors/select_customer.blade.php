@extends('layout')

@section('header')
  
@endsection

@section('content')


		<?php
		$args = array(
		);

		// The Query
		$user_query = new WP_User_Query( array( 'role' => 'Subscriber' ) );
		?>

    <div class="row">
        <div class="col-md-12">
			
		  <h2>Select Subscriber</h2>
	   
		  <table class="table table-bordered">
		    <thead>
		      <tr>
		        <th>Email</th>
				<th>Create site</th>
		        <th>Sites</th>
		        <th>Addons</th>
				<th>Servers</th>
				<th>Domains</th>
		      </tr>
		    </thead>
		    <tbody>
				
				@foreach ( $user_query->results as $user ) 
				
		      	  <tr>
		        	  <td>{{$user->user_email}}</td>
					  <td><a href="/create_site?user_id={{$user->ID}}">Create Site</a></td>
		        	  <td><a href="/my_sites?user_id={{$user->ID}}">My Sites</a></td>
		        	  <td><a href="/my_addons?user_id={{$user->ID}}">My Addons</a></td>
		        	  <td><a href="/my_servers?user_id={{$user->ID}}">My Servers</a></td>
		        	  <td><a href="/bardomains?user_id={{$user->ID}}">My Domains</a></td>
		      	  </tr>
				  
				@endforeach
		      
			  
		    </tbody>
		  </table>
 

        </div>
    </div>
	
	<br />
	
		<?php
		$args = array(
		);

		// The Query
		$user_query = new WP_User_Query( array( 'role' => 'Customer' ) );
		?>

    <div class="row">
        <div class="col-md-12">
			
		  <h2>Select Customer</h2>
	   
		  <table class="table table-bordered">
		    <thead>
		      <tr>
		        <th>Email</th>
				<th>Create site</th>
		        <th>Sites</th>
		        <th>Addons</th>
				<th>Servers</th>
				<th>Domains</th>
		      </tr>
		    </thead>
		    <tbody>
				
				@foreach ( $user_query->results as $user ) 
				
		      	  <tr>
		        	  <td>{{$user->user_email}}</td>
					  <td><a href="/create_site?user_id={{$user->ID}}">Create Site</a></td>
		        	  <td><a href="/my_sites?user_id={{$user->ID}}">My Sites</a></td>
		        	  <td><a href="/my_addons?user_id={{$user->ID}}">My Addons</a></td>
		        	  <td><a href="/my_servers?user_id={{$user->ID}}">My Servers</a></td>
		        	  <td><a href="/bardomains?user_id={{$user->ID}}">My Domains</a></td>
		      	  </tr>
				  
				@endforeach
		      
			  
		    </tbody>
		  </table>
 

        </div>
    </div>
	

@endsection

