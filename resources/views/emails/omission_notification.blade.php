<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>{{$email->title}}</title>

@include('emails/_css_email')
</head>

<body style="width:100% !important; color:#888888; font-family:  sans-serif, Arial; font-weight: 400; font-size:11px; line-height:1;" alink="#4a4a4a" link="#4a4a4a" text="#4a4a4a" yahoo="fix">

@include('emails/_email_header')

<p style="display: inline">{{$notification_body}}</p>

<div style="background: #43735D; border-radius: 5px; padding: 5px 5px 5px 5px; width: 140px; text-align: center; display: inline;">
<a href="{{$url}}" style=" font-family: sans-serif; line-height: 1.2; font-weight: 700; color: #fff; font-size: 19px; text-transform: uppercase; text-decoration: none;"> {{trans('addons.click_here')}}</a></div>

<br /><br />
@include('emails/_footer_email_en')

	</body>           

</html>