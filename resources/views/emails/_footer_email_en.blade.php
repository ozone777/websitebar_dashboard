{{trans('addons.footer_part_one')}}<a style="text-decoration:underline; color:#43735D;" href="mailto:{{$options->get_meta_value('contact_email')}}">{{$options->get_meta_value("contact_email")}}</a>{{trans('addons.footer_part_two')}}<a style="text-decoration:underline; color:#7CD0EC;" href="{{$options->get_meta_value('dashboard_url')}}"></a> {{trans('addons.footer_part_three')}}

<br /><br />
{{trans('addons.best_regards')}}
<br /><br />

</td>
												</tr>
												
											
												
											</table>
										</td>
										<td width="40" class="device-gutter"> </td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>		
		<!-- welcomeBox -->


		<!-- wideBox -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #b2905a; border-radius: 0 0 5px 5px; ">
									<tr>
										<td width="40" class="device-gutter"> </td>
										<td align="center">
											<table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" class="device-width">
												<tr>
													<td height="20"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
												<tr>
													<td>
														<table cellpadding="0" cellspacing="0" border="0" width="100%" align="center">
														    <tr>
																<td>
																	<table cellpadding="0" cellspacing="0" border="0" align="left" class="device-width txt-center">
																		<tr>
																			<td height="7"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
																		</tr>
																	    <tr>
																			<td style="padding: 0px; margin: 0px; font-family:  sans-serif; line-height: 1.4; font-weight: 400; color: #ffffff; font-size: 19px;">{{trans('addons.login_into')}}</b></td>
																		</tr>
																	</table>
																	<table cellpadding="0" cellspacing="0" border="0" align="left" width="30" class="device-width">
																		<tr>
																			<td height="20"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
																		</tr>
																	</table>
																	<table cellpadding="0" cellspacing="0" border="0" align="right" class="device-width">
																	    <tr>
																			<td>
																				<table cellspacing="0" cellpadding="0" border="0" align="center" style="background: #43735D; border-radius: 5px;">
																					<tr>
																						<td height="9"><img border="0" width="1" height="1" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" /></td>
																					</tr>
																					<tr>
																						<td>
																							<table cellspacing="0" cellpadding="0" border="0" width="100%">
																								<tr>
																									<td width="20"> </td>
																									<td style="padding: 0px; margin: 0px;"><a href="{{$url}}" style="font-family:  sans-serif; line-height: 1.2; font-weight: 700; color: #fff; font-size: 19px; text-transform: uppercase; text-decoration: none;"><a style="color: #fff" href="{{$url}}">{{trans('addons.click_here')}}</a></a></td>
																									<td width="20"> </td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr>
																						<td height="9"><img border="0" width="1" height="1" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" /></td>
																					</tr>
																				</table>																				
																			</td>
																		</tr>
																	</table>
																</td>
														    </tr>
														</table>
													</td>
												</tr>
												<tr>
													<td height="20"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
											</table>
										</td>
										<td width="40" class="device-gutter"> </td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>		
		<!-- wideBox -->

		<!-- footerCntr -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td width="40" class="device-gutter"> </td>
										<td align="center">
											<table cellpadding="0" cellspacing="0" border="0" width="400" align="center" class="device-width">
												<tr>
													<td height="28"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
												
												<tr>
													<td height="5"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
												<tr>
														<td style="padding: 0px; margin: 0px; font-family:  sans-serif; font-weight: 400; font-size:10px; color: #888888; line-height: 1.2; text-align: center;">{{trans('addons.email_mistake')}}<a href="mailto:{{$options->get_meta_value('contact_email')}}" style="text-decoration: underline; color: #888888;">{{$options->get_meta_value('contact_email')}}</a></td>
												</tr>
												<tr>
													<td height="15"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
												<tr>
													<td style="padding: 0px; margin: 0px; font-family:  sans-serif; font-weight: 400; font-size:10px; color: #888888; line-height: 1.2; text-align: center;">© Ozone Labs S.A.S. All Rights Reserved.</td>
												</tr>
												<tr>
													<td height="5"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
												
												<tr>
													<td height="25"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
											</table>
										</td>
										<td width="40" class="device-gutter"> </td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- footerCntr -->