<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">

<title>{{$email->title}}</title>

@include('emails/_css_email')
</head>

<body style="width:100% !important; color:#888888; font-family:  sans-serif, Arial; font-weight: 400; font-size:11px; line-height:1;" alink="#4a4a4a" link="#4a4a4a" text="#4a4a4a" yahoo="fix">

@include('emails/_email_header')

Your support ticket number: #{{$ticket_id}}. Has been answered.

<br />

{{$answer_body}}

<br />

To see more please go to url:  <a href="{{$show_more_link}}">{{$show_more_link}}</a>



	</body>           

</html>