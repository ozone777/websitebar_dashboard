		
		<!-- top headerCntr -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%">
									<tr>
										<td width="40" class="device-gutter"> </td>
										<td>
											<table cellpadding="0" cellspacing="0" border="0" width="100%">
												<tr>
													<td height="13"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
												<tr>
													<td >
														<table cellpadding="0" cellspacing="0" border="0" align="left" width="49%" class="device-width txt-center">
															<tr>
																<td style="padding: 0px; margin: 0px; font-family:  sans-serif; font-weight: 400; color: #888888; line-height: 1.2;"></td>
															</tr>
															<tr>
																<td height="10"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
															</tr>
														</table>
														<table cellpadding="0" cellspacing="0" border="0" align="right" width="49%" class="device-width txt-center">
												
														</table>
													</td>
												</tr>
												<tr>
													<td height="4"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
											</table>
										</td>
										<td width="40" class="device-gutter"> </td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>

		</table>
		<!-- top headerCntr-->

		<!-- top radius -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-radius: 5px 5px 0 0; background: #ffffff;">
									<tr>
										<td height="20"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
									</tr>
								</table>	
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- top radius -->
		
		
		<!-- logoBox -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper">
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #ffffff;">
									<tr>
										<td width="40" class="device-gutter"> </td>
										<td>
											<table cellpadding="0" cellspacing="0" border="0" width="100%" style="border-bottom: 1px solid #e7e7e7;">
												<tr>
													<td height="10"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
												<tr>
													<td align="center">
														
																
														<a href="#"><img border="0" width="30%" height="30%" src="{{$header_image}}" alt="" style="display: block;" /></a>	
															
														
														
														<table cellpadding="0" cellspacing="0" border="0" align="left" width="20" class="device-width-480">
															<tr>
																<td height="20"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
															</tr>	
														</table>
														<table cellpadding="0" cellspacing="0" border="0" align="right" class="device-width-480">
															<tr>
																<td height="3"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
															</tr>
															<tr>
																<td>
																	
																</td>
															</tr>	
														</table>
													</td>
												</tr>
												
											</table>
										</td>
										<td width="40" class="device-gutter"> </td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<!-- logoBox -->
		
		<!-- welcomeBox -->
		<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
			<tr>
				<td>
					<table cellpadding="0" cellspacing="0" border="0" width="680" align="center" class="wrapper" >
						<tr>
							<td>
								<table cellpadding="0" cellspacing="0" border="0" width="100%" style="background: #ffffff;">
									<tr>
										<td width="40" class="device-gutter"> </td>
										<td align="center">
											<table cellpadding="0" cellspacing="0" border="0" width="550" align="center" class="device-width">
												<tr>
													<td height="38"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
												<tr>
													<td align="left" style="padding: 0px; margin: 0px; font-family:  sans-serif; line-height: 1.2; font-weight: 400; font-size: 16px; letter-spacing: -0.02em;" class="font-14">{{trans('addons.hi')}} {{$customer_complete_name}},</td>
												</tr>
												<tr>
													<td height="12"><img border="0" src="https://dashboard.ozonegroup.co/viway_layout2/spacer.gif" alt="" width="1" height="1" /></td>
												</tr>
												<tr>
													<td align="left" style="padding: 0px; margin: 0px; font-family:  sans-serif; line-height: 1.5; font-weight: 400; color: #888888; font-size: 20px;" class="font-18">
