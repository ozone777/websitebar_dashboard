<style type="text/css">


	.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {
					line-height: 100%;
				}
				body {
					-webkit-text-size-adjust: none;
					-ms-text-size-adjust: none;
				}
				body, img, div, p, ul, li, span, strong, a {
					margin: 0;
					padding: 0;
				}

				table {
					border-spacing: 0;
				}
				table td {
					border-collapse: collapse;
				}

				body, #body_style {
					width: 100% !important;
					color: #888888;
					font-size: 11px;
					line-height: 1;
					font-family: "Open Sans", sans-serif;
					font-weight: 400;
					background: #e3e7e9;
				}
				a {
					color: #888888;
					text-decoration: none;
					outline: none;
				}
				a:link {
					color: #888888;
					text-decoration: none;
				}
				a:visited {
					color: #888888;
					text-decoration: none;
				}
				a:hover {
					text-decoration: none !important;
				}
				a[href^="tel"], a[href^="sms"] {
					text-decoration: none;
					color: #888888 !important;
					pointer-events: none;
					cursor: default;
				}
				img {
					border: none !important;
					outline: none !important;
					text-decoration: none;
					height: auto;
					max-width: 100%;
					display: block;
				}
				a {
					border: none !important;
					outline: none !important;
				}
				table {
					border-collapse: collapse;
					mso-table-lspace: 0pt;
					mso-table-rspace: 0pt;
				}
				tr, td {
					margin: 0;
					padding: 0;
				}

				@media screen and (max-width: 679px) {
					body[yahoo]	.wrapper {
						width: 100% !important;
					}
					body[yahoo]	.device-width {
						width: 100% !important;
					}
					body[yahoo]	.device-gutter {
						width: 15px !important;
					}
					body[yahoo]	.height-125 {
						height: 125px !important;
					}
					body[yahoo]	.height-50 {
						height: 50px !important;
					}
					body[yahoo]	.txt-center td {
						text-align: center !important;
					}
					body[yahoo]	.font-40 {
						font-size: 40px !important;
					}
					body[yahoo]	.font-32 {
						font-size: 32px !important;
					}
					body[yahoo]	.font-25 {
						font-size: 25px !important;
					}
					body[yahoo]	.font-20 {
						font-size: 20px !important;
					}
					body[yahoo]	.font-18 {
						font-size: 18px !important;
					}
					body[yahoo]	.font-16 {
						font-size: 16px !important;
					}
					body[yahoo]	.bg-size {
						background-size: auto 100% !important;
						-webkit-background-size: auto 100% !important;
					}
				}
				@media screen and (max-width: 480px) {
					body[yahoo]	.device-width-480 {
						width: 100% !important;
					}
				}
				@media screen and (max-width: 360px) {
					body[yahoo]	.device-width-360 {
						width: 100% !important;
					}
					body[yahoo]	.margin-btm-15 {
						margin-bottom: 15px;
					}
					body[yahoo]	.gutter-5 {
						width: 5px !important;
					}
				}

</style>