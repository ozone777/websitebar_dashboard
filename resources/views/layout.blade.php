<!doctype html>
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/faviconbig.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>WebsiteBar Dashboard</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

	<!-- FONTS FILES     -->
	
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
	<link href="/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
	
	<!-- END FONTS FILES     -->
	
	<!-- CSS FILES     -->
    <!-- Bootstrap core CSS     -->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!-- Animation library for notifications   -->
    <link href="/assets/css/animate.min.css" rel="stylesheet"/>
    <!--  Light Bootstrap Table core CSS    -->
    <link href="/assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">	
    <!--  GrowthTroop CSS    -->
	<link href="/assets/css/slick.css" rel="stylesheet"/>
	<link href="/assets/css/slick-theme.css" rel="stylesheet"/>
	<link href="/assets/css/galleries.css" rel="stylesheet"/> 
	
	<link href="/assets/css/websitebar.css" rel="stylesheet"/>
	
	
	
	<!-- END CSS FILES     -->
	
	<!-- JAVASCRIPT CSS FILES     --> 
	<script
	src="https://code.jquery.com/jquery-2.2.4.min.js"
	  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
	  crossorigin="anonymous"></script>
	<script src="/assets/js/jquery.countdown.min.js"></script>
	<script src="/assets/js/jquery-dateformat.js"></script>
	<script src="/assets/js/dateFormat.js"></script>
	
	<!-- END JAVASCRIPT FILES     --> 
	
	
	
	<!-- OMISSIONS JAVASCRIPT --> 
	<script src="/assets/js/jquery.stepy.min.js"></script>
	<script src="/omissions/jquery.observe_field.js"></script>
	
	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="/omissions/jquery.fancybox.js?v=2.1.5"></script>
	<link rel="stylesheet" type="text/css" href="/omissions/jquery.fancybox.css?v=2.1.5" media="screen" />

	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="/omissions/jquery.fancybox-buttons.css?v=1.0.5" />
	<script type="text/javascript" src="/omissions/jquery.fancybox-buttons.js?v=1.0.5"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="/omissions/jquery.fancybox-thumbs.css?v=1.0.7" />
	<script type="text/javascript" src="/omissions/jquery.fancybox-thumbs.js?v=1.0.7"></script>

	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="/omissions/jquery.fancybox-media.js?v=1.0.6"></script>
	
	<!-- OMISSIONS CSS FILES --> 
	<link href="/assets/css/jquery.stepy.css" rel="stylesheet"/>
	<link href="/omissions/omissions.css" rel="stylesheet"/>
	
	
	<!-- CALENDARS    --> 
	<link href='/assets/css/fullcalendar.min.css' rel='stylesheet' />
	<link href='/assets/css/fullcalendar.print.min.css' rel='stylesheet' media='print' />
	<script src='/assets/js/moment.min.js'></script>
	
	<script src='/assets/js/fullcalendar.min.js'></script>
	<!-- END CALENDARS -->
	
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-image="/assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="/" class="simple-text">
               	 @if($dashboard_option->get_meta_image('sidebar_header_image'))
				<img class="img-responsive header_logo" src="/options/{{$dashboard_option->get_meta_image('sidebar_header_image')}}">
				@endif
                </a>
            </div>

            <ul class="nav">
				
				
				
				@if(($role == "administrator") || ($role == "Contributor") || ($role == "editor"))
				
		
                <li @if($viewsw == "select_customer") class="active" @endif>
                    <a href="/select_customer">
                        <i class="pe-7s-smile"></i>
                        <p>{{trans('admin.select_customer')}}</p>
                    </a>
                </li>
				
                <li @if($viewsw == "open_tickets") class="active" @endif>
                    <a href="/open_tickets">
                        <i class="pe-7s-headphones"></i>
                        <p>{{trans('admin.open_tickets')}}</p>
                    </a>
                </li>
				
                @endif
				
				@if($role == "administrator")
				
                 <li @if($viewsw == "themes") class="active" @endif>
                    <a href="/barthemes">
                        <i class="pe-7s-joy"></i>
                        <p><p>{{trans('admin.themes')}}</p></p>
                    </a>
                </li>
				
                 <li @if($viewsw == "options") class="active" @endif>
                    <a href="/wp_global_options">
                        <i class="pe-7s-joy"></i>
                        <p><p>{{trans('admin.options')}}</p></p>
                    </a>
                </li>
				
				@endif
				
				@if(($role == "subscriber") || ($role == "customer"))
				
                <li @if($viewsw == "create_site") class="active" @endif>
                    <a href="/create_site">
                        <i class="pe-7s-browser"></i>
                        <p> {{trans('dashboard.create_site')}}</p>
                    </a>
                </li>
				
                <li @if($viewsw == "my_sites") class="active" @endif>
                    <a href="/my_sites">
                        <i class="pe-7s-share"></i>
                        <p> {{trans('dashboard.my_sites')}}</p>
                    </a>
                </li>
				
                <li @if($viewsw == "my_addons") class="active" @endif>
                    <a href="/my_addons">
                        <i class="pe-7s-server"></i>
                        <p> {{trans('dashboard.my_addons')}}</p>
                    </a>
                </li>
				
                 <li @if($viewsw == "addons") class="active" @endif>
                    <a href="/addons">
                        <i class="pe-7s-shopbag"></i>
                        <p>{{trans('dashboard.addons')}}</p>
                    </a>
                </li>
				
               <li @if($viewsw == "my_domains") class="active" @endif>
                   <a href="/bardomains">
                       <i class="pe-7s-way"></i>
                       <p> {{trans('dashboard.my_domains')}}</p>
                   </a>
               </li>
				
                <li @if($viewsw == "my_servers") class="active" @endif>
                    <a href="/my_servers">
                        <i class="pe-7s-server"></i>
                        <p> {{trans('dashboard.my_servers')}}</p>
                    </a>
                </li>
				
                 <li @if($viewsw == "support") class="active" @endif>
                    <a href="/otickets">
                        <i class="pe-7s-headphones"></i>
                        <p>{{trans('dashboard.need_help')}}</p>
                    </a>
                </li>
				
				@endif
				
				
				<div id="loading_area">
				
				</div>
				
               
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                       
                    </ul>

                    <ul class="nav navbar-nav navbar-right">

						
                        <li>
                            <a href="#">
                                {{$current_user->user_email}}
                            </a>
                        </li>
						
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    Languages
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                <li id ="englishbtn"><a href="#">English</a></li>
                                <li id ="spanishbtn"><a href="#">Español</a></li>
                              </ul>
                        </li>
                    	
                        <li>
                            <a href="/logout">
                                Log out
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">


               
						
						
			        @yield('header')
					@yield('errors')
			        @yield('content')
						
				
		       

            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                
                <p class="copyright pull-right">
                   {{$dashboard_option->get_meta_value("footer_copy")}}
                </p>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
   
	<script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.35.4/js/bootstrap-dialog.min.js" type="text/javascript"></script>
	<!--  Checkbox, Radio & Switch Plugins -->
    <!--  Notifications Plugin    -->
    <script src="/assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin   
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    -->
    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="/assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="/assets/js/demo.js"></script>

	<script>
		
		//////////////////////////
		//REDIRECT SITES
		//////////////////////////
		function redirect_to_sites(message=""){
		    var delayInMilliseconds = 6000; //3 second
			
			if($(".cssload-loader").length == 0){
				$("#loading_area").append('<div class="cssload-loader"></div>');
			}

		    setTimeout(function() {
		      //your code to be executed after 6 second
			  $(".cssload-loader").remove();
			  if(message !=""){
			  	 window.location.replace("/my_sites?action="+message);
			  }else{
			  	 window.location.replace("/my_sites");
			  }
	 
 
		    }, delayInMilliseconds);
		}


		//////////////////////////
		//RELOAD PETE LOGIC
		//////////////////////////
		function reload_pete(barserver_ip,email,pete_token){
    
			console.log("Reload pete");
	
			if($(".cssload-loader").length == 0){
				$("#loading_area").append('<div class="cssload-loader"></div>');
			}
	
			pete_url = "http://"+barserver_ip+"/reload_pete";
			
			$.ajax({
				url: pete_url, // url where to submit the request
				type : "GET", // type of action POST || GET
				dataType : 'JSONP', // data type
				data: {email : email, pete_token: pete_token},
		
				success : function(data) {
					//$(".cssload-loader" ).remove();
				},
				error: function(xhr, resp, text) {
					//console.log(xhr, resp, text);
					console.log("reload done!")
				}
		
			})
	

		}
		
    	$(document).ready(function(){
			
			@if(isset($success))
			
			@if($success == "yes")
        	
			
        	$.notify({
            	icon: 'pe-7s-arc',
            	message: "API Key Successfully Created"

            },{
                type: 'info',
                timer: 4000
            });
			
			@elseif($success == "no")
			
        	$.notify({
            	icon: 'pe-7s-arc',
            	message: "Sorry you need to upgrade your license"

            },{
                type: 'info',
                timer: 4000
            });
			
			@endif
			
			@endif
			
			
			
			@if($message)
        	
        	$.notify({
            	icon: 'pe-7s-arc',
            	message: "{{$message}}"

            },{
                type: 'info',
                timer: 20000
            });
			
			@endif
			
			
			
			$( "#spanishbtn" ).click(function() {
				
				$("#loading_area").prepend('<div class="cssload-loader"></div>');
				
		        $.ajax({
		         type: "POST",
		         url: "/set_lang",
		         dataType: "json",
		         data: { lang: "es", _token: "{{ csrf_token() }}"},
		         success: function(data){
					location.reload();
		         }
		       });
			});
			
			$( "#englishbtn" ).click(function() {
				
				$("#loading_area").prepend('<div class="cssload-loader"></div>');
				
		        $.ajax({
		         type: "POST",
		         url: "/set_lang",
		         dataType: "json",
		         data: { lang: "en", _token: "{{ csrf_token() }}"},
		         success: function(data){
					 location.reload();
		         }
		       });
			});
			
    	});
		
		var randomString = function(length) {
		    var text = "";
		    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		    for(var i = 0; i < length; i++) {
		        text += possible.charAt(Math.floor(Math.random() * possible.length));
		    }
		    return text;
		}
		
	</script>


	<script type="text/javascript">
	/*
	$( document ).ajaxStart(function() {
		
		if($(".cssload-loader").length == 0){
			
	      $("#loading_area").append('<div class="cssload-loader"></div>');
		}
		
	});
	
	$(document).ajaxSuccess(function() {
	  $(".cssload-loader" ).remove();
	  
	});
*/
	</script>
	
	<script src="/assets/js/slick.js" type="text/javascript" charset="utf-8"></script>
	
    <script>

    $(document).on('ready', function() {

    $( "#web_subscription" ).click(function() {
 
     if($("#template_selected").val() ==""){
     alert( "Please select a Web Template" );
     return false;
    }


    });

    

    });


    </script>
	

</html>
