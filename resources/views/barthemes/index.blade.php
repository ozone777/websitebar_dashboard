@extends('layout')

@section('header')
    <div class="page-header clearfix">
        <h3>
            Themes
            <a class="btn btn-success pull-right" href="{{ route('barthemes.create') }}">Create</a>
        </h3>

    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if($barthemes->count())
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
							<th>Theme URL</th>
							<th>Theme file</th>
							<th>Image</th>
                            <th class="text-right">OPTIONS</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($barthemes as $bartheme)
                            <tr>
                                <td>{{$bartheme->id}}</td>
                                <td>{{$bartheme->name}}</td>
								<td>{{$bartheme->theme_url}}</td>
								<td>{{$bartheme->theme_file}}</td>
								<td>
									@if($bartheme->image)
								<img src="/themes/{{$bartheme->image}}" height="100">
								
								@endif
								
								</td>
                                <td class="text-right">
                                    
                                    <a class="btn btn-xs btn-warning" href="{{ route('barthemes.edit', $bartheme->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                                    <form action="{{ route('barthemes.destroy', $bartheme->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $barthemes->render() !!}
            @else
                <h3 class="text-center alert alert-info">Empty!</h3>
            @endif

        </div>
    </div>

@endsection