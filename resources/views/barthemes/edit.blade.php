@extends('layout')

@section('header')
    <div class="page-header">
        <h3> Barthemes / Edit #{{$bartheme->id}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('barthemes.update', $bartheme->id) }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <label>Name</label>
                <input type="text" id="name-field" name="name" class="form-control" value="{{$bartheme->name}}"/>
				<br />
                <label>Theme url</label>
                <input type="text" id="name-theme_url" name="theme_url" class="form-control" value="{{ $bartheme->theme_url }}"/>
				<br />
				
                <label>Theme url</label>
                <input type="text" id="name-theme_file" name="theme_file" class="form-control" value="{{ $bartheme->theme_file }}"/>
				<br />
				
				<label>Theme file</label>
				<input type="file" name="image">
                
				
				<input type="submit" class="et_manage_submit create_barsite" value="Save">
				
                
            </form>

        </div>
    </div>
@endsection