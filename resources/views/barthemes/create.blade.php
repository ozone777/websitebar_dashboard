@extends('layout')

@section('header')
    <div class="page-header">
        <h3></i> Barthemes / Create </h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('barthemes.store') }}" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <label>Name</label>
                <input type="text" id="name-field" name="name" class="form-control" value="{{ old("option_name") }}"/>
				<br />
                <label>Theme url</label>
                <input type="text" id="name-template_url" name="template_url" class="form-control" value="{{ old("template_url") }}"/>
				<br />
				<label>Image file</label>
				<input type="file" name="image">
                <br />
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a class="btn btn-link pull-right" href="{{ route('barthemes.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection