@extends('layout')

@section('header')
    <div class="page-header">
       
		<h3>{{trans('dashboard.create_domain')}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('bardomains.store') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
				
				<p>{{trans('dashboard.create_domain_help_text')}}</p>
				
				<br />
				
				<label>{{trans('dashboard.select_barserver')}}</label><br />
				<select name="barserver_id" class="form-control">
				
				 @foreach($barservers as $barserver)
				
				  <option value="{{$barserver->id}}">{{$barserver->linode_label}}</option>
				
				 @endforeach
				</select>
				
				<br />
				<label>{{trans('dashboard.write_domain')}} </label> 
				<br />
				<h5>{{trans('dashboard.write_domain_help1')}} <i>www</i> {{trans('dashboard.not')}} <i>http</i> {{trans('dashboard.write_domain_help2')}}</h5> 
				 
				<input name="domain_zone" class="form-control" value="">
                <br />
                
				<input type="submit" class="et_manage_submit" value="{{trans('dashboard.create_domain')}}">	
				
				<br />
            </form>

        </div>
    </div>
@endsection