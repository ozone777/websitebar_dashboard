@extends('layout')

@section('header')
   
@endsection

@section('content')

	<div class="row">
		<div class="col-md-6">
			<h1>{{trans('dashboard.my_domains')}}</h1>
		</div>
		
		<div class="col-md-6">
			<a href="{{ route('bardomains.create') }}">{{trans('dashboard.create_domain')}}</a>
		</div>
	</div>

    <div class="row">
        <div class="col-md-12">
            
                <table class="table table-condensed table-striped">
                    <thead>
                        <tr>
                            <th>Domain</th>
                            <th>Server</th>
                            <th>OPTIONS</th>
							<th></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($bardomains as $bardomain)
                            <tr>
                                <td width="40%">
									@if($bardomain->subdomain)
										<a href="http://{{$bardomain->subdomain}}.websitebar.co">{{$bardomain->subdomain}}.websitebar.co</a>
									@else
										<a href="http://{{$bardomain->domain_zone}}">{{$bardomain->domain_zone}}</a>
									@endif
								</td>
                                <td width="20%">{{$bardomain->linode_label}}</td>
                                <td width="20%">
                                    
									@if(!$bardomain->subdomain)
									
                                    <form action="{{ route('bardomains.destroy', $bardomain->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <button type="submit" class="btn btn-xs btn-danger">Delete</button>
                                    </form>
									
									@endif
									
                                </td>
								
						<td width="20%">
							
						<?php
									
							$date = new Carbon\Carbon($bardomain->created_at);
							$conditional_date = Carbon\Carbon::now();
							$conditiona_date = $conditional_date->subMinutes($creating_delay);
									//echo "original date $date";
									
							$next_date = $date->addMinutes($creating_delay);
									//echo "next date $next_date";
						?>
									
						@if($date > $conditional_date)			
						<div bardomain_id="{{$bardomain->id}}" style="font-size:20px; margin-bottom: 10px" data-countdown="{{$next_date->format('Y/m/d H:i:s')}}"></div>
						<img id="bardomain_animation_{{$bardomain->id}}" src="/gifs/tenor.gif">
						@endif
						
						</td>
								
                            </tr>
                        @endforeach
                    </tbody>
                </table>
          
        </div>
    </div>
	
<script>
	
	////////////////////////////
	///COUNTDOWN LOGIC
	////////////////////////////

	function countdown_logic(){
	
		var offset = new Date().getTimezoneOffset();
	
		$('[data-countdown]').each(function() {
			var $this = $(this), finalDate = $(this).data('countdown');
	  
			//console.log(finalDate);
	  
			datex = new Date(finalDate);
			// console.log(datex);
			datex.setMinutes( datex.getMinutes() - offset );
			datex_string = $.format.date(datex, 'yyyy/MM/dd H:mm:ss');
			//console.log(datex_string);
	   
			$this.countdown(datex_string, function(event) {
				$this.html(event.strftime('%H:%M:%S'));
		
			}).on('finish.countdown', function() {
				
				$("#bardomain_animation_"+$(this).attr("bardomain_id")).remove();
				$(this).remove();
				//console.log("termino");
				//alert("done!");
			});
		});

	}
	
	$(document).ready(function(){
		countdown_logic();
	});
	
</script>
	
@endsection