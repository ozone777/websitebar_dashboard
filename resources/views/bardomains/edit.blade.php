@extends('layout')

@section('header')
    <div class="page-header">
        <h3>{{$bardomain->domain_zone}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
				
				<table class="list">
					<tbody><tr>
						<th colspan="5">MX Records</th>
					</tr>

					<tr class="list_head">
						<td>Mail Server</td>
						<td>Preference</td>
						<td>Subdomain</td>
						<td>TTL</td>
						<td align="center">Options</td>
					</tr>

					@foreach($barrecords as $barrecord)
					
					@if($barrecord->type == "MX")
					
					<tr class="list_head">
						<td>{{$barrecord->target}}</td>
						<td>{{$barrecord->priority}}</td>
						<td>{{$barrecord->name}}</td>
						@if($barrecord->ttl_sec == 0)
						  <td>Default</td>
						@else
						  <td>{{$barrecord->ttl_sec}}</td>
						@endif
						 <td align="center">
						<form action="/delete_record" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false }">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="bardomain_id" value="{{ $bardomain->id }}">
							<input type="hidden" name="barrecord_id" value="{{ $barrecord->id }}">
							<button type="submit" class="btn btn-xs btn-danger">Delete</button>
						</form>
						</td>
					</tr>
					
					@endif
					
					@endforeach
					

					<tr class="list_entry">
						<td colspan="5" align="right">
			
							<a class="et_manage_submit" href="/create_record?type=mx&bardomain_id={{$bardomain->id}}">Add a new MX record</a>
						</td>
					</tr>
				</tbody></table>
				
				
				<table class="list">
					<tbody><tr>
						<th colspan="4">A/AAAA Records</th>
					</tr>

					<tr class="list_head">
						<td>Hostname</td>
						<td>IP Address</td>
						<td>TTL</td>
						<td align="center">Options</td>
					</tr>

					@foreach($barrecords as $barrecord)
					
					@if($barrecord->type == "A")
					
					
						<tr class="list_entry">
							<td>{{$barrecord->name}}</td>
							<td>{{$barrecord->target}}</td>
							@if($barrecord->ttl_sec == 0)
							  <td>Default</td>
							@else
							  <td>{{$barrecord->ttl_sec}}</td>
							@endif
							
							 <td align="center">
							<form action="/delete_record" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false }">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="bardomain_id" value="{{ $bardomain->id }}">
								<input type="hidden" name="barrecord_id" value="{{ $barrecord->id }}">
								<button type="submit" class="btn btn-xs btn-danger">Delete</button>
							</form>
							</td>
						</tr>
	
					@endif
					
					@endforeach
	

					<tr class="list_entry">
						<td colspan="4" align="right">
			
							<a class="et_manage_submit" href="/create_record?type=a&bardomain_id={{$bardomain->id}}">Add a new A/AAAA record</a>
						</td>
					</tr>
				</tbody></table>
				
				
				
				<table class="list">
					<tbody><tr>
						<th colspan="4">CNAME Records</th>
					</tr>

					<tr class="list_head">
						<td>Hostname</td>
						<td>Aliases to</td>
						<td>TTL</td>
						<td align="center">Options</td>
					</tr>

					
					@foreach($barrecords as $barrecord)
					
					@if($barrecord->type == "CNAME")
					
					<tr class="list_entry">
						<td>{{$barrecord->name}}</td>
						<td>{{$barrecord->target}}</td>
						@if($barrecord->ttl_sec == 0)
						  <td>Default</td>
						@else
						  <td>{{$barrecord->ttl_sec}}</td>
						@endif
						 <td align="center">
						<form action="/delete_record" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false }">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="bardomain_id" value="{{ $bardomain->id }}">
							<input type="hidden" name="barrecord_id" value="{{ $barrecord->id }}">
							<button type="submit" class="btn btn-xs btn-danger">Delete</button>
						</form>
						</td>
					</tr>
					
					
					@endif
					
					@endforeach

					<tr class="list_entry">
						<td colspan="4" align="right">
			
							<a class="et_manage_submit" href="/create_record?type=cname&bardomain_id={{$bardomain->id}}">Add a new CNAME record</a>
						</td>
					</tr>
				</tbody></table>
				
				
				<table class="list">
					<tbody><tr>
						<th colspan="4">TXT Records</th>
					</tr>

					<tr class="list_head">
						<td>Name</td>
						<td>Value</td>
						<td>TTL</td>
						<td align="center">Options</td>
					</tr>

	
					@foreach($barrecords as $barrecord)
					
					@if($barrecord->type == "TXT")
					
					<tr class="list_entry">
						<td>{{$barrecord->name}}</td>
						<td>{{$barrecord->target}}</td>
						@if($barrecord->ttl_sec == 0)
						  <td>Default</td>
						@else
						  <td>{{$barrecord->ttl_sec}}</td>
						@endif
						 <td align="center">
						<form action="/delete_record" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false }">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<input type="hidden" name="bardomain_id" value="{{ $bardomain->id }}">
							<input type="hidden" name="barrecord_id" value="{{ $barrecord->id }}">
							<button type="submit" class="btn btn-xs btn-danger">Delete</button>
						</form>
						</td>
					</tr>
					
					@endif
					
					@endforeach

					<tr class="list_entry">
						<td colspan="4" align="right">
			
							<a class="et_manage_submit" href="/create_record?type=txt&bardomain_id={{$bardomain->id}}">Add a new TXT record</a>
						</td>
					</tr>
				</tbody></table>
                
				<br /><br />
				
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('bardomains.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
           

        </div>
    </div>
@endsection