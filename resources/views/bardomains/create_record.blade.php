@extends('layout')

@section('header')
    <div class="page-header">
       
		<h3>{{trans('dashboard.create_record')}}</h3>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">
			
			@if($type == "mx")

			<form action="/store_record" method="post">
			<input type="hidden" name="type" value="mx">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="bardomain_id" value="{{$bardomain->id}}">
			
				<table class="list">
					<tbody><tr>
						<th colspan="3">Add/edit an MX Record</th>
					</tr>

					<tr class="list_entry">
						<td class="table_form_header">Mail Server</td>
						<td><input type="text" name="target" size="30" value=""></td>
						<td class="hint">Example: mail2.example.com</td>
					</tr>
					<tr class="list_entry">
						<td class="table_form_header">Priority</td>
						<td><input type="number" min="0" max="255" required="" pattern="[0-9]+" name="priority" size="3" value="10"></td>
						<td class="hint">0-255</td>
					</tr>
					<tr class="list_entry">
						<td class="table_form_header">Subdomain</td>
						<td><input type="text" name="name" size="30" value=""></td>
						<td class="hint">Leave blank unless delegating a subdomain to the mail server above. A wildcard is also valid here.</td>
					</tr>

					<tr class="list_entry">
						<td class="table_form_header">TTL</td>
						<td>
							<select name="ttl_sec">
					
						<option value="0" selected="">Default
			
						</option><option value="300">300 (5 minutes) 
						</option><option value="3600">3600 (1 hour) 
						</option><option value="7200">7200 (2 hours) 
						</option><option value="14400">14400 (4 hours) 
						</option><option value="28800">28800 (8 hours) 
						</option><option value="57600">57600 (16 hours) 
						</option><option value="86400">86400 (1 day) 
						</option><option value="172800">172800 (2 days) 
						</option><option value="345600">345600 (4 days) 
						</option><option value="604800">604800 (1 week) 
						</option><option value="1209600">1209600 (2 weeks) 
						</option><option value="2419200">2419200 (4 weeks) 
							</option></select>
						</td>
						<td class="hint"></td>
					</tr>

					<tr class="list_entry">
						<td width="200"></td>
						<td colspan="3">
							<br>
							<input class="et_manage_submit" type="submit" value="Save Changes">
						</td>
					</tr>
				</tbody></table>
			</form>
			
			@elseif($type == "a")
			
			<form action="/store_record" method="post">
			<input type="hidden" name="type" value="a">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="bardomain_id" value="{{$bardomain->id}}">

				<table class="list">
					<tbody><tr>
						<th colspan="3">Add/edit an A/AAAA Record</th>
					</tr>

					<tr class="list_entry">
						<td class="table_form_header">Hostname</td>
						<td><input type="text" name="name" size="30" value=""></td>
						<td class="hint">Example: www.example.com</td>
					</tr>

					<tr class="list_entry">
						<td class="table_form_header">IP Address</td>
						<td><input type="text" name="target" size="30" value=""></td>
						<td class="hint"></td>
					</tr>

					<tr class="list_entry">
						<td class="table_form_header">TTL</td>
						<td>
							<select name="ttl_sec">
					
						<option value="0" selected="">Default
			
						</option><option value="300">300 (5 minutes) 
						</option><option value="3600">3600 (1 hour) 
						</option><option value="7200">7200 (2 hours) 
						</option><option value="14400">14400 (4 hours) 
						</option><option value="28800">28800 (8 hours) 
						</option><option value="57600">57600 (16 hours) 
						</option><option value="86400">86400 (1 day) 
						</option><option value="172800">172800 (2 days) 
						</option><option value="345600">345600 (4 days) 
						</option><option value="604800">604800 (1 week) 
						</option><option value="1209600">1209600 (2 weeks) 
						</option><option value="2419200">2419200 (4 weeks) 
							</option></select>
						</td>
						<td class="hint"></td>
					</tr>

					<tr class="list_entry">
						<td width="200"></td>
						<td colspan="3">
							<br>
							<input class="et_manage_submit" type="submit" value="Save Changes">
						</td>
					</tr>
				</tbody></table>
			</form>
			
			@elseif($type == "cname")
			
			<form action="/store_record" method="post">
			<input type="hidden" name="type" value="cname">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="bardomain_id" value="{{$bardomain->id}}">


				<table class="list">
					<tbody><tr>
						<th colspan="3">Add/edit an CNAME Record</th>
					</tr>

					<tr class="list_entry">
						<td class="table_form_header">Hostname</td>
						<td><input type="text" name="name" size="30" value=""></td>
						<td class="hint">Example: www.thisdomain.com</td>
					</tr>

					<tr class="list_entry">
						<td class="table_form_header">Aliases to</td>
						<td><input type="text" name="target" size="30" value=""></td>
						<td class="hint">Example: www.otherdomain.com</td>
					</tr>

					<tr class="list_entry">
						<td class="table_form_header">TTL</td>
						<td>
							<select name="ttl_sec">
					
						<option value="0" selected="">Default
			
						</option><option value="300">300 (5 minutes) 
						</option><option value="3600">3600 (1 hour) 
						</option><option value="7200">7200 (2 hours) 
						</option><option value="14400">14400 (4 hours) 
						</option><option value="28800">28800 (8 hours) 
						</option><option value="57600">57600 (16 hours) 
						</option><option value="86400">86400 (1 day) 
						</option><option value="172800">172800 (2 days) 
						</option><option value="345600">345600 (4 days) 
						</option><option value="604800">604800 (1 week) 
						</option><option value="1209600">1209600 (2 weeks) 
						</option><option value="2419200">2419200 (4 weeks) 
							</option></select>
						</td>
						<td class="hint"></td>
					</tr>

					<tr class="list_entry">
						<td width="200"></td>
						<td colspan="3">
							<br>
							<input class="et_manage_submit" type="submit" value="Save Changes">
						</td>
					</tr>
				</tbody></table>
			</form>
			
			@elseif($type == "txt")
			
				<form action="/store_record" method="post">
				<input type="hidden" name="type" value="txt">
				<input type="hidden" name="_token" value="{{csrf_token()}}">
				<input type="hidden" name="bardomain_id" value="{{$bardomain->id}}">


					<table class="list">
						<tbody><tr>
							<th colspan="3">Add/edit a TXT Record</th>
						</tr>

						<tr class="list_entry">
							<td class="table_form_header">Name</td>
							<td><input type="text" name="name" size="30" value=""></td>
							<td class="hint"></td>
						</tr>

						<tr class="list_entry">
							<td class="table_form_header">Value</td>
							<td><input type="text" name="target" size="50" value=""></td>
							<td class="hint"></td>
						</tr>

						<tr class="list_entry">
							<td class="table_form_header">TTL</td>
							<td>
								<select name="ttl_sec">
					
							<option value="0" selected="">Default
			
							</option><option value="300">300 (5 minutes) 
							</option><option value="3600">3600 (1 hour) 
							</option><option value="7200">7200 (2 hours) 
							</option><option value="14400">14400 (4 hours) 
							</option><option value="28800">28800 (8 hours) 
							</option><option value="57600">57600 (16 hours) 
							</option><option value="86400">86400 (1 day) 
							</option><option value="172800">172800 (2 days) 
							</option><option value="345600">345600 (4 days) 
							</option><option value="604800">604800 (1 week) 
							</option><option value="1209600">1209600 (2 weeks) 
							</option><option value="2419200">2419200 (4 weeks) 
								</option></select>
							</td>
							<td class="hint"></td>
						</tr>

						<tr class="list_entry">
							<td width="200"></td>
							<td colspan="3">
								<br>
								<input class="et_manage_submit" type="submit" value="Save Changes">
							</td>
						</tr>
					</tbody></table>
				</form>
			
			@endif
			
			
			
			
        </div>
    </div>
@endsection