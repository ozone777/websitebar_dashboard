@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')

	

	<div class="row">
		<div class="col-md-12">
			
			@if($admin)
				<p>Selected user: {{$user->user_email}}</p>
			@endif
			
			<h1>{{trans('dashboard.create_site')}}</h1>
		</div>
	</div>

		@if($state == "create_server")
			
			@include('dashboard/create_site/_create_server')
			
		@elseif($state == "creating_server")
		
			@include('dashboard/create_site/_creating_server')
		
		@elseif($state == "create_site")
		
			@include('dashboard/create_site/_create_site')
		
		@endif
		
@endsection

