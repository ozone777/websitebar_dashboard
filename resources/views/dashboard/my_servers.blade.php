@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')

	<div class="row">
		<div class="col-md-12">
			
			@if($admin)
				<p>Selected user: {{$user->user_email}}</p>
			@endif
			
			<h1>{{trans('dashboard.my_servers')}}</h1>
		</div>
	</div>

		<!-- code started -->			
		<?php
	
		
		$subscriptions = wcs_get_users_subscriptions($user->ID);
	
		foreach ($subscriptions as  $key => $subscription ){
					
			$subscription_status = $subscription->post->post_status;
			$subscription_id = $subscription->post->ID;
			$order_id = $subscription->order->post->ID; // order ID (corresponding to the subscription ID)
			$active_subscriptions_arr[] = $subscription->post->ID;
			$order_items = $subscription->get_items();
			$user_id = $subscription->get_user_id();
						
			if(!in_array($subscription_id, $subscription_ids)){
		
				//////////////////////////////////////////
				//MOSTRAR BARSERVERS QUE NECESITAN SER CREADOS
				//////////////////////////////////////////
				
				?>
				
				@if($subscription_status == 'wc-active' )
				
				@include('dashboard/my_servers/_create_servers_list')
				
				@endif
				
				<?php
							
			}else{
													
				if($my_barservers[$subscription_id]->created_at->addMinutes($creating_delay) < $now){
					
					//////////////////////////////////////////
					//BARSERVER CREADO HACE MAS DE 30 MINUTOS
					//////////////////////////////////////////
							
					?>
							
					@include('dashboard/my_servers/_created_servers_list')
						
					<?php
				
				}else{
								
					//////////////////////////////////////////
					//BARSERVER CREADO HACE MENOS DE 30 MINUTOS - QUE SE ESTAN CREANDO ACTUALMENTE
					//////////////////////////////////////////
							
					?>
					
					@include('dashboard/my_servers/_creating_servers_list')
					
					<?php
						
				}
						
			}
						
		}

?>
		

<script>	
	
	/////////////////////////////
	//OBTENER INFO DEL BARSERVER  
	/////////////////////////////
	
	function set_server_size(label,subscription_id,size_title,size){
		$("#"+size_title+subscription_id).html("<p>"+label+": "+size+"</p>");
	}
	
	$(".get_barserver_info").on('click', function(){
	
		if($(".cssload-loader").length == 0){
			$("#loading_area").append('<div class="cssload-loader"></div>');
		}
	
		// send ajax
		barserver_id = $(this).attr("barserver_id")
		$.ajax({
			url: '/get_barserver_info', // url where to submit the request
			type : "POST", // type of action POST || GET
			dataType : 'json', // data type
			data: {barserver_id: $(this).attr("barserver_id"), _token: "{{csrf_token()}}"},
			success : function(result) {
               
				console.log(result);
				console.log(result["ip"]);
				html = "";
				html += "<p style='padding: 5px 5px 5px 5px; font-size: 11px'>SSH ACCESS <br/ >";
				html += "ssh -p 13737 root@"+result["ip"]+"<br/ >";
				html += "Password: "+result["ssh_info"]+"</p>";
				
				html += "<p style='padding: 5px 5px 5px 5px; font-size: 11px'>PHPMYADMIN ACCESS <br/ >";
				phpmyadminurl = "http://"+result["ip"]+"/phpmyadmin";
				html += "<a target='_blank' href='"+phpmyadminurl+"'>"+phpmyadminurl+"</a><br/ >";
				html += "Usename: root <br/ >";
				html += "Password: "+result["db_info"]+"</p>";
				
				html += "<p style='padding: 5px 5px 5px 5px; font-size: 11px'>WORDPRESSPETE ACCESS <br/ >";
				pete_url = "http://"+result["ip"];
				html += "<a target='_blank' href='"+pete_url+"'>"+pete_url+"</a><br/ >";
				html += "Usename: "+result["user_email"]+" <br/ >";
				html += "Password: "+result["pete_info"]+"<br/ >";
				html += "Pete token: "+result["pete_token"]+"</p>";
				
				$("#info_"+result["barserver_id"]).html(html);
			
				$(".cssload-loader" ).remove();
				
			},
			error: function(xhr, resp, text) {
				console.log(xhr, resp, text);
			}
		})
		return false;
	});
	
	
	$(document).ready(function(){
		
		//////////////////////////
		//LOADING EVERY LINODE VPS 
		//////////////////////////
	
		$(".my_servers").each(function(index, element) {
			
		
			EMAIL = $(this).attr("email");
			PETE_TOKEN = $(this).attr("pete_token");
			BARSERVER_IP = $(this).attr("barserver_ip");
			pete_url = "http://"+BARSERVER_IP+"/list_sites";
			subscription_id = $(this).attr("subscription_id");
			barserver_id = $(this).attr("barserver_id");
		
			if($(this).attr("barserver_status") == "1"){
			
				if($(".cssload-loader").length == 0){
					$("#loading_area").append('<div class="cssload-loader"></div>');
				}
			
			
			$.ajax({
				url: pete_url,
				dataType: 'JSONP',
				type: 'GET',
				data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, barserver_ip: BARSERVER_IP},
				success : function(result) {
					
					set_server_size("{{trans('dashboard.disk_total_size')}}",result["subscription_id"],"disk_total_size_",result["disk_total_size"]);
					set_server_size("{{trans('dashboard.disk_used')}}",result["subscription_id"],"disk_used_",result["disk_used"]);
					set_server_size("{{trans('dashboard.disk_free')}}",result["subscription_id"],"disk_free_",result["disk_free"]);
					
					$(".cssload-loader").remove();
				
						
				}		
			});	
		
			}
			
		});
		
		
		

		var offset = new Date().getTimezoneOffset();
	
		$('[data-countdown]').each(function() {
		var $this = $(this), finalDate = $(this).data('countdown');
	  
		console.log(finalDate);
	  
		datex = new Date(finalDate);
		// console.log(datex);
		datex.setMinutes( datex.getMinutes() - offset );
		datex_string = $.format.date(datex, 'yyyy/MM/dd H:mm:ss');
		console.log(datex_string);
	   
		$this.countdown(datex_string, function(event) {
			$this.html(event.strftime('%M:%S'));
		
		}).on('finish.countdown', function() {
			location.reload();
		});
		});
		
		
	});

	</script>		



	

	

@endsection

