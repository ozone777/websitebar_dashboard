
<!-- code started -->			
<?php

//////////////////////////////////////////
//BARSERVERS QUE NECESITAN SER ACTIVADO
//////////////////////////////////////////
				
$USER_ID = $current_user->ID;
$subscriptions = wcs_get_users_subscriptions($USER_ID);
$cont = 0;


$subscription = reset($subscriptions);

//foreach ($subscriptions as  $key => $subscription ){
					
	$subscription_status = $subscription->post->post_status;
	$subscription_id = $subscription->post->ID;
	$order_id = $subscription->order->post->ID; // order ID (corresponding to the subscription ID)
	$active_subscriptions_arr[] = $subscription->post->ID;
	
			
	if($subscription_status == 'wc-active' ){
		
		$order_items = $subscription->get_items();
		$user_id = $subscription->get_user_id();
			
		if(!in_array($subscription_id, $subscription_ids)){

			?>
				
			<div class="row">
				<div class="col-md-12">
				
					<?php	
					foreach ( $order_items as $key => $item ) {
						$_product = get_product($item[product_id]);
			
						if (in_array($item[product_id], $plan_ids)) {
							echo '<h4 class="title">' . get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang) ) .'</h4>';
							
							?>
					
						</div>
					</div>
			
			  	
						
					<div class="row">
						<div class="col-md-12">

							<form action="/create_linode" method="post">
								<input type="hidden" name="_token" value="{{csrf_token()}}">
								<input type="hidden" name="product_id" value="{{$item[product_id]}}">
								<input type="hidden" name="subscription_id" value="{{$subscription_id}}">
								<input type="hidden" name="user_id" value="{{$user_id}}">
			
								@if($dashboard_option->get_meta_value("environment") == "development")
								<input type="checkbox" name="test" value="1" checked> Test<br />
								@endif
								
								@if( $subscription_status == 'wc-active' )
								<input type="submit" class="et_manage_submit" value="{{trans('dashboard.create_server')}}">	
								@else
								Not active subscription
								@endif				
							
							</form>
							<br />

						</div>
					</div>		
						
					
							
					<?php
			
				}
			
			}
		}
							
	}
	//}

?>
