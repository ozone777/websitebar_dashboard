 <div class="row">
 <div class="col-md-12">

<section class="regular slider">
 
 @foreach($barthemes as $bartheme)
 
 <div name ="{{$bartheme->name}}" template_url ="{{$bartheme->theme_url}}" theme_file ="{{$bartheme->theme_file}}">
 <img src="/themes/{{$bartheme->image}}" >
 </div>

 @endforeach 

 </section>
 
 </div>
 </div>
 
 <script>
	 
	 $(document).on('ready', function() {
		 
		
	 
     $(".regular").slick({
      dots: true,
      infinite: true,
      slidesToShow: 8,
      slidesToScroll: 4,
	  zoom_in: "{{trans('dashboard.zoom_in')}}",
      });

	   $(".slick-slide").removeClass("slick-current");

     $("img").click(function() {

      selected = $(this);

      $('img').each(function () {
      $(this).css('border', "solid 0px"); 
      });
     
 	  subscription_id = selected.parent().attr( "subscription_id" );
    
 	  $("#theme").val(selected.parent().attr( "name" )) ;
	  $("#theme_file").val(selected.parent().attr("theme_file")) ;
	
      //selected.css('border', "solid 5px #708B9F"); 
	  $(".slick-slide").removeClass("slick-current");
	  selected.parent().parent().parent().addClass("slick-current");
 
     });


     $(".slick-dots").css("text-align", "center");
     $(".slick_template_links").css("color", "#a5bcb4");
     $(".slick_template_links").css("font-weight", "bold");
     $(".slick_template_links").css("text-decoration", "underline");
	 
	 });
	 
</script>