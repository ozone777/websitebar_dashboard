 <div class="row">
 <div class="col-md-12">

<section class="regular slider">
 
 @foreach($barthemes as $bartheme)
 
 <div name ="{{$bartheme->name}}" subscription_id="{{$subscription_id}}" template_url ="{{$bartheme->template_url}}">
 <img src="/themes/{{$bartheme->image}}" >
 </div>

 @endforeach 

 </section>
 
 </div>
 </div>
 
 
  <script>
	 
 	 $(document).on('ready', function() {
	 
      $(".regular").slick({
       dots: true,
       infinite: true,
       slidesToShow: 4,
       slidesToScroll: 4
       });


      $("img").click(function() {

       selected = $(this);

       $('img').each(function () {
       $(this).css('border', "solid 0px"); 
       });
     
	 
  	 subscription_id = selected.parent().attr( "subscription_id" );
    
  	$("#theme_"+subscription_id).val(selected.parent().attr( "name" )) ;
	
       selected.css('border', "solid 5px #708B9F"); 
 
      });


      $(".slick-dots").css("text-align", "center");
      $(".slick_template_links").css("color", "#a5bcb4");
      $(".slick_template_links").css("font-weight", "bold");
      $(".slick_template_links").css("text-decoration", "underline");
	 
 	 });
	 
 </script>