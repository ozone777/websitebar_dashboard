<div class="row">
	<div class="col-md-12">				
		
		<div style="font-size:20px; margin-bottom: 10px" data-countdown="{{$last_barserver->created_at->addMinutes($creating_delay)->format('Y/m/d H:i:s')}}"></div>
		
		<img style="margin-bottom: 10px" src="/gifs/barcreation.gif">
		
		
	</div>
</div>


<script>
	
	
	$(document).ready(function(){
		
		//////////////////////////
		//VPS CREATION TIME LOGIC
		//////////////////////////
	
		var offset = new Date().getTimezoneOffset();
	
		$('[data-countdown]').each(function() {
		var $this = $(this), finalDate = $(this).data('countdown');
	  
		console.log(finalDate);
	  
		datex = new Date(finalDate);
		// console.log(datex);
		datex.setMinutes( datex.getMinutes() - offset );
		datex_string = $.format.date(datex, 'yyyy/MM/dd H:mm:ss');
		console.log(datex_string);
	   
		$this.countdown(datex_string, function(event) {
			$this.html(event.strftime('%M:%S'));
		
		}).on('finish.countdown', function() {
			location.reload();
		});
		});
	
	});
	

	
</script>
