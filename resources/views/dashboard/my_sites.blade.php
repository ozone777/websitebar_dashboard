@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')


	<div class="row">
		<div class="col-md-12">
			
			@if($admin)
				<p>Selected user: {{$user->user_email}}</p>
			@endif
			
			<h1>{{trans('dashboard.my_sites')}}</h1>
		</div>
	</div>
		
<!-- code started -->			
<?php
				
$subscriptions = wcs_get_users_subscriptions($user->ID);
$cont = 0;
foreach ($subscriptions as  $key => $subscription ){
					
	$subscription_status = $subscription->post->post_status;
	$subscription_id = $subscription->post->ID;
	$order_id = $subscription->order->post->ID; // order ID (corresponding to the subscription ID)
	$active_subscriptions_arr[] = $subscription->post->ID;
	$order_items = $subscription->get_items();
	$user_id = $subscription->get_user_id();
						
	if(in_array($subscription_id, $subscription_ids)){
													
		$diff = $my_barservers[$subscription_id]->created_at->diffInMinutes($now);
		
		if($my_barservers[$subscription_id]->created_at->addMinutes($creating_delay) < $now){
		
		echo '<div class="row">';
		echo '<div class="col-md-12">';
		echo '<h3>Server Name: '.$my_barservers[$subscription_id]->linode_label.'</h3>';
		echo '</div>';
		echo '</div>';
											
			//////////////////////////////////////////
			//BARSERVER CREADO HACE MAS DE 30 MINUTOS
			//////////////////////////////////////////
							
							
			foreach ( $order_items as $key => $item ) {
						
				$_product = get_product($item[product_id]);
								
				echo '<div class="row">';
				echo '<div class="col-md-12">';
						       
				echo '<p>' .get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang)).'</p>';
				echo '</div>';
				echo '</div>';
			}
							
			?>
							

			<div class="row">
				<div class="col-md-6">
		
					<input class="button sites_list_action" id="sites_list_action_{{$subscription_id}}" email= "{{$user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.sites')}}"> 
		
					<input class="button sites_trash_action" email= "{{$user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.trash')}}"> 
		
					<input class="button sites_backup_action" email= "{{$user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.backups')}}">
		
					</div>
		
					<div class="col-md-6">
		
					<div id="disk_total_size_{{$subscription_id}}"></div>
		
					<div id="disk_used_{{$subscription_id}}"></div>
		
					<div id="disk_free_{{$subscription_id}}"></div>	
		
				</div>
			</div>

			<div class="row">
				<div class="col-md-12">
		
					<div id="plan_div_{{$subscription_id}}">
		

				<table class="table table-hover table-striped barsites_table" width="100%" email= "{{$user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_status="{{$my_barservers[$subscription_id]->on}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}"  id="subscription_table_{{$subscription_id}}">
					<tr>
			
						<th>URL</th>
						<th>{{trans('dashboard.theme')}}</th>
			
						<th>{{trans('dashboard.wordpress_access')}}</th>
						<th>{{trans('dashboard.options')}}</th>
						<th></th>
					</tr>
		
					<tbody>
			
					</tbody>
		
					</table>
				</div>
	
	  
				</div>
			</div>
	

			
						
			<?php
						
		}
						
	}
						
	$cont ++;
}
?>
			

<br />
<br />
		
</div>
</div>

	
<script>	
	
	
//////////////////////////
//CRUD LOGIC PRO
//////////////////////////
function crud_logic(object,dashboard_route,pete_route){
			
		object.unbind().click(function() {
			
			if(dashboard_route=="/delete_barsite"){
				
			    if(!window.confirm("{{trans('dashboard.are_you_sure_delete_barsite')}}")){
			    	return false;
			    }
				
			}else if(dashboard_route=="/destroy_barsite"){
				
			    if(!window.confirm("{{trans('dashboard.are_you_sure_destroy_barsite')}}")){
			    	return false;
			    }
				
			}else if(dashboard_route=="/suspend_barsite"){
				
			    if(!window.confirm("{{trans('dashboard.are_you_sure_suspend_barsite')}}")){
			    	return false;
			    }
				
			}
			
			pete_id = $(this).attr("pete_id");
			barsite_id = $(this).attr("barsite_id");
			var EMAIL = $(this).attr('email');
			var PETE_TOKEN = $(this).attr('pete_token');
			var BARSERVER_IP =$(this).attr('barserver_ip'); 
			table_id =  $(this).attr("table_id");
			backup_id = $(this).attr("backup_id");
		
			pete_url = "http://"+BARSERVER_IP+pete_route;
		
			if($(".cssload-loader").length == 0){
				$("#loading_area").append('<div class="cssload-loader"></div>');
			}
	
			$.ajax({
				url: dashboard_route,
				dataType: 'JSON',
				type: 'GET',
				data: {pete_id: pete_id, barsite_id: barsite_id, table_id: table_id, email: EMAIL, pete_token: PETE_TOKEN, barserver_ip: BARSERVER_IP, backup_id: backup_id},
				success : function(data) {
			
					if(data["info"] != ""){
		
						$.notify({
							icon: 'pe-7s-arc',
							message: data["info"]

						},{
							type: 'info',
							timer: 4000
						});
					}
				
					//GO TO PETE/////////////////
					/////////////////////////////
			
					console.log("Debug connection");
					console.log("url: "+pete_url);
					console.log("email: "+EMAIL);
					console.log("pete_token: "+PETE_TOKEN);
					console.log("uniqid: "+data["uniqid"]);
				
					$.ajax({
						url: pete_url,
						dataType: 'JSONP',
						type: 'GET',
						data: {email : data["email"], pete_token: data["pete_token"], barserver_ip: data["barserver_ip"], pete_id: data["pete_id"], uniqid: data["uniqid"], name: data["barsite"].name, wp_user: data["barsite"].wp_user,  subdomain: data["subdomain"], table_id: data["table_id"], barsite_id: data["barsite"].id, backup_id: data["backup_id"]},
						success : function(result) {
			
							console.log("Exitoo!");
							console.log(result["table_id"]);
							console.log(result);
						    
							reload_pete(result['barserver_ip'],result['email'],result['pete_token']);
							
							/*
							
							if((dashboard_route=="/delete_barsite") || (dashboard_route=="/destroy_barsite")){
				
							   $("#barsite_"+result["site"].barsite_id).remove();
				
							}else if(dashboard_route=="/clone_barsite"){
								add_row(result,"first");
								crud_logic($(".delete_site"),"/delete_barsite","/delete_site");
								crud_logic($(".clone_site"),"/clone_barsite","/clone_site");
								crud_logic($(".continue_site"),"/continue_barsite","/continue_site");
								crud_logic($(".suspend_site"),"/suspend_barsite","/suspend_site");
								show_password_logic();
							}else if((dashboard_route=="/suspend_barsite") || (dashboard_route=="/continue_barsite")){
								add_row(result,"replace");
								crud_logic($(".delete_site"),"/delete_barsite","/delete_site");
								crud_logic($(".clone_site"),"/clone_barsite","/clone_site");
								crud_logic($(".continue_site"),"/continue_barsite","/continue_site");
								crud_logic($(".suspend_site"),"/suspend_barsite","/suspend_site");
								show_password_logic();
							}else{
								redirect_to_sites();
							}
							*/
							if((dashboard_route=="/delete_barsite") || (dashboard_route=="/destroy_barsite")){
				
							   $("#barsite_"+result["site"].barsite_id).remove();
							   $(".cssload-loader" ).remove();	
				
							}else if((dashboard_route=="/clone_barsite") || (dashboard_route=="restore_barsite_from_backup")){
							    redirect_to_sites("create");
							}else{
								redirect_to_sites();
							}
							
						}
					
					});
				
				},
		        error: function(xhr, resp, text) {
			
		            console.log(xhr, resp, text);
					location.reload();
		        }
					
			});
	
	
			return false;
		
		});
	
}


//////////////////////////
//PUBLISH LOGIC  
//////////////////////////

function publish_logic(){
	
$(".publish").unbind().click(function() {
	
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/publish_site";
	subscription_id = $(this).attr("subscription_id");
	pete_id = $(this).attr("pete_id");
	barsite_id = $(this).attr("barsite_id");
	barserver_id = $(this).attr("barserver_id");
	table_id =  $(this).attr("table_id");
	
	db_name = "db_"+randomString(9);
	db_user = "user_"+randomString(9);
	db_user_pass = randomString(9);
	
	console.log("Debug connection");
	console.log("url: "+pete_url);
	console.log("email: "+EMAIL);
	console.log("pete_token: "+PETE_TOKEN);
	
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
    $.ajax({
        url: '/get_domains', // url where to submit the request
        type : "GET", // type of action POST || GET
        dataType : 'json', // data type
        data : {barserver_id: barserver_id},
        success : function(result) {
           
            console.log(result);
			htmlform="";
			
			htmlform+='<select id="publish_domain_'+barsite_id+'" name="bardomain_id">';
			htmlform+='<option value="0">'+"{{trans('dashboard.select_domain')}}"+'</option>';					
			for (var item in result) {
				htmlform+='<option value="'+result[item].id+'">'+result[item].domain_zone+'</option>';
			}
			
			htmlform+="</select>";
			
			
			htmlform+="<a style='padding-left: 50px; text-decoration: underline;' href='/bardomains/create'>"+"{{trans('dashboard.create_domain')}}"+"</a>"
			$(".cssload-loader").remove();
			 var publish_dialog = BootstrapDialog.show({
			            title: "{{trans('dashboard.publish')}}",
			            message: $(htmlform),
				
				buttons: [{
				                label: "{{trans('dashboard.publish')}}",
				                action: function(dialog) {
				                    bardomain_id = $("#publish_domain_"+barsite_id).val();
									
									if($(".cssload-loader").length == 0){
										$("#loading_area").append('<div class="cssload-loader"></div>');
									}
									
									$.ajax({
										url: "/publish_barsite",
										type: 'GET',
										dataType: 'json',
										data: {pete_id: pete_id, barsite_id: barsite_id, table_id: table_id, email: EMAIL, pete_token: PETE_TOKEN, barserver_ip: BARSERVER_IP, bardomain_id: bardomain_id},
										success : function(result) {
											console.log("success");
											
											if(result["message"] != ""){
				
												$.notify({
													icon: 'pe-7s-arc',
													message: result["message"]

												},{
													type: 'info',
													timer: 4000
												});
				
												$(".cssload-loader" ).remove();
												dialog.close();
												return false;	
											}
											
											$.ajax({
												url: pete_url,
												dataType: 'JSONP',
												type: 'GET',
												data: {email : result["email"], pete_token: result["pete_token"], barserver_ip: result["barserver_ip"], pete_id: result["pete_id"], name: result["barsite"].name, table_id: result["table_id"], barsite_id: result["barsite"].id, domain: result["domain"]},
												success : function(resultb) {
													console.log(resultb);
													
													dialog.close();
													reload_pete(resultb["barserver_ip"],resultb["email"],resultb["pete_token"]);
													redirect_to_sites();
												}		
											});		
											
										}		
									});		
									
				                }
				            }]
				
			        });
			
        },
        error: function(xhr, resp, text) {
			
            console.log(xhr, resp, text);
			location.reload();
        }
    })
	
	return false;
 });
}


//////////////////////////
//SITES BACKUPS  
//////////////////////////

$(".sites_backup_action").click(function() {
		
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/list_backups";
	subscription_id = $(this).attr("subscription_id");
	barserver_id = $(this).attr("barserver_id");
	
	$('#plan_div_'+subscription_id).html("");
		
	$.ajax({
		url: pete_url,
		dataType: 'JSONP',
		type: 'GET',
		data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, barserver_ip: BARSERVER_IP},
		success : function(result) {
			//alert("successss");
			//console.log(result);
			sites = result["sites"];
		   
			html='';
			html+='<div class="content table-responsive table-full-width">';
			html+='<table class="table table-hover table-striped barsites_table" email= "'+result["email"]+'"'; 				
			html+='pete_token="'+result["pete_token"]+'" barserver_ip= "'+result["barserver_ip"]+'"'; 			
			html+='barserver_status="'+result["barserver_status"]+'" barserver_id="'+result["barserver_id"]+'" subscription_id='; 			
			html+='"'+result["subscription_id"]+'"  id="subscription_table_'+result["subscription_id"]+'">';
			html+='<thead>';
			html+='<th>{{trans("dashboard.file")}}</th>';
			html+='<th>URL</th>';
			html+='<th>{{trans("dashboard.created_at")}}</th>';
			html+='<th>{{trans("dashboard.options")}}</th>';
			html+='</thead>';
			html+='<tbody>';
            html+='</tbody>';
			html+='</table>';
			html+='</div>';
			
			$('#plan_div_'+result["subscription_id"]).html(html)
			
			
			for (var item in sites) {
				
				html='';
				html+='<tr id="barsite_'+sites[item].barsite_id+'">';
				html+='<td>'+sites[item].file_name+'</td>';
				html+='<td><a target="_blank" href="http://'+sites[item].url+'">'+sites[item].url+'</a></td>';
				html+='<td>'+sites[item].created_at+'</td>';
				
				html+="<td>";
				html+='<a class="btn btn-xs restore_from_backup" pete_id="'+sites[item].site_id+'" barserver_id="'+result["barserver_id"]+'" backup_id="'+sites[item].id+'" barserver_ip="'+result["barserver_ip"]+'" email="'+result["email"]+'" pete_token="'+result["pete_token"]+'" subscription_id="'+result["subscription_id"]+'" barsite_id="'+sites[item].barsite_id+'" href="/">'+"{{trans('dashboard.recover')}}"+' </a>';
						
				html+='</tr>';
						
				$('#subscription_table_'+result["subscription_id"]+' tr:last').after(html);
				
						
			}		
			$(".cssload-loader" ).remove();	
			
			crud_logic($(".restore_from_backup"),"/restore_barsite_from_backup","/restore_site_from_backup");
					
		},
		error: function(xhr, resp, text) {
			
            console.log(xhr, resp, text);
			location.reload();
        }		
	});		
});



//////////////////////////
//SITES TRASH  
//////////////////////////
$(".sites_trash_action").click(function() {
		
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/list_trash";
	subscription_id = $(this).attr("subscription_id");
	barserver_id = $(this).attr("barserver_id");
	
	$('#plan_div_'+subscription_id).html("");
		
	$.ajax({
		url: pete_url,
		dataType: 'JSONP',
		type: 'GET',
		data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, barserver_ip: BARSERVER_IP},
		success : function(result) {
			//alert("successss");
			//console.log(result);
			sites = result["sites"];
				
			html='';
			html+='<div class="content table-responsive table-full-width">';
			html+='<table class="table table-hover table-striped barsites_table" email= "'+result["email"]+'"'; 				
			html+='pete_token="'+result["pete_token"]+'" barserver_ip= "'+result["barserver_ip"]+'"'; 			
			html+='barserver_status="'+result["barserver_status"]+'" barserver_id="'+result["barserver_id"]+'" subscription_id='; 			
			html+='"'+result["subscription_id"]+'"  id="subscription_table_'+result["subscription_id"]+'">';
			html+='<thead>';
			html+='<th>{{trans("dashboard.project")}}</th>';
			html+='<th>{{trans("dashboard.delete_at")}}</th>';
			html+='<th>{{trans("dashboard.options")}}</th>';
			html+='</thead>';
			html+='<tbody>';
            html+='</tbody>';
			html+='</table>';
			html+='</div>';
			
			$('#plan_div_'+result["subscription_id"]).html(html)
			
			
			for (var item in sites) {
										
				html='';
				html+='<tr id="barsite_'+sites[item].barsite_id+'">';
				html+='<td><a target="_blank" href="http://'+sites[item].url+'">'+sites[item].url+'</a></td>';
				html+='<td>'+sites[item].updated_at+'</td>';
				html+="<td>";
				
				html+='<a class="btn btn-xs restore_site" barserver_id="'+result["barserver_id"]+'" pete_id="'+sites[item].id+'" barserver_ip="'+result["barserver_ip"]+'" email="'+result["email"]+'" pete_token="'+result["pete_token"]+'" subscription_id="'+result["subscription_id"]+'" barsite_id="'+sites[item].barsite_id+'" href="/">'+"{{trans('dashboard.recover')}}"+' </a>';
				html+="<br />";

				html+=' <a class="btn btn-xs btn-danger destroy_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.destroy')}}"+'</a>';
				html+="<td>";
				html+="<br />";					
				html+='</tr>';
						
				$('#subscription_table_'+result["subscription_id"]+' tr:last').after(html);
						
			}		
			$(".cssload-loader" ).remove();	
			
			crud_logic($(".restore_site"),"/restore_barsite","/restore_site");
			crud_logic($(".destroy_site"),"/destroy_barsite","/destroy_site");
					
		},
		error: function(xhr, resp, text) {
			
            console.log(xhr, resp, text);
			location.reload();
        }			
	});		
});



//////////////////////////
//ADITIONAL ACTIONS  
//////////////////////////

function add_row(item,position){
	
	html='';
	if(position=="first"){
		html+='<tr id="barsite_'+item["site"].barsite_id+'">';
	}
	html+='<td width="10%">'+item["site"].name+'</td>';
	html+='<td width="15%">';
	html+='<a target="_blank" href="http://'+item["site"].url+'">'
	html+=item["site"].url+"</a>";
	html+='</td>';
	html+='<td width="15%">'+item["site"].theme+'</td>';
	html+='<td width="5%">'+item["site"].app_name+'</td>';
	html+='<td width="15%">';
	html+="{{trans('dashboard.user')}}: "
	html+=item["site"].wp_user+"<br/ >"
	html+='<div id="wp_pass_'+item["site"].barsite_id+'">';
	html+='<a class="show_wp_password" barsite_id ="'+item["site"].barsite_id+'" href="">'+"{{trans('dashboard.show_password')}}"+'</a></div>';
	html+="{{trans('dashboard.admin_url')}}: <br/ >"
	html+='<a target="_blank" href="http://'+item["site"].url+'/wp-admin">'+item["site"].url+'/wp-admin</a>';
	html+='</td>';
	html+='<td width="30%">';
	if(item["site"].published != true){
		html+='<a class="btn btn-xs publish" barserver_id="'+item["barserver_id"]+'" pete_id="'+item["site"].id+'" barserver_ip="'+item["barserver_ip"]+'" email="'+item["email"]+'" pete_token="'+item["pete_token"]+'" subscription_id="'+item["subscription_id"]+'" barsite_id="'+item["site"].barsite_id+'" href="/">'+"{{trans('dashboard.publish')}}"+' </a>';
		html+="<br />";	
		
	}
	html+=' <a class="btn btn-xs clone_site" table_id="'+item["table_id"]+'" email="'+item["email"]+'" pete_token ="'+item["pete_token"]+'" barserver_ip="'+item["barserver_ip"]+'" pete_id="'+item["site"].id+'" barsite_id="'+item["site"].barsite_id+'">'+"{{trans('dashboard.clone')}}"+'</a>';
	html+="<br />";	
	
	if(item["site"].suspend==true){
		html+=' <a class="btn btn-xs continue_site" email="'+item["email"]+'" pete_token ="'+item["pete_token"]+'" barserver_ip="'+item["barserver_ip"]+'" pete_id="'+item["site"].id+'" barsite_id="'+item["site"].barsite_id+'">'+"{{trans('dashboard.continue')}}"+'</a>';
		html+="<br />";	
	}else{
		html+=' <a class="btn btn-xs btn-danger suspend_site" email="'+item["email"]+'" pete_token ="'+item["pete_token"]+'" barserver_ip="'+item["barserver_ip"]+'" pete_id="'+item["site"].id+'" barsite_id="'+item["site"].barsite_id+'">'+"{{trans('dashboard.suspend')}}"+'</a>';
		html+="<br />";	
	}	
	html+=' <a class="btn btn-xs btn-danger delete_site" email="'+item["email"]+'" pete_token ="'+item["pete_token"]+'" barserver_ip="'+item["barserver_ip"]+'" pete_id="'+item["site"].id+'" barsite_id="'+item["site"].barsite_id+'">'+"{{trans('dashboard.delete')}}"+'</a>';	
	html+="<br />";					
	html+='</td>';
	
	if(position=="first"){
		html+='</tr>';
	}
	
	if(position=="first"){
		$('#'+item["table_id"]+' tr:first').after(html);
	}else if(position=="replace"){
		$('#barsite_'+item["site"].barsite_id).html(html);
	}
	
	
}


//////////////////////////
//SHOW PASSWORD LOGIC  
//////////////////////////
function show_password_logic(){
	
$(".show_wp_password").unbind().click(function() {
	
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
	$.ajax({
		url: '/show_wp_pass', // url where to submit the request
		type : "GET", // type of action POST || GET
		dataType : 'json', // data type
		data: {barsite_id: $(this).attr("barsite_id")},
		success : function(result) {

			console.log(result);
			$("#wp_pass_"+result["barsite_id"]).html(result["password"]);
			$(".cssload-loader" ).remove();
		},
		error: function(xhr, resp, text) {
			
            console.log(xhr, resp, text);
			location.reload();
        }	

	});
	return false;
});

}

function set_server_size(label,subscription_id,size_title,size){
	$("#"+size_title+subscription_id).html("<p>"+label+": "+size+"</p>");
}



////////////////////////////
///COUNTDOWN LOGIC
////////////////////////////

function countdown_logic(){
	
	var offset = new Date().getTimezoneOffset();
	
	$('[data-countdown]').each(function() {
		var $this = $(this), finalDate = $(this).data('countdown');
	  
		//console.log(finalDate);
	  
		datex = new Date(finalDate);
		// console.log(datex);
		datex.setMinutes( datex.getMinutes() - offset );
		datex_string = $.format.date(datex, 'yyyy/MM/dd H:mm:ss');
		//console.log(datex_string);
	   
		$this.countdown(datex_string, function(event) {
			$this.html(event.strftime('%M:%S'));
		
		}).on('finish.countdown', function() {
			//location.reload();
			redirect_to_sites();
		});
	});

}

//////////////////////////
//SITES LIST  
//////////////////////////

function display_rows(conditional_time,result){
	
	sites = result["sites"];
	
	for (var item in sites) {
		
		var date = new Date(sites[item].created_at);
		
		if(sites[item].published == true){
			
			html='';
			html+='<tr id="barsite_'+sites[item].barsite_id+'">';
			html+='<td width="20%">';
			html+='<a target="_blank" href="http://'+sites[item].url+'">'
			html+=sites[item].url+"</a>";
			html+='</td>';
			
			html+='<td width="10%">'+sites[item].theme+'</td>';
			
			html+='<td width="30%">';
			html+="{{trans('dashboard.user')}}: "
			html+=sites[item].wp_user+"<br/ >"
			html+='<div id="wp_pass_'+sites[item].barsite_id+'">';
			html+='<a class="show_wp_password" barsite_id ="'+sites[item].barsite_id+'" href="">'+"{{trans('dashboard.show_password')}}"+'</a></div>';
			html+="{{trans('dashboard.admin_url')}}: <br/ >"
			html+='<a target="_blank" href="http://'+sites[item].url+'/wp-admin">'+sites[item].url+'/wp-admin</a>';
			html+='</td>';
			
			html+='<td width="30%">';
			
			html+=' <a class="btn btn-xs clone_site" table_id="subscription_table_'+result["subscription_id"]+'" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.clone')}}"+'</a>';
			html+="<br />";
		
			if(sites[item].suspend==true){
				html+=' <a class="btn btn-xs continue_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.continue')}}"+'</a>';
				html+="<br />";
			}else{
				html+=' <a class="btn btn-xs btn-danger suspend_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.suspend')}}"+'</a>';
				html+="<br />";
			}	
		
			html+=' <a class="btn btn-xs btn-danger delete_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.delete')}}"+'</a>';	
			html+="<br />";	
			
			html+='</td>';
			
			html+='<td width="10%">';
			html+='</td>';
			
		}else{
			
			site_url = sites[item].url;
			html='';
			html+='<tr id="barsite_'+sites[item].barsite_id+'">';
		
			html+='<td width="20%">';
			if(date < conditional_time){
				html+='<a target="_blank" href="http://'+sites[item].url+'">'
				html+=sites[item].url+"</a>";
			}else{
				html+=sites[item].url;
			}
			html+='</td>';
			html+='<td width="10%">'+sites[item].theme+'</td>';
		
			html+='<td width="30%">';
			if(date < conditional_time){
				html+="{{trans('dashboard.user')}}: "
				html+=sites[item].wp_user+"<br/ >"
				html+='<div id="wp_pass_'+sites[item].barsite_id+'">';
				html+='<a class="show_wp_password" barsite_id ="'+sites[item].barsite_id+'" href="">'+"{{trans('dashboard.show_password')}}"+'</a></div>';
				html+="{{trans('dashboard.admin_url')}}: <br/ >"
				html+='<a target="_blank" href="http://'+sites[item].url+'/wp-admin">'+sites[item].url+'/wp-admin</a>';
			}
		html+='</td>';
		
		html+='<td width="30%">';
		
		if(date < conditional_time){
		
		if(sites[item].published != true){
			
			html+=' <a class="btn btn-xs publish" barserver_id="'+result["barserver_id"]+'" table_id="subscription_table_'+result["subscription_id"]+'"  email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.publish')}}"+'</a>';
			html+="<br />";
			
		}
		
		html+=' <a class="btn btn-xs clone_site" table_id="subscription_table_'+result["subscription_id"]+'" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.clone')}}"+'</a>';
		html+="<br />";
		
		if(sites[item].suspend==true){
			html+=' <a class="btn btn-xs continue_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.continue')}}"+'</a>';
			html+="<br />";
		}else{
			html+=' <a class="btn btn-xs btn-danger suspend_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.suspend')}}"+'</a>';
			html+="<br />";
		}	
		
		html+=' <a class="btn btn-xs btn-danger delete_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.delete')}}"+'</a>';	
		html+="<br />";	
		
		}			
		
		html+='</td>';
		
		html+='<td width="10%">';

		if(date > conditional_time){
			date.setMinutes( date.getMinutes() + {{$creating_delay}} );
			date_string=$.format.date(date, 'yyyy/MM/dd H:mm:ss');
			html+='<div barsite_id="'+sites[item].barsite_id+'" style="font-size:20px; margin-bottom: 10px" data-countdown="'+date_string+'"></div>'
			html+='<img id="site_animation_'+sites[item].barsite_id+'" src="/gifs/tenor.gif">';
		}
		
		html+='</td>';
		html+='</tr>';
		
		}
	
		$('#subscription_table_'+result["subscription_id"]+' tr:last').after(html);
		
			
	}		
	
}

$(".sites_list_action").unbind().click(function() {
		
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/list_sites";
	subscription_id = $(this).attr("subscription_id");
	barserver_id = $(this).attr("barserver_id");
	
	$('#plan_div_'+subscription_id).html("");
	
	$.ajax({
		url: pete_url,
		dataType: 'JSONP',
		type: 'GET',
		data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, barserver_ip: BARSERVER_IP},
		success : function(result) {
			//alert("successss");
			//console.log(result);
			
			html='';
			html+='<div class="content table-responsive table-full-width">';
			html+='<table class="table table-hover table-striped barsites_table" email= "'+result["email"]+'"'; 				
			html+='pete_token="'+result["pete_token"]+'" barserver_ip= "'+result["barserver_ip"]+'"'; 			
			html+='barserver_status="'+result["barserver_status"]+'" barserver_id="'+result["barserver_id"]+'" subscription_id='; 			
			html+='"'+result["subscription_id"]+'"  id="subscription_table_'+result["subscription_id"]+'">';
			html+='<thead>';
			
			html+='<th>URL</th>';
			html+='<th>{{trans("dashboard.theme")}}</th>';
			
			html+='<th>{{trans("dashboard.wordpress_access")}}</th>';
			html+='<th>{{trans("dashboard.options")}}</th>';
			html+='</thead>';
			html+='<tbody>';
            html+='</tbody>';
			html+='</table>';
			html+='</div>';
			
			$('#plan_div_'+result["subscription_id"]).html(html)
			
		    conditional_time = new Date(result["server_time"]);
			conditional_time.setMinutes(conditional_time.getMinutes() - {{$creating_delay}});
			
			display_rows(conditional_time,result);
			
			$(".cssload-loader" ).remove();	
			show_password_logic();	
			publish_logic();
			
			crud_logic($(".delete_site"),"/delete_barsite","/delete_site");
			crud_logic($(".clone_site"),"/clone_barsite","/clone_site");
			crud_logic($(".continue_site"),"/continue_barsite","/continue_site");
			crud_logic($(".suspend_site"),"/suspend_barsite","/suspend_site");
			
			countdown_logic();		
		},
		error: function(xhr, resp, text) {
			
            console.log(xhr, resp, text);
			location.reload();
        }			
	});		
});


$(document).ready(function(){
		
	//////////////////////////
	//LOADING EVERY LINODE VPS 
	//////////////////////////
	
	
			
	$(".barsites_table").each(function(index, element) {
			
		
		EMAIL = $(this).attr("email");
		PETE_TOKEN = $(this).attr("pete_token");
		BARSERVER_IP = $(this).attr("barserver_ip");
		pete_url = "http://"+BARSERVER_IP+"/list_sites";
		subscription_id = $(this).attr("subscription_id");
		barserver_id = $(this).attr("barserver_id");
		
		if($(this).attr("barserver_status") == "1"){
			
			if($(".cssload-loader").length == 0){
				$("#loading_area").append('<div class="cssload-loader"></div>');
			}
			
			
		$.ajax({
			url: pete_url,
			dataType: 'JSONP',
			type: 'GET',
			data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, barserver_ip: BARSERVER_IP},
			success : function(result) {
				//alert("successss");
				//console.log(result);
				sites = result["sites"];
			    conditional_time = new Date(result["server_time"]);
				conditional_time.setMinutes(conditional_time.getMinutes() - {{$creating_delay}});
				
				display_rows(conditional_time,result);
				$(".cssload-loader" ).remove();	
				
				show_password_logic();	
				crud_logic($(".delete_site"),"/delete_barsite","/delete_site");
				crud_logic($(".clone_site"),"/clone_barsite","/clone_site");
				crud_logic($(".continue_site"),"/continue_barsite","/continue_site");
				crud_logic($(".suspend_site"),"/suspend_barsite","/suspend_site");
				publish_logic();
				
				set_server_size("{{trans('dashboard.disk_total_size')}}",result["subscription_id"],"disk_total_size_",result["disk_total_size"]);
				set_server_size("{{trans('dashboard.disk_used')}}",result["subscription_id"],"disk_used_",result["disk_used"]);
				set_server_size("{{trans('dashboard.disk_free')}}",result["subscription_id"],"disk_free_",result["disk_free"]);
				
				countdown_logic();
				
						
			}		
		});	
		
		}
			
	});
});

</script>
	

@endsection

