@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')

	<div class="row">
		<div class="col-md-12">
			<h1>{{trans('dashboard.buy_addons')}}</h1>
		</div>
	</div>
	
	<form method="get" action="{{$wordpress_url}}/checkout">
		<input type="hidden" name="fast_checkout_addons" value="true">
		
	<?php
	
    $args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => 'add-ons', 'orderby' => 'rand' );
    $loop = new WP_Query( $args );
	
	//Columns must be a factor of 12 (1,2,3,4,6,12)
	$numOfCols = 4;
	$rowCount = 0;
	$bootstrapColWidth = 12 / $numOfCols;
	?>
	<div class="row">
	<?php
	 while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
	
	        <div class="col-md-<?php echo $bootstrapColWidth; ?>">
	            <div class="thumbnail">
					<label><?php the_title(); ?></label>
					 <span class="price"><?php echo $product->get_price_html(); ?></span>  
	                <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.wc_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>
					
					<input id="number" class="number_field" name="quantity_{{$loop->post->ID}}" product_id="{{$loop->post->ID}}" type="number" value="0" min="0">
	            </div>
	        </div>
	<?php
	    $rowCount++;
	    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
	
	endwhile;
	wp_reset_query(); ?>
	
	</div>
	
	
	<div class="row">
		<div class="col-md-12">
			<input type="submit" class="et_manage_submit" value="{{trans('dashboard.buy')}}">
		</div>
	</div>
	
	</form>
	
	
	<div class="row">
		<div class="col-md-12">
			<h1>{{trans('dashboard.buy_addons_subscriptions')}}</h1>
		</div>
	</div>
	
	
	<?php
	
    $args = array( 'post_type' => 'product', 'posts_per_page' => -1, 'product_cat' => 'add-on-subscriptions', 'orderby' => 'rand' );
    $loop = new WP_Query( $args );
	
	//Columns must be a factor of 12 (1,2,3,4,6,12)
	$numOfCols = 4;
	$rowCount = 0;
	$bootstrapColWidth = 12 / $numOfCols;
	?>
	<div class="row">
	<?php
	 while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
	
	        <div class="col-md-<?php echo $bootstrapColWidth; ?>">
	            <div class="thumbnail">
					<label><?php the_title(); ?></label>
					 <span class="price"><?php echo $product->get_price_html(); ?></span>  
	                <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.wc_placeholder_img_src().'" alt="Placeholder" width="300px" height="300px" />'; ?>
	            </div>
				
				<form method="get" action="{{$wordpress_url}}/checkout">

				<input type="hidden" name="fast_checkout" value="true">
				<input type="hidden" name="product_id" value="{{$loop->post->ID}}">
				<input id="number" name="quantity" type="number" value="1" min="0">

				 <input type="submit" class="et_manage_submit" value="{{trans('dashboard.buy')}}">
				
	        </div>
	<?php
	    $rowCount++;
	    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
	
	endwhile;
	wp_reset_query(); ?>
	
	</div>

@endsection

