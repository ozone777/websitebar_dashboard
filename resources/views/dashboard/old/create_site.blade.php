@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')

@if(count($my_barservers) > 0) 
		

<div class="row">
<div class="col-md-6">   
	
						
								
		<select id="barserver_info" class="form-control">	
		    <option selected disabled>{{trans('dashboard.choose_server')}}</option>
			
<?php
				
$USER_ID = $wp_user->ID;
$subscriptions = wcs_get_users_subscriptions($USER_ID);
$cont = 0;
foreach ($subscriptions as  $key => $subscription ){
					
	$subscription_status = $subscription->post->post_status;
	$subscription_id = $subscription->post->ID;
	$order_id = $subscription->order->post->ID; // order ID (corresponding to the subscription ID)
	$active_subscriptions_arr[] = $subscription->post->ID;
	$order_items = $subscription->get_items();
	$user_id = $subscription->get_user_id();
						
	if(!in_array($subscription_id, $subscription_ids)){
							
							
	}else{
													
		$diff = $my_barservers[$subscription_id]->created_at->diffInMinutes($now);
							
		if($diff>=30){
								
			//////////////////////////////////////////
			//BARSERVER CREADO HACE MAS DE 30 MINUTOS
			//////////////////////////////////////////
							
		if( $subscription_status == 'wc-active' ){
	echo "<option data-subscription_id = '$subscription_id' data-barserver_id='".$my_barservers[$subscription_id]->id."' data-email= '$current_user->user_email' data-barserver_ip= '".$my_barservers[$subscription_id]->ip."' data-pete_token='$pete_tokens[$subscription_id]' value='".$my_barservers[$subscription_id]->id."'>".$my_barservers[$subscription_id]->linode_label."</option>";
			}
			
			?>							
						
			<?php
						
		}
	}
						
	$cont ++;
}

?>

</select>

<br />

	</div>
</div>


<div class="row">
	<div class="col-md-12">
		<form action="/create_websitebar" method="post">
			
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="wp_barserver_id" value="{{$my_barservers[$subscription_id]->id}}">
			<input type="hidden" name="user_id" value="{{$user_id}}">
			<input type="hidden" name="theme" id="theme" value="">
			<input type="hidden" name="theme_file" id="theme_file" value="">
			
			<input name="subdomain" placeholder="{{trans('dashboard.project_name')}}" id="subdomain" class="form-control subdomain_check" value="">

			<br /><br />
			@include('dashboard/_new_gallery')
			
			
	
	</div>
</div>
	
<div class="row">
	<div class="col-md-12">
		
			<input type="submit" class="et_manage_submit create_barsite" value="{{trans('dashboard.create_website')}}">
		
	</div>
	</div>
					
</form>


	</div>
</div>

<script>
	
//////////////////////////
//VPS CREATION TIME LOGIC
//////////////////////////
	
var offset = new Date().getTimezoneOffset();
	
$('[data-countdown]').each(function() {
var $this = $(this), finalDate = $(this).data('countdown');
	  
console.log(finalDate);
	  
datex = new Date(finalDate);
// console.log(datex);
datex.setMinutes( datex.getMinutes() - offset );
datex_string = $.format.date(datex, 'yyyy/MM/dd H:mm:ss');
console.log(datex_string);
	   
$this.countdown(datex_string, function(event) {
	$this.html(event.strftime('%M:%S'));
		
}).on('finish.countdown', function() {
	location.reload();
});
});
	
</script>
	
<script>	
		
//////////////////////////
//CREATE BARSITE 
//////////////////////////

$(".create_barsite").click(function() {  
	
	if($("#barserver_info").val() == null){
		
		$.notify({
			icon: 'pe-7s-arc',
			message: "{{trans('dashboard.choose_server_error')}}"

		},{
			type: 'info',
			timer: 4000
		});
		
		$(".cssload-loader" ).remove();
		return false;	
		
	}
	
	console.log("create_barsite");
	console.log("####################");
	
	site_name = $("#subdomain").val();
	site_name = site_name.toLowerCase();
	
	var sel = document.getElementById('barserver_info');
	var selected = sel.options[sel.selectedIndex];
	
	var EMAIL = selected.getAttribute('data-email');
	var PETE_TOKEN = selected.getAttribute('data-pete_token');
	var BARSERVER_IP = selected.getAttribute('data-barserver_ip'); 
	var subscription_id = selected.getAttribute('data-subscription_id'); 
	
	pete_url = "http://"+BARSERVER_IP+"/create_site";
	barserver_id = $("#barserver_info").val();
	theme = $("#theme").val();
	theme_file = $("#theme_file").val();	
	
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
	
	$.ajax({
		url: "/create_barsite",
		dataType: 'JSON',
		type: 'GET',
		data: {barserver_id: barserver_id, site_name: site_name, theme: theme, theme_file: theme_file},
		success : function(data) {
					
			if(data["message"] != ""){
				
				$.notify({
					icon: 'pe-7s-arc',
					message: data["message"]

				},{
					type: 'info',
					timer: 4000
				});
				
				$(".cssload-loader" ).remove();
				return false;	
			}
						
			//GO TO PETE/////////////////
			/////////////////////////////
			
			console.log("Debug connection");
			console.log("url: "+pete_url);
			console.log("email: "+EMAIL);
			console.log("pete_token: "+PETE_TOKEN);
				
			$.ajax({
				url: pete_url,
				dataType: 'JSONP',
				type: 'GET',
				data: {email: EMAIL, pete_token: PETE_TOKEN, site_name: data["barsite"].name, subdomain: data["subdomain"], theme: data["barsite"].theme, db_name: data["db_name"], db_user: data["db_user"], db_user_pass: data["db_user_pass"], wp_user: data["barsite"].wp_user, wp_pass: data["wp_pass"], barsite_id: data["barsite"].id,  theme_file: data["theme_file"]},
				success : function(result) {
				
					if(result["message"] != ""){
				
						$.notify({
							icon: 'pe-7s-arc',
							message: result["message"]

						},{
							type: 'info',
							timer: 4000
						});
						return false;
					}
					
					
					reload_pete(BARSERVER_IP,EMAIL,PETE_TOKEN);
					redirect_to_sites("create");
					
				}
					
			});
				
		}
					
	});
	
	
	return false;
		
});


	
$(document).ready(function(){
		
	
});

</script>

@else


<div class="row">
	<div class="col-md-6">   
		<!-- code started -->			
		<?php

		//////////////////////////////////////////
		//MOSTRAR BARSERVERS QUE NECESITAN SER ACTIVADOS
		//////////////////////////////////////////
				
		$USER_ID = $wp_user->ID;
		$subscriptions = wcs_get_users_subscriptions($USER_ID);
		$cont = 0;
		foreach ($subscriptions as  $key => $subscription ){
					
			$subscription_status = $subscription->post->post_status;
			$subscription_id = $subscription->post->ID;
			$order_id = $subscription->order->post->ID; // order ID (corresponding to the subscription ID)
			$active_subscriptions_arr[] = $subscription->post->ID;
			$order_items = $subscription->get_items();
			$user_id = $subscription->get_user_id();
						
			if(!in_array($subscription_id, $subscription_ids)){
		
				echo '<div class="row">';
						
				foreach ( $order_items as $key => $item ) {
					$_product = get_product($item[product_id]);
			
			
					if($subscription_status == 'wc-active' ){
				
						if (in_array($item[product_id], $plan_ids)) {
					
					
							echo '<div class="col-md-6">';   
							echo '<h4 class="title">' . get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang) ) .'</h4>';
							echo '</div>';
					
			
					?>
			
					<div class="col-md-6">		  	
						@include('dashboard/_no_initialized_plan')
					</div>
							
					<?php
			
					}
			
					}
				}
		
			 echo '</div>';
							
			}else{
													
				$diff = $my_barservers[$subscription_id]->created_at->diffInMinutes($now);
							
				if($diff < 30){
								
					//////////////////////////////////////////
					//BARSERVER CREADO HACE MENOS DE 30 MINUTOS
					//////////////////////////////////////////
							
							
					foreach ( $order_items as $key => $item ) {
						
						$_product = get_product($item[product_id]);
								
						echo '<div class="row">';
						echo '<div class="col-md-12">';
						       
						echo '<h4 class="title">' .get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang)).'</h4>';
						echo '</div>';
						echo '</div>';
					}
							
					?>
							
					@include('dashboard/_creating_barserver')
						
					<?php
						
				}
						
			}
						
			$cont ++;
		}

		?>
		
	</div>
</div>

@endif
	

@endsection

