
<div class="row">
	<div class="col-md-6">
		
		<input class="button sites_list_action" id="sites_list_action_{{$subscription_id}}" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.sites')}}"> 
		
		<input class="button sites_trash_action" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.trash')}}"> 
		
		<input class="button sites_backup_action" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.backups')}}">
		
		</div>
		
		<div class="col-md-6">
		
		<div id="disk_total_size_{{$subscription_id}}"></div>
		
		<div id="disk_used_{{$subscription_id}}"></div>
		
		<div id="disk_free_{{$subscription_id}}"></div>	
		
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		
		<div id="plan_div_{{$subscription_id}}">
		

	<table class="table table-hover table-striped barsites_table" width="100%" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_status="{{$my_barservers[$subscription_id]->on}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}"  id="subscription_table_{{$subscription_id}}">
		<tr>
			
			<th>URL</th>
			<th>{{trans('dashboard.theme')}}</th>
			
			<th>{{trans('dashboard.wordpress_access')}}</th>
			<th>{{trans('dashboard.options')}}</th>
			<th></th>
		</tr>
		
		<tbody>
			
		</tbody>
		
		</table>
	</div>
	
	  
	</div>
</div>
	

					