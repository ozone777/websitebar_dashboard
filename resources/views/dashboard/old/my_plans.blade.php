@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')
		
<!-- code started -->			
<?php
				
$USER_ID = $wp_user->ID;
$subscriptions = wcs_get_users_subscriptions($USER_ID);
$cont = 0;
foreach ($subscriptions as  $key => $subscription ){
					
	$subscription_status = $subscription->post->post_status;
	$subscription_id = $subscription->post->ID;
	$order_id = $subscription->order->post->ID; // order ID (corresponding to the subscription ID)
	$active_subscriptions_arr[] = $subscription->post->ID;
	$order_items = $subscription->get_items();
	$user_id = $subscription->get_user_id();
						
	if(!in_array($subscription_id, $subscription_ids)){
							
		/////////////////////
		//BARSERVER NO CREADO
		/////////////////////
		/*
		if (in_array("Irix", $os)) {
    		echo "Existe Irix";
		}
		*/
		foreach ( $order_items as $key => $item ) {
			$_product = get_product($item[product_id]);
			
			if (in_array($item[product_id], $plan_ids)) {
					
			echo '<div class="row">';
			echo '<div class="col-md-12">';   
								
			echo '<h4 class="title">' . get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang) ) .'</h4>';
			echo '</div>';
			echo '</div>';
			
			?>
						  	
			@include('dashboard/_no_initialized_plan')
							
			<?php
			
			}
		}
							
	}else{
													
		$diff = $my_barservers[$subscription_id]->created_at->diffInMinutes($now);
							
		if($diff>=30){
								
			//////////////////////////////////////////
			//BARSERVER CREADO HACE MAS DE 30 MINUTOS
			//////////////////////////////////////////
							
							
			foreach ( $order_items as $key => $item ) {
						
				$_product = get_product($item[product_id]);
								
				echo '<div class="row">';
				echo '<div class="col-md-12">';
						       
				echo '<h4 class="title">' .get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang)).'</h4>';
				echo '</div>';
				echo '</div>';
			}
							
			?>
							
			@include('dashboard/_initialized_plan')
						
			<?php
						
		}else{
							
			//////////////////////////////////////////
			//BARSERVER CREADO HACE MENOS DE 30 MINUTOS
			//////////////////////////////////////////
							
							
			foreach ( $order_items as $key => $item ) {
							
				$_product = get_product($item[product_id]);
				echo '<div class="row">';
				echo '<div class="col-md-12">';     
				echo '<h4 class="title">' . get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang) ) .'</h4>';
				echo '</div>';
				echo '</div>';
			}
							
			?>
							
			@include('dashboard/_creating_barserver')
							
			<?php
							
		}
						
	}
						
	$cont ++;
}
?>
			

<br />
<br />
		
</div>
</div>

<script>
	
//////////////////////////
//VPS CREATION TIME LOGIC
//////////////////////////
	
var offset = new Date().getTimezoneOffset();
	
$('[data-countdown]').each(function() {
var $this = $(this), finalDate = $(this).data('countdown');
	  
console.log(finalDate);
	  
datex = new Date(finalDate);
// console.log(datex);
datex.setMinutes( datex.getMinutes() - offset );
datex_string = $.format.date(datex, 'yyyy/MM/dd H:mm:ss');
console.log(datex_string);
	   
$this.countdown(datex_string, function(event) {
	$this.html(event.strftime('%M:%S'));
		
}).on('finish.countdown', function() {
	location.reload();
});
});
	
</script>
	
<script>	
	
//////////////////////////
//GET BARSERVER VPS INFO 
//////////////////////////
	
$(".get_barserver_info").on('click', function(){
	
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
	
	// send ajax
	barserver_id = $(this).attr("barserver_id")
	$.ajax({
		url: '/get_barserver_info', // url where to submit the request
		type : "POST", // type of action POST || GET
		dataType : 'json', // data type
		data: {barserver_id: $(this).attr("barserver_id"), _token: "{{csrf_token()}}"},
		success : function(result) {
               
			console.log(result);
			console.log(result["ip"]);
			html = "";
			html += "<p style='padding: 5px 5px 5px 5px; font-size: 11px'>SSH ACCESS <br/ >";
			html += "ssh -p 13737 root@"+result["ip"]+"<br/ >";
			html += "Password: "+result["ssh_info"]+"</p>";
				
			html += "<p style='padding: 5px 5px 5px 5px; font-size: 11px'>PHPMYADMIN ACCESS <br/ >";
			phpmyadminurl = "http://"+result["ip"]+"/phpmyadmin";
			html += "<a target='_blank' href='"+phpmyadminurl+"'>"+phpmyadminurl+"</a><br/ >";
			html += "Usename: root <br/ >";
			html += "Password: "+result["db_info"]+"</p>";
				
			html += "<p style='padding: 5px 5px 5px 5px; font-size: 11px'>WORDPRESSPETE ACCESS <br/ >";
			pete_url = "http://"+result["ip"];
			html += "<a target='_blank' href='"+pete_url+"'>"+pete_url+"</a><br/ >";
			html += "Usename: "+result["user_email"]+" <br/ >";
			html += "Password: "+result["pete_info"]+"<br/ >";
			html += "Pete token: "+result["pete_token"]+"</p>";
				
			$("#info_"+result["barserver_id"]).html(html);
			
			$(".cssload-loader" ).remove();
				
		},
		error: function(xhr, resp, text) {
			console.log(xhr, resp, text);
		}
	})
	return false;
});
	
	
//////////////////////////
//CREATE BARSITE 
//////////////////////////

$(".create_barsite").click(function() {  
		
	subscription_id = $(this).attr("subscription_id");
	site_name = $("#subdomain_"+subscription_id).val();
	site_name = site_name.toLowerCase();
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/import_wordpress_theme";
	barserver_id = $(this).attr("barserver_id");
	theme = $("#theme_"+subscription_id).val();
	
	db_name = "db_"+randomString(9);
	db_user = "user_"+randomString(9);
	db_user_pass = randomString(9);
	wp_user= "{{$current_user->user_nicename}}";
	wp_pass= randomString(9);
	
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
	
	$.ajax({
		url: "/save_barsite",
		dataType: 'JSON',
		type: 'GET',
		data: {barserver_id: barserver_id, site_name: site_name, theme: theme, db_name: db_name, db_user: db_user, db_user_pass: db_user_pass, wp_user: wp_user, wp_pass: wp_pass, subscription_id: subscription_id, user_nicename: "{{$current_user->user_nicename}}"},
		success : function(data) {
					
			if(data["message"] != ""){
				
				$.notify({
					icon: 'pe-7s-arc',
					message: data["message"]

				},{
					type: 'info',
					timer: 4000
				});
				
				$(".cssload-loader" ).remove();
				return false;	
			}
			
			if(data["info"] != ""){
		
				$.notify({
					icon: 'pe-7s-arc',
					message: data["info"]

				},{
					type: 'info',
					timer: 4000
				});
			}
				
			//GO TO PETE/////////////////
			/////////////////////////////
			
			console.log("Debug connection");
			console.log("url: "+pete_url);
			console.log("email: "+EMAIL);
			console.log("pete_token: "+PETE_TOKEN);
				
			$.ajax({
				url: pete_url,
				dataType: 'JSONP',
				type: 'GET',
				data: {email : EMAIL, pete_token: PETE_TOKEN, barserver_id: barserver_id, site_name: site_name, site_url: data["barsubdomain"].subdomain, theme: theme, db_name: db_name, db_user: db_user, db_user_pass: db_user_pass, wp_user: wp_user, wp_pass: wp_pass, subscription_id: subscription_id, user_nicename: "{{$current_user->user_nicename}}", barsite_id: data["site"].id, barserver_ip: BARSERVER_IP},
				success : function(data) {
			
					console.log(data["message"])
				
					if(data["message"] != ""){
				
						$.notify({
							icon: 'pe-7s-arc',
							message: data["message"]

						},{
							type: 'info',
							timer: 4000
						});
						return false;
					}
					
					
					
					
					html='';
					html+='<tr id="barsite_'+data["site"].barsite_id+'">';
					html+='<td><a target="_blank" href="http://'+data["site"].url+'">'+data["site"].url+'</a></td>';
					html+='<td>'+data["site"].theme+'</td>';
					html+='<td>'+data["site"].wp_user+'</td>';
					html+='<td><div id="wp_pass_'+data["site"].barsite_id+'"><a class="show_wp_password" barsite_id ="'+data["site"].barsite_id+'" href="">'+"{{trans('dashboard.show_password')}}"+'</a></div></td>';
					html+='<td><a target="_blank" href="http://'+data["site"].url+'/wp-admin">'+data["site"].url+'/wp-admin</a></td>	';
					//html+='<td><a class="btn btn-xs publish" barsite_id="'+data["site"].id+'" barserver_id="'+data["site"].barserver_id+'" href="/">'+"{{trans('dashboard.publish')}}"+' </a>';
					
					html+='<td><a class="btn btn-xs publish" barserver_id="'+data["site"].barserver_id+'" pete_id="'+data["site"].id+'" barserver_ip="'+data["barserver_ip"]+'" email="'+data["email"]+'" pete_token="'+data["pete_token"]+'" subscription_id="'+data["subscription_id"]+'" barsite_id="'+data["site"].barsite_id+'" href="/">'+"{{trans('dashboard.publish')}}"+' </a>';
					
					html+='<a class="btn btn-xs btn-danger delete_site" email="'+data["email"]+'" pete_token ="'+data["pete_token"]+'" barserver_ip="'+data["barserver_ip"]+'" pete_id="'+data["site"].id+'">'+"{{trans('dashboard.delete')}}"+'</a>';			
					html+='</tr>';
					$('#subscription_table_'+data["subscription_id"]+' tr:last').after(html);
	 
					$(".cssload-loader" ).remove();
					
					show_password_logic();
					delete_sites_logic();
					publish_logic();
					
					reload_pete(data["barserver_ip"],data["email"],data["pete_token"]);
					
				}
					
			});
				
		}
					
	});
		
	return false;
		
});



//////////////////////////
//RELOAD PETE LOGIC
//////////////////////////
function reload_pete(barserver_ip,email,pete_token){
    
	console.log("Reload pete");
	
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
	
	pete_url = "http://"+barserver_ip+"/reload_pete";
	
    var delayInMilliseconds = 3000; //1 second

    setTimeout(function() {
      //your code to be executed after 1 second
   	 $(".cssload-loader").remove();
    }, delayInMilliseconds);
	
		
	$.ajax({
		url: pete_url, // url where to submit the request
		type : "GET", // type of action POST || GET
		dataType : 'JSONP', // data type
		data: {email : email, pete_token: pete_token},
		
		success : function(data) {
			$(".cssload-loader" ).remove();
		},
		error: function(xhr, resp, text) {
			//console.log(xhr, resp, text);
			console.log("reload done!")
		}
		
	})
	

}


	
	
//////////////////////////
//DELETE SITE LOGIC
//////////////////////////
function delete_sites_logic(){

$(".delete_site").unbind().click(function() {
	// send ajax
	
    if(!window.confirm("{{trans('dashboard.are_you_sure')}}")){
    	return false;
    }
      
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
	
	PETE_ID = $(this).attr("pete_id");
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/delete_site";
	
	$.ajax({
		url: pete_url, // url where to submit the request
		type : "GET", // type of action POST || GET
		dataType : 'JSONP', // data type
		data: {email : EMAIL, pete_token: PETE_TOKEN, pete_id: PETE_ID, barserver_ip: BARSERVER_IP},
		success : function(data) {
			$("#barsite_"+data["site"].barsite_id).remove();
			$(".cssload-loader" ).remove();
			
			
			reload_pete(data['barserver_ip'],data['email'],data['pete_token']);
		},
		error: function(xhr, resp, text) {
			console.log(xhr, resp, text);
		}
	})
	return false;
});

}


//////////////////////////
//DESTROY SITE LOGIC
//////////////////////////
function destroy_sites_logic(){
	
$(".destroy_site").unbind().click(function() {
	
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/destroy_site";
	subscription_id = $(this).attr("subscription_id");
	pete_id = $(this).attr("pete_id");
	barsite_id = $(this).attr("barsite_id");
	barserver_id = $(this).attr("barserver_id");
	
	console.log("Debug connection");
	console.log("url: "+pete_url);
	console.log("email: "+EMAIL);
	console.log("pete_token: "+PETE_TOKEN);
	
    if(!window.confirm("{{trans('dashboard.are_you_sure_destroy')}}")){
    	return false;
    }
	
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}	
       			
	$.ajax({
		url: "/delete_barsite",
		type: 'GET',
		dataType: 'json',
		data: {barsite_id: barsite_id},
		success : function(result) {
		console.log("success");
											
			$.ajax({
				url: pete_url,
				dataType: 'JSONP',
				type: 'GET',
				data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, pete_id: pete_id},
				success : function(data) {
					console.log(result);
					$(".cssload-loader" ).remove();		
					//window.reload();	
					$("#barsite_"+data["barsite_id"]).remove();						
				}		

			});
			
        },
        error: function(xhr, resp, text) {
            console.log(xhr, resp, text);
        }
    })
	
	return false;
 });
}

//////////////////////////
//SHOW PASSWORD LOGIC  
//////////////////////////
function show_password_logic(){
	
$(".show_wp_password").unbind().click(function() {
	
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
	$.ajax({
		url: '/show_wp_pass', // url where to submit the request
		type : "GET", // type of action POST || GET
		dataType : 'json', // data type
		data: {barsite_id: $(this).attr("barsite_id")},
		success : function(result) {

			console.log(result);
			$("#wp_pass_"+result["barsite_id"]).html(result["password"]);
			$(".cssload-loader" ).remove();
		}

	});
	return false;
});

}

//////////////////////////
//PUBLISH LOGIC  
//////////////////////////
function publish_logic(){
	
$(".publish").unbind().click(function() {
	
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/publish_site";
	subscription_id = $(this).attr("subscription_id");
	pete_id = $(this).attr("pete_id");
	barsite_id = $(this).attr("barsite_id");
	barserver_id = $(this).attr("barserver_id");
	
	db_name = "db_"+randomString(9);
	db_user = "user_"+randomString(9);
	db_user_pass = randomString(9);
	
	console.log("Debug connection");
	console.log("url: "+pete_url);
	console.log("email: "+EMAIL);
	console.log("pete_token: "+PETE_TOKEN);
	
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
    $.ajax({
        url: '/get_domains', // url where to submit the request
        type : "GET", // type of action POST || GET
        dataType : 'json', // data type
        data : {barserver_id: barserver_id},
        success : function(result) {
           
            console.log(result);
			htmlform="";
			
			htmlform+='<select id="publish_domain_'+barsite_id+'" name="bardomain_id">';
								
			for (var item in result) {
				htmlform+='<option value="'+result[item].domain_zone+'">'+result[item].domain_zone+'</option>';
			}
			
			htmlform+="</select>";
			$(".cssload-loader" ).remove();
			
			 var publish_dialog = BootstrapDialog.show({
			            title: 'Publish Website',
			            message: $(htmlform),
				
				buttons: [{
				                label: 'Publish website',
				                action: function(dialog) {
				                    domain = $("#publish_domain_"+barsite_id).val();
									
									if($(".cssload-loader").length == 0){
										$("#loading_area").append('<div class="cssload-loader"></div>');
									}
									
									$.ajax({
										url: "/update_barsite",
										type: 'GET',
										dataType: 'json',
										data: {domain : domain, barsite_id: barsite_id},
										success : function(result) {
											console.log("success");
											
											$.ajax({
												url: pete_url,
												dataType: 'JSONP',
												type: 'GET',
												data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, pete_id: pete_id, domain: domain, db_name: db_name, db_user: db_user, db_user_pass: db_user_pass},
												success : function(result) {
													console.log(result);
													$(".cssload-loader" ).remove();
													//alert("exito!!")
													
													$("#barsite_"+result["site"].barsite_id).remove();
													
													dialog.close();
													
													html='';
													html+='<tr id="barsite_'+result["site"].barsite_id+'">';
													html+='<td><a target="_blank" href="http://'+result["site"].url+'">'+result["site"].url+'</a></td>';
													html+='<td>'+result["site"].theme+'</td>';
													html+='<td>'+result["site"].wp_user+'</td>';
													html+='<td><div id="wp_pass_'+result["site"].barsite_id+'"><a class="show_wp_password" barsite_id ="'+result["site"].barsite_id+'" href="">'+"{{trans('dashboard.show_password')}}"+'</a></div></td>';
													html+='<td><a target="_blank" href="http://'+result["site"].url+'/wp-admin">'+result["site"].url+'/wp-admin</a></td>	';
													html+='<td><a class="btn btn-xs btn-danger delete_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" site_id="'+result["site"].id+'">'+"{{trans('dashboard.delete')}}"+'</a></td>';			
													html+='</tr>';
													
													$('#subscription_table_'+result["subscription_id"]+' tr:last').after(html);
	 
													$(".cssload-loader" ).remove();
					
													show_password_logic();
													delete_sites_logic();
													
													reload_pete(result["barserver_ip"],result["email"],result["pete_token"]);
													
												}		
											});		
											
										}		
									});		
									
				                }
				            }]
				
			        });
			
        },
        error: function(xhr, resp, text) {
			
            console.log(xhr, resp, text);
        }
    })
	
	return false;
 });
}

//////////////////////////
//SITES LIST  
//////////////////////////

$(".sites_list_action").unbind().click(function() {
		
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/sites_list";
	subscription_id = $(this).attr("subscription_id");
	barserver_id = $(this).attr("barserver_id");
	
	$('#plan_div_'+subscription_id).html("");
	
	$.ajax({
		url: pete_url,
		dataType: 'JSONP',
		type: 'GET',
		data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, barserver_ip: BARSERVER_IP},
		success : function(result) {
			//alert("successss");
			//console.log(result);
			sites = result["sites"];
		    console.log(sites);
			
			html='';
			html+='<div class="content table-responsive table-full-width">';
			html+='<table class="table table-hover table-striped barsites_table" email= "'+result["email"]+'"'; 				
			html+='pete_token="'+result["pete_token"]+'" barserver_ip= "'+result["barserver_ip"]+'"'; 			
			html+='barserver_status="'+result["barserver_status"]+'" barserver_id="'+result["barserver_id"]+'" subscription_id='; 			
			html+='"'+result["subscription_id"]+'"  id="subscription_table_'+result["subscription_id"]+'">';
			html+='<thead>';
			html+='<th>{{trans("dashboard.project")}}</th>';
			html+='<th>URL</th>';
			html+='<th>{{trans("dashboard.theme")}}</th>';
			html+='<th>{{trans("dashboard.wordpress_user")}}</th>';
			html+='<th>{{trans("dashboard.wordpress_pass")}}</th>';
			html+='<th>Admin url</th>';
			html+='<th>{{trans("dashboard.options")}}</th>';
			html+='</thead>';
			html+='<tbody>';
            html+='</tbody>';
			html+='</table>';
			html+='</div>';
			
			$('#plan_div_'+result["subscription_id"]).html(html)
			
			
			for (var item in sites) {
				
				html = "";
				html+='<tr id="barsite_'+sites[item].barsite_id+'">';
				html+='<td>'+sites[item].name+'</td>';
				html+='<td><a target="_blank" href="http://'+sites[item].url+'">'+sites[item].url+'</a></td>';
				html+='<td>'+sites[item].theme+'</td>';
				html+='<td>'+sites[item].wp_user+'</td>';
				html+='<td><div id="wp_pass_'+sites[item].barsite_id+'"><a class="show_wp_password" barsite_id ="'+sites[item].barsite_id+'" href="">'+"{{trans('dashboard.show_password')}}"+'</a></div></td>';
				html+='<td><a target="_blank" href="http://'+sites[item].url+'/wp-admin">'+sites[item].url+'/wp-admin</a></td>';
				html+="<td>";
				
				if(sites[item].published != true){
					html+='<a class="btn btn-xs publish" barserver_id="'+result["barserver_id"]+'" pete_id="'+sites[item].id+'" barserver_ip="'+result["barserver_ip"]+'" email="'+result["email"]+'" pete_token="'+result["pete_token"]+'" subscription_id="'+result["subscription_id"]+'" barsite_id="'+sites[item].barsite_id+'" href="/">'+"{{trans('dashboard.publish')}}"+' </a>';
				}
				
				html+='<a class="btn btn-xs btn-danger delete_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.delete')}}"+'</a></td>';					
				html+='</tr>';
						
				$('#subscription_table_'+result["subscription_id"]+' tr:last').after(html);
				
			}	
				
			$(".cssload-loader" ).remove();	
			
			
			delete_sites_logic();
			show_password_logic();	
			publish_logic();
			
					
		}		
	});		
});

//////////////////////////
//SITES TRASH  
//////////////////////////
$(".sites_trash_action").click(function() {
		
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/sites_trash";
	subscription_id = $(this).attr("subscription_id");
	barserver_id = $(this).attr("barserver_id");
	
	$('#plan_div_'+subscription_id).html("");
		
	$.ajax({
		url: pete_url,
		dataType: 'JSONP',
		type: 'GET',
		data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, barserver_ip: BARSERVER_IP},
		success : function(result) {
			//alert("successss");
			//console.log(result);
			sites = result["sites"];
		    console.log(sites);
			
			html='';
			html+='<div class="content table-responsive table-full-width">';
			html+='<table class="table table-hover table-striped barsites_table" email= "'+result["email"]+'"'; 				
			html+='pete_token="'+result["pete_token"]+'" barserver_ip= "'+result["barserver_ip"]+'"'; 			
			html+='barserver_status="'+result["barserver_status"]+'" barserver_id="'+result["barserver_id"]+'" subscription_id='; 			
			html+='"'+result["subscription_id"]+'"  id="subscription_table_'+result["subscription_id"]+'">';
			html+='<thead>';
			html+='<th>URL</th>';
			html+='<th>{{trans("dashboard.delete_at")}}</th>';
			html+='<th>{{trans("dashboard.options")}}</th>';
			html+='</thead>';
			html+='<tbody>';
            html+='</tbody>';
			html+='</table>';
			html+='</div>';
			
			$('#plan_div_'+result["subscription_id"]).html(html)
			
			
			for (var item in sites) {
				
				console.log("entro");
						
				html='';
				html+='<tr id="barsite_'+sites[item].barsite_id+'">';
				html+='<td><a target="_blank" href="http://'+sites[item].url+'">'+sites[item].url+'</a></td>';
				html+='<td>'+sites[item].updated_at+'</td>';
				
				html+="<td>";
				
				
				html+='<a class="btn btn-xs restore_action" barserver_id="'+result["barserver_id"]+'" pete_id="'+sites[item].id+'" barserver_ip="'+result["barserver_ip"]+'" email="'+result["email"]+'" pete_token="'+result["pete_token"]+'" subscription_id="'+result["subscription_id"]+'" barsite_id="'+sites[item].barsite_id+'" href="/">'+"{{trans('dashboard.recover')}}"+' </a>';
				
				
				html+='<a class="btn btn-xs btn-danger destroy_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.destroy')}}"+'</a></td>';					
				html+='</tr>';
						
				$('#subscription_table_'+result["subscription_id"]+' tr:last').after(html);
						
			}		
			$(".cssload-loader" ).remove();	
			
				
			restore_action_logic();
			destroy_sites_logic();
					
		}		
	});		
});

//////////////////////////
//SITES BACKUPS  
//////////////////////////
$(".sites_backup_action").click(function() {
		
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/sites_backup";
	subscription_id = $(this).attr("subscription_id");
	barserver_id = $(this).attr("barserver_id");
	
	$('#plan_div_'+subscription_id).html("");
		
	$.ajax({
		url: pete_url,
		dataType: 'JSONP',
		type: 'GET',
		data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, barserver_ip: BARSERVER_IP},
		success : function(result) {
			//alert("successss");
			//console.log(result);
			sites = result["sites"];
		    console.log(sites);
			
			html='';
			html+='<div class="content table-responsive table-full-width">';
			html+='<table class="table table-hover table-striped barsites_table" email= "'+result["email"]+'"'; 				
			html+='pete_token="'+result["pete_token"]+'" barserver_ip= "'+result["barserver_ip"]+'"'; 			
			html+='barserver_status="'+result["barserver_status"]+'" barserver_id="'+result["barserver_id"]+'" subscription_id='; 			
			html+='"'+result["subscription_id"]+'"  id="subscription_table_'+result["subscription_id"]+'">';
			html+='<thead>';
			html+='<th>{{trans("dashboard.file")}}</th>';
			html+='<th>URL</th>';
			html+='<th>{{trans("dashboard.created_at")}}</th>';
			html+='<th>{{trans("dashboard.options")}}</th>';
			html+='</thead>';
			html+='<tbody>';
            html+='</tbody>';
			html+='</table>';
			html+='</div>';
			
			$('#plan_div_'+result["subscription_id"]).html(html)
			
			
			for (var item in sites) {
				
				html='';
				html+='<tr id="barsite_'+sites[item].barsite_id+'">';
				html+='<td>'+sites[item].file_name+'</td>';
				html+='<td><a target="_blank" href="http://'+sites[item].url+'">'+sites[item].url+'</a></td>';
				html+='<td>'+sites[item].created_at+'</td>';
				
				html+="<td>";
				html+='<a class="btn btn-xs restore_from_backup" barserver_id="'+result["barserver_id"]+'" backup_id="'+sites[item].id+'" barserver_ip="'+result["barserver_ip"]+'" email="'+result["email"]+'" pete_token="'+result["pete_token"]+'" subscription_id="'+result["subscription_id"]+'" barsite_id="'+sites[item].barsite_id+'" href="/">'+"{{trans('dashboard.recover')}}"+' </a>';
						
				html+='</tr>';
						
				$('#subscription_table_'+result["subscription_id"]+' tr:last').after(html);
				
						
			}		
			$(".cssload-loader" ).remove();	
			
				
			restore_from_backups_logic()
					
		}		
	});		
});


//////////////////////////
//SHUT DOWN SERVER  
//////////////////////////
$(".shut_down_action").click(function() {
		
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
	subscription_id = $(this).attr("subscription_id");
	barserver_id = $(this).attr("barserver_id");
		
	$.ajax({
		url: "/shut_down",
		dataType: 'JSON',
		type: 'GET',
		data: {subscription_id: subscription_id, barserver_id: barserver_id},
		success : function(result) {
			console.log("shut_down done");
		    var delayInMilliseconds = 30000; //1 second

		    setTimeout(function() {
		      //your code to be executed after 1 second
		   	 $(".cssload-loader").remove();
			 location.reload();
		    }, delayInMilliseconds);
		}		
	});		
});

//////////////////////////
//BOOT SERVER  
//////////////////////////
$(".boot_action").click(function() {
		
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
	subscription_id = $(this).attr("subscription_id");
	barserver_id = $(this).attr("barserver_id");
		
	$.ajax({
		url: "/boot",
		dataType: 'JSON',
		type: 'GET',
		data: {subscription_id: subscription_id, barserver_id: barserver_id},
		success : function(result) {
			console.log("boot done");
		    var delayInMilliseconds = 60000; //1 second

		    setTimeout(function() {
		      //your code to be executed after 1 second
		   	 $(".cssload-loader").remove();
			 location.reload();
		    }, delayInMilliseconds);
			
			
		}		
	});		
});

//////////////////////////
//REBOOT SERVER  
//////////////////////////
$(".reboot_action").click(function() {
		
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
		
	subscription_id = $(this).attr("subscription_id");
	barserver_id = $(this).attr("barserver_id");
		
	$.ajax({
		url: "/reboot",
		dataType: 'JSON',
		type: 'GET',
		data: {subscription_id: subscription_id, barserver_id: barserver_id},
		success : function(result) {
			console.log("reboot done");
		    var delayInMilliseconds = 60000; //1 second
		    setTimeout(function() {
		      //your code to be executed after 1 second
		   	 $(".cssload-loader").remove();
			 location.reload();
		    }, delayInMilliseconds);
			
			
		}		
	});		
});

//////////////////////////
//RESTORE SITE LOGIC  
//////////////////////////
function restore_action_logic(){
	
	$(".restore_action").unbind().click(function() {
		// send ajax
	
		if($(".cssload-loader").length == 0){
			$("#loading_area").append('<div class="cssload-loader"></div>');
		}
	
		PETE_ID = $(this).attr("pete_id");
		EMAIL = $(this).attr("email");
		PETE_TOKEN = $(this).attr("pete_token");
		BARSERVER_IP = $(this).attr("barserver_ip");
		pete_url = "http://"+BARSERVER_IP+"/restore_site";
	
		$.ajax({
			url: pete_url, // url where to submit the request
			type : "GET", // type of action POST || GET
			dataType : 'JSONP', // data type
			data: {email : EMAIL, pete_token: PETE_TOKEN, pete_id: PETE_ID, barserver_ip: BARSERVER_IP},
			success : function(data) {
				
				reload_pete(data['barserver_ip'],data['email'],data['pete_token']);
				
			    var delayInMilliseconds = 3000; //1 second
			    setTimeout(function() {
			      //your code to be executed after 1 second
			   	 $(".cssload-loader").remove();
				 location.reload();
			    }, delayInMilliseconds);
				
			},
			error: function(xhr, resp, text) {
				console.log(xhr, resp, text);
			}
		})
		return false;
	});

}

//////////////////////////
//RESTORE FROM BACKUPS
//////////////////////////
function restore_from_backups_logic(){

$(".restore_from_backup").unbind().click(function() {
	// send ajax
	
    if(!window.confirm("{{trans('dashboard.are_you_sure')}}")){
    	return false;
    }
      
	if($(".cssload-loader").length == 0){
		$("#loading_area").append('<div class="cssload-loader"></div>');
	}
	
	BACKUP_ID = $(this).attr("backup_id");
	EMAIL = $(this).attr("email");
	PETE_TOKEN = $(this).attr("pete_token");
	BARSERVER_IP = $(this).attr("barserver_ip");
	pete_url = "http://"+BARSERVER_IP+"/restore_from_backup";
	
	$.ajax({
		url: pete_url, // url where to submit the request
		type : "GET", // type of action POST || GET
		dataType : 'JSONP', // data type
		data: {email : EMAIL, pete_token: PETE_TOKEN, backup_id: BACKUP_ID, barserver_ip: BARSERVER_IP},
		success : function(data) {
			
			reload_pete(data['barserver_ip'],data['email'],data['pete_token']);
			
		    var delayInMilliseconds = 3000; //1 second
		    setTimeout(function() {
		      //your code to be executed after 1 second
		   	 $(".cssload-loader").remove();
			 location.reload();
		    }, delayInMilliseconds);
			
			
		},
		error: function(xhr, resp, text) {
			console.log(xhr, resp, text);
		}
	})
	return false;
});

}



function set_server_size(label,subscription_id,size_title,size){
	$("#"+size_title+subscription_id).html("<p>"+label+": "+size+"</p>");
}
	
	
$(document).ready(function(){
		
	//////////////////////////
	//LOADING EVERY LINODE VPS 
	//////////////////////////
			
	$(".barsites_table").each(function(index, element) {
			
		
		EMAIL = $(this).attr("email");
		PETE_TOKEN = $(this).attr("pete_token");
		BARSERVER_IP = $(this).attr("barserver_ip");
		pete_url = "http://"+BARSERVER_IP+"/sites_list";
		subscription_id = $(this).attr("subscription_id");
		barserver_id = $(this).attr("barserver_id");
		
		if($(this).attr("barserver_status") == "1"){
			
			if($(".cssload-loader").length == 0){
				$("#loading_area").append('<div class="cssload-loader"></div>');
			}
			
			
		$.ajax({
			url: pete_url,
			dataType: 'JSONP',
			type: 'GET',
			data: {email : EMAIL, pete_token: PETE_TOKEN, subscription_id: subscription_id, barserver_id: barserver_id, barserver_ip: BARSERVER_IP},
			success : function(result) {
				//alert("successss");
				//console.log(result);
				sites = result["sites"];
			
				for (var item in sites) {
							
					html='';
					html+='<tr id="barsite_'+sites[item].barsite_id+'">';
					html+='<td>'+sites[item].name+'</td>';
					html+='<td><a target="_blank" href="http://'+sites[item].url+'">'+sites[item].url+'</a></td>';
					html+='<td>'+sites[item].theme+'</td>';
					html+='<td>'+sites[item].wp_user+'</td>';
					html+='<td><div id="wp_pass_'+sites[item].barsite_id+'"><a class="show_wp_password" barsite_id ="'+sites[item].barsite_id+'" href="">'+"{{trans('dashboard.show_password')}}"+'</a></div></td>';
					html+='<td><a target="_blank" href="http://'+sites[item].url+'/wp-admin">'+sites[item].url+'/wp-admin</a></td>';
					html+="<td>";
					
					if(sites[item].published != true){
						html+='<a class="btn btn-xs publish" barserver_id="'+result["barserver_id"]+'" pete_id="'+sites[item].id+'" barserver_ip="'+result["barserver_ip"]+'" email="'+result["email"]+'" pete_token="'+result["pete_token"]+'" subscription_id="'+result["subscription_id"]+'" barsite_id="'+sites[item].barsite_id+'" href="/">'+"{{trans('dashboard.publish')}}"+' </a>';
					}
					
					html+='<a class="btn btn-xs btn-danger delete_site" email="'+result["email"]+'" pete_token ="'+result["pete_token"]+'" barserver_ip="'+result["barserver_ip"]+'" pete_id="'+sites[item].id+'" barsite_id="'+sites[item].barsite_id+'">'+"{{trans('dashboard.delete')}}"+'</a></td>';					
					html+='</tr>';
							
					$('#subscription_table_'+result["subscription_id"]+' tr:last').after(html);
							
				}		
				$(".cssload-loader" ).remove();	
				
				
				delete_sites_logic();
				show_password_logic();	
				publish_logic();
				
				set_server_size("{{trans('dashboard.disk_total_size')}}",result["subscription_id"],"disk_total_size_",result["disk_total_size"]);
				set_server_size("{{trans('dashboard.disk_used')}}",result["subscription_id"],"disk_used_",result["disk_used"]);
				set_server_size("{{trans('dashboard.disk_free')}}",result["subscription_id"],"disk_free_",result["disk_free"]);
						
			}		
		});	
		
		}
			
	});
});

</script>
	

@endsection

