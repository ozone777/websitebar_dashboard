
<div class="row">
    <div class="col-md-12">
		<h1>{{trans('dashboard.publish')}}</h1>
	</div>
</div>

<div class="row">
    <div class="col-md-6">
<select id="domain_selector">
	
	 @foreach($bardomains as $bardomain)
	
  		<option value="{{$bardomain->domain_zone}}">{{$bardomain->domain_zone}}</option>
		
   	@endforeach
	
</select>
	</div>
	
	  <div class="col-md-6">
		  <a class="btn btn-success" href="{{ route('bardomains.create') }}">Create</a>
	  </div>
	
</div>

<div class="row">
	<div class="col-md-12">
		
		<div id="plan_div_{{$subscription_id}}">
		

	<table class="table table-hover table-striped barsites_table" width="100%" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_status="{{$my_barservers[$subscription_id]->on}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}"  id="subscription_table_{{$subscription_id}}">
		<tr>
			<th>{{trans('dashboard.project')}}</th>
			<th>URL</th>
			<th>{{trans('dashboard.theme')}}</th>
			<th>{{trans('dashboard.app_name')}}</th>
			<th>{{trans('dashboard.wordpress_access')}}</th>
			<th>{{trans('dashboard.options')}}</th>
		</tr>
		
		<tbody>
			
		</tbody>
		
		</table>
	</div>
	
	  
	</div>
</div>
	

					