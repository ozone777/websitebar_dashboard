	public function my_subscriptions(){
		
		//linode-cli regions list --json --pretty
		//$result= shell_exec("linode-cli linodes list --json --pretty");
		//Log::info($result);
		$dashboard_option = new DashboardOption();
		
		$current_user = Auth::user();
		$role = getUserRole($current_user->ID);
		if(($role == "administrator") || ($role == "Contributor")){
			return redirect('/customers');
		}
		
		$wp_user = get_user_by('id',$current_user->ID);
		$viewsw = "my_subscriptions";
		
		//Que tenga id de usuario logeado
		$sites = Site::orderBy('id', 'desc')->where('user_id', $current_user->ID)->get();	
		 
		$success = Input::get('success');
		$message = Input::get('message');
		
		$bar_templates = Bartemplate::orderBy('title', 'asc')->get();	
		$barservers = Barserver::orderBy('id', 'desc')->where('user_id', $current_user->ID)->get();
		$subscription_ids = $barservers->lists('subscription_id')->toArray();
		$my_barservers = $barservers->keyBy('subscription_id');
		$payment_status = ['wc-active' => "Active", 'wc-pending' => "pending", 'wc-on-hold' => "On hold"];
		$pete_versions = Pete::orderBy('id', 'desc')->get()->keyBy('id');
		$barthemes = Bartheme::orderBy('id', 'desc')->get();
		
		$my_barsites = DB::select("SELECT u.user_email as email, s.pete_token, bsub.subdomain as subdomain, bdo.domain_zone as domain_zone, bs.id as barsite_id, s.subscription_id, s.user_id, bs.theme, bs.wp_user, bs.wp_info, s.id as barserver_id, s.ip FROM wp_barservers s 
		inner join wp_barsites bs on s.id = bs.wp_barserver_id
		inner join wp_users u on u.ID = s.user_id
		left join wp_barsubdomains bsub on bsub.id = bs.barsubdomain_id
        left join wp_bardomains bdo on bdo.id = bs.bardomain_id
		 where s.user_id = $current_user->ID");
		
		$pete_tokens = [];
		foreach ( $my_barservers as $barserver ) {
			$pete_tokens[$barserver->subscription_id] = Crypt::decrypt($barserver->pete_token);
		}
		
		$my_barsites_array = [];
		foreach ( $my_barsites as $item ) {
			
			Log::info("Barserver email");
			Log::info($item->email);
			
			
			
			if(isset($my_barsites_array[$item->subscription_id])){
				array_push($my_barsites_array[$item->subscription_id],$item);
			}else{
				$my_barsites_array[$item->subscription_id] = [];
				array_push($my_barsites_array[$item->subscription_id],$item);
			}
		}
		
		Log::info("PETE TOKENS");
		Log::info($pete_tokens);
		
		Log::info("mybar");
		Log::info($pete_tokens);
		
		$now =Carbon::now();
		$creating_delay = 30;
		if($dashboard_option->get_meta_value("environment") == "development"){
			//Log::info("entro en development");
			$date = $now->addMinutes(29);
			$creating_delay = 1;
			$now = $date;
		}
		
		return view('dashboard.my_subscriptions',compact('wp_user','sites','success','viewsw','message','bar_templates','subscription_ids','payment_status','pete_versions','my_barservers','my_barsites_array','now','barthemes','creating_delay','pete_tokens'));
	}


<div class="row">
	<div class="col-md-6">
		
		<input class="button sites_list_action" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" pete_token= "{{$pete_tokens[$subscription_id]}}" email= "{{$current_user->user_email}}" type="submit" value="Sites list">
		
		<input class="button" type="submit" name="action" value="Shut down" onclick="return confirm('Are you sure you want to issue a Shutdown?')"> 
		<input class="button" type="submit" name="action" value="Reboot" onclick="return confirm('Are you sure you want to issue a Shutdown?')">
		<br />
		<br />
							
					
		<p>IP: <a target="_blank" href="http://{{$my_barservers[$subscription_id]->ip}}">{{$my_barservers[$subscription_id]->ip}}</a></p>
		<p>status: {{$my_barservers[$subscription_id]->status}}</p>
		<p>OS: {{$my_barservers[$subscription_id]->image}}</p>
		<p>region: {{$my_barservers[$subscription_id]->region}}</p>
		<p>apache_version: {{$pete_versions[$my_barservers[$subscription_id]->wp_pete_id]->apache_version}}</p>
		<p>php_version: {{$pete_versions[$my_barservers[$subscription_id]->wp_pete_id]->php_version}}</p>
		<p>db_version: {{$pete_versions[$my_barservers[$subscription_id]->wp_pete_id]->db_version}}</p>
		<p>phpmyadmin_version: {{$pete_versions[$my_barservers[$subscription_id]->wp_pete_id]->phpmyadmin_version}}</p>
		
		<br />

	</div>
					
	<div class="col-md-6">
		
		<a class="btn btn-xs get_barserver_info" style="padding: 5px 5px 5px 5px" barserver_id="{{$my_barservers[$subscription_id]->id}}" href="/backups?barserver_id={{$my_barservers[$subscription_id]->id}}">{{trans('dashboard.get_server_credentials')}}</a>
							
		<div id ="info_{{$my_barservers[$subscription_id]->id}}"></div>
	</div>
</div>
					

	<form action="/create_websitebar" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="wp_barserver_id" value="{{$my_barservers[$subscription_id]->id}}">
		<input type="hidden" name="user_id" value="{{$user_id}}">
		<input type="hidden" name="theme" id="theme_{{$subscription_id}}" value="">
							
	<div class="row">
	<div class="col-md-6">
		<input name="subdomain" id="subdomain_{{$subscription_id}}" class="form-control" value="">
	</div>
							
	<div class="col-md-6">
		<p>.websitebar.co</p>
	</div>
	
	</div>
	
	<div class="row">
	<div class="col-md-12">
							
	@include('dashboard/_gallery',array('subscription_id' => $subscription_id))
	
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-12">
	@if( $subscription_status == 'wc-active' )
	<input type="submit" class="et_manage_submit import_wordpress_theme_action" subscription_id="{{$subscription_id}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" value="Create website">
	@else
	<p>Not active subscription</p>
	@endif
	
	</div>
	</div>
					
</form>
					
<div class="content table-responsive table-full-width">
	<table class="table table-hover table-striped" id="barserver_table_{{$my_barservers[$subscription_id]->id}}">
		<thead>
			<th>URL</th>
			<th>{{trans('dashboard.theme')}}</th>
			<th>{{trans('dashboard.wordpress_user')}}</th>
			<th>{{trans('dashboard.wordpress_pass')}}</th>
			<th>Admin url</th>
			<th>{{trans('dashboard.options')}}</th>
		</thead>
		<tbody>
							
			@if(isset($my_barsites_array[$subscription_id]))
							
				@foreach ($my_barsites_array[$subscription_id] as $barsite )

					<tr>
						@if($barsite->subdomain)
							<td><a target="_blank" href="http://{{$barsite->subdomain}}">{{$barsite->subdomain}}</a></td>	
						@else
							<td><a target="_blank" href="http://{{$barsite->domain_zone}}">{{$barsite->domain_zone}}</a></td>	
						@endif
						<td>{{$barsite->theme}}</td>
						<td>{{$barsite->wp_user}}</td>
						<td><div id="wp_pass_{{$barsite->barsite_id}}"><a class="show_wp_password" barsite_id ="{{$barsite->barsite_id}}" href="">{{trans('dashboard.show_password')}}</a></div></td>
						@if($barsite->subdomain)
							<td><a target="_blank" href="http://{{$barsite->subdomain}}/wp-admin">{{$barsite->subdomain}}/wp-admin</a></td>	
						@else
							<td><a target="_blank" href="http://{{$barsite->domain_zone}}/wp-admin">{{$barsite->domain_zone}}/wp-admin</a></td>	
						@endif
						<td>

								
							
							
							@if($barsite->subdomain)								
							<a class="btn btn-xs publish" theme="{{$barsite->theme}}" barserver_id ="{{$my_barservers[$subscription_id]->id}}" barsite_id="{{$barsite->barsite_id}}" href="/">{{trans('dashboard.publish')}}</a>
							@endif	
							
							<form action="/delete_websitebar" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false }">
                                    
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="websitebar_id" value="{{ $barsite->barsite_id }}">
								<button type="submit" class="btn btn-xs btn-danger">{{trans('dashboard.delete')}}</button>
							</form>
								
						</td>
									
					</tr>
					
					@endforeach
					
					@endif
                      						
				</tbody>
			</table>
		</div>

<script>
	
	var randomString = function(length) {
	    var text = "";
	    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
	    for(var i = 0; i < length; i++) {
	        text += possible.charAt(Math.floor(Math.random() * possible.length));
	    }
	    return text;
	}
	
	$(".show_wp_password").on('click', function(){
		
		barsite_id = $(this).attr("barsite_id")
		
		$.ajax({
        url: '/show_wp_pass', // url where to submit the request
        type : "GET", // type of action POST || GET
        dataType : 'json', // data type
        data: {barsite_id: barsite_id},
        success : function(result) {
			
			console.log(result);
			$("#wp_pass_"+barsite_id).html(result["password"]);
            
		 }
		 
		 });
		 return false;
	 });
	
    $(".get_barserver_info").on('click', function(){
		
        // send ajax
		barserver_id = $(this).attr("barserver_id")
        $.ajax({
            url: '/get_barserver_info', // url where to submit the request
            type : "POST", // type of action POST || GET
            dataType : 'json', // data type
            data: {barserver_id: $(this).attr("barserver_id"), _token: "{{csrf_token()}}"},
            success : function(result) {
               
				console.log(result);
				console.log(result["ip"]);
				html = "";
				html += "<p style='padding: 5px 5px 5px 5px; font-size: 11px'>SSH ACCESS <br/ >";
				html += "ssh -p 13737 root@"+result["ip"]+"<br/ >";
				html += "Password: "+result["ssh_info"]+"</p>";
				
				html += "<p style='padding: 5px 5px 5px 5px; font-size: 11px'>PHPMYADMIN ACCESS <br/ >";
				phpmyadminurl = "http://"+result["ip"]+"/phpmyadmin";
				html += "<a target='_blank' href='"+phpmyadminurl+"'>"+phpmyadminurl+"</a><br/ >";
				html += "Usename: root <br/ >";
				html += "Password: "+result["db_info"]+"</p>";
				
				html += "<p style='padding: 5px 5px 5px 5px; font-size: 11px'>WORDPRESSPETE ACCESS <br/ >";
				pete_url = "http://"+result["ip"];
				html += "<a target='_blank' href='"+pete_url+"'>"+pete_url+"</a><br/ >";
				html += "Usename: "+result["user_email"]+" <br/ >";
				html += "Password: "+result["pete_info"]+"<br/ >";
				html += "Pete token: "+result["pete_token"]+"</p>";
				
				$("#info_"+barserver_id).html(html);
				
            },
            error: function(xhr, resp, text) {
                console.log(xhr, resp, text);
            }
        })
		return false;
    });
	
	
	function sites_list_callback(data){
	  alert("sites_list_callback");
	  console.log(data);
	  $(".cssload-loader").remove();

	}
	 
	$(".sites_list_action").click(function() {  
		
		if($(".cssload-loader").length == 0){
	      $("#loading_area").append('<div class="cssload-loader"></div>');
		}
		
		/*production
		EMAIL = $(this).attr("email");
		PETE_TOKEN = $(this).attr("pete_token");
		BARSERVER_IP = $(this).attr("barserver_ip");
		pete_url = "http://"+BARSERVER_IP+"/sites_list"
		*/
		
		/*Development*/
		EMAIL = "pedroconsuegrat@gmail.com";
		PETE_TOKEN = "l6W91FQafnwGJb0DA8OxqNMIHdmr4P";
		pete_url = "http://pete.test/sites_list";	
		
		jQuery.ajax({
			url: pete_url,
			dataType: 'JSONP',
			jsonpCallback: 'sites_list_callback',
			type: 'GET',
			data: {email : EMAIL, pete_token: PETE_TOKEN},
			
		});
		
		return false;
		
	});
	
	
	function create_new_wordpress_callback(data){
	  
	  console.log(data);  
      $.ajax({
          url: '/get_barserver_info', // url where to submit the request
          type : "POST", // type of action POST || GET
          dataType : 'json', // data type
          data: {barserver_id: $(this).attr("barserver_id"), _token: "{{csrf_token()}}"},
          success : function(result) {
             
			 $(".cssload-loader").remove();
			
          },
          error: function(xhr, resp, text) {
              console.log(xhr, resp, text);
          }
      })
	  

	}
	 
	$(".create_new_wordpress_action").click(function() {  
		
		if($(".cssload-loader").length == 0){
	      $("#loading_area").append('<div class="cssload-loader"></div>');
		}
		
		subscription_id = $(this).attr("subscription_id");
		site_name = $("#subdomain_"+subscription_id).val();
		/*production
		EMAIL = $(this).attr("email");
		PETE_TOKEN = $(this).attr("pete_token");
		BARSERVER_IP = $(this).attr("barserver_ip");
		pete_url = "http://"+BARSERVER_IP+"/create_new_wordpress"
		site_url = site_name+".websitebar.co";
		*/
		
		/*Development*/
		EMAIL = "pedroconsuegrat@gmail.com";
		PETE_TOKEN = "l6W91FQafnwGJb0DA8OxqNMIHdmr4P";
		pete_url = "http://pete.test/create_new_wordpress";	
		site_url = site_name+".test";
		
		barserver_id = $(this).attr("barserver_id");
		theme = $("#theme_"+subscription_id).val();
		
		error = "";
		if(site_name == "") {
			error = "Please insert the url. " ;
		}
		
		if(theme == "") {
		  error += "Please select the theme" 
		}
		
		if(error !=""){
    		$.notify({
        		icon: 'pe-7s-arc',
        		message: error

        	},{
            	type: 'info',
            	timer: 4000
        	});
			$(".cssload-loader").remove();
		  return false
			
		}
		
		
		db_name = "db_"+randomString(9);
		db_user = "user_"+randomString(9);
		db_user_pass = randomString(9);
		wp_user= "mywebsitebar";
		wp_pass= "Ozone301.";
		
		console.log(barserver_id);
		console.log(site_name);
		console.log(site_url);
		console.log(theme);
		console.log(db_name);
		console.log(db_user);
		console.log(db_user_pass);	
		console.log(wp_user);
		console.log(wp_pass);
		
		
		jQuery.ajax({
			url: pete_url,
			dataType: 'JSONP',
			jsonpCallback: 'create_new_wordpress_callback',
			type: 'GET',
			data: {email : EMAIL, pete_token: PETE_TOKEN, barserver_id: barserver_id, site_name: site_name, site_url: site_url, theme: theme, db_name: db_name, db_user: db_user, db_user_pass: db_user_pass, wp_user: wp_user, wp_pass: wp_pass},			
		});
		
		
		return false;
		
	});
	
	
	///import_wordpress_theme
	
	function save_barsite_from_pete_callback(data){

		barserver_id = data.barserver_id;
		site_url = data.url;
		
        $.ajax({
            url: '/save_barsite_from_pete', // url where to submit the request
            type : "GET", // type of action POST || GET
            dataType : 'json', // data type
            data: {action_name: data.action_name, barserver_id: data.barserver_id, site_url: data.url, theme: data.theme, wp_user: data.wp_user, wp_pass: data.wp_pass },
            success : function(result) {
                console.log(result);
				
				html='';
				html+='<tr>';
				html+='<td><a target="_blank" href="http://'+site_url+'">'+site_url+'</a></td>';
				html+='<td>'+result.theme+'</td>';
				html+='<td>'+result.wp_user+'</td>';
				html+='<td><div id="wp_pass_'+result.id+'"><a class="show_wp_password" barsite_id ="'+result.id+'" href="">'+"{{trans('dashboard.show_password')}}"+'</a></div></td>';
				html+='<td><a target="_blank" href="http://'+site_url+'/wp-admin">'+site_url+'/wp-admin</a></td>	';
				html+='<td><a class="btn btn-xs publish" barsite_id="'+result.id+'" href="/">'+"{{trans('dashboard.publish')}}"+' </a>';
				//html+='<button type="submit" class="btn btn-xs btn-danger">Delete</button></td>';
				
				html+='<form action="/delete_websitebar" method="POST" style="display: inline;" onsubmit="if(confirm('+'"Delete? Are you sure?"'+')) { return true } else {return false }">';
                html+='<input type="hidden" name="_token" value="{{ csrf_token() }}">';
				html+='<input type="hidden" name="websitebar_id" value="'+result.id+'">';
				html+='<button type="submit" class="btn btn-xs btn-danger">'+"{{trans('dashboard.delete')}}"+'</button>'
				html+='</form>';
				
				html+='</tr>';
			
				$('#barserver_table_'+barserver_id+' tr:last').after(html);
  			 	$(".cssload-loader").remove();
			
            },
            error: function(xhr, resp, text) {
                console.log(xhr, resp, text);
            }
        })

	}
	 
	$(".import_wordpress_theme_action").click(function() {  
		
		if($(".cssload-loader").length == 0){
	      $("#loading_area").append('<div class="cssload-loader"></div>');
		}
		
		subscription_id = $(this).attr("subscription_id");
		site_name = $("#subdomain_"+subscription_id).val();
		
		if("{{$dashboard_option->get_meta_value('environment')}}" == "production"){
			//production
			EMAIL = $(this).attr("email");
			PETE_TOKEN = $(this).attr("pete_token");
			BARSERVER_IP = $(this).attr("barserver_ip");
			pete_url = "http://"+BARSERVER_IP+"/import_wordpress_theme"
			site_url = site_name+".websitebar.co";
		}else{
			/*Development*/
			EMAIL = "pedroconsuegrat@gmail.com";
			PETE_TOKEN = "l6W91FQafnwGJb0DA8OxqNMIHdmr4P";
			pete_url = "http://pete.test/import_wordpress_theme";	
			site_url = site_name+".test";
		}
		
		barserver_id = $(this).attr("barserver_id");
		theme = $("#theme_"+subscription_id).val();
		
		error = "";
		if(site_name == "") {
			error = "Please insert the url. " ;
		}
		
		if(theme == "") {
		  error += "Please select the theme" 
		}
		
		if(error !=""){
    		$.notify({
        		icon: 'pe-7s-arc',
        		message: error

        	},{
            	type: 'info',
            	timer: 4000
        	});
			$(".cssload-loader").remove();
		  return false
			
		}
		
		
		db_name = "db_"+randomString(9);
		db_user = "user_"+randomString(9);
		db_user_pass = randomString(9);
		wp_user= "mywebsitebar";
		wp_pass= "Ozone301.";
		
		console.log(barserver_id);
		console.log(site_name);
		console.log(site_url);
		console.log(theme);
		console.log(db_name);
		console.log(db_user);
		console.log(db_user_pass);	
		console.log(wp_user);
		console.log(wp_pass);
		
		
		jQuery.ajax({
			url: pete_url,
			dataType: 'JSONP',
			jsonpCallback: 'save_barsite_from_pete_callback',
			type: 'GET',
			data: {email : EMAIL, pete_token: PETE_TOKEN, barserver_id: barserver_id, site_name: site_name, site_url: site_url, theme: theme, db_name: db_name, db_user: db_user, db_user_pass: db_user_pass, wp_user: wp_user, wp_pass: wp_pass},			
		});
		
		
		return false;
		
	});
	
	
	
</script>
					