@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')

	<div class="row">
		<div class="col-md-12">
			<input type="button" id="sites_list_action" value="Sites list" >
			
		</div>
	</div>

			<!-- code started -->			
			 <?php
				
			   $USER_ID = $wp_user->ID;
				$subscriptions = wcs_get_users_subscriptions($USER_ID);
				$cont = 0;
				foreach ($subscriptions as  $key => $subscription ){
					
					$subscription_status = $subscription->post->post_status;
						$subscription_id = $subscription->post->ID;
						$order_id = $subscription->order->post->ID; // order ID (corresponding to the subscription ID)
						$active_subscriptions_arr[] = $subscription->post->ID;
						$order_items = $subscription->get_items();
						$user_id = $subscription->get_user_id();
						
						if(!in_array($subscription_id, $subscription_ids)){
							
							/////////////////////
							//BARSERVER NO CREADO
							/////////////////////
							
							foreach ( $order_items as $key => $item ) {
							
								$_product = get_product($item[product_id]);
								echo '<div class="row">';
	        					echo '<div class="col-md-12">';   
								
								echo '<h4 class="title">' . get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang) ) .'</h4>';
								echo '</div>';
								echo '</div>';
							}
							
							?>
						  	
							@include('dashboard/_not_initialized_barserver',array('order_items' => $order_items))
							
							<?php
						}else{
										
							$diff = $my_barservers[$subscription_id]->created_at->diffInMinutes($now);
							
							if($diff>=30){
								
								//////////////////////////////////////////
								//BARSERVER CREADO HACE MAS DE 30 MINUTOS
								//////////////////////////////////////////
							
							
							foreach ( $order_items as $key => $item ) {
						
								$_product = get_product($item[product_id]);
								
								echo '<div class="row">';
	        					echo '<div class="col-md-12">';
						       
								echo '<h4 class="title">' .get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang)).'</h4>';
								echo '</div>';
								echo '</div>';
							}
							
							?>
							
							@include('dashboard/_initialized_barserver')
						
						<?php
						
						}else{
							
							//////////////////////////////////////////
							//BARSERVER CREADO HACE MENOS DE 30 MINUTOS
							//////////////////////////////////////////
							
							
							foreach ( $order_items as $key => $item ) {
							
								$_product = get_product($item[product_id]);
								echo '<div class="row">';
	        					echo '<div class="col-md-12">';     
								echo '<h4 class="title">' . get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang) ) .'</h4>';
								echo '</div>';
								echo '</div>';
							}
							
							?>
							
							@include('dashboard/_creating_barserver')
							
							<?php
							
						}
						
						}
						
						$cont ++;
				}
			?>
			

 	   <br />
	    <br />
		
</div>
</div>



<script>
				
 		
		
		 
	</script>
 

<script>
	
	var offset = new Date().getTimezoneOffset();
	
	$('[data-countdown]').each(function() {
	  var $this = $(this), finalDate = $(this).data('countdown');
	  
	  console.log(finalDate);
	  
	  datex = new Date(finalDate);
	 // console.log(datex);
	  datex.setMinutes( datex.getMinutes() - offset );
	  datex_string = $.format.date(datex, 'yyyy/MM/dd H:mm:ss');
	  console.log(datex_string);
	   
	  $this.countdown(datex_string, function(event) {
	    $this.html(event.strftime('%M:%S'));
		
		}).on('finish.countdown', function() {
	           location.reload();
	    });
	});
	
</script>
	
<script>	
    $(document).ready(function(){
		
		
		$(".publish").on('click', function(){
			
			barsite_id = $(this).attr("barsite_id");
			barserver_id = $(this).attr("barserver_id");
			
            $.ajax({
                url: '/get_domains', // url where to submit the request
                type : "GET", // type of action POST || GET
                dataType : 'json', // data type
                data : {barserver_id: barserver_id},
                success : function(result) {
                    // you can see the result from the console
                    // tab of the developer tools
                    console.log(result);
					htmlform="";
					htmlform+='<form action="/publish_website" method="POST">';
					htmlform+='<select name="bardomain_id">';
										
					for (var item in result) {
						htmlform+='<option value="'+result[item].id+'">'+result[item].domain_zone+'</option>';
					  // console.log(result[item]);     // => "Bill", then 25, then "Masters"
					}
					
					htmlform+="</select>";
					
                    htmlform+='<input type="hidden" name="_token" value="{{ csrf_token() }}">';
					htmlform+='<input type="hidden" name="barsite_id" value="'+barsite_id+'">';
					htmlform+='<input type="submit" class="et_manage_submit" value="Create website">';
					htmlform+='</form>';
					
					BootstrapDialog.show({
					            title: 'Publish Website',
					            message: $(htmlform),
					        });
					
                },
                error: function(xhr, resp, text) {
					
                    console.log(xhr, resp, text);
                }
            })
			
			return false;
		 });
			
		function explode(){
		  //alert("Boom!");
		}
		setTimeout(explode, 2000);
		
        // click on button submit
        $("#create_linode").on('click', function(){
            // send ajax
            $.ajax({
                url: '/create_linode', // url where to submit the request
                type : "POST", // type of action POST || GET
                dataType : 'json', // data type
                data : $("#create_linode_form").serialize(), // post data || get data
                success : function(result) {
                    // you can see the result from the console
                    // tab of the developer tools
                    console.log(result);
					console.log(result[0]['ipv4']);
					alert("done");
                },
                error: function(xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            })
			return false;
        });
		
		
       
		
		
		
		
    });

</script>
	

@endsection

