@extends('layout')

@section('header')
  
@endsection

@section('content')

@extends('errors')
			

<h3>Backups</h3>

<table class="list">
	<tbody><tr>
		<th colspan="3">Backups</th>
	</tr>
	<tr class="list_head">
		<td colspan="3">Schedule</td>
	</tr>

	<form name="backup_schedule_save" id="backup_schedule_save" action="/linodes/backup%5Fschedule%5Fsave/WebsiteBarADN" method="post" onsubmit="return _CF_checkbackup_schedule_save(this)"></form> <input type="hidden" name="authenticity_token" value="4B32DF0B-F9A5-3FED-A1F1458EF89A81ED">

	<tr class="list_entry">
		<td class="table_form_header">Backup Window (GMT-5)</td>
		<td colspan="2">
			<select name="window">
				<option value="0">0000 - 0200
				</option><option value="1">0200 - 0400
				</option><option value="2" selected="">0400 - 0600
				</option><option value="3">0600 - 0800
				</option><option value="4">0800 - 1000
				</option><option value="5">1000 - 1200
				</option><option value="6">1200 - 1400
				</option><option value="7">1400 - 1600
				</option><option value="8">1600 - 1800
				</option><option value="9">1800 - 2000
				</option><option value="10">2000 - 2200
				</option><option value="11">2200 - 0000
			</option></select>
		</td>
	</tr>
	<tr class="list_entry">
		<td class="table_form_header">Weekly Backup</td>
		<td colspan="2">
			<select name="weekly">
				<option value="1">Sunday
				</option><option value="2">Monday
				</option><option value="3">Tuesday
				</option><option value="4">Wednesday
				</option><option value="5">Thursday
				</option><option value="6">Friday
				</option><option value="7">Saturday
			</option></select>
		</td>
	</tr>

	<tr class="list_entry">
		<td width="200"></td>
		<td colspan="3">
			<input class="button" type="submit" value="Save Changes">
		</td>
	</tr>

	



	<tr class="list_head">
		<td colspan="3">Automatic Backups</td>
	</tr>

	
		<tr class="list_entry">
			<td class="table_form_header">Daily Backup</td>
			<td colspan="2">From today<br><a class="hint" href="/linodes/backup%5Fdetails/WebsiteBarADN?id=103405368">Restore to...</a></td>
		</tr>
	


	<tr class="list_head">
		<td colspan="3">Manual Snapshot</td>
	</tr>
	<tr class="list_entry">
		<td class="table_form_header">Snapshot</td>
		<td colspan="2">
			snap1<br>
					<span class="hint">From 4 days ago</span>
				
				<br>
				<a class="hint" href="/linodes/backup%5Fdetails/WebsiteBarADN?id=103163954">Restore to...</a> 
		</td>
	</tr>
	
		<tr class="list_entry">
			<td width="200"></td>
			<td colspan="3">
				<form name="backup_snapshot" id="backup_snapshot" action="/linodes/backup%5Fsnapshot/WebsiteBarADN" method="post" onsubmit="return _CF_checkbackup_snapshot(this)"><input type="hidden" name="authenticity_token" value="4B32DF17-F520-0A6E-27FE720F2219FB0C"> <input name="label" id="label" type="text" maxlength="150" size="32" placeholder="My New Snapshot Label"> 
						<input class="button" name="snapshot" type="submit" value="Take a New Snapshot Now" onclick="return confirm('Taking a snapshot will back up your Linode in its current state, over-writing your previous snapshot.  Are you sure?');">
					</form>

			</td>
		</tr>
	
</tbody></table>


<table class="list">
	<tbody><tr>
		<th colspan="6">Backup History</th>
	</tr>
	<tr class="list_head">
		<td>Type</td>
		<td>Started</td>
		<td>Finished</td>
		<td>Duration</td>
		<td>Status</td>
		<td>Message</td>
	</tr>

	
		<tr class="list_entry">
			<td>auto</td>
			<td>
				2018-10-18 03:12:34 &nbsp;
			</td>
			<td>
				2018-10-18 03:15:15 &nbsp;
			</td>
			<td>
				2 minutes, 41 seconds 
				&nbsp;
			</td>
			<td>successful</td>
			<td>&nbsp;</td>
		</tr>
	
		<tr class="list_entry">
			<td>auto</td>
			<td>
				2018-10-17 03:12:30 &nbsp;
			</td>
			<td>
				2018-10-17 03:14:35 &nbsp;
			</td>
			<td>
				2 minutes, 5 seconds 
				&nbsp;
			</td>
			<td>successful</td>
			<td>&nbsp;</td>
		</tr>
	
		<tr class="list_entry">
			<td>auto</td>
			<td>
				2018-10-16 03:12:21 &nbsp;
			</td>
			<td>
				2018-10-16 03:14:23 &nbsp;
			</td>
			<td>
				2 minutes, 2 seconds 
				&nbsp;
			</td>
			<td>successful</td>
			<td>&nbsp;</td>
		</tr>
	
		<tr class="list_entry">
			<td>auto</td>
			<td>
				2018-10-15 03:12:12 &nbsp;
			</td>
			<td>
				2018-10-15 03:14:26 &nbsp;
			</td>
			<td>
				2 minutes, 14 seconds 
				&nbsp;
			</td>
			<td>successful</td>
			<td>&nbsp;</td>
		</tr>
	
		<tr class="list_entry">
			<td>snapshot</td>
			<td>
				2018-10-14 11:17:26 &nbsp;
			</td>
			<td>
				2018-10-14 11:20:36 &nbsp;
			</td>
			<td>
				3 minutes, 10 seconds 
				&nbsp;
			</td>
			<td>successful</td>
			<td>&nbsp;</td>
		</tr>
	
</tbody></table>


<script>
	
	$(document).ready(function(){
		
		$("button").click(function(){
		    $.getJSON("/backups.js", function(result){
				
				alert("Hi!!");
				
				/*
		        $.each(result, function(i, field){
		            $("div").append(field + " ");
		        });
				*/
		    });
		});
		
		/*
       
            $.ajax({
                url: '/backups', // url where to submit the request
                type : "GET", // type of action POST || GET
                dataType : 'json', // data type
               // data : $("#create_linode_form").serialize(), // post data || get data
                success : function(result) {
                    // you can see the result from the console
                    // tab of the developer tools
                    console.log(result);
					//console.log(result[0]['ipv4']);
					alert("done");
                },
                error: function(xhr, resp, text) {
                    console.log(xhr, resp, text);
                }
            })
		
		*/
	});
	
</script>			
	

@endsection

