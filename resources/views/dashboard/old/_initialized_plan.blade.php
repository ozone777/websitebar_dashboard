<div class="row">
	<div class="col-md-6">
		
		@if($my_barservers[$subscription_id]->on == true)
		
		<input class="button shut_down_action" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value=" {{trans('dashboard.shut_down')}}"> 
		<input class="button reboot_action" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.reboot')}}">
		
		@else
		
		<input class="button boot_action" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.boot')}}"> 
		
		@endif
		
		<br />
		<br />
							
		
		<p>Server: {{$my_barservers[$subscription_id]->linode_label}}</p>	
		<p>IP: <a target="_blank" href="http://{{$my_barservers[$subscription_id]->ip}}">{{$my_barservers[$subscription_id]->ip}}</a></p>
		@if($my_barservers[$subscription_id]->on == true)
		<p>status: {{trans('dashboard.running')}}</p>
		@else
		<p>status: {{trans('dashboard.off')}}</p>
		@endif
		<p>OS: {{$my_barservers[$subscription_id]->image}}</p>
		<p>region: {{$my_barservers[$subscription_id]->region}}</p>
		
		
		<div id="disk_total_size_{{$subscription_id}}"></div>
		
		<div id="disk_used_{{$subscription_id}}"></div>
		
		<div id="disk_free_{{$subscription_id}}"></div>
		
		<br />

	</div>
					
	<div class="col-md-6">
		
		<p>apache_version: {{$my_barservers[$subscription_id]->apache_version}}</p>
		<p>php_version: {{$my_barservers[$subscription_id]->php_version}}</p>
		<p>db_version: {{$my_barservers[$subscription_id]->db_version}}</p>
		<p>phpmyadmin_version: {{$my_barservers[$subscription_id]->phpmyadmin_version}}</p>
		
		<p>OWS rules: 
		
		<select>
		  <option value="on">On</option>
		  <option value="off">Off</option>
		</select>
		
		</p>
		
		<a class="btn btn-xs get_barserver_info" style="padding: 5px 5px 5px 5px" barserver_id="{{$my_barservers[$subscription_id]->id}}" href="/backups?barserver_id={{$my_barservers[$subscription_id]->id}}">{{trans('dashboard.get_server_credentials')}}</a>
							
		<div id ="info_{{$my_barservers[$subscription_id]->id}}"></div>
	</div>
</div>
					

	<form action="/create_websitebar" method="post">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<input type="hidden" name="wp_barserver_id" value="{{$my_barservers[$subscription_id]->id}}">
		<input type="hidden" name="user_id" value="{{$user_id}}">
		<input type="hidden" name="theme" id="theme_{{$subscription_id}}" value="">
		
							
	<div class="row">
	<div class="col-md-12">
		 
		<input name="subdomain" id="subdomain_{{$subscription_id}}" class="form-control subdomain_check" value="">
	</div>
	
	</div>
	
	<div class="row">
	<div class="col-md-12">
							
	@include('dashboard/_gallery',array('subscription_id' => $subscription_id))
	
	</div>
	</div>
	
	<div class="row">
	<div class="col-md-12">
	@if( $subscription_status == 'wc-active' )
	<input type="submit" class="et_manage_submit create_barsite" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" subscription_id="{{$subscription_id}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" value="Create website">
	@else
	<p>Not active subscription</p>
	@endif
	
	</div>
	</div>
					
</form>

<div class="row">
	<div class="col-md-12">
		<br />
		
		<input class="button sites_list_action" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="Sites"> 
		
		<input class="button sites_trash_action" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="Trash"> 
		
		<input class="button sites_backup_action" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="Backups"> 
		
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		
		<div id="plan_div_{{$subscription_id}}">
		
<div class="content table-responsive table-full-width">
	<table class="table table-hover table-striped barsites_table" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_status="{{$my_barservers[$subscription_id]->on}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}"  id="subscription_table_{{$subscription_id}}">
		<thead>
			<th>{{trans('dashboard.project')}}</th>
			<th>URL</th>
			<th>{{trans('dashboard.theme')}}</th>
			<th>{{trans('dashboard.wordpress_user')}}</th>
			<th>{{trans('dashboard.wordpress_pass')}}</th>
			<th>Admin url</th>
			<th>{{trans('dashboard.options')}}</th>
		</thead>
		<tbody>
                      						
		</tbody>
		</table>
	</div>
	
	  </div>
	</div>
</div>
	

					