@extends('layout')

@section('header')
  
@endsection

@section('content')

<div class="row">
	<div class="col-md-12">
		
		@if($admin)
			<p>Selected user: {{$user->user_email}}</p>
		@endif
		
		<h1>{{trans('dashboard.my_addons_products')}}</h1>
	</div>
</div>

<?php


////////////////MY ADDONS PRODUCTS////////////////
//////////////////////////////////////////////////
//////////////////////////////////////////////////

$customer_orders = get_posts( array(
	           'numberposts' => -1,
	           'meta_key'    => '_customer_user',
	           'meta_value'  => $user->ID,
	           'post_type'   => "shop_order",
	           'post_status' => ['wc-completed','wc-active'],
) );


//Columns must be a factor of 12 (1,2,3,4,6,12)
$numOfCols = 4;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;
?>
<div class="row">
<?php
foreach($customer_orders as $key => $order){
	
	$order_id = $order->ID;
    $order = new WC_Order( $order->ID );
    $order_items = $order->get_items();
	
	foreach ( $order_items as $key => $item ) {
	
?>  
        <div class="col-md-<?php echo $bootstrapColWidth; ?>">
            <div class="thumbnail">
				
               <?php
			$_product = get_product($item[product_id]);
			echo("<h4><a href='/set_omission?order_id=$order_id&user_id=$user->ID&product_id=$item[product_id]&order_item_id=$key'>" . get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang ) ) . '</a></h4>');
			echo("<a href='/set_omission?order_id=$order_id&user_id=$user->ID&product_id=$item[product_id]&order_item_id=$key'>" . $_product->get_image($size = array(300, 300)) . '</a>');
			?> 
			   
            </div>
        </div>
<?php
	}
    $rowCount++;
    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
}
?>
</div>


<div class="row">
	<div class="col-md-12">
		<h1>{{trans('dashboard.my_addons_subscriptions')}}</h1>
	</div>
</div>

<?php
////////////////MY ADDONS SUBSCRIPTIONS////////////////
///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

//Devuelve todas las subscripciones 
//Cada subscripcion puede tener varias subscripciones
$subscriptions = wcs_get_users_subscriptions($user->ID);

//Columns must be a factor of 12 (1,2,3,4,6,12)
$numOfCols = 4;
$rowCount = 0;
$bootstrapColWidth = 12 / $numOfCols;
?>
<div class="row">
<?php
foreach ($subscriptions as  $key => $subscription ){
	$subscription_status = $subscription->post->post_status;
	//if ($subscription_status == 'wc-active' ){ // active subscriptions only
		$subscription_id = $subscription->post->ID;
		$order_id = $subscription->order->post->ID; // order ID (corresponding to the subscription ID)
		$active_subscriptions_arr[] = $subscription->post->ID;
		
		//Cada subscripcion puede tener varias subscripciones
		$order_items = $subscription->get_items();
		
		foreach ( $order_items as $key => $item ) {
	
			if((!in_array($item[product_id], $plan_ids)) & ($subscription_status == 'wc-active' )) {
?>  
        <div class="col-md-<?php echo $bootstrapColWidth; ?>">
			
            <div class="thumbnail">
                
				<?php
				
				
					$_product = get_product($item[product_id]);
					echo("<h4><a href='/set_omission?order_id=$subscription_id&user_id=$user->ID&product_id=$item[product_id]&order_item_id=$key'>" . get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang ) ) . '</a></h4>');
					echo("<a href='/set_omission?order_id=$subscription_id&user_id=$user->ID&product_id=$item[product_id]&order_item_id=$key'>" . $_product->get_image($size = array(300, 300)) . '</a>');
				
			
				?> 
				
            </div>
        </div>
<?php
	}
}
    $rowCount++;
    if($rowCount % $numOfCols == 0) echo '</div><div class="row">';
}
?>
</div>


@endsection

