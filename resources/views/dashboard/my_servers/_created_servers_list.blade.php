@if($subscription_status == 'wc-active' )
				
	<div class="my_servers" width="100%" email= "{{$current_user->user_email}}" pete_token="{{$pete_tokens[$subscription_id]}}" barserver_ip= "{{$my_barservers[$subscription_id]->ip}}" barserver_status="{{$my_barservers[$subscription_id]->on}}" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}"  id="subscription_table_{{$subscription_id}}"></div>

@endif

<div class="row">
	<div class="col-md-12">

		<?php	
					
		foreach ( $order_items as $key => $item ) {			
			
			$_product = get_product($item[product_id]);    
			echo '<h4 class="title">' .get_the_title( icl_object_id( $item[product_id], 'product', false, $global_lang)).'</h4>';
		
		}
							
		?>

	</div>
</div>				

<div class="row">
	<div class="col-md-6">
		
		@if($my_barservers[$subscription_id]->on == true)
		
		<input class="button shut_down_action" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value=" {{trans('dashboard.shut_down')}}"> 
		<input class="button reboot_action" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.reboot')}}">
		
		@else
		
		<input class="button boot_action" barserver_id="{{$my_barservers[$subscription_id]->id}}" subscription_id= "{{$subscription_id}}" type="submit" name="action" value="{{trans('dashboard.boot')}}"> 
		
		@endif
		
		<br />
		<br />
							
		
		<p>Server: {{$my_barservers[$subscription_id]->linode_label}}</p>	
		<p>IP: <a target="_blank" href="http://{{$my_barservers[$subscription_id]->ip}}">{{$my_barservers[$subscription_id]->ip}}</a></p>
		@if($my_barservers[$subscription_id]->on == true)
		<p>status: {{trans('dashboard.running')}}</p>
		@else
		<p>status: {{trans('dashboard.off')}}</p>
		@endif
		<p>OS: {{$my_barservers[$subscription_id]->image}}</p>
		<p>region: {{$my_barservers[$subscription_id]->region}}</p>
		
		
		<div id="disk_total_size_{{$subscription_id}}"></div>
		
		<div id="disk_used_{{$subscription_id}}"></div>
		
		<div id="disk_free_{{$subscription_id}}"></div>
		
		<br />

	</div>
					
	<div class="col-md-6">
		
		<p>apache_version: {{$my_barservers[$subscription_id]->apache_version}}</p>
		<p>php_version: {{$my_barservers[$subscription_id]->php_version}}</p>
		<p>db_version: {{$my_barservers[$subscription_id]->db_version}}</p>
		<p>phpmyadmin_version: {{$my_barservers[$subscription_id]->phpmyadmin_version}}</p>
		
		<p>OWS rules: 
		
			<select>
				<option value="on">On</option>
				<option value="off">Off</option>
			</select>
		
		</p>
		
		<a class="btn btn-xs get_barserver_info" style="padding: 5px 5px 5px 5px" barserver_id="{{$my_barservers[$subscription_id]->id}}" href="/backups?barserver_id={{$my_barservers[$subscription_id]->id}}">{{trans('dashboard.get_server_credentials')}}</a>
							
		<div id ="info_{{$my_barservers[$subscription_id]->id}}"></div>
	</div>
</div>



					
	

					