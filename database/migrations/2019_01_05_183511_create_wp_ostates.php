<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpOstates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('wp_ostates', function(Blueprint $table) {
			
            $table->increments('id');
			
			$table->integer('order')->nullable();
			$table->string('en')->nullable();
			$table->string('es')->nullable();
			$table->boolean('admin')->nullable();
			$table->boolean('customers')->nullable();
			$table->string('views')->nullable();
			$table->string('label')->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
