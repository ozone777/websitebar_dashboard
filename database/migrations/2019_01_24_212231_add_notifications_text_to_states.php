<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNotificationsTextToStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
  	  Schema::table('wp_ostates', function(Blueprint $table) {

  			$table->text('notification_subject_en')->nullable();
			$table->text('notification_subject_es')->nullable();
			$table->text('notification_body_en')->nullable();
			$table->text('notification_body_es')->nullable();
	
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
