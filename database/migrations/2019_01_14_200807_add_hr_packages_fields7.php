<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddHrPackagesFields7 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::table('wp_omissions', function(Blueprint $table) {
	    $table->integer('hr_package_webtender_selected_id')->nullable();

 	 	$table->string('hr_package_webtender_name')->nullable();
		$table->string('hr_package_webtender_id')->nullable();
		
 	 	$table->string('hr_package_client_name')->nullable();
		$table->string('hr_package_client_id')->nullable();
		
 	 	$table->string('hr_package_title')->nullable();
		$table->text('hr_package_todo')->nullable();
		
		$table->datetime('hr_package_delivery_time')->nullable();
		
		$table->string('hr_package_webtender_approved_by')->nullable();
		$table->datetime('hr_package_webtender_approved_time')->nullable();
		
		$table->string('hr_package_customer_approved_by')->nullable();
		$table->datetime('hr_package_customer_approved_time')->nullable();
		 });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
