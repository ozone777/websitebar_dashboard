<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
		Schema::create('wp_otickets', function(Blueprint $table) {
            $table->increments('id');
            $table->string('summary')->nullable();
			$table->string('regarding')->nullable();
			$table->string('status')->nullable();
			$table->integer('user_id');
			$table->text('body')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
