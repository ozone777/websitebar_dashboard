<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpOmissionsStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
  	  Schema::create('wp_omissions_states', function(Blueprint $table) {
		
          	$table->increments('id');
  			$table->integer('omission_id')->nullable();
  			$table->integer('ostate_id')->nullable();
  			$table->integer('product_id')->nullable();
  			$table->integer('user_id')->nullable();
  			$table->text('current_url')->nullable();
          	$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
