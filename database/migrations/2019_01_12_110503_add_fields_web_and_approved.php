<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsWebAndApproved extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	  Schema::table('wp_omissions', function(Blueprint $table) {
			
      		$table->string('web_concept_one')->nullable();
  			$table->string('web_concept_two')->nullable();
  			$table->string('web_concept_three')->nullable();
    		$table->integer('web_concept_selected')->nullable();
    		$table->string('logo_approved_by')->nullable();
    		$table->string('web_approved_by')->nullable();
			
			
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
