<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpOmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create("wp_omissions", function(Blueprint $table) {
			
            $table->increments('id');
            $table->integer('user_id');
			$table->integer('order_id');
			$table->integer('order_item_id');
			$table->integer('product_id');
			$table->integer('subscription_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
