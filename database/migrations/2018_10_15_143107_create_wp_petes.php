<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpPetes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('wp_petes', function(Blueprint $table) {
            $table->increments('id');
			
			$table->string('db_version')->nullable();
			$table->string('php_version')->nullable();
			$table->string('apache_version')->nullable();
            $table->string('os_version')->nullable();
			$table->string('phpmyadmin_version')->nullable();
			$table->string('pete_version')->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
