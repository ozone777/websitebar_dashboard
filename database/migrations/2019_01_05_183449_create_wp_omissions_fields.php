<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpOmissionsFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('wp_omissions_fields', function(Blueprint $table) {
			
            $table->increments('id');
			$table->integer('row')->nullable();
			$table->integer('product_id')->nullable();
			
			$table->string('meta_key')->nullable();
			$table->string('type')->nullable();
			$table->string('radio_value')->nullable();
			$table->string('placeholder')->nullable();
			$table->string('placeholder_es')->nullable();
			
			$table->text('help_text')->nullable();
			$table->text('help_text_es')->nullable();
			
			$table->string('section')->nullable();
			$table->string('fnumber')->nullable();
			$table->string('permissions')->nullable();
			
			$table->string('default_value')->nullable();
			$table->string('set_value')->nullable();
			$table->integer('omission_id')->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
