<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddConceptsGlobals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
  	  Schema::table('wp_omissions', function(Blueprint $table) {
		
    		$table->string('concept_one')->nullable();
			$table->string('concept_two')->nullable();
			$table->string('concept_three')->nullable();
  			$table->integer('concept_selected')->nullable();
  			$table->string('concept_approved_by')->nullable();
  			$table->datetime('concept_approved_time')->nullable();
			
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
