<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpBarsites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('wp_barsites', function(Blueprint $table) {
            $table->increments('id');
			
			
            $table->string('template')->nullable();
			$table->text('wp_user')->nullable();
			$table->text('wp_info')->nullable();
			
			$table->integer('wp_barserver_id')->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
