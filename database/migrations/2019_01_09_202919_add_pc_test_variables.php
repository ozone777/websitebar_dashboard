<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPcTestVariables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	  Schema::table('wp_omissions', function(Blueprint $table) {

    			$table->integer('pc_bohemian_value')->nullable();
  		  	$table->string('pc_bohemian_letter')->nullable();
			
    			$table->integer('pc_outdoorsy_value')->nullable();
  		  	$table->string('pc_outdoorsy_letter')->nullable();
			
    			$table->integer('pc_informal_value')->nullable();
  		  	$table->string('pc_informal_letter')->nullable();
			
    			$table->integer('pc_laid_back_value')->nullable();
  		  	$table->string('pc_laid_back_letter')->nullable();
			
    			$table->integer('pc_subtle_value')->nullable();
  		  	$table->string('pc_subtle_letter')->nullable();
			
    			$table->integer('pc_delicate_value')->nullable();
  		  	$table->string('pc_delicate_letter')->nullable();
			
    			$table->integer('pc_understated_value')->nullable();
  		  	$table->string('pc_understated_letter')->nullable();
			
    			$table->integer('pc_monochrome_value')->nullable();
  		  	$table->string('pc_monochrome_letter')->nullable();
			
    			$table->integer('pc_flowy_value')->nullable();
  		  	$table->string('pc_flowy_letter')->nullable();
			
    			$table->integer('pc_rugged_value')->nullable();
  		  	$table->string('pc_rugged_letter')->nullable();
			
    			$table->integer('pc_handmade_value')->nullable();
  		  	$table->string('pc_handmade_letter')->nullable();
			
    			$table->integer('pc_rustic_value')->nullable();
  		  	$table->string('pc_rustic_letter')->nullable();
			
			$table->integer('pc_visual_character')->nullable();
	  		$table->integer('pc_visual_contrast')->nullable();
			$table->integer('pc_visual_geometry')->nullable();
  
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
