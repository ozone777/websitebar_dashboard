<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSystemFieldsToBarservers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('wp_barservers', function (Blueprint $table) {

			$table->string('apache_version')->nullable();
			$table->string('php_version')->nullable();
			$table->string('db_version')->nullable();
			$table->string('phpmyadmin_version')->nullable();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
