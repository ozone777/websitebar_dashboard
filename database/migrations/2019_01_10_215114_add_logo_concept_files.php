<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLogoConceptFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	  Schema::table('wp_omissions', function(Blueprint $table) {

    		$table->string('logo_concept_one')->nullable();
			$table->string('logo_concept_two')->nullable();
			$table->string('logo_concept_three')->nullable();

         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
