<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpBarservers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		
		Schema::create('wp_barservers', function(Blueprint $table) {
            $table->increments('id');
			
			$table->string('linode_label')->nullable();
            $table->string('linode_id')->nullable();
			$table->text('db_root_info')->nullable();
			$table->text('ssh_info')->nullable();
			$table->string('ipv6')->nullable();
			$table->string('ip')->nullable();
			$table->text('pete_token')->nullable();
			
			$table->integer('subscription_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->integer('product_id')->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
