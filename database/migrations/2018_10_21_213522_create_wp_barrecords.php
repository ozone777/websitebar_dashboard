<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpBarrecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('wp_barrecords', function(Blueprint $table) {
			
            $table->increments('id');
            $table->integer('domain_id')->nullable();
            
			$table->string('hostname')->nullable();
			$table->string('ip')->nullable();
			$table->string('aliases_to')->nullable();
			$table->string('name')->nullable();
			$table->string('value')->nullable();
			$table->string('ttl')->nullable();
			$table->string('subdomain')->nullable();
			$table->string('preference')->nullable();
			$table->string('mail_server')->nullable();
			
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
