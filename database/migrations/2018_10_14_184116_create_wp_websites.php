<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpWebsites extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
		Schema::create('wp_webistes', function(Blueprint $table) {
            $table->increments('id');
			
            $table->string('subdomain')->nullable();
            $table->string('domain')->nullable();
            $table->string('ip')->nullable();
            $table->string('template')->nullable();
            $table->integer('subscription_id')->nullable();
            $table->string('php_version')->nullable();
            $table->string('linode_id')->nullable();
            $table->string('linode_label')->nullable();
			
			$table->text('ssh_info')->nullable();
			$table->text('root_info')->nullable();
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
