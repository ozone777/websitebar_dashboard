<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWpOmissionmetas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
		Schema::create("wp_omissionmetas", function(Blueprint $table) {
			
            $table->increments('id');
            $table->string('meta_key');
			$table->text('meta_value');
			$table->integer('omission_id');
            $table->timestamps();
			
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
