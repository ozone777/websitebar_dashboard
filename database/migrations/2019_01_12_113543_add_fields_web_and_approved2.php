<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsWebAndApproved2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
  	  Schema::table('wp_omissions', function(Blueprint $table) {
		
		$table->datetime('logo_approved_time')->nullable();
		$table->datetime('web_approved_time')->nullable();
		
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
