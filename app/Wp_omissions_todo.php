<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Wp_ostate;
use Carbon\Carbon;
use Log;

class Wp_omissions_todo extends Model
{
	public static function create_todo($omissions_state_id,$ostate_id){
		
		$ostate =  Wp_ostate::findOrFail($ostate_id);
		
		if(isset($ostate->todo_delay)){
			
			$todo = Wp_omissions_todo::where('omissions_state_id', $omissions_state_id)->first();
			
			if(isset($todo)){
				$start_time = Carbon::now();
				$todo->start_time = $start_time;
				$end_time = Carbon::now();
				$todo->end_time = $end_time->addDays($ostate->todo_delay);
			}else{
				$todo = new Wp_omissions_todo();
				$todo->omissions_state_id = $omissions_state_id;
				$start_time = Carbon::now();
				$todo->start_time = $start_time;
				$end_time = Carbon::now();
				$todo->end_time = $end_time->addDays($ostate->todo_delay);
			}
			$todo->save();
		}else{
			Log::info("debe borrar: $omissions_state_id");
			$todo = Wp_omissions_todo::where('omissions_state_id', $omissions_state_id)->first();
			if(isset($todo)){
				$todo->delete();
			}
		}
	}
}
