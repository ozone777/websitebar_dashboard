<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Wp_ostate extends Model
{
	public function lang_name($lang){
		
		if($lang == "en"){
			return $this->en;
		}else if($lang == "es"){
			return $this->es;
		}
	}
	
	public static function get_states($view){
		$states = DB::select("select * from wp_omissions_states as os 
			INNER JOIN wp_ostates as o ON o.id = os.ostate_id WHERE o.view = '$view'");
		return $states;
	}
	
	
}