<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DateTime;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'wp_users';
	protected $primaryKey = 'ID';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'email_token','email_token_date'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
	
    public function generate_email_token(){
  	  //If email_token_date is more than 2 weeks generate another one
	  
  	  $date_2_weeks_ago = date('Y/m/d' ,strtotime('2 weeks ago'));
  	  $date_2_weeks_ago = new DateTime($date_2_weeks_ago);
	  
  	  if($this->email_token_date){
  		 if($date_2_weeks_ago > $this->email_token_date){
		 	
  	   	  	$date = new DateTime();
  	      	$date_number = $date->getTimestamp();
  	   	  	$this->email_token = md5($date_number+$this->ID);
  	   	  	$this->email_token_date = $date;
  			$this->save();
  		 }
  	  }else{
     	  	$date = new DateTime();
        	$date_number = $date->getTimestamp();
  	   	  	$this->email_token = md5($date_number+$this->ID);
  	   	  	$this->email_token_date = $date;
			$this->save();
  	  }
	  
	  return $this->email_token;
    }
	
}
