<?php
// My common functions
function hellopete()
{
	return "hello pete!";
}

function oback_url($link,$role,$wp_omission)
{
	if($link){
		return $link;
	}else{
		if(($role == "administrator") || ($role == "contributor")){
			return "/get_customer/{$wp_omission->user_id}";
		}else{
			return "/";
		}
		
	}
	
}

function state_translator($obj,$lang){
	if($lang == "en"){
		return $obj->en;
	}else if($lang == "es"){
		return $obj->es;
	}
}


?>