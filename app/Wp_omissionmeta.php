<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wp_omissionmeta extends Model
{
	public static function get_meta($omission_id,$meta_key){
		$wp_omissionmeta = Wp_omissionmeta::where('omission_id', '=', $omission_id)->where('meta_key', '=', $meta_key)->first();
		return $wp_omissionmeta;
	}
}
