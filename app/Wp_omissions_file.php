<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Orchestra\Imagine\Facade as Imagine;

class Wp_omissions_file extends Model
{
    //
	
	public static function create_thumbnail($path, $filename, $extension)
	{
	    $width  = 100;
	    $height = 100;
	    $mode   = ImageInterface::THUMBNAIL_OUTBOUND;
	    $size   = new Box($width, $height);

	    $thumbnail   = Imagine::open("{$path}/{$filename}.{$extension}")->thumbnail($size, $mode);
	    $destination = "{$filename}.thumb.{$extension}";

	    $thumbnail->save("{$path}/{$destination}");
	}
	
	
	public static function create_mockup($path, $filename, $extension)
	{
	    $width  = 900;
	    $height = 900;
	    $mode   = ImageInterface::THUMBNAIL_OUTBOUND;
	    $size   = new Box($width, $height);

	    $thumbnail   = Imagine::open("{$path}/{$filename}.{$extension}")->thumbnail($size, $mode);
	    $destination = "{$filename}.mockup.{$extension}";

	    $thumbnail->save("{$path}/{$destination}");
	}
	
	public static function social_image($path, $filename, $extension)
	{
	    $width  = 260;
	    $height = 260;
	    $mode   = ImageInterface::THUMBNAIL_OUTBOUND;
	    $size   = new Box($width, $height);

	    $thumbnail   = Imagine::open("{$path}/{$filename}.{$extension}")->thumbnail($size, $mode);
	    $destination = "{$filename}.social.{$extension}";

	    $thumbnail->save("{$path}/{$destination}");
	}
	
}
