<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wp_omissions_field extends Model
{
	 	protected $fillable = ['row', 'meta_key', 'type', 'placeholder', 'placeholder_es', 'default_value', 'omission_id', 'dinamycs_space','help_text', 'help_text_es', 'set_value', 'section' ];
		
}
