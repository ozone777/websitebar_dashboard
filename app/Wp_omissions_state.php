<?php

namespace App;
use Mail;
use Log;
use App\Options;
use Mailjet;
use App\User;
use View;
use App\Wp_omission;
use App;
use App\Wp_omissions_watcher;
use Auth;
use App\Wp_ostate;
use App\DashboardOption;
use DB;
use Mailjet\Resources;

use Illuminate\Database\Eloquent\Model;

class Wp_omissions_state extends Model
{
    //
	
    public function __construct(){
		
		include_once(env('WP_LOAD_PATH')."/wp-load.php"); 
		
	}
	
	public function get_todos(){
		
		$sql = "SELECT todos.end_time, todos.start_time, s.es, s.en FROM wp_omissions_todos todos 
inner join wp_omissions_states os on os.id = todos.omissions_state_id 
inner join wp_ostates s on s.id = os.ostate_id 
where os.id = $this->id";
		
		return DB::select($sql);
	}
	
	public function ostate(){
		return Wp_ostate::findOrFail($this->ostate_id);
	}
	
	public function change_state($state_label){
		$ostate = Wp_ostate::where('label', '=', $state_label)->first();
		$this->ostate_id = $ostate->id;
		$this->save();
	}
	
 	public function send_email_notification() {
		Mail::send('emails.test',['testVar' => 'Mail desde el modelo'],
		 function($message) {
		 $message->to('pedroconsuegrat@gmail.com')->subject('A simple test desde el modelo');
	   });
 	}
	
	public static function get_state($wp_omission,$view)
	{
		
		//Crea estado si no tiene ningun estado
		$selected_state = Wp_omissions_state::where('omission_id', '=', $wp_omission->id)->first();
		
		if(!isset($selected_state)){
			
	     	$selected_state = new Wp_omissions_state();
		 	$ostate = Wp_ostate::where('view', '=', $view)->where('first_state', '=', true)->first();
			$selected_state->omission_id  = $wp_omission->id;		
			$selected_state->ostate_id = $ostate->id;
			$selected_state->save();
		}
	   
		return $selected_state;
	}
	
	public function send_notification(){
		Log::info("entro en send_notification");
		
		$omission = Wp_omission::findOrFail($this->omission_id);
		$customer = User::findOrFail($omission->user_id);
		$ostate = Wp_ostate::findOrFail($this->ostate_id);
		$options = new DashboardOption();
		
		$first_name = ucfirst(get_user_meta( $customer->ID, "first_name", true )); 
		$last_name = ucfirst(get_user_meta( $customer->ID, "last_name", true )); 
		$customer_complete_name = $first_name ." " .$last_name;
			
		$customer->generate_email_token();
		$url=$options->get_meta_value('dashboard_url')."edit/$omission->id?email_token=$customer->email_token";
		$header_image = "https://ozonegroup.co/logo_websitebar.png";
		
		if($customer->dashboard_lang){
			$global_lang = $customer->dashboard_lang;
			App::setLocale($customer->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		
		$here = trans('addons.here');
		
		if($global_lang == "es"){
			
			$email_subject = $ostate->notification_subject_es;
			$email_body = $ostate->notification_body_es;
			
		}else if($global_lang == "en"){
			
			$email_subject = $ostate->notification_subject_en;
			$email_body = $ostate->notification_body_en;
			
		}
		
		Log::info("DESPUES DE ");
		Log::info("email_subject: $email_subject");
		Log::info("email_body: $email_body");
		Log::info("url: $url");
		
		$mj = new \Mailjet\Client(getenv('MJ_APIKEY_PUBLIC'), getenv('MJ_APIKEY_PRIVATE'),true,['version' => 'v3.1']);
		
		$view = View::make('emails.omission_notification', ["header_image" => $header_image, "url" => $url, "options" => $options, "here" => $here, "notification_body" => $email_body, "customer_complete_name" => $customer_complete_name]);
		
		$body = [
		    'Messages' => [
		        [
		            'From' => [
		                'Email' => "software@ozonelabs.us",
		                'Name' => "WebsiteBar"
		            ],
		            'To' => [
		                [
		                    'Email' => "$customer->user_email",
		                    'Name' => "$first_name $last_name"
		                ]
		            ],
					'Subject' => $email_subject,
					'HTMLPart' => $view->render()
		        ]
		    ]
		];
		
		$response = $mj->post(Resources::$Email, ['body' => $body]);
		
		if ($response->success())
		  Log::info($response->getData());
		else
		  Log::info($response->getStatus());
		
	}
	
	
	
	
	
 	public function mission_notification($wp_user,$omission,$todo=null) {
		
		//OBTENER CURRENT USER
		$current_user = Auth::user();
		
		//OBTENER LAS OPCIONES DEL SISTEMA
		$options = new Options();
		
		//OBTENER EL PRODUCTO ORIGINAL
 	    $product_origin_id =icl_object_id($this->product_id, 'product', false);
  	    $product = get_product($product_origin_id);
		
		//OBTENER EL TOKEN
		$wp_user->generate_email_token();
		
		//OBTENER EL CORREO QUE ENVIA
		$sender_email = $options->get_meta_value('sender_email');
		
		//CONSTRUCCIÓN DE LA URL 
		if (strpos($this->current_url, 'package_url') !== false) {
		  $url=$this->current_url . "&email_token=$wp_user->email_token";
		}
		else{
		  $url=$this->current_url . "?email_token=$wp_user->email_token";
		}
		
		//Log::info("Esta es la url desde mission_notification");
		//Log::info("URL: $url");
		
		//OBTENER EL LENGUAJE Y TITULO DEL PRODUCTO
		if($wp_user->ogrowthlang == "es"){
			$lang = "es";
		}else if ($wp_user->ogrowthlang == "en"){
			$lang = "en";
		}else{
			$lang = $options->get_meta_value('default_lang');
		}
		
		//$product_title = get_the_title( icl_object_id($product_origin_id, 'product', false, $lang));
        $product_title = ucfirst(strtolower(get_the_title( icl_object_id( $product_origin_id, 'product', false, $lang))));
		
		//ASIGNAR EL LENGUAJE AL DASHBOARD 
		App::setLocale($lang);
		
		if(($this->ostate()->label == "social_aceptada") || ($this->ostate()->label == "web_aceptada") || ($this->ostate()->label == "email_aceptada") || ($this->ostate()->label == "branding_aceptada") || ($this->ostate()->label == "edit_aceptada")) {
			if($lang == "es"){
				$mail_subject = "Por favor llena el brief";
				$view = View::make('emails.mision_aceptada', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}else if($lang == "en"){
				$mail_subject = "Please fill out the brief";
				$view = View::make('emails.mission_accepted', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}	
		}else if(($this->ostate()->label == "social_completada") || ($this->ostate()->label == "web_completada") || ($this->ostate()->label == "email_completada") || ($this->ostate()->label == "branding_completada") || ($this->ostate()->label == "edit_completada")) {
			if($lang == "es"){
				$mail_subject = "Tu servicio de ". $product_title ." ha sido completado";
				$view = View::make('emails.mision_completada', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}else if($lang == "en"){
				$mail_subject = "Your " . $product_title . " service has been completed";
				$view = View::make('emails.mission_completed', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
		    }
		}else if(($this->ostate()->label == "social_suspendida") || ($this->ostate()->label == "web_suspendida") || ($this->ostate()->label == "email_suspendida") || ($this->ostate()->label == "branding_suspendida") || ($this->ostate()->label == "edit_suspendida")) {
			if($lang == "es"){
				$url=$options->get_meta_value('dashboard_url')."/go_to_payment?email_token=$wp_user->email_token";
				$mail_subject = "Tu servicio de ". $product_title ." ha sido suspendido";
				$view = View::make('emails.mision_suspendida', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}else if($lang == "en"){
				$url=$options->get_meta_value('dashboard_url')."/go_to_payment?email_token=$wp_user->email_token";
				$mail_subject = "Your " . $product_title . " service has been suspended";
				$view = View::make('emails.mission_suspended', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}
		}else if(($this->ostate()->label == "social_responde_el_quiz") || ($this->ostate()->label == "web_responde_el_quiz") || ($this->ostate()->label == "email_responde_el_quiz") || ($this->ostate()->label == "branding_responde_el_quiz")) {
			if($lang == "es"){
				$mail_subject = "Responde el quiz de estilos visuales";
				$generating_title = "responde el quiz de estilos visuales";
				$view = View::make('emails.responde_el_quiz', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title]);
			}else if($lang == "en"){
				$mail_subject = "Answer the visual styles quiz";
				$generating_title = "Answer the visual styles quiz";
				$view = View::make('emails.answer_the_quiz', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title]);
			}
		}
		else if(($this->ostate()->label == "social_resultados_del_quiz") || ($this->ostate()->label == "web_resultados_del_quiz") || ($this->ostate()->label == "email_resultados_del_quiz") || ($this->ostate()->label == "branding_resultados_del_quiz")) {
					if($lang == "es"){
						$mail_subject = "Puedes revisar los resultados del quiz de estilos visuales";
						$generating_title = "Puedes revisar los resultados del quiz de estilos visuales";
						$view = View::make('emails.revisa_los_resultados_del_quiz', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title]);
					}else if($lang == "en"){
						$mail_subject = "Check the visual styles quiz results";
						$generating_title = "Check the visual styles quiz results";
						$view = View::make('emails.answer_the_quiz', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title]);
					}
				}
		else if($this->ostate()->label == "social_creando_calendario"){
			if($lang == "es"){
				$mail_subject = "Estamos creando el calendario editorial para tus redes sociales";
				$creating_subject = "crear el calendario editorial";
				$view = View::make('emails.creando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options, "todo" => $todo, "creating_subject" => $creating_subject]);
			}else if($lang == "en"){
				$mail_subject = "We are creating the editorial calendar for your social networks";
				$creating_subject = "the editorial calendar";
				$view = View::make('emails.creating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options, "todo" => $todo, "creating_subject" => $creating_subject]);
			}	
		}
		else if($this->ostate()->label == "social_creando_pauta"){
			if($lang == "es"){
				$mail_subject = "Estamos creando la pauta para tus redes sociales";
				$creating_subject = "la creación de la pauta online";
				$view = View::make('emails.creando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options, "todo" => $todo, "creating_subject" => $creating_subject]);
			}else if($lang == "en"){
				$mail_subject = "We are creating the ads for your social networks";
				$creating_subject = "the social networks ads";
				$view = View::make('emails.creating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options, "todo" => $todo, "creating_subject" => $creating_subject]);
			}	
		}
		else if($this->ostate()->label == "social_aprobar_estrategia"){
			if($lang == "es"){
				$mail_subject = "Ahora es tu turno de aprobar la estrategia de tus redes sociales";
				$approve_title = "aprobar la estrategia para tus redes sociales";
				$approve_button = "Aprobar estrategia";
				$view = View::make('emails.aprobar_mision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
				
			}else if($lang == "en"){
				$mail_subject = "Now it's your turn to approve the social media strategy";
				$approve_title = "approve the social media strategy";
				$approve_button = "Approve strategy";
				$view = View::make('emails.approve_mission', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}	
		}else if($this->ostate()->label == "social_aprobar_copy"){
			if($lang == "es"){
				$mail_subject = "Ahora es tu turno de aprobar el copy para tus redes sociales";
				$approve_title = "aprobar el copy para tus redes sociales";
				$approve_button = "Aprobar copy";
				$view = View::make('emails.aprobar_mision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}else if($lang == "en"){
				$mail_subject = "Now it's your turn to approve the social media copy we've drafted";
				$approve_title = "approve the social media copy we've drafted";
				$approve_button = "Approve copy";
				$view = View::make('emails.approve_mission', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}	
		}else if($this->ostate()->label == "social_aprobar_imagenes"){
			if($lang == "es"){
				$mail_subject = "Ahora es tu turno de aprobar las imágenes para tus redes sociales";
				$approve_title = "aprobar las imágenes para tus redes sociales";
				$approve_button = "Aprobar imágenes";
				$view = View::make('emails.aprobar_mision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}else if($lang == "en"){
				$mail_subject = "Now it's your turn to approve the social media images we've drafted";
				$approve_title = "approve the social media images we've drafted";
				$approve_button = "Approve images";
				$view = View::make('emails.approve_mission', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}	
		}
		else if($this->ostate()->label == "social_creando_contenido_pauta"){
			if($lang == "es"){
				$mail_subject = "Estamos creando el contenido para la pauta de tus redes sociales";
				$generating_title = "creando el contenido para la pauta de tus redes sociales";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "We are creating the ad content";
				$generating_title = "ad designs";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
			}	
		}else if($this->ostate()->label == "social_creando_imagenes_pauta"){
			if($lang == "es"){
				$mail_subject = "Estamos creando los diseños para la pauta de tus redes sociales";
				$generating_title = "creando los diseños para la de tus pauta de redes sociales";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "We are creating the ad designs";
				$generating_title = "ad designs";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
					}	
		 }else if($this->ostate()->label == "social_aprobar_pauta"){
			if($lang == "es"){
				$mail_subject = "Ahora es tu turno de aprobar la pauta para tus redes sociales";
				$approve_title = "aprobar el pauta para tus redes sociales";
				$approve_button = "Aprobar pauta";
				$view = View::make('emails.aprobar_mision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}else if($lang == "en"){
				$mail_subject = "Now it's your turn to approve the ad we've drafted";
				$approve_title = "approve the social media ad we've drafted";
				$approve_button = "Approve ad";
				$view = View::make('emails.approve_mission', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}	
		}else if($this->ostate()->label == "social_publicando"){
			if($lang == "es"){
				$mail_subject = "Ya estamos publicando en tus redes sociales";
				$view = View::make('emails.social_publicando_es', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options ]);
			}else if($lang == "en"){
				$mail_subject = "We are now publishing in your social networks";
				$view = View::make('emails.social_publicando_en', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options ]);
			}	
		}
		else if($this->ostate()->label == "web_generando_bocetos"){
			if($lang == "es"){
				$mail_subject = "Estamos generando los bocetos de tu sitio web";
				$generating_title = "generando los bocetos";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "We are generating the web sketchs";
				$generating_title = "web sketchs";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
			}	
		}else if($this->ostate()->label == "web_esperando_contenido"){
			if($lang == "es"){
				$mail_subject = "Estamos esperando el contenido para tu sitio web";
				$view = View::make('emails.esperando_contenido', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}else if($lang == "en"){
				$mail_subject = "We are waiting for the content of your web site";
				$view = View::make('emails.waiting_content', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}	
		}else if($this->ostate()->label == "web_desarrollando"){
			if($lang == "es"){
				$mail_subject = "Estamos desarrolando tu sitio web";
				$generating_title = "desarrollando tu sitio web";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "We are developing your website";
				$generating_title = "develop your website";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
			}	
		}else if($this->ostate()->label == "web_disenando"){
			if($lang == "es"){
				$mail_subject = "Estamos diseñando tu sitio web";
				$generating_title = "diseñando tu sitio web";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "We are designing your website";
				$generating_title = "design your website";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
			}	
		} else if($this->ostate()->label == "web_desarrollando_cambios"){
					if($lang == "es"){
						$mail_subject = "Estamos desarrollando los cambios en tu sitio web";
						$generating_title = "desarrollando cambios a tu sitio web";
						$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
					}else if($lang == "en"){
						$mail_subject = "We are developing changes on your website";
						$generating_title = "developing your website";
						$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
					}	
				}
		else if($this->ostate()->label == "web_aprobar_diseno"){
			if($lang == "es"){
				$mail_subject = "Ahora es tu turno de aprobar el diseño del sitio web";
				$approve_title = "aprobar diseño del sitio web";
				$approve_button = "Aprobar diseño";
				$view = View::make('emails.aprobar_mision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}else if($lang == "en"){
				$mail_subject = "Now it's your turn to approve the website design";
				$approve_title = "approve the website we've designed";
				$approve_button = "Approve design";
				$view = View::make('emails.approve_mission', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}	
		}else if($this->ostate()->label == "email_generando_plantillas"){
			if($lang == "es"){
				$mail_subject = "Estamos generando los bocetos de plantillas para email";
				$generating_title = "generando los plantillas de emails";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "We are generating the email concepts";
				$generating_title = "email concepts";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
			}	
			
		}else if($this->ostate()->label == "email_aprobar_plantilla"){
			if($lang == "es"){
				$mail_subject = "Ahora es tu turno de aprobar la plantilla de email desarrollada";
				$approve_title = "aprobar la plantilla de email";
				$approve_button = "Aprobar plantilla de email";
				$view = View::make('emails.aprobar_mision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}else if($lang == "en"){
				$mail_subject = "Now it's your turn to approve the email template we've drafted";
				$approve_title = "approve the email template we've drafted";
				$approve_button = "Approve email template";
				$view = View::make('emails.approve_mission', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}	
		}else if($this->ostate()->label == "email_esperando_contenido"){
			if($lang == "es"){
				$mail_subject = "Estamos esperando el contenido para tu email";
				$view = View::make('emails.esperando_contenido', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}else if($lang == "en"){
				$mail_subject = "We are waiting for the content of your email";
				$view = View::make('emails.waiting_content', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}	
		}else if($this->ostate()->label == "email_creando_contenido"){
			if($lang == "es"){
				$mail_subject = "Estamos creando el contenido para tu campaña de email marketing";
				$generating_title = "creando el contenido para email marketing";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "We are creating content for your email marketing campaign";
				$generating_title = "create the email marketing content";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
			}	
		}else if($this->ostate()->label == "email_creando_email"){
			if($lang == "es"){
				$mail_subject = "Estamos creando el email para tu campaña de email marketing";
				$generating_title = "creando el email para tu campaña de email marketing";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "We are creating the email for your email marketing campaign";
				$generating_title = "create the email for your email marketing campaign";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
			}	
		}
		else if($this->ostate()->label == "email_aprobar_email"){
			if($lang == "es"){
				$mail_subject = "Ahora es tu turno de aprobar el email que hemos desarrollado";
				$approve_title = "aprobar el email";
				$approve_button = "Aprobar email";
				$view = View::make('emails.aprobar_mision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}else if($lang == "en"){
				$mail_subject = "Now it's your turn to approve the email we've drafted";
				$approve_title = "approve the email we've drafted";
				$approve_button = "Approve email";
				$view = View::make('emails.approve_mission', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}	
		}else if($this->ostate()->label == "email_enviando"){
			if($lang == "es"){
				$mail_subject = "Estamos enviando tu email";
				$view = View::make('emails.enviando_email', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}else if($lang == "en"){
				$mail_subject = "We are sending your email";
				$view = View::make('emails.sending_email', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}	
		}
		else if($this->ostate()->label == "branding_generando_conceptos"){
			if($lang == "es"){
				$mail_subject = "Estamos generando los conceptos de marca";
				$generating_title = "generando los conceptos de marca";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "We are generating the branding concepts";
				$generating_title = "generating brand concepts";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'generating_title' => $generating_title , "todo" => $todo]);
			}	
			
		}else if($this->ostate()->label == "branding_aprobar_concepto"){
			if($lang == "es"){
				$mail_subject = "Ahora es tu turno de aprobar el concepto de marca desarrollado";
				$approve_title = "aprobar el concepto de marca";
				$approve_button = "Aprobar el concepto de marca";
				$view = View::make('emails.aprobar_mision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}else if($lang == "en"){
				$mail_subject = "Now it's your turn to approve the brand concept we've developed";
				$approve_title = "approve the brand concept";
				$approve_button = "Approve the brand concept";
				$view = View::make('emails.approve_mission', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options,'approve_title' => $approve_title, 'approve_button' =>  $approve_button ]);
			}	
		}else if($this->ostate()->label == "branding_implementando_cambios"){
			if($lang == "es"){
				$mail_subject = "Estamos implementando los cambios";
				$view = View::make('emails.implementando_cambios', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}else if($lang == "en"){
				$mail_subject = "We are implementing the changes";
				$view = View::make('emails.implementing_changes', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}	
		}else if($this->ostate()->label == "branding_creando_manual_de_marca"){
			if($lang == "es"){
				$mail_subject = "Estamos generando el manual de marca";
				$view = View::make('emails.generando', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}else if($lang == "en"){
				$mail_subject = "We are creating the brand manual";
				$view = View::make('emails.generating', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}	
		}
		else if($this->ostate()->label == "edit_en_progreso"){
			if($lang == "es"){
				$mail_subject = "tu misión $product_title esta en progreso";
				$view = View::make('emails.mision_en_progreso', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options, "todo" => $todo]);
			}else if($lang == "en"){
				$mail_subject = "Your mission $product_title is in progress";
				$view = View::make('emails.mission_in_progress', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options, "todo" => $todo]);
			}	
		}else if($this->ostate()->label == "edit_lista_para_revision"){
			if($lang == "es"){
				$mail_subject = "Tu misión $product_title esta lista para revisión";
				$view = View::make('emails.lista_para_revision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}else if($lang == "en"){
				$mail_subject = "Your mission $product_title is ready for revision";
				$view = View::make('emails.ready_for_revision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title, "options" => $options]);
			}	
		}
		
		
		//ENVÍO DE EMAIL CON MAILJET A CLIENTE
		$params = [
		        "method" => "POST",
				"from" => $sender_email,
		        "to" => $wp_user->user_email,
		        "subject" => $mail_subject,
				"html" => $view->render()
		    ];

		    $result = Mailjet::sendEmail($params);

		    if (!Mailjet::getResponseCode() == 200){
			    echo "error - ".Mailjet::getResponseCode();
			    return $result;
		    }
			
			
		//ENVÍO DE EMAIL CON MAILJET A CURRENT USER
		$params = [
			        "method" => "POST",
					"from" => $sender_email,
			        "to" => $current_user->user_email,
			        "subject" => $mail_subject . " --COPY--",
					"html" => $view->render()
			    ];

			    $result = Mailjet::sendEmail($params);

			    if (!Mailjet::getResponseCode() == 200){
				    echo "error - ".Mailjet::getResponseCode();
				    return $result;
			    }
				
		
		///NOTIFICACIONES PARA LOS WATCHERS
		/*
		$watchers= Wp_omissions_watcher::where('omission_id',$this->omission_id)->where('product_id',$product->ID)->get();
		foreach($watchers as $watcher ) {
			
			Log::info("watcher_ema: $watcher->watcher_email");
			
			$params = [
			        "method" => "POST",
					"from" => $sender_email,
			        "to" => $watcher->watcher_email,
			        "subject" => $mail_subject,
					"html" => $view->render()
			    ];

			    $result = Mailjet::sendEmail($params);

			    if (!Mailjet::getResponseCode() == 200){
				    echo "error - ".Mailjet::getResponseCode();
				    return $result;
			    }
			
		}
		*/
		
 	}
	
 	public function brief_sent($product_title) {
		
		Log::info("entro en brief_sent");
		
		$options = new Options();
		
		$hi = trans('dashboard.hi');
		$notice = trans('dashboard.notice');
		$more_information = trans('dashboard.more_information');
		$click_it = trans('dashboard.click_it');
		$phone = trans('dashboard.phone');
		$contact_info = trans('dashboard.contact_info');
		$connect_with_us = trans('dashboard.connect_with_us');
		
		$sender_email = $options->get_meta_value('sender_email');
		$omission = Wp_omission::findOrFail($this->omission_id);
		
		$wp_user = User::findOrFail($omission->user_id);
		$wp_user->generate_email_token();
		
		if (strpos($this->current_url, 'package_url') !== false) {
		  $url=$this->current_url . "&email_token=$wp_user->email_token";
		}
		else{
		  $url=$this->current_url . "?email_token=$wp_user->email_token";
		}
		
		if($omission_state->time_interval){
		  $url = $url . "&time_interval={$omission_state->time_interval}";
		  Log::info("URL despues de add: $url");
		}
		
		$url=$url . "&stepy=0";
		$mail_subject = trans('dashboard.mission_submitted');
		
		$view = View::make('emails.brief_enviado', ['user' => $wp_user, "url" => $url]);
		
		//Customer notification
		$params = [
		        "method" => "POST",
				"from" => $sender_email,
		        "to" => $wp_user->user_email,
		        "subject" => $mail_subject,
				"html" => $view->render()
		    ];

		    $result = Mailjet::sendEmail($params);

		    if (!Mailjet::getResponseCode() == 200){
			    echo "error - ".Mailjet::getResponseCode();
			    return $result;
		    }
		Log::info("final en brief_sent");
 	}
	
 	public function in_progress($product_title) {
		$options = new Options();
		
		$hi = trans('dashboard.hi');
		$notice = trans('dashboard.notice');
		$more_information = trans('dashboard.more_information');
		$click_it = trans('dashboard.click_it');
		$phone = trans('dashboard.phone');
		$contact_info = trans('dashboard.contact_info');
		$connect_with_us = trans('dashboard.connect_with_us');
		
		$sender_email = $options->get_meta_value('sender_email');
		$omission = Wp_omission::findOrFail($this->omission_id);
		
		$wp_user = User::findOrFail($omission->user_id);
		$wp_user->generate_email_token();
		
		if (strpos($this->current_url, 'package_url') !== false) {
		  $url=$this->current_url . "&email_token=$wp_user->email_token";
		}
		else{
		  $url=$this->current_url . "?email_token=$wp_user->email_token";
		}
		
		if($omission_state->time_interval){
		  $url = $url . "&time_interval={$omission_state->time_interval}";
		  Log::info("URL despues de add: $url");
		}
		
		$url=$url . "&stepy=0";
		$mail_subject = trans('dashboard.mission_in_progress');
		
		$view = View::make('emails.mision_en_progreso', ['user' => $wp_user, "url" => $url, "product_title" => $product_title]);
		
		//Customer notification
		$params = [
		        "method" => "POST",
				"from" => $sender_email,
		        "to" => $wp_user->user_email,
		        "subject" => $mail_subject,
				"html" => $view->render()
		    ];

		    $result = Mailjet::sendEmail($params);

		    if (!Mailjet::getResponseCode() == 200){
			    echo "error - ".Mailjet::getResponseCode();
			    return $result;
		    }
		
 	}
	
 	public function ready_for_revision($product_title) {
		$options = new Options();
		
		$hi = trans('dashboard.hi');
		$notice = trans('dashboard.notice');
		$more_information = trans('dashboard.more_information');
		$click_it = trans('dashboard.click_it');
		$phone = trans('dashboard.phone');
		$contact_info = trans('dashboard.contact_info');
		$connect_with_us = trans('dashboard.connect_with_us');
		
		$sender_email = $options->get_meta_value('sender_email');
		$omission = Wp_omission::findOrFail($this->omission_id);
		
		$wp_user = User::findOrFail($omission->user_id);
		$wp_user->generate_email_token();
		
		if (strpos($this->current_url, 'package_url') !== false) {
		  $url=$this->current_url . "&email_token=$wp_user->email_token";
		}
		else{
		  $url=$this->current_url . "?email_token=$wp_user->email_token";
		}
		
		if($omission_state->time_interval){
		  $url = $url . "&time_interval={$omission_state->time_interval}";
		  Log::info("URL despues de add: $url");
		}
		
		$url=$url . "&stepy=0";
		$mail_subject = trans('dashboard.mission_ready_for_revision');
		
		$view = View::make('emails.lista_para_revision', ['user' => $wp_user, "url" => $url, "product_title" => $product_title]);
		
		//Customer notification
		$params = [
		        "method" => "POST",
				"from" => $sender_email,
		        "to" => $wp_user->user_email,
		        "subject" => $mail_subject,
				"html" => $view->render()
		    ];

		    $result = Mailjet::sendEmail($params);

		    if (!Mailjet::getResponseCode() == 200){
			    echo "error - ".Mailjet::getResponseCode();
			    return $result;
		    }
 	}
	
 	public function completed($product_title) {
		$options = new Options();
		
		$hi = trans('dashboard.hi');
		$notice = trans('dashboard.notice');
		$more_information = trans('dashboard.more_information');
		$click_it = trans('dashboard.click_it');
		$phone = trans('dashboard.phone');
		$contact_info = trans('dashboard.contact_info');
		$connect_with_us = trans('dashboard.connect_with_us');
		
		$sender_email = $options->get_meta_value('sender_email');
		$omission = Wp_omission::findOrFail($this->omission_id);
		
		$wp_user = User::findOrFail($omission->user_id);
		$wp_user->generate_email_token();
		
		if (strpos($this->current_url, 'package_url') !== false) {
		  $url=$this->current_url . "&email_token=$wp_user->email_token";
		}
		else{
		  $url=$this->current_url . "?email_token=$wp_user->email_token";
		}
		
		if($omission_state->time_interval){
		  $url = $url . "&time_interval={$omission_state->time_interval}";
		  Log::info("URL despues de add: $url");
		}
		
		$url=$url . "&stepy=0";
		$mail_subject = trans('dashboard.mission_completed');
		
		$view = View::make('emails.mision_completada', ['user' => $wp_user, "url" => $url, "product_title" => $product_title]);
		
		//Customer notification
		$params = [
		        "method" => "POST",
				"from" => $sender_email,
		        "to" => $wp_user->user_email,
		        "subject" => $mail_subject,
				"html" => $view->render()
		    ];

		    $result = Mailjet::sendEmail($params);

		    if (!Mailjet::getResponseCode() == 200){
			    echo "error - ".Mailjet::getResponseCode();
			    return $result;
		    }
 	}
	
 	public function suspended($product_title) {
		$options = new Options();
		
		$hi = trans('dashboard.hi');
		$notice = trans('dashboard.notice');
		$more_information = trans('dashboard.more_information');
		$click_it = trans('dashboard.click_it');
		$phone = trans('dashboard.phone');
		$contact_info = trans('dashboard.contact_info');
		$connect_with_us = trans('dashboard.connect_with_us');
		
		$sender_email = $options->get_meta_value('sender_email');
		$omission = Wp_omission::findOrFail($this->omission_id);
		
		$wp_user = User::findOrFail($omission->user_id);
		$wp_user->generate_email_token();
		
		if (strpos($this->current_url, 'package_url') !== false) {
		  $url=$this->current_url . "&email_token=$wp_user->email_token";
		}
		else{
		  $url=$this->current_url . "?email_token=$wp_user->email_token";
		}
		
		if($omission_state->time_interval){
		  $url = $url . "&time_interval={$omission_state->time_interval}";
		  Log::info("URL despues de add: $url");
		}
		
		$url=$url . "&stepy=0";
		$mail_subject = trans('dashboard.mission_suspended');
		
		$view = View::make('emails.state_notification', ['user' => $wp_user, "hi" => $hi, "mail_subject" => $mail_subject, "product_title" => $product_title, "url" => $url, "notice" => $notice, "more_information" => $more_information, "click_it" => $click_it, "phone" => $phone, "contact_info" => $contact_info, "connect_with_us" => $connect_with_us, "options" => $options ]);
		
		//Customer notification
		$params = [
		        "method" => "POST",
				"from" => $sender_email,
		        "to" => $wp_user->user_email,
		        "subject" => $mail_subject,
				"html" => $view->render()
		    ];

		    $result = Mailjet::sendEmail($params);

		    if (!Mailjet::getResponseCode() == 200){
			    echo "error - ".Mailjet::getResponseCode();
			    return $result;
		    }
 	}
	
}