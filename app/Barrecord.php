<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barrecord extends Model
{
    //
	//protected $fillable = ['bardomain_id', 'hostname','ip','aliases_to','name','value','ttl','subdomain','preference','mail_server','target','linode_id'];
	protected $fillable = ['bardomain_id', 'type', 'hostname','ip','aliases_to','name','value','ttl','subdomain','preference','mail_server','target','linode_id'];
	
	protected $table = 'wp_barrecords';

	
}
