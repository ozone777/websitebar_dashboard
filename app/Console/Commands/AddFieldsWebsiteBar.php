<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;
use App\Wp_post;

class AddFieldsWebsiteBar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'add_fields_to_websitebar';
	#protected $signature = 'addfields {--queue=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add fields to websitebar omissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		
		//DB::table('wp_omissions_fields')->delete();
		//Wp_omissions_field::whereNull('product_id')->where('omission_id', '=', "$omission_id")->get();
		
		DB::table('wp_omissions_fields')->whereNotNull('product_id')->delete();
		
 $rows = [];
 
 
 $rows["Premium Branding Package"] = [
	 
	 
     ['row' => 1,'meta_key' => 'branding_kit_select','type' => 'label', 'placeholder' => 'Project manager info:', 'placeholder_es' => 'Información del encargado del proyecto:'],
	 
     ['row' => 2,'meta_key' => 'branding_kit_project_manager','type' => 'text', 'help_text' => 'Name', 'help_text_es' => 'Nombre'],
  
     ['row' => 2,'meta_key' => 'branding_kit_project_manager_phone','type' => 'text', 'help_text' => 'Phone', 'help_text_es' => 'Telefono'],
  
     ['row' => 2,'meta_key' => 'branding_kit_project_manager_email','type' => 'text', 'help_text' => 'Email', 'help_text_es' => 'Correo electrónico'],
	 
	  
	 ['row' => 3,'meta_key' => 'branding_kit_company_name','type' => 'text','help_text' => "What is your brand’s name? (This will be the name used in your logo design)",'help_text_es' => '¿Cuál es el nombre de la marca? (Este será el nombre usado en el logo)'],
	 
	 ['row' => 4,'meta_key' => 'branding_kit_slogan','type' => 'text','help_text' => "Do you have a company tagline or slogan that could be used as part of the logo?",'help_text_es' => '¿Tiene la compañía un slogan o una frase que pueda ser usada en la creación del logo?'],
	 
	 ['row' => 5,'meta_key' => 'branding_kit_years_in_business','type' => 'text','help_text' => "How many years has your company been in business?",'help_text_es' => '¿Cuantos años tiene la compañia en el negocio?'],
	 	  
	 ['row' => 5,'meta_key' => 'branding_kit_number_of_employees','type' => 'text','help_text' => "How many employees work for your company?",'help_text_es' => '¿Cuántos años tiene la compañía en el mercado?'],
	 
	 ['row' => 6,'meta_key' => 'branding_kit_services','type' => 'textarea','help_text' => "What product(s) or service(s) do your business provide?",'help_text_es' => '¿Qué producto(s) o servicio(s) ofrece la compañía?'],
	 
	 ['row' => 7,'meta_key' => 'branding_kit_adjectives','type' => 'textarea','help_text' => "Type 5 adjectives/descriptors that you’d like your customers to associate with your company",'help_text_es' => 'Escriba 5 calificativos/adjetivos con los que le gustaría que los clientes asociaran tu empresa.'],
	 
	 ['row' => 7,'meta_key' => 'branding_kit_competitors','type' => 'textarea','help_text' => "Who are your competitors?",'help_text_es' => '¿Quiénes son tus competidores? '],
	 
	 ['row' => 8,'meta_key' => 'branding_kit_competitors_difference','type' => 'textarea','help_text' => "What differentiates you from your competitors?",'help_text_es' => '¿Qué te diferencia de tus competidores?'],
	 
	 ['row' => 8,'meta_key' => 'branding_kit_unique_history','type' => 'textarea','help_text' => "Is there a unique story behind your business?",'help_text_es' => '¿Hay alguna historia única detrás de tu compañía?'],
	  
	  ['row' => 9,'meta_key' => 'branding_kit_five_years_from_now','type' => 'textarea','help_text' => "Where do you see your business in 5 years time?",'help_text_es' => '¿Dónde ves a tu compañía en cinco años?'],
	  
	  ['row' => 9,'meta_key' => 'branding_kit_audience_care_about','type' => 'textarea','help_text' => "What does your audience care about?",'help_text_es' => '¿Qué es lo que a tus clientes más les importa?'],
	  
	  ['row' => 10,'meta_key' => 'branding_kit_audience_want','type' => 'textarea','help_text' => "What does your audience want?",'help_text_es' => '¿Qué es lo que tus clientes quieren?'],
	  
	  ['row' => 10,'meta_key' => 'branding_kit_learn','type' => 'textarea','help_text' => "How does your audience learn about your product, organization or service?",'help_text_es' => '¿Cómo se enteran tus clientes de tus productos, organización o servicio?'],
	  
	  ['row' => 11,'meta_key' => 'branding_kit_choose','type' => 'textarea','help_text' => 'Why should your audience choose you over the competition?','help_text_es' => '¿Por qué tus clientes deberían escogerte por encima de tu competencia?'],
	  
	  ['row' => 11,'meta_key' => 'branding_kit_words','type' => 'textarea','help_text' => 'What words do you want your audience to associate with your company?','help_text_es' => '¿Qué palabras te gustaría que tu audiencia asocie con tu compañía?'],
	  
	  ['row' => 12,'meta_key' => 'branding_kit_another_brands','type' => 'textarea','help_text' => 'Generally, what logos or brands do you think will appeal to your audience and why?','help_text_es' => '¿De manera general, qué otras marcas o logos piensas que le gustan a tus clientes y por qué?'],
	  
	  ['row' => 13,'meta_key' => 'branding_kit_design_likes','type' => 'textarea','help_text' => "What are your design preferences or expectations?",'help_text_es' => '¿Cuáles son tus preferencias de diseño?'],	
	
	['row' => 14,'meta_key' => 'branding_kit_approved','type' => 'approval', 'placeholder' => 'Approve concept #1','placeholder_es' => 'Aprueba el concepto #1', 'set_value' => "1", "next_state" => "branding_kit_creating_files"],
	
	['row' => 15,'type' => 'one_file', 'help_text' => 'Logo concept #1', 'help_text_es' => 'Concepto de logo #1', 'meta_key' => 'branding_kit_concept_one','permissions' => 'admin', 'set_value' => "1"],
	
	['row' => 16,'meta_key' => 'branding_kit_approved','type' => 'approval', 'placeholder' => 'Approve concept #2','placeholder_es' => 'Aprueba el concepto #2', 'set_value' => "2","next_state" => "branding_kit_creating_files"],
	
	['row' => 17,'type' => 'one_file', 'help_text' => 'Logo concept #2', 'help_text_es' => 'Concepto de logo #2', 'meta_key' => 'branding_kit_concept_two','permissions' => 'admin'],
	
	['row' => 18,'type' => 'file', 'help_text' => 'Logo final files', 'help_text_es' => 'Archivos finales del Logo', 'section' => 'branding_kit_final_files','permissions' => 'admin'],

 ]; 
 
 
 $rows["8-hr Designer Package"] = [
	  
      ['row' => 1,'meta_key' => 'hr_designer_select','type' => 'label', 'placeholder' => 'Select Designer', 'placeholder_es' => 'Selecciona al Diseñador'],
	  
	  ['row' => 3,'meta_key' => 'hr_designer_todo_list','type' => 'textarea','help_text' => "Todo List",'help_text_es' => 'Lista de entregables'],
	  
	  ['row' => 4,'type' => 'file', 'help_text' => 'Please attach the neccesary files for the work: typography, color palette, logo in editable format (.psd .ai .eps). If they exist', 'help_text_es' => 'Adjunta los archivos necesarios para la realización de la Obra Encargada: tipografía, paleta de colores, logo en formato editable (.psd .ai .eps). Si existen', 'section' => 'hr_designer_necessary_files_for_the_work','permissions' => 'all'],	 
	  
	  ['row' => 5,'meta_key' => 'hr_designer_approval','type' => 'approval', 'placeholder' => 'Desginer approval','placeholder_es' => 'Aprobación del diseñador', 'set_value' => "1", "next_state" => "hr_design_waiting_approval", "next_state_logic" => "hr_designer_approval,hr_designer_client_approval", "next_state_logic_result" => "yes,yes"],
	  
	  ['row' => 5,'meta_key' => 'hr_designer_client_approval','type' => 'approval', 'placeholder' => 'Client approval','placeholder_es' => 'Aprobación del cliente', 'set_value' => "1","next_state" => "hr_design_working", "next_state_logic" => "hr_designer_approval,hr_designer_client_approval","next_state_logic_result" => "yes,yes"],
	  
	  ['row' => 6,'meta_key' => 'hr_designer_client_final_approval','type' => 'approval', 'placeholder' => 'I approve the delivery of the work on request','placeholder_es' => 'Apruebo la entrega de la obra por encargo', 'set_value' => "1","next_state" => "hr_design_completed"],
 ]; 
 
 $rows["8-hr Web Developer Package"] = [
	 
  ['row' => 1,'meta_key' => 'hr_developer_select','type' => 'label', 'placeholder' => 'Project manager info:', 'placeholder_es' => 'Información del encargado del proyecto:'],
	 
  ['row' => 2,'meta_key' => 'hr_developer_project_manager','type' => 'text', 'help_text' => 'Name', 'help_text_es' => 'Nombre'],
  
  ['row' => 2,'meta_key' => 'hr_developer_project_manager_phone','type' => 'text', 'help_text' => 'Phone', 'help_text_es' => 'Telefono'],
  
  ['row' => 2,'meta_key' => 'hr_developer_project_manager_email','type' => 'text', 'help_text' => 'Email', 'help_text_es' => 'Correo electrónico'],
	 
  ['row' => 3,'meta_key' => 'hr_developer_select','type' => 'label', 'placeholder' => 'Select Developer', 'placeholder_es' => 'Selecciona a un desarrollador'],
	  
  ['row' => 4,'meta_key' => 'hr_developer_todo_list','type' => 'textarea','help_text' => "List of deliverables",'help_text_es' => 'Lista de entregables'],
  
  ['row' => 5,'type' => 'file', 'help_text' => 'Please attach the necessary files for this project: brand fonts, color palette, logo in editable format (.ai or .eps).', 'help_text_es' => 'Adjunta los archivos necesarios para la realización de este proyecto: fuentes de marca, paleta de colores y logo en formato editable (.ai o .eps).', 'section' => 'hr_developer_necessary_files_for_the_work','permissions' => 'all'],	 
  
  ['row' => 6,'meta_key' => 'hr_developer_approval','type' => 'approval', 'placeholder' => 'Developer approval','placeholder_es' => 'Aprobación del desarrollador', 'set_value' => "1", "next_state" => "hr_development_waiting_approval", "next_state_logic" => "hr_developer_client_approval,hr_developer_approval","next_state_logic_result" => "yes,yes"],
  
  ['row' => 6,'meta_key' => 'hr_developer_client_approval','type' => 'approval', 'placeholder' => 'Client approval','placeholder_es' => 'Aprobación del cliente', 'set_value' => "1", "next_state" => "hr_development_working", "next_state_logic" => "hr_developer_approval,hr_developer_client_approval","next_state_logic_result" => "yes,yes"],
  
  ['row' => 7,'meta_key' => 'hr_developer_client_final_approval','type' => 'approval', 'placeholder' => 'I approve the delivery of this commissioned work','placeholder_es' => 'Apruebo la entrega de la obra por encargo', 'set_value' => "1", "next_state" => "hr_development_completed"],
	
 ]; 
 
 
 $rows["Web Package with Hours of Design, Content and Web Development"] = [
	 
     ['row' => 1,'meta_key' => 'hr_developer_select','type' => 'label', 'placeholder' => 'Project manager info:', 'placeholder_es' => 'Información del encargado del proyecto:'],
	 
     ['row' => 2,'meta_key' => 'hr_developer_project_manager','type' => 'text', 'help_text' => 'Name', 'help_text_es' => 'Nombre'],
  
     ['row' => 2,'meta_key' => 'hr_developer_project_manager_phone','type' => 'text', 'help_text' => 'Phone', 'help_text_es' => 'Telefono'],
  
     ['row' => 2,'meta_key' => 'hr_developer_project_manager_email','type' => 'text', 'help_text' => 'Email', 'help_text_es' => 'Correo electrónico'],
	 
	 ['row' => 3,'meta_key' => 'web_package_company_name','type' => 'text','help_text' => "What is the name of your company? ",'help_text_es' => '¿Cuál es el nombre de la compañía?'],
	 
	  ['row' => 5,'type' => 'file', 'help_text' => 'Please attach the files of the brand manual: typography, color palette, logo in editable format (.psd .ai .eps)', 'help_text_es' => 'Por favor adjunta los archivos del manual de marca: tipografía, paleta de colores, logo en formato editable (.psd .ai .eps).', 'section' => 'web_package_files','permissions' => 'all'],	
	 
	 ['row' => 6,'meta_key' => 'web_package_years_in_business','type' => 'textarea','help_text' => "How many years has your company been in business?",'help_text_es' => '¿Tiene su compañía un slogan o una frase que pueda ser usada en la creación de su logo?'],
	 
	 ['row' => 6,'meta_key' => 'web_package_services','type' => 'textarea','help_text' => "What product(s) or service(s) do your business provide?",'help_text_es' => '¿Qué producto(s) o servicio(s) ofrece su compañía?'],
	 
	 ['row' => 7,'meta_key' => 'web_package_adjectives','type' => 'textarea','help_text' => "Type 5 adjectives / adjectives that customers would like to identify your company",'help_text_es' => 'Escriba 5 calificativos/adjetivos con los que le gustaría que los clientes identificaran su empresa.'],
	 
	 ['row' => 7,'meta_key' => 'web_package_competitors','type' => 'textarea','help_text' =>  "Who are your competitors?",'help_text_es' => '¿Quiénes son tus competidores?'],
	 
	 ['row' => 8,'meta_key' => 'web_package_competitors_difference','type' => 'textarea','help_text' => "What differentiates you from your competitors?",'help_text_es' => '¿Qué te diferencia de tus competidores?'],
	 
	 ['row' => 8,'meta_key' => 'web_package_unique_history','type' => 'textarea','help_text' => "Is there a unique story behind your business?",'help_text_es' => '¿Hay alguna historia única detrás de tu compañía?'],
	  
	  ['row' => 9,'meta_key' => 'web_package_five_years_from_now','type' => 'textarea','help_text' => "Where do you see your business/service in 5 years time?",'help_text_es' => '¿Dónde ves a tu compañía/servicio en cinco años?'],
	  
	  ['row' => 9,'meta_key' => 'web_package_audience_care_about','type' => 'textarea','help_text' => "What does your audience care about?",'help_text_es' => '¿Qué es lo que a tus clientes les importa más?'],
	  
	  ['row' => 10,'meta_key' => 'web_package_audience_want','type' => 'textarea','help_text' => "What does your audience want?",'help_text_es' => '¿Qué es lo que tus clientes quieren?'],
	  
	  ['row' => 10,'meta_key' => 'web_package_learn','type' => 'textarea','help_text' => "How does your audience learn about your product, organisation or service?",'help_text_es' => '¿Cómo se enteran tus clientes de tus productos, organización o servicio?'],
	  
	  ['row' => 11,'meta_key' => 'web_package_choose','type' => 'textarea', 'help_text' => 'Why should your audience choose you over the competition?','help_text_es' => '¿Por qué deberían escogerte tus clientes por encima de tu competencia?'],
	  
	  ['row' => 11,'meta_key' => 'web_package_words','type' => 'textarea','help_text' => 'What words do you want your audience to associate with your company?','help_text_es' => '¿Qué palabras te gustaría que tu audiencia asocie con tu compañía?'],
	   
	   ['row' => 12,'meta_key' => 'web_package_address','type' => 'textarea','help_text' => "Where is your office located?",'help_text_es' => '¿Dónde se encuentra la oficina principal de la compañía y/o sus sedes?'],
	   
	   ['row' => 12,'meta_key' => 'web_package_phones','type' => 'textarea','help_text' => "What’s your company’s phone number?",'help_text_es' => '¿Cuál es el teléfono de contacto de tu compañía?'],
	   
	   ['row' => 13,'meta_key' => 'web_package_social_urls','type' => 'textarea','help_text' => "What are your company’s social URLs?",'help_text_es' => '¿Cuáles son las URLs de las redes sociales de tu compañía?'],
	  
	//Sections Web
	
   	['row' => 20,'meta_key' => 'web_package_section_1_title','type' => 'text','help_text' => "Section 1 title",'help_text_es' => 'Título de la sección 1'],
	
	['row' => 21,'meta_key' => 'web_package_section_1_content','type' => 'textarea','help_text' => "Section 1 content",'help_text_es' => 'Contenido de la sección 1'],
	
	['row' => 22,'type' => 'file', 'help_text' => 'Section 1 files', 'help_text_es' => 'Archivos de la sección 1', 'section' => 'section_1','permissions' => 'all'],
	
   	['row' => 23,'meta_key' => 'web_package_section_2_title','type' => 'text','help_text' => "Section 1 title",'help_text_es' => 'Título de la sección 2'],
	
	['row' => 24,'meta_key' => 'web_package_section_2_content','type' => 'textarea','help_text' => "Section 2 content",'help_text_es' => 'Contenido de la sección 2'],
	
	['row' => 25,'type' => 'file', 'help_text' => 'Section 2 files', 'help_text_es' => 'Archivos de la sección 2', 'section' => 'section_2','permissions' => 'all'],
	
   	['row' => 26,'meta_key' => 'web_package_section_3_title','type' => 'text','help_text' => "Section 3 title",'help_text_es' => 'Título de la sección 3'],
	
	['row' => 27,'meta_key' => 'web_package_section_3_content','type' => 'textarea','help_text' => "Section 3 content",'help_text_es' => 'Contenido de la sección 3'],
	
	['row' => 28,'type' => 'file', 'help_text' => 'Section 3 files', 'help_text_es' => 'Archivos de la sección 3', 'section' => 'section_3','permissions' => 'all'],
	
   	['row' => 29,'meta_key' => 'web_package_section_4_title','type' => 'text','help_text' => "Section 4 title",'help_text_es' => 'Título de la sección 4'],

	['row' => 30,'meta_key' => 'web_package_section_4_content','type' => 'textarea','help_text' => "Section 4 content",'help_text_es' => 'Contenido de la sección 4'],
	
	['row' => 31,'type' => 'file', 'help_text' => 'Section 4 files', 'help_text_es' => 'Archivos de la sección 4', 'section' => 'section_4','permissions' => 'all'],
	
   	['row' => 32,'meta_key' => 'web_package_section_5_title','type' => 'text','help_text' => "Section 5 title",'help_text_es' => 'Título de la sección 5'],
	
	['row' => 33,'meta_key' => 'web_package_section_5_content','type' => 'textarea','help_text' => "Section 5 content",'help_text_es' => 'Contenido de la sección 5'],
	
	['row' => 34,'type' => 'file', 'help_text' => 'Section 5 files', 'help_text_es' => 'Archivos de la sección 5', 'section' => 'section_5','permissions' => 'all'],
	
	['row' => 35,'meta_key' => 'web_package_concept_approval','type' => 'approval', 'placeholder' => 'Approve concept','placeholder_es' => 'Aprobar concepto', 'set_value' => "1","next_state" => "web_package_waiting_content"],
	 
	['row' => 36,'meta_key' => 'web_package_iframe','type' => 'iframe'],
	
	['row' => 37,'meta_key' => 'web_package_concept_comments','type' => 'textarea','help_text' => "Customer's comments",'help_text_es' => 'Comentarios del cliente'],
	
	['row' => 38,'meta_key' => 'web_package_copy_approval','type' => 'approval', 'placeholder' => 'Approve copy','placeholder_es' => 'Aprobar el texto', 'set_value' => "1","next_state" => "web_package_completed", 'set_value' => "1","next_state" => "web_package_completed"],
	 
	['row' => 39,'meta_key' => 'web_package_iframe','type' => 'iframe'],
	
	['row' => 40,'meta_key' => 'web_package_copy_comments','type' => 'textarea','help_text' => "Customer's comments",'help_text_es' => 'Comentarios del cliente'],
	
 ]; 
 
 
 $rows["SSl Certificate"] = [
	  
	 ['row' => 1,'meta_key' => 'ssl_domain','type' => 'text','help_text' => "Enter the domain in which you want to install the SSL Certificate",'help_text_es' => 'Escribe el dominio al que quieres instalar el Certificado SSL'],
	 
	['row' => 2,'meta_key' => 'ssl_domain_info','type' => 'textarea','help_text' => "Enter the account information where you purchased the domain (URL of the domain company, username, and password)",'help_text_es' => 'Ingresa la información de la compañía donde compraste el dominio (URL de la compañía de dominio, usuario y contraseña)'],

 ]; 
 
 $rows["Email Accounts"] = [
	  
	 ['row' => 1,'meta_key' => 'email_domain','type' => 'text','help_text' => "Domain with which you want to configure your emails. For example hi@mydomain.com",'help_text_es' => 'Dominio con el que quieres configurar tus correos electrónicos. Por ejemplo: hola@midominio.com'],
	 
	 ['row' => 2,'meta_key' => 'email_domain_info','type' => 'textarea','help_text' => "Enter the account information where you purchased the domain (URL of the domain company, username, and password)",'help_text_es' => 'Ingresa la información de la compañía donde compraste el dominio (URL de la compañía de dominio, usuario y contraseña)'],
	 
	 ['row' => 3,'meta_key' => 'email_list','type' => 'textarea','help_text' => "Write up to 10 emails, exactly how you’d like us to create them.",'help_text_es' => 'Ingresa hasta 10 correos electrónicos, escribiendolos exactamente como quieres que los configuremos.'],

 ]; 
 
 $rows["Social Media Branding Kit"] = [
	  
	  ['row' => 1,'type' => 'file', 'help_text' => 'Please attach the files of the brand manual: typography, color palette, logo in editable format (.psd .ai .eps)', 'help_text_es' => 'Por favor adjunta los archivos del manual de marca: tipografía, paleta de colores, logo en formato editable (.psd .ai .eps).', 'section' => 'social_media_files','permissions' => 'all'],	
	  
	  ['row' => 2,'type' => 'file', 'help_text' => 'Social Media Branding Kit', 'help_text_es' => 'Archivos de los estilos visuales', 'section' => 'social_media_final_files','permissions' => 'admin'],

 ]; 
 
    //Con este algoritmos llenamos la tabla fields
    $i = 0;
	$fnumber = 0;
	foreach ($rows as $key => $row){
		
		//Encontrar el producto
		$p = Wp_post::where('post_title', '=', $key)->where('post_type','=','product')->first();	
		if(isset($p)){
			
		  foreach ($row as $field){
			  //Loop por las columnas
		    if($field["type"] == "file"){
		 	  $field += ['fnumber' => $fnumber];
			  $fnumber++;   
		    }  
			
	   	   $field += ['product_id' => $p->ID];     
		   DB::table('wp_omissions_fields')->insert($field);
		   $i++;
          }
	      print_r($row);
	   }else{
		print_r("no encontro {$key}");
	   }
	}
  }
}
