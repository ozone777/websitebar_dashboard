<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;
use App\Wp_omissions_state;
use App\Wp_omissions_todo;

class ResetOmissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset_wp_omissions';
	#protected $signature = 'addfields {--queue=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset wp_omissions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
		DB::table('wp_omissions')->delete();	
		DB::table('wp_omissions_states')->delete();	
		DB::table('wp_omissions_todos')->delete();	
		
    }
}
