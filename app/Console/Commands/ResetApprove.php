<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;
use App\Wp_omission;

class ResetApprove extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reset_approve {--omission_id=}';
	#protected $signature = 'addfields {--queue=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset omission approve';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $omission_id = $this->option('omission_id');
		$omission = Wp_omission::findOrFail($omission_id);
		
		$omission->concept_selected = null;
		$omission->concept_approved_by = null;
		$omission->concept_approved_time = null;	
		$omission->save();
    }
}
