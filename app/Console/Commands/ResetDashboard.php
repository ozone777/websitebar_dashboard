<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;

class ResetDashboard extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	
	//Correr en modo production Esta
	//php artisan initializepete --m production
	
    protected $signature = 'reset_dashboard {--m=}';
	#protected $signature = 'addfields {--queue=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize default themes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
 
	  DB::table('wp_barservers')->delete();
	  DB::table('wp_barsites')->delete();
	  DB::table('wp_bardomains')->delete();
	  DB::table('wp_barsubdomains')->delete();
 	  DB::table('wp_barrecords')->delete();
		
    }
}
