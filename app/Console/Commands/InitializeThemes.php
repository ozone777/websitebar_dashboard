<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;

class InitializeThemes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
	
	//Correr en modo production Esta
	//php artisan initializepete --m production
	
    protected $signature = 'initialize_themes {--m=}';
	#protected $signature = 'addfields {--queue=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize default themes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	 
	  DB::table('wp_barthemes')->delete();
	  
	  
  	 DB::table('wp_barthemes')->insert(
  	 ['id' => 1,
 	 'name' => 'manhattan', 
 	 'theme_url' => "http://manhattan.websitebar.co",
 	 'theme_file' => 'https://www.dropbox.com/s/ybmwbr1o11b43yq/manhattan.tar.gz?dl=0',
  	 'created_at' => new DateTime, 
  	 'updated_at' => new DateTime
  	  ]);	
	  
  	 DB::table('wp_barthemes')->insert(
  	 ['id' => 2,
 	 'name' => 'amaretto', 
 	 'theme_url' => "http://amaretto.websitebar.co",
	 'theme_file' => 'https://www.dropbox.com/s/fhm4i0sq895wp68/amaretto.tar.gz?dl=0',
  	 'created_at' => new DateTime, 
  	 'updated_at' => new DateTime
  	  ]);	
	 
	 
 	 DB::table('wp_barthemes')->insert(
 	 ['id' => 3,
	 'name' => 'americano', 
	 'theme_url' => "http://americano.websitebar.co",
	 'theme_file' => 'https://www.dropbox.com/s/rmcnk6u3lias1dl/americano.tar.gz?dl=0',
 	 'created_at' => new DateTime, 
 	 'updated_at' => new DateTime
 	  ]);	
	 
 	 DB::table('wp_barthemes')->insert(
 	 ['id' => 4,
	 'name' => 'cognac', 
	 'theme_url' => "http://cognac.websitebar.co",
	 'theme_file' => 'https://www.dropbox.com/s/4h1vg9hdd3s0icy/cognac.tar.gz?dl=0',
 	 'created_at' => new DateTime, 
 	 'updated_at' => new DateTime
 	  ]);	
	 
 	 DB::table('wp_barthemes')->insert(
 	 ['id' => 5,
	 'name' => 'latte', 
	 'theme_url' => "http://latte.websitebar.co",
	 'theme_file' => 'https://www.dropbox.com/s/gqvsdz24sneufjz/latte.tar.gz?dl=0',
 	 'created_at' => new DateTime, 
 	 'updated_at' => new DateTime
 	  ]);	
	 
	 
 	 DB::table('wp_barthemes')->insert(
 	 ['id' => 6,
	 'name' => 'milkshake', 
	 'theme_url' => "http://milkshake.websitebar.co",
	 'theme_file' => 'https://www.dropbox.com/s/2h9n21fmh9muz2d/milkshake.tar.gz?dl=0',
 	 'created_at' => new DateTime, 
 	 'updated_at' => new DateTime
 	  ]);	
	 
	 
 	 DB::table('wp_barthemes')->insert(
 	 ['id' => 7,
	 'name' => 'mimosa', 
	 'theme_url' => "http://mimosa.websitebar.co",
	 'theme_file' => 'https://www.dropbox.com/s/1yyde56pg2z2n4x/mimosa.tar.gz?dl=0',
 	 'created_at' => new DateTime, 
 	 'updated_at' => new DateTime
 	  ]);		
	 
 	 
 	 DB::table('wp_barthemes')->insert(
 	 ['id' => 8,
	 'name' => 'sauvignonblanc', 
	 'theme_url' => "http://sauvignonblanc.websitebar.co",
	 'theme_file' => 'https://www.dropbox.com/s/5r9jkn3ooo0kbnn/souvignon.tar.gz?dl=0',
 	 'created_at' => new DateTime, 
 	 'updated_at' => new DateTime
 	  ]);	

	 
 	 DB::table('wp_barthemes')->insert(
 	 ['id' => 9,
	 'name' => 'scotch', 
	 'theme_url' => "http://scotch.websitebar.co",
	 'theme_file' => 'https://www.dropbox.com/s/rko8b66zbuvgm8g/scotch.tar.gz?dl=0',
 	 'created_at' => new DateTime, 
 	 'updated_at' => new DateTime
 	  ]);	
	 
	 
 	 DB::table('wp_barthemes')->insert(
 	 ['id' => 10,
	 'name' => 'zinfandel', 
	 'theme_url' => "http://zinfandel.websitebar.co",
	 'theme_file' => 'https://www.dropbox.com/s/4orro83qtt4sme7/zinfandel.tar.gz?dl=0',
 	 'created_at' => new DateTime, 
 	 'updated_at' => new DateTime
 	  ]);	
		
    }
}
