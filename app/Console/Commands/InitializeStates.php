<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use DateTime;
use App\Option;

class InitializeStates extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'initialize_states';
	#protected $signature = 'addfields {--queue=}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add states records to multidashboard';

    /**
     * Create a new command instance.
     *
     * @return void
     */
	
	
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       
		DB::table('wp_ostates')->delete();
			
		
		////////////////////////////////////////
		//Estados premium branding kit
		////////////////////////////////////////
		
		DB::table('wp_ostates')->insert(['id' => 5, 'label' => 'branding_kit_waiting_brief', 'view' => "branding_kit", 'es' => 'Esperando el brief', 'en' => 'Waiting brief','order' => 3,'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 0, "first_state" => true, "notification_subject_es" => "Estamos esperando tus repuestas", "notification_body_es" => "A continuación podrás respondernos una serie de preguntas que nos permitirán conocer más sobre tu marca","notification_subject_en" => "We are waiting for your answers", "notification_body_en" => "Please answer a series of questions that will allow us to learn more about your brand." ]);
		
		DB::table('wp_ostates')->insert(['id' => 6, 'label' => 'branding_kit_creating_concepts', 'view' => "branding_kit", 'es' => 'Creando conceptos', 'en' => 'Creating concepts','order' => 4, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 0, "todo_delay" => 10, "notification_subject_es" => "Estamos creando los conceptos de marca", "notification_body_es" => "Realizaremos una investigación y crearemos dos conceptos de marca en un tiempo de 10 días.","notification_subject_en" => "We are creating the brand concepts you requested", "notification_body_en"=> "We will research, create, and deliver two brand concepts within 10 days."]);
		
		DB::table('wp_ostates')->insert(['id' => 7, 'label' => 'branding_kit_waiting_approval', 'view' => "branding_kit", 'es' => 'Esperando aprobación', 'en' => 'Waiting for approval','order' => 5, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 1,"notification_subject_es" => "Estamos esperando tu aprobación", "notification_body_es" => "A continuación podrás revisar los conceptos de marca que hemos creado para tí","notification_subject_en" => "We are waiting for your approval", "notification_body_en" => "You can now review the brand concepts we’ve created"]);
		
		DB::table('wp_ostates')->insert(['id' => 8, 'label' => 'branding_kit_creating_files', 'view' => "branding_kit", 'es' => 'Creando archivos', 'en' => 'Creating files','order' => 6, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 1, "todo_delay" => 7,"notification_subject_es" => "Creando los archivos de tu marca", "notification_body_es" => "En un período de 7 días incorporaremos los cambios según tu retroalimentación y enviaremos un paquete a tu correo electrónico con el kit de marca completo.","notification_subject_en" => "Creating your brand files", "notification_body_en" => "In 7 days we will incorporate your feedback and email a final package with all the assets included in your brand kit."]);
		
		DB::table('wp_ostates')->insert(['id' => 9, 'label' => 'branding_kit_download_files', 'view' => "branding_kit", 'es' => 'Descarga los archivos', 'en' => 'Download files','order' => 7, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 2,"notification_subject_es" => "Descarga los archivos de tu marca", "notification_body_es" => "A continuación podrás descargar los archivos de tu marca","notification_subject_en" => "Download your brand’s final assets.", "notification_body_en" => "You can now download your brand’s assets"]);
		
		
		/////////////////////////////////////////////
		//Estados hr package design 
		/////////////////////////////////////////////
		
		DB::table('wp_ostates')->insert(['id' => 10, 'label' => 'hr_design_select_user', 'view' => "hr_design", 'en' => 'Select designer', 'es' => 'Selecciona al diseñador', 'order' => 1,'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 0, "first_state" => true,"notification_subject_es" => "Seleccion un Diseñador", "notification_body_es" => "Para seleccionar un diseñador haz click: "]);
		
		DB::table('wp_ostates')->insert(['id' => 11, 'label' => 'hr_design_waiting_agreement', 'view' => "hr_design", 'en' => 'Waiting for agreement', 'es' => 'Esperando acuerdo','order' => 2, 'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 1,"notification_subject_es" => "Estamos esperando la firma del acuerdo", "notification_body_es" => "Para completar el acuerdo y firmarlo haz click: "]);
		
		DB::table('wp_ostates')->insert(['id' => 12, 'label' => 'hr_design_working', 'view' => "hr_design", 'es' => 'Trabajando', 'en' => 'Working','order' => 3,'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 2,  "todo_delay" => 5,"notification_subject_es" => "Estamos trabajando", "notification_body_es" => "Desde este momento el diseñador esta trabajando en la obra por encargo"]);
		
		DB::table('wp_ostates')->insert(['id' => 13, 'label' => 'hr_design_waiting_approval', 'view' => "hr_design", 'es' => 'Esperando aprobación del cliente', 'en' => 'Waiting for customer approval','order' => 3,'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 2,"notification_subject_es" => "Estamos esperando la aprobación de la obra por encargo", "notification_body_es" => "Para aprobar la realización de la obra por encargo haz click: "]);
		
		DB::table('wp_ostates')->insert(['id' => 14, 'label' => 'hr_design_completed', 'view' => "hr_design", 'es' => 'Completado', 'en' => 'Completed','order' => 4,'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 2,"notification_subject_es" => "La obra por encargo ha sido terminada", "notification_body_es" => "La obra por encargo ha sido terminada para saber más acerca del proceso haz click: "]);
		
		
		
		/////////////////////////////////////////////
		//Estados hr package development
		/////////////////////////////////////////////
		
		DB::table('wp_ostates')->insert(['id' => 15, 'label' => 'hr_development_select_user', 'view' => "hr_development", 'en' => 'Select developer', 'es' => 'Selecciona a un desarrollador', 'order' => 1, 'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 0, "first_state" => true,"notification_subject_es" => "Selecciona un desarrollador", "notification_body_es" => "Selecciona un desarrollador para tu proyecto","notification_subject_en" => "Select a developer", "notification_body_en" => "Select a developer for your project"]);
		
		DB::table('wp_ostates')->insert(['id' => 16, 'label' => 'hr_development_waiting_agreement', 'view' => "hr_development", 'en' => 'Waiting for agreement', 'es' => 'Esperando acuerdo','order' => 2,'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 1, "notification_subject_es" => "Estamos esperando la firma del acuerdo", "notification_body_es" => "Para completar el acuerdo requerimos tu firma","notification_subject_en" => "We are waiting for you to sign the agreement", "notification_body_en" => "We need your signature in order to complete the agreement"]);
		
		DB::table('wp_ostates')->insert(['id' => 17, 'label' => 'hr_development_working', 'view' => "hr_development", 'es' => 'Trabajando', 'en' => 'Working','order' => 3,'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 2, "todo_delay" => 5, "notification_subject_es" => "Estamos trabajando", "notification_body_es" => "Desde este momento el desarrollador está trabajando en tu obra por encargo","notification_subject_en" => "We are working on your project", "notification_body_en" => "The developer is now working on your commissioned work."]);
		
		DB::table('wp_ostates')->insert(['id' => 18, 'label' => 'hr_development_waiting_approval', 'view' => "hr_development", 'es' => 'Esperando aprobación del cliente', 'en' => 'Waiting for customer approval','order' => 3,'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 2,"notification_subject_es" => "Estamos esperando tu aprobación", "notification_body_es" => "Necesitamos tu aprobación para finalizar esta obra por encargo","notification_subject_en" => "We are waiting for your approval", "notification_body_en" => "We need your approval to get started"]);
		
		DB::table('wp_ostates')->insert(['id' => 19, 'label' => 'hr_development_completed', 'view' => "hr_development", 'es' => 'Completado', 'en' => 'Completed','order' => 5,'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 2,"notification_subject_es" => "Tu obra por encargo ha sido terminada", "notification_body_es" => "La obra por encargo ha sido terminada. Conoce más acerca del proceso"]);
		
		
	
		
		////////////////////////////////////////
		//Estados web_package
		////////////////////////////////////////
		
		DB::table('wp_ostates')->insert(['id' => 23, 'label' => 'web_package_waiting_brief', 'view' => "web_package", 'es' => 'Esperando el brief', 'en' => 'Waiting brief','order' => 3,'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 0,"first_state" => true, "notification_subject_es" => "Estamos esperando tus repuestas", "notification_body_es" => "A continuación podrás respondernos una serie de preguntas que nos permitirán conocer más sobre tu marca.","notification_subject_en" => "We are waiting for your answers", "notification_body_en" => "Please answer a series of questions that will allow us to learn more about your brand."]);
		
		DB::table('wp_ostates')->insert(['id' => 24, 'label' => 'web_package_creating_concepts', 'view' => "web_package", 'es' => 'Creando concepto', 'en' => 'Creating concept','order' => 4, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 0, "todo_delay" => 4, "notification_subject_es" => "Estamos creando un concepto de tu sitio web", "notification_body_es" => "Desde este momento estamos creando un concepto de sitio web para tu marca. Este proceso puede durar hasta 72 horas.","notification_subject_en" => "We are creating a concept for your website.", "notification_body_en" => "We are now creating a website concept for your brand. This process can last up to 72 hours."]);
		
		DB::table('wp_ostates')->insert(['id' => 25, 'label' => 'web_package_waiting_concept_approval', 'view' => "web_package", 'es' => 'Esperando aprobación del concepto', 'en' => 'Waiting for concept approval','order' => 5, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 1,"notification_subject_es" => "Estamos esperando la aprobación del concepto", "notification_body_es" => "A continuación podrás revisar el concepto de sitio web que hemos creado para tí.","notification_subject_en" => "We are waiting for the approval of the concept", "notification_body_en" => "You can now review the website concept we’ve created for you."]);
		
		DB::table('wp_ostates')->insert(['id' => 26, 'label' => 'web_package_waiting_content', 'view' => "web_package", 'es' => 'Esperando el contenido', 'en' => 'Waiting content','order' => 6, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 2, "notification_subject_es" => "EsEsperando el contenido de tu sitio Web", "notification_body_es" => "A continuación podrás enviarnos el contenido de tu sitio web.","notification_subject_en" => "Waiting for the content of your website", "notification_body_en" => "You can now send us the content of your website."]);
		
		DB::table('wp_ostates')->insert(['id' => 27, 'label' => 'web_package_creating_copy', 'view' => "web_package", 'es' => 'Creando el copy', 'en' => 'Creating copy','order' => 6, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 2, "todo_delay" => 5, "notification_subject_es" => "Estamos creando el texto para tu sitio web", "notification_body_es" => "Realizaremos una investigación y crearemos el copy para tu sitio web en 7 días.","notification_subject_en" => "We are currently creating copy for your website", "notification_body_en" => "We will research your brand’s voice and create the copy for your website in 7 days."]);
		
		DB::table('wp_ostates')->insert(['id' => 28, 'label' => 'web_package_copy_approval', 'view' => "web_package", 'es' => 'Esperando aprobación del copy', 'en' => 'Approve content','order' => 6, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 3, "notification_subject_es" => "Estamos esperando la aprobación del copy", "notification_body_es" => "A continuación podrás revisar los textos que hemos preparado para tu sitio web.","notification_subject_en" => "We are waiting for the approval of the copy", "notification_body_en" => "You can now review the copy we’ve created for your website."]);
		
		DB::table('wp_ostates')->insert(['id' => 29, 'label' => 'web_package_completed', 'view' => "web_package", 'es' => 'Completado', 'en' => 'Completed','order' => 7, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 3, "notification_subject_es" => "Sitio web completado", "notification_body_es" => "Este proyecto fue completado. Para más información.","notification_subject_en" => "Your website project has been completed.", "notification_body_en" => "This website project was completed. For more information click here:"]);
		
		
		/////////////////////////////////////////////
		//SSL certificate
		/////////////////////////////////////////////
		
		DB::table('wp_ostates')->insert(['id' => 20, 'label' => 'ssl_waiting_brief', 'view' => "ssl_certificate", 'es' => 'Esperando el brief', 'en' => 'Waiting brief', 'order' => 1,  'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 0, "first_state" => true, "notification_subject_es" => "Estamos esperando tus repuestas", "notification_body_es" => "A continuación podrás respondernos una serie de preguntas que nos permitirán instalarle el Certificado SSL a tu dominio","notification_subject_en" => "We are waiting for your answers", "notification_body_en" => "Please answer these questions so we can install the SSL certificate in your domain."]);
		
		DB::table('wp_ostates')->insert(['id' => 21, 'label' => 'ssl_installing', 'view' => "ssl_certificate", 'en' => 'Installing SSL certificate', 'es' => 'Instalando certificado SSL','order' => 2,'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 0, "todo_delay" => 3, "notification_subject_es" => "Instalando Certificado SSL", "notification_body_es" => "Estamos instalando el Certificado SSL. Este proceso puede tardar hasta 72 horas","notification_subject_en" => "Installing SSL Certificate", "notification_body_en" => "We are installing your SSL Certificate. This process can take up to 72 hours."]);
		
		DB::table('wp_ostates')->insert(['id' => 22, 'label' => 'ssl_completed', 'view' => "ssl_certificate", 'es' => 'Completado', 'en' => 'Completed','order' => 3,'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 0, "notification_subject_es" => "Certificado SSL instalado", "notification_body_es" => "El Certificado SSL fue instalado con éxito","notification_subject_en" => "SSL Certificate installed", "notification_body_en" => "The SSL Certificate was installed successfully."]);
		
		
		
		
		/////////////////////////////////////////////
		//EMAILS ACCOUNTS
		/////////////////////////////////////////////
		
		DB::table('wp_ostates')->insert(['id' => 30, 'label' => 'emails_waiting_brief', 'view' => "emails", 'es' => 'Esperando el brief', 'en' => 'Waiting brief', 'order' => 1,  'created_at' => new DateTime, 'updated_at' => new DateTime, 'selected_index' => 0, "first_state" => true, "notification_subject_es" => "A continuación podrás respondernos una serie de preguntas que nos permitirán configurar tus correos electrónicos","notification_subject_en" => "We are waiting for your answers", "notification_body_en" => "Please answer these questions so we can set up your custom emails"]);
		
		DB::table('wp_ostates')->insert(['id' => 31, 'label' => 'emails_installing', 'view' => "emails", 'en' => 'Configuring emails', 'es' => 'Configurando correos electrónicos','order' => 2,'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 0, "todo_delay" => 3, "notification_subject_es" => "Configurando correos electrónicos", "notification_body_es" => "Estamos configurando tus correos electronicos, este proceso puede tardar hasta 72 horas","notification_subject_en" => "Configuring emails", "notification_body_en" => "We are setting up your emails. This process can take up to 72 hours"]);
		
		DB::table('wp_ostates')->insert(['id' => 32, 'label' => 'emails_completed', 'view' => "emails", 'es' => 'Completado', 'en' => 'Completed','order' => 3,'created_at' => new DateTime, 'updated_at' => new DateTime,'selected_index' => 0, "notification_subject_es" => "Correos configurados correctamente", "notification_body_es" => "Los correos electrónicos fueron configurados correctamente. Conoce más sobre el proceso","notification_subject_en" => "Your emails are all set.", "notification_body_en" => "The emails were set up correctly. Learn more about the process."]);
		
		
		
    }
}
