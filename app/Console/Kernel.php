<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Inspire::class,
		\App\Console\Commands\Dbkeys::class,		
		\App\Console\Commands\Ticketseed::class,		
		\App\Console\Commands\InitializePete::class,
		\App\Console\Commands\InitializeDashboard::class,
		\App\Console\Commands\InitializeThemes::class,
		\App\Console\Commands\ResetDashboard::class,
		\App\Console\Commands\AddFieldsWebsiteBar::class,
		\App\Console\Commands\InitializeStates::class,
		\App\Console\Commands\ResetOmissionsStates::class,
		\App\Console\Commands\ResetOmissions::class,
		\App\Console\Commands\ResetApprove::class,
		
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
    }
}
