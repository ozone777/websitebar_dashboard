<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use View;
use App;
use Log;
use App\Barsite;
use App\Barsubdomain;
use App\Bardomain;
use App\DashboardOption;
use App\Barserver;
use App\Wp_omissions_state;
use App\Wp_omissions_todo;
use Mailjet\Resources;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        
		//LOGICA DE CALLBACKS///////////////////
		///////////////////////////////////////
		
		
		Wp_omissions_state::updating(function($model){
			
			Wp_omissions_todo::create_todo($model->id,$model->ostate_id);	
			$model->send_notification();
			
			
		});
		
		Wp_omissions_state::creating(function($model){
			
			Wp_omissions_todo::create_todo($model->id,$model->ostate_id);	
			$model->send_notification();
			
			
		});
		
		Barsubdomain::creating(function($model){
		
		  if (!strpos($model->target, '.test') !== false) {
			//PRODUCTION
  		  	Log::info("sudo linode-cli domains records-create 1141758 --type A --name $model->subdomain --target $model->target --json 2>&1");
  			$result = shell_exec("sudo linode-cli domains records-create 1141758 --type A --name $model->subdomain --target $model->target --json 2>&1");
  			$result = json_decode($result);
  			$model->linode_id = $result[0]->id; 	
		  }
		  
		});
		
		Barsubdomain::deleting(function($model){
			
  		  if(!strpos($model->target, '.test') !== false) {
    	  	 $result = shell_exec("sudo linode-cli domains records-delete $model->linode_id --json 2>&1"); 
  		  }
		  
		});
		
		
		Bardomain::creating(function($model){
		
  		 	$dashboard_option = new DashboardOption();
  		  	
  		  	//if($dashboard_option->get_meta_value("environment") == "production") {
	  			$result = shell_exec("sudo linode-cli domains create --type master --domain $model->domain_zone --soa_email software@ozonegroup.co --json");
	  			$result = json_decode($result);
				$model->linode_id = $result[0]->id;
				
				Log::info("sudo linode-cli domains records-create $model->linode_id --type A --target $model->target --port 80 --json");
				
	  			$result = shell_exec("sudo linode-cli domains records-create $model->linode_id --type A --target $model->target --port 80 --json");
	  			$result = shell_exec("sudo linode-cli domains records-create $model->linode_id --type A --name www --target $model->target --port 80 --json");	
				//}
			
		});
		
		Bardomain::deleting(function($model){
		  $dashboard_option = new DashboardOption();
		  
  		  if($dashboard_option->get_meta_value("environment") == "production") {
    	  	 $result = shell_exec("sudo linode-cli domains delete $model->linode_id --json 2>&1"); 
  		  }
		  
		});
		
		Barserver::creating(function($model){
			
  		  if($model->test == true) {
			  
			  $model->pete_token = "nBlAY6LhxZqJfvo9Udmz0GIgk5DKO8";
			  $model->ip = "pete.test";
			  
  		  }else{
  		  	
  			$var_array = ["dbpassword" => $model->db_info, "email" => $model->email, "name" => $model->linode_label, "petepassword" => $model->pete_info, "petetoken" => $model->pete_token];		
  			$json = json_encode($var_array);
  			$json = "'".$json."'";
  			Log::info($json);
  			$command = "sudo linode-cli linodes create --label Bar_$model->linode_label --image linode/ubuntu18.04 --root_pass $model->ssh_info --booted true --stackscript_id $model->stackscript_id --region $model->region --type $model->type --authorized_keys 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCduvixlVRXXOiBm9zI/dOstZhpW3DBICKKn1YnuicWG3AG04mEdo7zIVy5AqAM1HBAd1XA1FyERnBYluhm32wSiIDVg+3bMFKYstucfWog4uCrpc1+vjMaJadztQTIRegF72BcKEIJ4LVydZ3yif1Gl4/+BPSCYIlQ3Q6RVf9Sn7bRDMu9jLoqRYBp8BnOSFDWLNHH/OgbLNdtSbbXr1n3mlidM1D7lgwtO0sTw44Al2yD82Mts7c08GkW8L0mPJg7OFi9AZmWSUDRi+IvymDVKItra2fj/jutxZlNDePBZqOID6Wd//gbt4gr3lLlu1nOESWBKX/Nak2cAnLZ5/sR pedroconsuegra@Pedros-iMac.local'";
  			$command = $command . ' --stackscript_data '.$json.' --json';
  			Log::info("comandoa ejecutar:");
			Log::info($command);
  			$result = shell_exec($command);
  			Log::info($result);
  			$result = json_decode($result);
  			Log::info($result);
			$model->linode_id = $result[0]->id;
			$model->ip = $result[0]->ipv4[0];
			
  		  }
		  
		});
		
		Barserver::deleting(function($model){
			
  		  if($model->test == false) {
			  
    	  	 $result = shell_exec("sudo linode-cli linodes delete $model->linode_id --json 2>&1"); 
			 
  		  }
		  
		});
		
		
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
