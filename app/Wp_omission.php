<?php

namespace App;

use App\Wp_omissions_row;
use App\Wp_omissionmeta;
use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class Wp_omission extends Model
{
    //
	 protected $fillable = array('completed', 'accepted', 'in_progress','suspended','aborted','ready_for_revision','keywords','examples','guidelines','blog_title','blog_goal_improve_seo','blog_goal_become_a_thought_leader','blog_goal_recycle_existing_content','blog_goal_provide_instructions','blog_tone_serious_experienced_formal','blog_tone_funny_witty_satirical','blog_tone_empathetic_instructional_detailed','blog_tone_casual_conversational_friendly','color1','color2','color3','color4','color5','web_design_admin_email','web_design_admin_username','web_design_admin_desired_password','web_design_admin_site_name','web_design_domain','web_design_logo','web_design_logo','web_design_section1_title','web_design_section2_title','web_design_section3_title','web_design_section4_title','web_design_section5_title','web_design_section6_title','web_design_section7_title','web_design_section1_content','web_design_section2_content','web_design_section3_content','web_design_section4_content','web_design_section5_content','web_design_section6_content','web_design_section7_content','web_design_contact_form_email','web_design_social_urls','web_design_domain_user_and_password','store_types_of_products','store_product_fields_size','store_product_fields_color','store_product_fields_price','store_product_fields_weight','store_product_fields_images','store_currency','store_delivery_options','store_delivery_options','store_delivery_areas','store_location','store_product_checkout_yes','store_product_checkout_no','landing_lead_mailchimp','landing_lead_csv','landing_lead_email','landing_lead_admin_email','landing_main_cta','social_media_users_and_passwords','infographic_title','infographic_content');
	 	
	
	
	 public function osections() {
	 	return $this->hasMany('App\Wp_osection', 'wp_omission_id', 'id');
	 }
	 
	 //LOGICA PARA DYNAMICS FIELDS
	 
	 public function get_metas(){
		 //Areglamos los meta value de manera conveniente
		 
		$meta_values = Wp_omissionmeta::where('omission_id', '=', "$this->id")->get();
		$omissionmeta = array();
        foreach ($meta_values as $meta) {
	      $omissionmeta["$meta->meta_key"] = $meta;
        }
		return $omissionmeta;	 
	 }
	 
     public static function get_product_fields($product_id)
     {
 		$fields = Wp_omissions_field::where('product_id', '=', "$product_id")->get();
 		$rows = array();
 	     foreach ($fields as $field) {
 			 if(isset($rows[$field->row])){
 			    array_push($rows[$field->row],$field);
 			 }else{
 			 	$rows[$field->row] = array();
 				array_push($rows[$field->row],$field);
 			 }
 	     }
		 
		 return $rows;
     }
	 
     public static function get_dynamic_fields($omission_id)
     {
 		$fields = Wp_omissions_field::where('omission_id', '=', $omission_id)->get();
 		$rows = array();
 	     foreach ($fields as $field) {
 			 if(isset($rows[$field->row])){
 			    array_push($rows[$field->row],$field);
 			 }else{
 			 	$rows[$field->row] = array();
 				array_push($rows[$field->row],$field);
 			 }
		  
 	     }
		 
		 return $rows;
     }
	 
	 public static function get_index_rows($key,$omission_id){
	 	$rows = Wp_omissions_row::where('meta_key', '=',$key)->where('omission_id', '=', $omission_id)->first();
		if(isset($rows)){
			$array = explode(',', $rows->meta_value);
			array_pop($array);
			return $array;
		}else{
			return[];
		}
	 }
	 
	 public static function create_field($key,$array,$omission_id){
  	   
	   //CREO EL CAMPO EN FIELDS
	   $field = new Wp_omissions_field($array);
	   $field->save();
	   
	   //ASIGNA DEFAULT VALUE AL META
	   if(isset($array["default_value"])){
	   	  DB::table('wp_omissionmetas')->insert(["meta_key" => $array["meta_key"], "meta_value" => $array["default_value"], "omission_id" => $array["omission_id"] ]);
	   }
	   
	   $rows = Wp_omissions_row::where('meta_key', '=',$key)->where('omission_id', '=', $omission_id)->first();
	   if($rows){
		   
		   if (strpos($rows->meta_value, $array["row"]) !== false) {
			   Log::info('misma row');
		   }else{
			   $rows->meta_value = $rows->meta_value . $field->row . ","; 
			   $rows->save();
		   }   
		   
	   }else{
		  $rows = new Wp_omissions_row();
	 	  $rows->meta_key = $key;
		  $rows->meta_value = $field->row.",";
		  $rows->omission_id = $omission_id;
		  $rows->save();
	   }
	   
	 }
	 
	 
	 public function get_value($meta_key,$metas){
		 return $metas[$meta_key];
	 }
	 
	 
	 
}
