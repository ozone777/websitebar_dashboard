<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bardomain extends Model
{
    //
	protected $fillable = ['linode_id', 'domain_zone', 'barserver_id'];
	protected $table = 'wp_bardomains';
	
	
}
