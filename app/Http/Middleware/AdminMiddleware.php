<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Log;
use Illuminate\Contracts\Auth\Guard;


class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	
    protected $wp_user;

    public function __construct(Guard $auth)
    {
		
    	include_once(env('WP_LOAD_PATH')."/wp-load.php");  
        $this->wp_user = wp_get_current_user();
		
    	//include_once(env('WP_LOAD_PATH')."/wp-load.php");  
       
    }
	
    public function handle($request, Closure $next)
    {
		
		$role = getUserRole($this->wp_user->ID);
		Log::info("Role in AdminMiddleware: ".$role);
		if($role != "administrator"){
			return Redirect::to('/?message="Forbidden access"');
		}
		
        return $next($request);
    }
}
