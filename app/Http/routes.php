<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//API validation routes no login required
Route::get('/validate_site_json', 'OapiController@validate_site_json');
Route::get('/validate_update_json', 'OapiController@validate_update_json');

//Dashboard routes
Route::get("/my_subscriptions","DashboardController@my_subscriptions");
Route::get("/my_plans","DashboardController@my_plans");
Route::get("/create_site","DashboardController@create_site");
Route::get("/my_sites","DashboardController@my_sites");
Route::get("/my_servers","DashboardController@my_servers");


Route::post("/create_linode","DashboardController@create_linode");
Route::post("/create_websitebar","DashboardController@create_websitebar");
Route::post("/delete_websitebar","DashboardController@delete_websitebar");
Route::post("/create_api_key","DashboardController@create_api_key");
Route::get('/logout', 'DashboardController@logout');
Route::post("/set_lang","DashboardController@set_lang");
Route::get("/addons","DashboardController@addons");
Route::get("/backups","DashboardController@backups");
Route::get("/restore","DashboardController@restore");
Route::get("/my_domains","DashboardController@my_domains");
Route::post("/publish_website","DashboardController@publish_website");
Route::post("/get_barserver_info","DashboardController@get_barserver_info");
Route::get("/save_barsite_from_pete","DashboardController@save_barsite_from_pete");
Route::get("/show_wp_pass","DashboardController@show_wp_pass");
Route::get("/check_websitebar_subdomain","DashboardController@check_websitebar_subdomain");
Route::get("/testui","DashboardController@testui");
Route::get("/save_barsite","DashboardController@save_barsite");

Route::get("/get_domains","DashboardController@get_domains");
Route::get("/shut_down","DashboardController@shut_down");
Route::get("/boot","DashboardController@boot");
Route::get("/reboot","DashboardController@reboot");

Route::get("/clone_barsite","DashboardController@clone_barsite");
Route::get("/delete_barsite","DashboardController@delete_barsite");
Route::get("/restore_barsite","DashboardController@restore_barsite");
Route::get("/destroy_barsite","DashboardController@destroy_barsite");
Route::get("/continue_barsite","DashboardController@continue_barsite");
Route::get("/suspend_barsite","DashboardController@suspend_barsite");
Route::get("/restore_barsite_from_backup","DashboardController@restore_barsite_from_backup");
Route::get("/publish_barsite","DashboardController@publish_barsite");
Route::get("/create_barsite","DashboardController@create_barsite");
Route::get("/my_addons","DashboardController@my_addons");

/////OMISSIONS ROUTES///////////

Route::get("/set_omission","OmissionsController@set_omission");
Route::get("/edit/{id}","OmissionsController@edit");
Route::post('/answer_test', 'OmissionsController@answer_test');
Route::post('/mission_submitted', 'OmissionsController@mission_submitted');
Route::post('/wp_select_user', 'OmissionsController@wp_select_user');
Route::post('/save_omission_file', 'OmissionsController@save_omission_file');
Route::post('/save_brief', 'OmissionsController@save_brief');
Route::get("/send_notification_test/{id}","OmissionsController@send_notification_test");

/////END OMISSIONS ROUTES///////

//Contributor routes
Route::get('/select_customer', 'ContributorsController@select_customer');

//Admin routes
Route::get('/open_tickets', 'AdminController@open_tickets');
Route::get('/customers', 'AdminController@customers');
Route::get('/get_customer/{id}', 'AdminController@get_customer');
Route::get('/test_email', 'AdminController@test_email');

//Otickets routes
Route::resource("otickets","OticketController");
Route::post("store_answer","OticketController@store_answer");
Route::post("update_status","OticketController@update_status");
Route::get("all_tickets","OticketController@all");

//Options routes
Route::resource("wp_global_options","WpGlobalOptionController");
Route::resource("barthemes","BarthemeController");
Route::resource("barrecords","BarrecordController");

Route::resource("bardomains","BardomainController");
Route::resource("wp_omissions_files","Wp_omissions_fileController");
Route::get("/create_record","BardomainController@create_record");
Route::post("/store_record","BardomainController@store_record");
Route::post("/delete_record","BardomainController@delete_record");


Route::get('/', array('as' => 'dashboard.create_site', 'uses' => 'DashboardController@create_site'));

