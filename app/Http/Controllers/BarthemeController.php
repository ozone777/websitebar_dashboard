<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Bartheme;
use Illuminate\Http\Request;
use Log;
use Illuminate\Support\Facades\Auth;
use App\DashboardOption;
use App;

class BarthemeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct()
	{
	      
        $this->middleware('auth.wp');		
		$this->middleware('auth.admin');
			
	}
	
	public function index()
	{
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		
		$barthemes = Bartheme::orderBy('id', 'desc')->paginate(10);
		$viewsw = "themes";
		return view('barthemes.index', compact('barthemes','viewsw','current_user','wordpress_url','dashboard_option','role','global_lang'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		
		$viewsw = "themes";
		return view('barthemes.create',compact('viewsw','current_user','wordpress_url','dashboard_option','role','global_lang'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$bartheme = new Bartheme();

		$bartheme->name = $request->input("name");
        $bartheme->template_url = $request->input("template_url");
		$bartheme->theme_file = $request->input("theme_file");
		$bartheme->theme_url = $request->input("theme_url");
		
		if($request->file('image')!= ""){
			Log::info('Entro en logica imagen 777');
				
			$file = $request->file('image');
	        // SET UPLOAD PATH
	        $destinationPath = 'themes';
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = rand(11111, 99999) . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$bartheme->image = $fileName;
			
		}

		$bartheme->save();

		return redirect()->route('barthemes.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$bartheme = Bartheme::findOrFail($id);
		
		return view('barthemes.show', compact('bartheme'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		
		$bartheme = Bartheme::findOrFail($id);
		$viewsw = "themes";
		return view('barthemes.edit', compact('bartheme','viewsw','current_user','wordpress_url','dashboard_option','role','global_lang'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$bartheme = Bartheme::findOrFail($id);

		$bartheme->name = $request->input("name");
        $bartheme->template_url = $request->input("template_url");
		$bartheme->theme_file = $request->input("theme_file");
		$bartheme->theme_url = $request->input("theme_url");
		
		if($request->file('image')!= ""){
			Log::info('Entro en logica imagen 777');
				
			$file = $request->file('image');
	        // SET UPLOAD PATH
	        $destinationPath = 'themes';
	         // GET THE FILE EXTENSION
	        $extension = $file->getClientOriginalExtension();
	         // RENAME THE UPLOAD WITH RANDOM NUMBER
	        $fileName = rand(11111, 99999) . '.' . $extension;
	         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
	        $upload_success = $file->move($destinationPath, $fileName);
			$bartheme->image = $fileName;
			
		}

		$bartheme->save();

		return redirect()->route('barthemes.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$bartheme = Bartheme::findOrFail($id);
		$bartheme->delete();

		return redirect()->route('barthemes.index')->with('message', 'Item deleted successfully.');
	}

}
