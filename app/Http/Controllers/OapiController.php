<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Site;
use Input;
use App\User;
use App\Oupdate;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pete;
use Response;


class OapiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
    public function __construct()
    {
        //global $current_user;
        //$this->wp_user = getUserWP();
		
    	include_once(env('WP_LOAD_PATH')."/wp-load.php");  
	
    }
	
    public function world()
    {
       
	   Log::info("Entro en World");
	   //Response::json(["ok"=>"ok"], 200, array('Content-Type' => 'application/javascript'));
	   Response::json(["ok"=>"ok"])->setCallback(Input::get('callback'));
    }
	
	public function validate_update_json(){
	  $oupdates = Oupdate::orderBy('created_at', 'desc')->get();		
	  echo Input::get('callback') . '('.json_encode(['title'=>$oupdates[0]->title, 'branch'=>$oupdates[0]->branch ]).')';
	}
	
	public function validate_site_json(){
		
		//Devuelve todas las subscripciones 
		//Cada subscripcion puede tener varias subscripciones
		
		$api_key = Input::get('oapi_key');
		$email = Input::get('oemail');
		$domain = Input::get('odomain');
		
		Log::info('validate_site_json logic');
	    Log::info('apikey: '.$api_key);
		Log::info('email: '.$email);
		Log::info('domain: '.$domain);
		
		$site = Site::where('user_email', $email)->where('api_key', $api_key)->first();
		
		if($site){
			if(($site->validated == false) & ($site->domain == null)){
				$site->validated = true;
				$site->domain = $domain;
				$site->save();
				//echo Input::get('callback') . '('.json_encode(['message'=>'success new']).')';
				echo Input::get('callback') . '('.json_encode($site).')';
			}else if(($site->validated == true) & ($site->domain == $domain)){
				//echo Input::get('callback') . '('.json_encode(['message'=>'success validada de nuevo']).')';
				echo Input::get('callback') . '('.json_encode($site).')';
			}else if(($site->validated == true) & ($site->domain != $domain)){
				echo Input::get('callback') . '('.json_encode(['message'=>'Sorry this API key has already been validated']).')';
			}
		} else{
			echo Input::get('callback') . '('.json_encode(['message'=>'Sorry Wrong Keys']).')';
		}
	}

}
