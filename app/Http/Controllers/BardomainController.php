<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Bardomain;
use App\Barsubdomain;
use App\Barserver;
use App\Barrecord;
use App\User;
use Input;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Log;
use App;
use App\DashboardOption;

class BardomainController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct()
	{
	     $this->middleware('auth.wp');		
	}
	
	
	public function index()
	{
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		$viewsw = "my_domains";
		
		//HACK FOR ADMIN
		$user = $current_user;
		if(($role == "administrator") || ($role =="contributor")){
			if(input::get('user_id')){
				$admin = true;
				$user = User::findOrFail(input::get('user_id'));
				$viewsw = "select_customer";
			}else{
				return redirect()->action('ContributorsController@select_customer');
			}
			
		}
		///////////		
			
		$bardomains = DB::select("SELECT bd.created_at as created_at, bs.linode_label, bd.domain_zone, bd.id  FROM wp_barservers bs 
				inner join wp_bardomains bd on bs.id = bd.barserver_id 
		        where bs.user_id = $user->ID order by bs.linode_label, bd.created_at desc");
				
				
		$creating_delay = 30;
		
		$action = input::get('action');
		if($action == "create"){
			$message = trans('dashboard.please_wait_domain_creation');
		}

		return view('bardomains.index', compact('bardomains','viewsw','barsubdomains','creating_delay','message','current_user','wordpress_url','dashboard_option','role','global_lang','user','admin'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		
		$viewsw = "my_domains";
		$current_user = Auth::user();
		$barservers = Barserver::orderBy('id', 'desc')->where('user_id', $current_user->ID)->get();	
		return view('bardomains.create', compact('barservers','viewsw','current_user','wordpress_url','dashboard_option','role','global_lang'));
	}
	
	public function create_record()
	{
		$viewsw = "my_domains";
		$type = Input::get('type');
		$bardomain = Bardomain::findOrFail(Input::get('bardomain_id'));
		
		return view('bardomains.create_record', compact('viewsw','type','bardomain'));
	}
	
	public function store_record(){
		
		$bardomain = Bardomain::findOrFail(Input::get('bardomain_id'));
		
		$target = Input::get('target');
		$name = Input::get('name');
		$ttl_sec = Input::get('ttl_sec');
		$priority = Input::get('priority');
		
		if(Input::get('type') == "a"){
			$result = shell_exec("linode-cli domains records-create $bardomain->linode_id --type A --name $name --target $target --port 80 --ttl_sec $ttl_sec --json 2>&1");
		}
		
		if(Input::get('type') == "mx"){
			$result = shell_exec("linode-cli domains records-create $bardomain->linode_id --type MX --name $name --priority $priority --target $target --ttl_sec $ttl_sec --json 2>&1");
		}
		
		if(Input::get('type') == "cname"){
			$result = shell_exec("linode-cli domains records-create $bardomain->linode_id --type CNAME --name $name --target $target --ttl_sec $ttl_sec --json 2>&1");
		}
		
		if(Input::get('type') == "txt"){
			Log::info("linode-cli domains records-create $bardomain->linode_id --type TXT --name $name --target $target --ttl_sec $ttl_sec --json");
			$result = shell_exec("linode-cli domains records-create $bardomain->linode_id --type TXT --name $name --target $target --ttl_sec $ttl_sec --json 2>&1");
		}
		
		$result = str_replace("Request failed: 400\n", "", $result);
		Log::info($result);
		$result = json_decode($result);
		Log::info($result);
		
		if(isset($result[0])){
         	return redirect()->to("/bardomains/$bardomain->id/edit")->withErrors(['message1'=>$result[0]->reason]);
	 	}else{
	 		return redirect("/bardomains/$bardomain->id/edit");
	 	}
	}
	
	public function delete_record(){
		$barrecord_id = Input::get('barrecord_id');
		$bardomain = Bardomain::findOrFail(Input::get('bardomain_id'));
		
		Log::info("linode-cli domains records-delete $bardomain->linode_id $barrecord_id");
		$result = shell_exec("linode-cli domains records-delete $bardomain->linode_id $barrecord_id");
		
		return redirect("/bardomains/$bardomain->id/edit");
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$current_user = Auth::user();
		
		if(isset($current_user->dashboard_lang)){
	   	  	  App::setLocale($current_user->dashboard_lang);
		  }else{
			  App::setLocale("en");
		}
		
		$fields_to_validator = $request->all();
		$barsever = Barserver::findOrFail($request->input("barserver_id"));
		$user = User::findOrFail($barsever->user_id);
		
    	$validator = Validator::make($fields_to_validator, [
	   	 'domain_zone' =>  'required|unique:wp_bardomains|regex:/^(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}$/',
    	 ]);
		 
         if ($validator->fails()) {
            return redirect::back()
            		->withErrors($validator)
            			->withInput();
         }
		
 		$domain_zone = $request->input("domain_zone");
 		$domain_zone = strtolower($request->input('domain_zone'));
		$domain_zone = str_replace("www.","",$domain_zone);
		
		$bardomain =  new Bardomain();
		$bardomain->domain_zone = $domain_zone;
		$bardomain->barserver_id =  $barsever->id;
		$bardomain->target =  $barsever->ip;
		$bardomain->user_id = $barsever->user_id;
		$bardomain->save();
		
		return redirect('/bardomains?action=create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$bardomain = Bardomain::findOrFail($id);

		return view('bardomains.show', compact('bardomain'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$viewsw = "my_domains";
		$bardomain = Bardomain::findOrFail($id);
		$barrecords = shell_exec("linode-cli domains records-list $bardomain->linode_id --json");
		//Log::info($barrecords);
		$barrecords = json_decode($barrecords);
		//Log::info($barrecords);
		return view('bardomains.edit', compact('bardomain','viewsw','barrecords'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$bardomain = Bardomain::findOrFail($id);

		$bardomain->domain_zone = $request->input("domain_zone");

		$bardomain->save();

		return redirect()->route('bardomains.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$bardomain = Bardomain::findOrFail($id);
		$bardomain->delete();

		return redirect()->route('bardomains.index')->with('message', 'Item deleted successfully.');
	}

}
