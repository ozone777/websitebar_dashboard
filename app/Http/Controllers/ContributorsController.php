<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Log;
use App\Site;
use Input;
use App\User;
use App\Oticket;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Pete;
use Response;
use Mailjet;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use App\DashboardOption;
use App;


class ContributorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function __construct()
	    {
	        $this->middleware('auth.wp');		
 			$this->middleware('auth.admin');
			
	    }
	
	public function select_customer(){
	
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		$viewsw = "select_customer";
		
		return view('contributors.select_customer',compact('viewsw','otickets','current_user','wordpress_url','dashboard_option','role','global_lang'));
	}
	
	
}
