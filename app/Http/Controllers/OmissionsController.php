<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\DashboardOption;
use Illuminate\Support\Facades\Auth;
use App;
use Log;
use App\Wp_omission;
use App\Wp_ostate;
use App\Wp_omissions_state;
use App\Wp_omissionmeta;
use App\Wp_omissions_todo;
use App\Bardomain;
use App\Pete;
use App\User;
use DB;
use Input;
use Illuminate\Support\Facades\Redirect;

class OmissionsController extends Controller
{
	
  public function __construct()
  {
	  
	if(isset($_GET["email_token"])){

	  include_once(env('WP_LOAD_PATH')."/wp-load.php"); 
	  
	  $token = $_GET['email_token'];
	  $user = User::where('email_token',$token)->first();
	  Log::info('Tiene token');	
	  if($user){
		 Log::info("Encontro el usuario: $user->ID");	
		 Log::info("Entra al constructor");
	  	 wp_set_auth_cookie( $user->ID );
		 Auth::loginUsingId($user->ID);
		 
	  }else{
	  	$this->middleware('auth.wp');	
	  }
	}else{
		$this->middleware('auth.wp');	
	}
	  	
  }
  
  
public function set_omission(){
	
	$viewsw = "my_addons";
	$order_id = Input::get('order_id');
	$order_item_id = Input::get('order_item_id');
	$user_id = Input::get('user_id');
	$product_id = Input::get('product_id');
	
	$wp_omission = Wp_omission::where('user_id', $user_id)->where("order_id",$order_id)->where("order_item_id",$order_item_id)->where("product_id",$product_id)->first();
	
	if(!isset($wp_omission)){
		
		Log::info("No existe");
		$wp_omission = new Wp_omission();
		$wp_omission->order_id = $order_id;
		$wp_omission->order_item_id = $order_item_id;
		$wp_omission->user_id = $user_id;
		$wp_omission->product_id = $product_id;
		$wp_omission->save();
	}
	
	//return redirect()->action('DashboardController@edit_addon', ['id' => $wp_omission->id]);
	return redirect('/edit'."/$wp_omission->id");
	
}
  
public function edit($id){
	
	$wp_omission = Wp_omission::findOrFail($id);
	
	//////SHARED HTML VARIABLES///
    $current_user = Auth::user();			
    $wordpress_url = env('WP_URL');
    $dashboard_option = new DashboardOption();
	$role = getUserRole($current_user->ID);
	if($current_user->dashboard_lang){
		$global_lang = $current_user->dashboard_lang;
		App::setLocale($current_user->dashboard_lang);
	}else{
		$global_lang = "en";
		App::setLocale("en");
	}
	//////////////////////////
	
	$viewsw = "my_addons";
	
	//HACK FOR ADMIN
	$user = $current_user;
	if(($role == "administrator") || ($role =="contributor")){
		
		$admin = true;
		$viewsw = "select_customer";
		$user =  User::findOrFail($wp_omission->user_id);
		
		if($user->dashboard_lang){
			$global_lang = $user->dashboard_lang;
			App::setLocale($user->dashboard_lang);
		}
			
	}
	//////////////

	//OBTENER TODOS LOS META VALUES DEL BRIEF
	$omissionmeta = $wp_omission->get_metas();
	
	//OBTENER TODOS LOS ARCHIVOS DEL BRIEF
	$files = DB::select("select * from wp_omissions_files where omission_id = {$wp_omission->id}");
	
	$product_origin_id =icl_object_id($wp_omission->product_id, 'product', false);
	$product = get_product($product_origin_id);
	
	//OBTENER TODS LOS CAMPOS DEL BRIEF PARA ESTE PRODUCTO		
	$rows = Wp_omission::get_product_fields($product_origin_id);
	
	//OBETENER EL ESTADO SELECCIONADO
	
	if($product->get_title() == "Premium Branding Package"){
		
		//OBTENER ESTADOS
		$states = Wp_ostate::orderBy('order', 'asc')->where('view',"branding_kit")->get();
		$selected_state = Wp_omissions_state::get_state($wp_omission,"branding_kit");
		$todos = $selected_state->get_todos();
		
		return view('omissions.branding_kit',compact('viewsw','rows','omissionmeta','files','wp_omission','product','current_user','wordpress_url','dashboard_option','role','global_lang','states','selected_state','user','admin','product_owner','todos'));
		
	}else if($product->get_title() == "8-hr Designer Package"){
		$product_owner =  User::findOrFail($wp_omission->user_id);
		//OBTENER ESTADOS
		$states = Wp_ostate::orderBy('order', 'asc')->where('view', "hr_design")->get();
		$selected_state = Wp_omissions_state::get_state($wp_omission,"hr_design");
		$todos = $selected_state->get_todos();
		
		
		if(isset($wp_omission->hr_package_webtender_selected_id)){
			$webtender = User::findOrFail($wp_omission->hr_package_webtender_selected_id);
		}
		return view('omissions.hr_design',compact('viewsw','rows','omissionmeta','files','wp_omission','product','current_user','wordpress_url','dashboard_option','role','global_lang','states','selected_state','user','admin','webtender','product_owner','todos'));
		
	}else if($product->get_title() == "8-hr Web Developer Package"){
		$product_owner =  User::findOrFail($wp_omission->user_id);
		//OBTENER ESTADOS
		$states = Wp_ostate::orderBy('order', 'asc')->where('view', "hr_development")->get();
		$selected_state = Wp_omissions_state::get_state($wp_omission,"hr_development");
		$todos = $selected_state->get_todos();
		
		if(isset($wp_omission->hr_package_webtender_selected_id)){
			$webtender = User::findOrFail($wp_omission->hr_package_webtender_selected_id);
		}
		
		return view('omissions.hr_development',compact('viewsw','rows','omissionmeta','files','wp_omission','product','current_user','wordpress_url','dashboard_option','role','global_lang','states','selected_state','user','admin','webtender','product_owner','todos'));
		
	}else if($product->get_title() == "Social Media Branding Kit"){
		
		//OBTENER ESTADOS
		$states = Wp_ostate::orderBy('order', 'asc')->where('view', "social_media_kit")->get();
		$selected_state = Wp_omissions_state::get_state($wp_omission,"social_media_kit");
		$todos = $selected_state->get_todos();
		
		return view('omissions.social_media_kit',compact('viewsw','rows','omissionmeta','files','wp_omission','product','current_user','wordpress_url','dashboard_option','role','global_lang','states','selected_state','user','admin','todos'));
		
	}else if($product->get_title() == "SSl Certificate"){
		
		//OBTENER ESTADOS
		$states = Wp_ostate::orderBy('order', 'asc')->where('view', "ssl_certificate")->get();
		$selected_state = Wp_omissions_state::get_state($wp_omission,"ssl_certificate");
		$todos = $selected_state->get_todos();
		$select_options_array = Bardomain::orderBy('created_at', 'asc')->where('user_id', $user->ID)->get();
		
		return view('omissions.ssl_certificate',compact('viewsw','rows','omissionmeta','files','wp_omission','product','current_user','wordpress_url','dashboard_option','role','global_lang','states','selected_state','user','admin','select_options_array','todos'));
	}else if($product->get_title() == "Web Package with Hours of Design, Content and Web Development"){
		
		//OBTENER ESTADOS
		$states = Wp_ostate::orderBy('order', 'asc')->where('view',"web_package")->get();
		$selected_state = Wp_omissions_state::get_state($wp_omission,"web_package");
		$todos = $selected_state->get_todos();
		
		return view('omissions.web_package',compact('viewsw','rows','omissionmeta','files','wp_omission','product','current_user','wordpress_url','dashboard_option','role','global_lang','states','selected_state','user','admin','product_owner','todos'));
		
	}else if($product->get_title() == "SSl Certificate"){
		
		//OBTENER ESTADOS
		$states = Wp_ostate::orderBy('order', 'asc')->where('view', "ssl_certificate")->get();
		$selected_state = Wp_omissions_state::get_state($wp_omission,"ssl_certificate");
		$todos = $selected_state->get_todos();
		$select_options_array = Bardomain::orderBy('created_at', 'asc')->where('user_id', $user->ID)->get();
		
		return view('omissions.ssl_certificate',compact('viewsw','rows','omissionmeta','files','wp_omission','product','current_user','wordpress_url','dashboard_option','role','global_lang','states','selected_state','user','admin','select_options_array','todos'));
		
	}else if($product->get_title() == "Email Accounts"){
		
		//OBTENER ESTADOS
		$states = Wp_ostate::orderBy('order', 'asc')->where('view',"emails")->get();
		$selected_state = Wp_omissions_state::get_state($wp_omission,"emails");
		$todos = $selected_state->get_todos();
		$select_options_array = Bardomain::orderBy('created_at', 'asc')->where('user_id', $user->ID)->get();
		
		return view('omissions.emails',compact('viewsw','rows','omissionmeta','files','wp_omission','product','current_user','wordpress_url','dashboard_option','role','global_lang','states','selected_state','user','admin','product_owner','select_options_array','todos'));
		
		
		
		
	}
	
}

public function answer_test(){
	
	$current_user = Auth::user();
	if($current_user->dashboard_lang){
		App::setLocale($current_user->dashboard_lang);
	}else{
		App::setLocale("en");
	}
	
	$wp_omission = Wp_omission::findOrFail(input::get('omission_id'));
	
	$sw = true;
	$row_error = "";
	$wp_omission->pc_rows_error = "";
	
	if(input::get('pc_bohemian_value')){
		//A
		$value = input::get('pc_bohemian_value');
		$wp_omission->pc_bohemian_value = (int) $value;
		
	}else{
		$wp_omission->pc_rows_error .= "0,";
		$sw = false;
	}
	if(input::get('pc_outdoorsy_value')){
		//B
		$value = input::get('pc_outdoorsy_value');
		$wp_omission->pc_outdoorsy_value = (int) $value;
	}else{
		$wp_omission->pc_rows_error .= "1,";
		$sw = false;
	}
	if(input::get('pc_informal_value')){
		//C
		$value = input::get('pc_informal_value');
		$wp_omission->pc_informal_value = (int) $value;
		
		
	}else{
		$wp_omission->pc_rows_error .= "2,";
		$sw = false;
	}
	if(input::get('pc_laid_back_value')){
		//D
		$value = input::get('pc_laid_back_value');
		$wp_omission->pc_laid_back_value = (int) $value;
		
		
	}else{
		$wp_omission->pc_rows_error .= "3,";
		$sw = false;
	}
	if(input::get('pc_subtle_value')){
		//E
		$value = input::get('pc_subtle_value');
		$wp_omission->pc_subtle_value = (int) $value;
		
		
	}else{
		$wp_omission->pc_rows_error .= "4,";
		$sw = false;
	}
	if(input::get('pc_delicate_value')){
		//F
		$value = input::get('pc_delicate_value');
		$wp_omission->pc_delicate_value = (int) $value;
		
		
	}else{
		$wp_omission->pc_rows_error .= "5,";
		$sw = false;
	}
	if(input::get('pc_understated_value')){
		//G
		$value = input::get('pc_understated_value');
		$wp_omission->pc_understated_value = (int) $value;
		
		
	}else{
		$wp_omission->pc_rows_error .= "6,";
		$sw = false;
	}
	if(input::get('pc_monochrome_value')){
		//H
		$value = input::get('pc_monochrome_value');
		$wp_omission->pc_monochrome_value = (int) $value;
		
		
	}else{
		$wp_omission->pc_rows_error .= "7,";
		$sw = false;
	}
	if(input::get('pc_flowy_value')){
		//I
		$value = input::get('pc_flowy_value');
		$wp_omission->pc_flowy_value = (int) $value;
		
		
	}else{
		$wp_omission->pc_rows_error .= "8,";
		$sw = false;
	}
	if(input::get('pc_rugged_value')){
		//J
		$value = input::get('pc_rugged_value');
		$wp_omission->pc_rugged_value = (int) $value;
		
		
	}else{
		$wp_omission->pc_rows_error .= "9,";
		$sw = false;
	}
	if(input::get('pc_handmade_value')){
		//K
		$value = input::get('pc_handmade_value');
		$wp_omission->pc_handmade_value = (int) $value;
		
	}else{
		$wp_omission->pc_rows_error .= "10,";
		$sw = false;
	}
	if(input::get('pc_rustic_value')){
		//L
		$value = input::get('pc_rustic_value');
		$wp_omission->pc_rustic_value = (int) $value;
		
	}else{
		$wp_omission->pc_rows_error .= "11,";
		$sw = false;
	}
	
	$wp_omission->pc_rows_error = substr($wp_omission->pc_rows_error, 0, -1);
	//Log::info("pc_rows_error: $wp_omission->pc_rows_error");
	
	if($sw == true){
		//SI EL FOEMULARIO ESTA COMPLETO
		$wp_omission->pc_visual_character = $wp_omission->pc_bohemian_value + $wp_omission->pc_outdoorsy_value + $wp_omission->pc_informal_value + $wp_omission->pc_laid_back_value;
		$wp_omission->pc_visual_contrast = $wp_omission->pc_subtle_value + $wp_omission->pc_delicate_value + $wp_omission->pc_understated_value + $wp_omission->pc_monochrome_value;
		$wp_omission->pc_visual_geometry = $wp_omission->pc_flowy_value + $wp_omission->pc_rugged_value + $wp_omission->pc_handmade_value + $wp_omission->pc_rustic_value;
		$wp_omission->pc_errors = null;
		$wp_omission->pc_rows_error = null;
		
		$next_state = input::get('next_state');
			if($next_state){
				$selected_state = Wp_omissions_state::findOrFail(input::get("selected_state_id"));
				$ostate = Wp_ostate::where('label', $next_state)->first();
				$selected_state->ostate_id = $ostate->id;
				$selected_state->save();
			}
		
	}else{
		$wp_omission->pc_errors = trans('addons.complete_pc_test'); 
	}
	
	$wp_omission->save();
	
	return Redirect::back();
	
	
}


public function mission_submitted(Request $request){
	
	//ENCONTRAR AL ESTADO DE LA MISION
	$selected_state = Wp_omissions_state::findOrFail(input::get("ostate_mission_id"));
	$ostate = Wp_ostate::where('label', input::get("label"))->first();
	
	//CONFIGURAR SELECTED_STATE
	$selected_state->current_url = input::get("current_url");
	$selected_state->ostate_id = $ostate->id;
	$selected_state->save();

	if($request->ajax()){
	  return response()->json($selected_state);
	}
	
	return Redirect::back();
	
}

public function wp_select_user(){
	
	$wp_omission = Wp_omission::findOrFail(input::get('omission_id'));
	$action = input::get('action');
	$selected_state = Wp_omissions_state::findOrFail(input::get("selected_state_id"));
	$ostate = Wp_ostate::where('label',input::get('next_state'))->first();
	$selected_state->current_url = input::get("current_url");
	$selected_state->ostate_id = $ostate->id;
	$selected_state->save();
	
	$wp_omission->wp_selected_user = input::get('user_id');
	$wp_omission->save();
	
	return response()->json($wp_omission);
}


public function save_omission_file(Request $request){
	
	if($request->file('filem')){
		
		$file = $request->file('filem');
		// SET UPLOAD PATH
		$destinationPath = "solo_files/".input::get('omission_id');
		// GET THE FILE EXTENSION
		$extension = $file->getClientOriginalExtension();
		// RENAME THE UPLOAD WITH RANDOM NUMBER
		$fileName_solo = md5(uniqid(rand(), true));
		$fileName = $fileName_solo . '.' . $extension;
		// MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY
		$upload_success = $file->move($destinationPath, $fileName);
	
		$wp_omissionmeta = Wp_omissionmeta::where('omission_id', '=', input::get('omission_id'))->where('meta_key', '=', input::get('meta_key'))->first();
	
		if(isset($wp_omissionmeta)){
			$wp_omissionmeta->meta_value = $fileName;
			$wp_omissionmeta->save();
		}else{
			$wp_omissionmeta = new Wp_omissionmeta();
			$wp_omissionmeta->omission_id = input::get('omission_id');
			$wp_omissionmeta->meta_key = input::get('meta_key');
			$wp_omissionmeta->meta_value = $fileName;
			$wp_omissionmeta->save();
		}
	}

	return Redirect::back();
}


public function save_brief(){
	
	Log::info("action: save_brief");
	//$post_data = Input::all();
	$current_user = Auth::user();	
	$wp_omissionmeta = Wp_omissionmeta::get_meta(input::get('omission_id'),input::get('meta_key'));
	
	if(isset($wp_omissionmeta)){
		$wp_omissionmeta->meta_value = input::get('meta_value');
		$wp_omissionmeta->user_login = $current_user->user_login;
		$wp_omissionmeta->save();
	}else{
		$wp_omissionmeta = new Wp_omissionmeta();
		$wp_omissionmeta->omission_id = input::get('omission_id');
		$wp_omissionmeta->meta_key = input::get('meta_key');
		$wp_omissionmeta->meta_value = input::get('meta_value');
		$wp_omissionmeta->user_login = $current_user->user_login;
		$wp_omissionmeta->save();
	}
	
	//DEBUG
	//
	$next_state = input::get('next_state');
	$next_state_logic = input::get('next_state_logic');
	$next_state_logic_result = input::get('next_state_logic_result');
	$omission_id = input::get('omission_id');
	$meta_key = input::get('meta_key');
	$reload = "false";
	
	Log::info("omission_id: $omission_id");
	Log::info("meta_key: $meta_key");
	Log::info("meta_value: $meta_value");
	Log::info("next_state: $next_state");
	Log::info("next_state_logic: $next_state_logic");
	Log::info("next_state_logic_result: $next_state_logic_result");
	
	if(($next_state != "next_state_var") && ($next_state_logic == "next_state_logic_var")){
		$reload = "true";
		$selected_state = Wp_omissions_state::findOrFail(input::get("selected_state_id"));
		$ostate = Wp_ostate::where('label', $next_state)->first();
		if(isset($ostate)){
			$selected_state->ostate_id = $ostate->id;
			$selected_state->save();
		}
	}
	
	//Caso excepcional agreement
	if(($next_state != "next_state_var") && ($next_state_logic != "next_state_logic_var") && ($next_state_logic != "next_state_logic_result_var")){
		$reload = "true";
		$meta_keys = explode(",", $next_state_logic);
		$condition_resuls = [];
		$next_state_logic_result_array = explode(",", $next_state_logic_result);
		
		foreach ($meta_keys as $meta_key) {
			$meta = Wp_omissionmeta::get_meta(input::get('omission_id'),$meta_key);
			if(isset($meta)){
				if($meta->meta_value == "1"){
					array_push($condition_resuls,"yes");
				}else{
					array_push($condition_resuls,"no");
				}
			}
		}
		
		if($next_state_logic_result_array == $condition_resuls){
			$selected_state = Wp_omissions_state::findOrFail(input::get("selected_state_id"));
			$ostate = Wp_ostate::where('label', $next_state)->first();
			if(isset($ostate)){
				$selected_state->ostate_id = $ostate->id;
				$selected_state->save();
			}
		}
		
	}
	
	return response()->json(["meta_key" => input::get('meta_key'), "object_id" => input::get('omission_id'), "reload" => $reload]);
	
}

public function send_notification_test($id){
	
	Log::info("entro en send_notification_test");
	
	$omissions_state = Wp_omissions_state::findOrFail($id);
	
	$omission = Wp_omission::findOrFail($omissions_state->omission_id);
	$customer = User::findOrFail($omission->user_id);
	$ostate = Wp_ostate::findOrFail($omissions_state->ostate_id);
	$options = new DashboardOption();
	
	$first_name = strtoupper(get_user_meta( $customer->ID, "first_name", true )); 
	$last_name = strtoupper(get_user_meta( $customer->ID, "last_name", true )); 
	$customer_complete_name = $first_name ." " .$last_name;
		
	$customer->generate_email_token();
	$url=$options->get_meta_value('dashboard_url')."edit/$omission->id?email_token=$customer->email_token";
	$header_image = "https://ozonegroup.co/logo_websitebar.png";
	
	if($customer->dashboard_lang){
		$global_lang = $customer->dashboard_lang;
		App::setLocale($customer->dashboard_lang);
	}else{
		$global_lang = "en";
		App::setLocale("en");
	}
	
	$here = trans('addons.here');
	
	if($global_lang == "es"){
		
		$email_subject = $ostate->notification_subject_es;
		$notification_body = $ostate->notification_body_es;
		
	}else if($global_lang == "en"){
		
		$email_subject = $ostate->notification_subject_en;
		$notification_body = $ostate->notification_body_en;
		
	}
	
	Log::info("DESPUES DE ");
	Log::info("email_subject: $email_subject");
	Log::info("email_body: $email_body");
	Log::info("url: $url");
	
	/*
	$mj = new \Mailjet\Client(getenv('MJ_APIKEY_PUBLIC'), getenv('MJ_APIKEY_PRIVATE'),true,['version' => 'v3.1']);
	
	$view = View::make('emails.omission_notification', ["header_image" => $header_image, "show_more_link" => $url, "options" => $options, "here" => $here, "notification_body" => $email_body, "customer_complete_name" => $customer_complete_name]);
	
	
	$body = [
	    'Messages' => [
	        [
	            'From' => [
	                'Email' => "software@ozonelabs.us",
	                'Name' => "WebsiteBar"
	            ],
	            'To' => [
	                [
	                    'Email' => "$customer->user_email",
	                    'Name' => "$first_name $last_name"
	                ]
	            ],
				'Subject' => $email_subject,
				'HTMLPart' => $view->render()
	        ]
	    ]
	];
	
	$response = $mj->post(Resources::$Email, ['body' => $body]);
	
	if ($response->success())
	  Log::info($response->getData());
	else
	  Log::info($response->getStatus());
	*/
	
	return view('emails.omission_notification',compact('header_image','show_more_link','options','here','notification_body','customer_complete_name'));
	
}

  
  
}