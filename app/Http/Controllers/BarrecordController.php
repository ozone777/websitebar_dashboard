<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Barrecord;
use Illuminate\Http\Request;

class BarrecordController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	
	public function __construct()
	{
	      
	     $this->middleware('auth.wp');
			
	}
	
	public function index()
	{
		$barrecords = Barrecord::orderBy('id', 'desc')->paginate(10);

		return view('barrecords.index', compact('barrecords'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('barrecords.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$barrecord = new Barrecord();

		$barrecord->hostname = $request->input("hostname");

		$barrecord->save();

		return redirect()->route('barrecords.index')->with('message', 'Item created successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$barrecord = Barrecord::findOrFail($id);

		return view('barrecords.show', compact('barrecord'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$barrecord = Barrecord::findOrFail($id);

		return view('barrecords.edit', compact('barrecord'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$barrecord = Barrecord::findOrFail($id);

		$barrecord->hostname = $request->input("hostname");

		$barrecord->save();

		return redirect()->route('barrecords.index')->with('message', 'Item updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$barrecord = Barrecord::findOrFail($id);
		$barrecord->delete();

		return redirect()->route('barrecords.index')->with('message', 'Item deleted successfully.');
	}

}
