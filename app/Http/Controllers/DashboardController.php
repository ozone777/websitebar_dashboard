<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Log;
use DB;
use App;
use App\User;
use App\Site;
use App\Barserver;
use App\Bardomain;
use App\Barsite;
use App\Wp_omission;
use App\Wp_ostate;
use App\Wp_omissions_state;
use App\Pete;
use App\Bartemplate;
use App\Bartheme;
use App\Barsubdomain;
use App\DashboardOption;
use App\Wp_omissionmeta;
use Input;
use Illuminate\Support\Facades\Auth;
use App\Standard\Generator;
use App\Standard\License;
use Illuminate\Support\Facades\Redirect;
use Kudosagency\Linodev4\Controllers\Linode;
use Validator;
use Carbon\Carbon;
use Rule;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	
	public function __construct()
	{
	     
	     $this->middleware('auth.wp');
		
			
	}
	
	
    /**
     * #################################################
     * HTML VIEWS
     * #################################################
     */
	
	
	public function create_site(){
		
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		$viewsw = "create_site";
		
		//HACK FOR ADMIN
		$user = $current_user;
		if(($role == "administrator") || ($role =="contributor")){
			if(input::get('user_id')){
				$admin = true;
				$user = User::findOrFail(input::get('user_id'));
				$viewsw = "select_customer";
				
				if($user->dashboard_lang){
					$global_lang = $user->dashboard_lang;
					App::setLocale($user->dashboard_lang);
				}
				
			}else{
				return redirect()->action('ContributorsController@select_customer');
			}
			
		}
		///////////
		
		$plan_ids = array(10,11,12,13,3793,4227,4229,4231);
		
		$barservers = Barserver::orderBy('id', 'desc')->where('user_id', $user->ID)->get();
		$subscription_ids = $barservers->lists('subscription_id')->toArray();
		$my_barservers = $barservers->keyBy('subscription_id');
		$barthemes = Bartheme::orderBy('id', 'desc')->get();
		
		$now = Carbon::now();
		$creating_delay = 30;
		
		$pete_tokens = [];
		foreach ( $my_barservers as $barserver ) {
			$pete_tokens[$barserver->subscription_id] = Crypt::decrypt($barserver->pete_token);
		}
		
		$action = input::get('action');
		if($action == "create_server"){
			$message = trans('dashboard.please_wait_server_creation');
		}
		
		if($dashboard_option->get_meta_value("environment") == "development"){
			$creating_delay = 1;
		}
		
		$now = Carbon::now();
		$last_barserver = $barservers[0];
		if(count($barservers) == 0){
			//Without server
			$state = "create_server";
		}else if(count($barservers) > 0){
			//With at least one server
			$state = "create_site";
			if($barservers[0]->created_at->addMinutes($creating_delay) >= $now){
				//Creating server
				$state = "creating_server";
			}
		}
		
		if(input::get('message')){
			$message = input::get('message');
		}
		
		return view('dashboard.create_site',compact('viewsw','state','last_barserver','subscription_ids','my_barservers','now','creating_delay','barthemes','pete_tokens','plan_ids','message','current_user','wordpress_url','dashboard_option','role','global_lang','user','admin'));
	}
	
	
	public function my_sites(){
		
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		$viewsw = "my_sites";
		
		//HACK FOR ADMIN
		$user = $current_user;
		if(($role == "administrator") || ($role =="contributor")){
			if(input::get('user_id')){
				$admin = true;
				$user = User::findOrFail(input::get('user_id'));
				$viewsw = "select_customer";
				
				if($user->dashboard_lang){
					$global_lang = $user->dashboard_lang;
					App::setLocale($user->dashboard_lang);
				}
				
			}else{
				return redirect()->action('ContributorsController@select_customer');
			}
			
		}
		///////////		
		$plan_ids = array(10,11,12,13,3793,4227,4229,4231);
		
		$barservers = Barserver::orderBy('id', 'desc')->where('user_id', $user->ID)->get();
		$subscription_ids = $barservers->lists('subscription_id')->toArray();
		$my_barservers = $barservers->keyBy('subscription_id');
		$barthemes = Bartheme::orderBy('id', 'desc')->get();
		
		$pete_tokens = [];
		foreach ( $my_barservers as $barserver ) {
			$pete_tokens[$barserver->subscription_id] = Crypt::decrypt($barserver->pete_token);
		}
		
		$action = input::get('action');
		if($action == "create"){
			$message = trans('dashboard.please_wait_dns_propagation');
		}
		
		$creating_delay = 30;
		
		if($dashboard_option->get_meta_value("environment") == "development"){
			$creating_delay = 1;
		}
		
		$now = Carbon::now();
		
		return view('dashboard.my_sites',compact('viewsw','creating_delay','subscription_ids','my_barservers','now','creating_delay','barthemes','pete_tokens','plan_ids','message','current_user','wordpress_url','dashboard_option','role','global_lang','user','admin'));
	}
	
	
	public function my_addons(){
		
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		$viewsw = "my_addons";
		
		//HACK FOR ADMIN
		$user = $current_user;
		if(($role == "administrator") || ($role =="contributor")){
			if(input::get('user_id')){
				$admin = true;
				$user = User::findOrFail(input::get('user_id'));
				$viewsw = "select_customer";
				
				if($user->dashboard_lang){
					$global_lang = $user->dashboard_lang;
					App::setLocale($user->dashboard_lang);
				}
				
			}else{
				return redirect()->action('ContributorsController@select_customer');
			}
			
		}
		///////////		
		
		//SERVER PLANS
		$plan_ids = array(10,11,4227,3793);
		
		return view('dashboard.my_addons',compact('viewsw','plan_ids','current_user','wordpress_url','dashboard_option','role','global_lang','user','admin'));
	}
	
	
	public function addons(){
		
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		
		$viewsw = "addons";
		
		return view('dashboard.addons',compact('viewsw','current_user','wordpress_url','dashboard_option','role','global_lang'));
	}
	
	public function my_servers(){
		
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		$viewsw = "my_servers";
		
		//HACK FOR ADMIN
		$user = $current_user;
		if(($role == "administrator") || ($role =="contributor")){
			if(input::get('user_id')){
				$admin = true;
				$user = User::findOrFail(input::get('user_id'));
				$viewsw = "select_customer";
				
				if($user->dashboard_lang){
					$global_lang = $user->dashboard_lang;
					App::setLocale($user->dashboard_lang);
				}
				
			}else{
				return redirect()->action('ContributorsController@select_customer');
			}
			
		}
		///////////		
		
		//SERVER PLANS
		$plan_ids = array(10,11,4227,3793);
		$barservers = Barserver::orderBy('id', 'desc')->where('user_id', $user->ID)->get();
		$subscription_ids = $barservers->lists('subscription_id')->toArray();
		$my_barservers = $barservers->keyBy('subscription_id');
		$barthemes = Bartheme::orderBy('id', 'desc')->get();
		
		$now = Carbon::now();
		$creating_delay = 30;
		
		if($dashboard_option->get_meta_value("environment") == "development"){
			$creating_delay = 1;
		}
		
		$pete_tokens = [];
		foreach ( $my_barservers as $barserver ) {
			$pete_tokens[$barserver->subscription_id] = Crypt::decrypt($barserver->pete_token);
		}
		
		return view('dashboard.my_servers',compact('viewsw','subscription_ids','my_barservers','now','creating_delay','barthemes','pete_tokens','plan_ids','current_user','wordpress_url','dashboard_option','role','global_lang','user','admin'));
	}
	
	
	
	public function select_customer(){
		
		//////SHARED HTML VARIABLES///
	    $current_user = Auth::user();			
	    $wordpress_url = env('WP_URL');
	    $dashboard_option = new DashboardOption();
		$role = getUserRole($current_user->ID);
		if($current_user->dashboard_lang){
			$global_lang = $current_user->dashboard_lang;
			App::setLocale($current_user->dashboard_lang);
		}else{
			$global_lang = "en";
			App::setLocale("en");
		}
		//////////////////////////
		
		$viewsw = "select_customer";
		
		return view('dashboard.select_customer',compact('viewsw','current_user','wordpress_url','dashboard_option','role','global_lang'));
		
	}

    
    /**
     * #################################################
     * BARSITE CRUD
     * #################################################
     */
	
	public function create_barsite(){
		
		$current_user = Auth::user();
		
		if(isset($current_user->dashboard_lang)){
	   	  	  App::setLocale($current_user->dashboard_lang);
		  }else{
			  App::setLocale("en");
		}
		
		$dashboard_option = new DashboardOption();
		$barserver = Barserver::findOrFail(input::get('barserver_id'));
		$info = "";
		////////////////////START VALIDATION PROCESS///////////////////
		/////////////////////////////////////////////////////////////
		
		//Verify empty URL condition
		if(input::get('site_name') == ""){
			return response()->json(['message'=>trans('dashboard.empty_url_error')]);
		}
		
		//Verify theme condition
		if(input::get('theme')==""){
			return response()->json(['message'=>trans('dashboard.invalid_theme')]);
		}else{
			$theme = Bartheme::where('name', input::get('theme'))->first();
		}
	 	
		//Verify no special character condition
		if(!preg_match('/(^[A-Za-z0-9]+$)+/', input::get('site_name'))){
			return response()->json(['message'=>trans('dashboard.invalid_domain')]);
		}
		
		//CREATE SUBDOMAIN
		$project_name = input::get('site_name');
		$site_name = input::get('site_name');
		$site_name = strtolower($site_name);
		$domain_template = $dashboard_option->get_meta_value("domain_template");
		
		$site_name = $site_name.$domain_template;
		
		//VERIFY SUBDOMAIN
		$barsubdomain = Barsubdomain::where('subdomain', $site_name)->first();
		if($barsubdomain){
			return response()->json(['message'=> trans('dashboard.taken_domain')]);
		}
		
		////////////////////END VALIDATION PROCESS///////////////////
		/////////////////////////////////////////////////////////////
		
		$barsubdomain = new Barsubdomain();
	 	$barsubdomain->barserver_id = $barserver->id;
		$barsubdomain->subdomain = $site_name;
		$barsubdomain->target = $barserver->ip;
		$barsubdomain->save();
		
		$barsite = new Barsite();
		$barsite->bartheme_id = $theme->id;
		$barsite->wp_barserver_id = $barserver->id;
		$barsite->theme = input::get('theme');
		$barsite->wp_user =  $current_user->user_nicename;
		$wp_pass = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10); 
		$barsite->wp_info =  Crypt::encrypt($wp_pass);
		$barsite->barsubdomain_id = $barsubdomain->id;
		$barsite->name = $project_name;
		$barsite->user_id = $current_user->ID;
		$barsite->save();
			
		$barsite->db_user =  "db_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$barsite->db_user_pass =  "usr_" . substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$barsite->db_name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
				
		$var_array = ["barsite" => $barsite, "subdomain" => $barsubdomain->subdomain, "db_user" => $barsite->db_user, "db_user_pass" => $barsite->db_user_pass, "db_name" => $barsite->db_name, "theme_file" =>  Input::get('theme_file'), "wp_pass" => $wp_pass, "message" => ""];	
		
		return response()->json($var_array);
		
	}
	
	public function publish_barsite(){

		$current_user = Auth::user();
		
		if(isset($current_user->dashboard_lang)){
	   	  	  App::setLocale($current_user->dashboard_lang);
		  }else{
			  App::setLocale("en");
		}
		
		//ERROR ALREADY PUBLISHED 
		$barsite_test = Barsite::where('user_id', $current_user->ID)->where("bardomain_id",input::get('bardomain_id'))->first();
		if(isset($barsite_test)){
			return response()->json(['message'=>trans('dashboard.already_published_error')]);
		}
		
		//ERROR DOMAIN NOT SELECTED
		if(input::get('bardomain_id')=="0"){
			return response()->json(['message'=>trans('dashboard.select_domain_error')]);
		}
		
		$dashboard_option = new DashboardOption();
		$site_to_clone = Barsite::findOrFail(input::get('barsite_id'));
		$pete_id = input::get('pete_id');
		
		$bardomain = Bardomain::findOrFail(input::get('bardomain_id'));
		
		$info = "";
		$domain = input::get('domain');

		$barsite = $site_to_clone->replicate();
		$barsite->name = $barsite->name."Published";
		$barsite->bardomain_id = $bardomain->id;
		$barsite->published = true;
		$barsite->save();
		$var_array = ["barsite" => $barsite, "domain" => $bardomain->domain_zone, "info" => $info, "pete_id" => $pete_id, "message" => "", "table_id" => input::get('table_id'),"pete_id" => input::get('pete_id'), "barserver_ip" => input::get('barserver_ip'), "pete_token" => input::get('pete_token'), "email" => input::get('email')];	
		
		return response()->json($var_array);
		
	}
	
	public function clone_barsite(){
		
		$dashboard_option = new DashboardOption();
		
		$site_to_clone = Barsite::findOrFail(input::get('barsite_id'));
		$barsubdomain_to_clone = Barsubdomain::findOrFail($site_to_clone->barsubdomain_id);
		$pete_id = input::get('pete_id');
		
		$info = "";
		$domain_template = $dashboard_option->get_meta_value("domain_template");
		
	 	
		if (strpos($site_to_clone->name, '_') !== false) {
			$aux = $site_to_clone->name;
			$pos = strpos($aux, "_");
			$str1 = substr($aux,0,$pos+1);
			$original_name = str_replace($str1,"",$aux);
		}
		
		$uniqid="z";
		$uniqid =$uniqid.uniqid();
		
		$barsite = $site_to_clone->replicate();
		if(isset($original_name)){
			$barsite->name = $uniqid."_".$original_name;
		}else{
			$barsite->name = $uniqid."_".$barsite->name;
		}
		
		$new_barsubdomain = $barsubdomain_to_clone->replicate();
		$new_barsubdomain->subdomain = $barsite->name.$domain_template;
		$new_barsubdomain->save();
		
		$barsite->barsubdomain_id = $new_barsubdomain->id;
		$barsite->save();
		
		$var_array = ["barsite" => $barsite, "subdomain" => $new_barsubdomain->subdomain, "info" => $info, "uniqid" => $uniqid, "pete_id" => $pete_id, "message" => "", "table_id" => input::get('table_id'),"pete_id" => input::get('pete_id'), "barserver_ip" => input::get('barserver_ip'), "pete_token" => input::get('pete_token'), "email" => input::get('email')];	
		
		return response()->json($var_array);
		
	}
	
	public function restore_barsite_from_backup(){
		
		$dashboard_option = new DashboardOption();
		
		$site_to_clone = Barsite::findOrFail(input::get('barsite_id'));
		$barsubdomain_to_clone = Barsubdomain::findOrFail($site_to_clone->barsubdomain_id);
		$pete_id = input::get('pete_id');
		
		$info = "";
		$domain_template = $dashboard_option->get_meta_value("domain_template");
		
		if (strpos($site_to_clone->name, '_') !== false) {
			$aux = $site_to_clone->name;
			$pos = strpos($aux, "_");
			$str1 = substr($aux,0,$pos+1);
			$original_name = str_replace($str1,"",$aux);
		}
		
		$uniqid="z";
		$uniqid =$uniqid.uniqid();
		
		$barsite = $site_to_clone->replicate();
		if(isset($original_name)){
			$barsite->name = $uniqid."_".$original_name;
		}else{
			$barsite->name = $uniqid."_".$barsite->name;
		}
		
		$new_barsubdomain = $barsubdomain_to_clone->replicate();
		$new_barsubdomain->subdomain = $barsite->name.$domain_template;
		$new_barsubdomain->save();
		
		$barsite->barsubdomain_id = $new_barsubdomain->id;
		$barsite->save();
		
		$var_array = ["barsite" => $barsite, "subdomain" => $new_barsubdomain->subdomain, "info" => $info, "uniqid" => $uniqid, "pete_id" => $pete_id, "message" => "", "table_id" => input::get('table_id'),"pete_id" => input::get('pete_id'), "barserver_ip" => input::get('barserver_ip'), "pete_token" => input::get('pete_token'), "email" => input::get('email'), "backup_id" => input::get('backup_id')];	
		
		return response()->json($var_array);
		
	}
	
	public function delete_barsite(){
		$barsite = Barsite::findOrFail(input::get('barsite_id'));
		$barsite_id = $barsite->id;
		$oldbarsite = $barsite;
		$barsite->delete();
		$var_array = ["barsite" => $oldbarsite, "subscription_id" => Input::get('subscription_id'), "message" => "", "info" => "", "table_id" => input::get('table_id'), "pete_id" => input::get('pete_id'),"barserver_ip" => input::get('barserver_ip'), "pete_token" => input::get('pete_token'), "email" => input::get('email')];
		
		
		return response()->json($var_array);
	}
	
	public function restore_barsite(){
		$barsite = Barsite::withTrashed()->findOrFail(Input::get('barsite_id'));
		$barsite->restore();
		$var_array = ["barsite" => $barsite, "subscription_id" => Input::get('subscription_id'), "message" => "", "info" => "", "table_id" => input::get('table_id'), "pete_id" => input::get('pete_id'),"barserver_ip" => input::get('barserver_ip'), "pete_token" => input::get('pete_token'), "email" => input::get('email')];
		
		return response()->json($var_array);
	}
	
	public function destroy_barsite(){
		
 		$barsite = Barsite::onlyTrashed()->findOrFail(Input::get('barsite_id'));	
		$oldbarsite = $barsite;
		$barsite->forceDelete();
		
		if($barsite->barsubdomain_id){
		   $barsubdomain = Barsubdomain::findOrFail($barsite->barsubdomain_id);
		   $barsubdomain->delete();
		}
		
		$var_array = ["barsite" => $oldbarsite, "subscription_id" => Input::get('subscription_id'), "message" => "", "info" => "", "pete_id" => input::get('pete_id'), "barserver_ip" => input::get('barserver_ip'), "pete_token" => input::get('pete_token'), "email" => input::get('email')];
		
		return response()->json($var_array);
	}
	
	public function continue_barsite(){
		
		$barsite = Barsite::findOrFail(input::get('barsite_id'));
		$barsite->suspend = false;
		$barsite->save();
		$var_array = ["barsite" => $barsite, "subscription_id" => Input::get('subscription_id'), "message" => "", "info" => "", "pete_id" => input::get('pete_id'), "barserver_ip" => input::get('barserver_ip'), "pete_token" => input::get('pete_token'), "email" => input::get('email')];
		
		return response()->json($var_array);
	}
	
	
	public function suspend_barsite(){
		
		$barsite = Barsite::findOrFail(input::get('barsite_id'));
		$barsite->suspend = true;
		$barsite->save();
		$var_array = ["barsite" => $barsite, "subscription_id" => Input::get('subscription_id'), "message" => "", "info" => "", "pete_id" => input::get('pete_id'), "barserver_ip" => input::get('barserver_ip'), "pete_token" => input::get('pete_token'), "email" => input::get('email')];
		
		return response()->json($var_array);
	}
	
    /**
    * #################################################
    * CREATE SERVER
    * #################################################
    */
	
	public function create_linode(Request $request){
		
		$current_user = Auth::user();
		$barserver = new Barserver();
		$barserver->linode_label = $current_user->user_nicename.input::get('subscription_id');
		$pete = Pete::orderBy('created_at', 'desc')->first();
		
		//OBTENER PRODUCTO ORIGINAL NO LA TRADUCCIÓN
		$product_origin_id =icl_object_id(input::get('product_id'), 'product', false);
		$product = get_product($product_origin_id);
		Log::info($product->get_title());
		
		if($product->get_title() == "Hustle Plan"){
			$linode_type = "g6-nanode-1";
		}elseif($product->get_title() == "Pro Plan"){
			$linode_type = "g6-standard-1";
		}elseif($product->get_title() == "WebsiteBar subscription 4GB"){
			$linode_type = "g6-standard-2";
		}elseif($product->get_title() == "WebsiteBar subscription 8GB"){
			$linode_type = "g6-standard-4";
		}
		
		$ssh_info = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$db_info = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$pete_info = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
		$pete_token = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 30);
		
		if(input::get('test') == true){
			$pete_token = "nBlAY6LhxZqJfvo9Udmz0GIgk5DKO8";
		}
		
		$barserver->type = $linode_type;
		$barserver->stackscript_id = "349141";
		$barserver->email = $current_user->user_email;
		$barserver->test = input::get('test');
		$barserver->ssh_info =  $ssh_info;
		$barserver->db_info =  $db_info;
		$barserver->pete_info =  $pete_info;
		$barserver->pete_token =  $pete_token;
		$barserver->status = "Running";
		$barserver->on = true;
		$barserver->image = "ubuntu18.04";
		$barserver->region = "us-central";		
		$barserver->apache_version = $pete->apache_version;
		$barserver->php_version = $pete->php_version;
		$barserver->db_version = $pete->db_version;
		$barserver->phpmyadmin_version = $pete->phpmyadmin_version;
		$barserver->wp_pete_id = $pete->id;
		$barserver->subscription_id = input::get('subscription_id');
		$barserver->product_id = input::get('product_id');
	    $barserver->user_id = input::get('user_id');
		$barserver->save();
		
		$barserver->ssh_info =  Crypt::encrypt($ssh_info);
		$barserver->db_info =  Crypt::encrypt($db_info);
		$barserver->pete_info =  Crypt::encrypt($pete_info);
		$barserver->pete_token =  Crypt::encrypt($pete_token);
		$barserver->save();
		
		$role = getUserRole($current_user->ID);
		
		/*
		if($role=="administrator"){
			return redirect('/my_plans');
		}else{
			return redirect('/create_site?action=create_server');
		}
		*/
		
		return Redirect::back()->with(['action','create_server']);
		
	}
	
     /**
     * #################################################
     * OTHER ACTIONS
     * #################################################
     */
	 
	
 	public function get_barserver_info(){
		
 		$current_user = Auth::user();
 		$barserver = Barserver::where('user_id', $current_user->ID)->where("id",input::get('barserver_id'))->first();
		
 		$ssh_info =  Crypt::decrypt($barserver->ssh_info);
 		$db_info =  Crypt::decrypt($barserver->db_info);
 		$pete_info =  Crypt::decrypt($barserver->pete_info);
 		$pete_token =  Crypt::decrypt($barserver->pete_token);
 		$user_email = $current_user->user_email;
 		$ip = $barserver->ip;
		
 		return response()->json(['ssh_info' => $ssh_info, "db_info" => $db_info, "pete_info" => $pete_info, "pete_token" => $pete_token, "ip"=> $ip, "user_email" => $user_email, "barserver_id" => $barserver->id ]);
		
 	}
	
	
 	public function set_lang(){
 		$user = Auth::user();
		
 		DB::table('wp_users')->where('ID', $user->ID)->update(['dashboard_lang' => input::get('lang')]);
 		return response()->json(['lang' => input::get('lang')]);
 	}
	
	
  	public function shut_down(){
		
  		$barserver = Barserver::findOrFail(input::get('barserver_id'));
  		$barserver->on = false;
  		$barserver->save();
  		if(!$barserver->test){
  			$output = shell_exec("linode-cli linodes shutdown $barserver->linode_id");
  			Log::info("shutdown linode output: $output");
  		}
  		return response()->json($barserver);
  	}
	
  	public function boot(){
		
  		$barserver = Barserver::findOrFail(input::get('barserver_id'));
  		$barserver->on = true;
  		$barserver->save();
  		if(!$barserver->test){
  			$output = shell_exec("linode-cli linodes boot $barserver->linode_id");
  			Log::info("boot linode output: $output");
  		}
  		return response()->json($barserver);
  	}
	
  	public function reboot(){
		
  		$barserver = Barserver::findOrFail(input::get('barserver_id'));
  		$barserver->on = true;
  		$barserver->save();
  		if(!$barserver->test){
  			$output = shell_exec("linode-cli linodes reboot $barserver->linode_id");
  			Log::info("reboot linode output: $output");
  		}
  		return response()->json($barserver);
  	}
	
	
  	public function get_domains()
  	{
  		$viewsw = "my_domains";
  		$barserver = Barserver::findOrFail(Input::get('barserver_id'));
  		$bardomains = Bardomain::where('barserver_id', $barserver->id)->get();
		
  		return response()->json($bardomains);
  	}
	
	
  	public function show_wp_pass(){
		
  		$barsite = Barsite::findOrFail(input::get('barsite_id'));
  		$wp_pass = Crypt::decrypt($barsite->wp_info);		
  		return response()->json(['password' => $wp_pass, "barsite_id" => $barsite->id]);
			
  	}
	
	public function logout(){
		
		Auth::logout();
	    $url=wp_logout_url(env('WP_URL'));
		return redirect($url);

	}
   
}
